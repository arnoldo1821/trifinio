/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.CuentasBO;
import com.sigefi.bo.PresupuestoBO;
import com.sigefi.clases.FechaSistema;
import com.sigefi.entity.CuentasPresupuesto;
import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.UnidadesEjecutoras;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.model.TreeNode;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class PresupuestoBean implements Serializable {

    @ManagedProperty(name = "presupuestoBO", value = "#{presupuestoBO}")
    private PresupuestoBO presupuestoBO;

    @ManagedProperty(value = "#{cuentasBO}")
    private CuentasBO cuentasBO;

//    private TreeNode treeNode;
//    private TreeNode[] treeNodeSeleccionado;

    private short unidadesEjecutoras;
    private Integer anyoPresupuesto;
    private Character monedaPresupuesto;
    private Date fechaCreacionPresupuesto;
    private String usuarioCreador;
    private Boolean liquidado;
    private Boolean esProyecto;
    private Map<String, Short> mapaUnidadEjecutora;
    private List<Integer> listAnios;

    private Map<String, Integer> mapCatalogoCuentas;
    private int catalogoSeleccionado;

    private List<CuentasPresupuesto> listCuentasAgre;
    private List<Presupuestos> listAllPresupuestos;
    private List<PresupuestoBean> filtroPresupuesto;

    private int pkPresupuesto;

    private boolean auxEstadoCrear;

    private int idCuenta;
    private int idsubCuenta;

    /**
     * Creates a new instance of PresupuestoBean
     */
    public PresupuestoBean() {
        FechaSistema hoy = new FechaSistema();
        fechaCreacionPresupuesto = hoy.getFecha();
    }
    
    public void llenarListaPresupuesto() {
        List<Presupuestos> listP = presupuestoBO.listAllPresupuestos();
        if (listP != null) {
            setListAllPresupuestos(listP);
        }
    }

    @PostConstruct
    public void main() {
        llenarListaPresupuesto();
        
        SessionBean referenciaBeanSession
                = (SessionBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("sessionBean");

        usuarioCreador = referenciaBeanSession.getNombreUsuario();

        List<UnidadesEjecutoras> listUnidadesEjecutoras = presupuestoBO.listUnidadesEjecutoras();
        if (listUnidadesEjecutoras != null) {
            mapaUnidadEjecutora = new HashMap();
            for (UnidadesEjecutoras obj : listUnidadesEjecutoras) {
                mapaUnidadEjecutora.put(obj.getNombreUnidad(), obj.getCodigo());
            }
        }

        FechaSistema anioHoy = new FechaSistema();
        int anio = anioHoy.getAnio();
        anio = anio - 1;
        listAnios = new ArrayList<>();
        for (int i = 0; i <= 3; i++) {
            listAnios.add(anio);
            anio = anio + 1;
        }

        auxEstadoCrear = false;

//        treeNode = new DefaultTreeNode("principal", null);
    }

//    public void llenarTablaCuenta() {
//        treeNode = cuentasBO.listNodoCuentasCatalogo(unidadesEjecutoras,anyoPresupuesto);
//        expandirAll(treeNode);
//    }
    private void expandirAll(TreeNode treeNode) {
        if (treeNode.getChildren().size() > 0) {
            treeNode.setExpanded(true);
            for (int i = 0; i < treeNode.getChildren().size(); i++) {
                expandirAll(treeNode.getChildren().get(i));
            }
        }
    }

//    public void displaySelectedMultiple(TreeNode[] nodes) {
//        if (nodes != null && nodes.length > 0) {
//            int i = 0, tam = nodes.length;
//            List<Integer> listNum = new ArrayList<>();
//            for (TreeNode node : nodes) {
//                listNum.add(((CatalogoCuentas) nodes[i].getData()).getIdCuenta());
//                if (i == (tam - 1)) {
//                    listNum.add(((CatalogoCuentas) nodes[i].getData()).getSubcuentaDe());
//                }
//                i++;
//            }
//            if (presupuestoBO.inserCuentas(listNum, this) == 1) {
//                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cuentas Agregadas corectamente", ""));
//            } else {
//                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Cuentas No Agregadas ya estan agregadas", ""));
//            }
//        }
//
//        setListCuentasAgre(presupuestoBO.listCuentasAgregadas(this.pkPresupuesto));
//        RequestContext.getCurrentInstance().update("fromPresupuesto:cuentas");
//    }

    public void insertPresupuesto() {
        if (presupuestoBO.insertPresupuesto(this)) {
            auxEstadoCrear = true;
//                llenarTablaCuenta();
            setListCuentasAgre(presupuestoBO.listCuentasAgregadas(this.pkPresupuesto));
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Presupuesto agregado correctamente, agregar cuentas", ""));
            llenarListaPresupuesto();
            RequestContext.getCurrentInstance().update("fromPresupuesto");
        } else {
//            llenarTablaCuenta();
            setListCuentasAgre(presupuestoBO.listCuentasAgregadas(this.pkPresupuesto));
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Presupuesto No agregado presupuesto ya existe", ""));
            RequestContext.getCurrentInstance().update("fromPresupuesto:cuentas");
        }
    }

    public void limpiar() {
        unidadesEjecutoras = 0;
        anyoPresupuesto = null;
        monedaPresupuesto = null;
        usuarioCreador = null;
        liquidado = null;
        esProyecto = null;
        pkPresupuesto = -1;
        auxEstadoCrear = false;
        if (getListCuentasAgre() != null) {
            getListCuentasAgre().clear();
        }
//        treeNode = new DefaultTreeNode("principal", null);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Presupuesto y cuentas agregadas correctamente", ""));
        RequestContext.getCurrentInstance().update("fromPresupuesto");
    }

    public int getPkPresupuesto() {
        return pkPresupuesto;
    }

    public void setPkPresupuesto(int pkPresupuesto) {
        this.pkPresupuesto = pkPresupuesto;
    }

    public List<CuentasPresupuesto> getListCuentasAgre() {
        return listCuentasAgre;
    }

    public void setListCuentasAgre(List<CuentasPresupuesto> listCuentasAgre) {
        this.listCuentasAgre = listCuentasAgre;
    }

    public List<Integer> getListAnios() {
        return listAnios;
    }

    public void setListAnios(List<Integer> listAnios) {
        this.listAnios = listAnios;
    }

    public PresupuestoBO getPresupuestoBO() {
        return presupuestoBO;
    }

    public void setPresupuestoBO(PresupuestoBO presupuestoBO) {
        this.presupuestoBO = presupuestoBO;
    }

    public short getUnidadesEjecutoras() {
        return unidadesEjecutoras;
    }

    public void setUnidadesEjecutoras(short unidadesEjecutoras) {
        this.unidadesEjecutoras = unidadesEjecutoras;
    }

    public Integer getAnyoPresupuesto() {
        return anyoPresupuesto;
    }

    public void setAnyoPresupuesto(Integer anyoPresupuesto) {
        this.anyoPresupuesto = anyoPresupuesto;
    }

    public Character getMonedaPresupuesto() {
        return monedaPresupuesto;
    }

    public void setMonedaPresupuesto(Character monedaPresupuesto) {
        this.monedaPresupuesto = monedaPresupuesto;
    }

    public Date getFechaCreacionPresupuesto() {
        return fechaCreacionPresupuesto;
    }

    public void setFechaCreacionPresupuesto(Date fechaCreacionPresupuesto) {
        this.fechaCreacionPresupuesto = fechaCreacionPresupuesto;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public Boolean getLiquidado() {
        return liquidado;
    }

    public void setLiquidado(Boolean liquidado) {
        this.liquidado = liquidado;
    }

    public Boolean getEsProyecto() {
        return esProyecto;
    }

    public void setEsProyecto(Boolean esProyecto) {
        this.esProyecto = esProyecto;
    }

    public Map<String, Short> getMapaUnidadEjecutora() {
        return mapaUnidadEjecutora;
    }

    public void setMapaUnidadEjecutora(Map<String, Short> mapaUnidadEjecutora) {
        this.mapaUnidadEjecutora = mapaUnidadEjecutora;
    }

    public Map<String, Integer> getMapCatalogoCuentas() {
        return mapCatalogoCuentas;
    }

    public void setMapCatalogoCuentas(Map<String, Integer> mapCatalogoCuentas) {
        this.mapCatalogoCuentas = mapCatalogoCuentas;
    }

    public int getCatalogoSeleccionado() {
        return catalogoSeleccionado;
    }

    public void setCatalogoSeleccionado(int catalogoSeleccionado) {
        this.catalogoSeleccionado = catalogoSeleccionado;
    }

    public boolean isAuxEstadoCrear() {
        return auxEstadoCrear;
    }

    public void setAuxEstadoCrear(boolean auxEstadoCrear) {
        this.auxEstadoCrear = auxEstadoCrear;
    }

    public CuentasBO getCuentasBO() {
        return cuentasBO;
    }

    public void setCuentasBO(CuentasBO cuentasBO) {
        this.cuentasBO = cuentasBO;
    }
    
    public int getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(int idCuenta) {
        this.idCuenta = idCuenta;
    }

    public int getIdsubCuenta() {
        return idsubCuenta;
    }

    public void setIdsubCuenta(int idsubCuenta) {
        this.idsubCuenta = idsubCuenta;
    }

    public List<Presupuestos> getListAllPresupuestos() {
        return listAllPresupuestos;
    }

    public void setListAllPresupuestos(List<Presupuestos> listAllPresupuestos) {
        this.listAllPresupuestos = listAllPresupuestos;
    }

    public List<PresupuestoBean> getFiltroPresupuesto() {
        return filtroPresupuesto;
    }

    public void setFiltroPresupuesto(List<PresupuestoBean> filtroPresupuesto) {
        this.filtroPresupuesto = filtroPresupuesto;
    }

}
