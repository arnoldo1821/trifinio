/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.TipoCambioBO;
import com.sigefi.clases.FechaSistema;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class TipoCambioBean implements Serializable {

    @ManagedProperty(name = "tipoCambioBO", value = "#{tipoCambioBO}")
    private TipoCambioBO tipoCambioBO;

    private Date fechaCambio;
    private BigDecimal quetzales;
    private BigDecimal lempiras;
    private BigDecimal dolar;

    private List<TipoCambioBean> listTipoCambio;

    /**
     * Creates a new instance of TipoCambioBean
     */
    public TipoCambioBean() {
        FechaSistema hoy = new FechaSistema();
        fechaCambio = hoy.getFecha();
    }

    public void actualizarTipoCambio(RowEditEvent event) {
        if(tipoCambioBO.updateTipoCambio((TipoCambioBean) event.getObject())){ 
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Registro actualizado correctamente "));
        }
    }

    public void cancelarActualizarTipoCambio(RowEditEvent event) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Operacion Cancelada"));
    }

    public void insertTipoCambio() {
        if (tipoCambioBO.insertTipoCambio(this)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Tipo de cambio agregado correctamente", ""));
            llenarLista();
            limpiar();
        }
        RequestContext.getCurrentInstance().update("fromTipoCambio");
        RequestContext.getCurrentInstance().update("form");
    }

    public void llenarLista() {
        List<TipoCambioBean> listT = tipoCambioBO.listTipoCambioAll();
        if (listT != null) {
            setListTipoCambio(listT);
        }
    }
    
    @PostConstruct
    public void main() {
        llenarLista();
    }

    public void limpiar() {
        quetzales = null;
        lempiras = null;
        dolar = null;
        RequestContext.getCurrentInstance().update("fromTipoCambio");
        RequestContext.getCurrentInstance().update("form");
    }

    public TipoCambioBO getTipoCambioBO() {
        return tipoCambioBO;
    }

    public void setTipoCambioBO(TipoCambioBO tipoCambioBO) {
        this.tipoCambioBO = tipoCambioBO;
    }

    public Date getFechaCambio() {
        return fechaCambio;
    }

    public void setFechaCambio(Date fechaCambio) {
        this.fechaCambio = fechaCambio;
    }

    public BigDecimal getQuetzales() {
        return quetzales;
    }

    public void setQuetzales(BigDecimal quetzales) {
        this.quetzales = quetzales;
    }

    public BigDecimal getLempiras() {
        return lempiras;
    }

    public void setLempiras(BigDecimal lempiras) {
        this.lempiras = lempiras;
    }

    public BigDecimal getDolar() {
        return dolar;
    }

    public void setDolar(BigDecimal dolar) {
        this.dolar = dolar;
    }

    public List<TipoCambioBean> getListTipoCambio() {
        return listTipoCambio;
    }

    public void setListTipoCambio(List<TipoCambioBean> listTipoCambio) {
        this.listTipoCambio = listTipoCambio;
    }

}
