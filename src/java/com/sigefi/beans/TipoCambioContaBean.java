/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.TipoCambioContaBO;
import com.sigefi.clases.FechaSistema;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.SimpleFormatter;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class TipoCambioContaBean implements Serializable{
    
    @ManagedProperty(name = "tipoCambioContaBO",value = "#{tipoCambioContaBO}")
    private TipoCambioContaBO tipoCambioContaBO;
    
    private Date fechaCambio;
    private BigDecimal quetzales;
    private BigDecimal lempiras;
    private BigDecimal dolar;
    
    private List<TipoCambioContaBean> listTipoCambio;
    
    /**
     * Creates a new instance of TipoCambioBean
     */
    public TipoCambioContaBean() {
        FechaSistema hoy = new FechaSistema();
        fechaCambio = hoy.getFecha();
    }

    public void insertTipoCambio(){
        if(tipoCambioContaBO.insertTipoCambioConta(this)){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Tipo de cambio agregado correctamente" , ""));
            llenarLista();
            limpiar();
        }
    }
    public void llenarLista(){
        List<TipoCambioContaBean> listT = tipoCambioContaBO.listTipoCambioContaAll();
        if(listT != null){
            setListTipoCambio(listT);
        }
    }
    
    @PostConstruct
    public void main(){
        llenarLista();
    }
    
    public void limpiar() {
        quetzales = null;
        lempiras = null;
        dolar = null;
        RequestContext.getCurrentInstance().update("fromTipoCambioConta");
    }
    
    public TipoCambioContaBO getTipoCambioContaBO() {
        return tipoCambioContaBO;
    }

    public void setTipoCambioContaBO(TipoCambioContaBO tipoCambioContaBO) {
        this.tipoCambioContaBO = tipoCambioContaBO;
    }

    public Date getFechaCambio() {
        return fechaCambio;
    }

    public void setFechaCambio(Date fechaCambio) {
        this.fechaCambio = fechaCambio;
    }

    public BigDecimal getQuetzales() {
        return quetzales;
    }

    public void setQuetzales(BigDecimal quetzales) {
        this.quetzales = quetzales;
    }

    public BigDecimal getLempiras() {
        return lempiras;
    }

    public void setLempiras(BigDecimal lempiras) {
        this.lempiras = lempiras;
    }

    public BigDecimal getDolar() {
        return dolar;
    }

    public void setDolar(BigDecimal dolar) {
        this.dolar = dolar;
    }

    public List<TipoCambioContaBean> getListTipoCambio() {
        return listTipoCambio;
    }

    public void setListTipoCambio(List<TipoCambioContaBean> listTipoCambio) {
        this.listTipoCambio = listTipoCambio;
    }
    
    
}
