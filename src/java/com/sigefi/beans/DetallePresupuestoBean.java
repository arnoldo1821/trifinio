/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.DetallePresupuestoBO;
import com.sigefi.bo.PresupuestoBO;
import com.sigefi.bo.ReprogramarPreBO;
import com.sigefi.clases.FechaSistema;
import com.sigefi.entity.CatalogoCuentas;
import com.sigefi.entity.CuentasPresupuesto;
import com.sigefi.entity.CuentasPresupuestoId;
import com.sigefi.entity.DetalleCuentas;
import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.UnidadesEjecutoras;
import com.sigefi.informe.ReportePresupuesto;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIData;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.model.StreamedContent;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class DetallePresupuestoBean {

    @ManagedProperty(value = "#{detallePresupuestoBO}")
    private DetallePresupuestoBO detallePresupuestoBO;

    @ManagedProperty(name = "presupuestoBO", value = "#{presupuestoBO}")
    private PresupuestoBO presupuestoBO;

    @ManagedProperty(value = "#{reprogramarPreBO}")
    private ReprogramarPreBO reprogramarPreBO;

    @ManagedProperty("#{dataSource}")
    private DriverManagerDataSource dataSource;

    private short unidadesEjecutoras;
    private Map<String, Short> mapaUnidadEjecutora;
    private Date fechaCreacionPresupuesto;

    private List<Integer> listAnios;
    private Integer anyoPresupuesto;
    private String monedaPresupuesto;

    private List<CuentasPresupuesto> listCuentasPreSelec;
    private Set<DetalleCuentas> listDetallePresupuesto;
    private UIData detalleDataTable;

    private CuentasPresupuestoId id;
    private CatalogoCuentas catalogoCuentas;
    private Presupuestos presupuestos;
    private String descripcionCuentaMayor;
    private String dependeDe;
    private Boolean conDetalle;
    private List<DetallePresupuestoBean> listAllDetalle;

//    private BigDecimal enero;
//    private BigDecimal febrero;
//    private BigDecimal marzo;
//    private BigDecimal abril;
//    private BigDecimal mayo;
//    private BigDecimal junio;
//    private BigDecimal julio;
//    private BigDecimal agosto;
//    private BigDecimal septiembre;
//    private BigDecimal octubre;
//    private BigDecimal noviembre;
//    private BigDecimal diciembre;
    private BigDecimal presupuestoAprobado;

    private int codigoPresup;
    private int codigoCuent;

    private ReportePresupuesto controlReporte;
    public StreamedContent pdfDoc;

    public DetallePresupuestoBean() {
    }

    @PostConstruct
    public void main() {
        List<UnidadesEjecutoras> listUnidadesEjecutoras = presupuestoBO.listUnidadesEjecutoras();
        if (listUnidadesEjecutoras != null) {
            mapaUnidadEjecutora = new HashMap();
            for (UnidadesEjecutoras obj : listUnidadesEjecutoras) {
                mapaUnidadEjecutora.put(obj.getNombreUnidad(), obj.getCodigo());
            }
        }

        FechaSistema anioHoy = new FechaSistema();
        int anio = anioHoy.getAnio();
        listAnios = new ArrayList<>();
        for (int i = 0; i <= 2; i++) {
            listAnios.add(anio);
            anio = anio + 1;
        }

        controlReporte = new ReportePresupuesto(dataSource);

    }

    public void llenarListCuentasPresupuesto() {
        setListAllDetalle(detallePresupuestoBO.listAllDetallePresupuesto(this));
    }

    public void buscarCuentasPre() {
        if (this.unidadesEjecutoras > 0) {
            if (this.anyoPresupuesto > 0) {
                List<CuentasPresupuesto> listCuentasPre = detallePresupuestoBO.listCuentasSelec(this);
                setListAllDetalle(detallePresupuestoBO.listAllDetallePresupuesto(this));
                if (listCuentasPre != null) {
                    if (listCuentasPre.size() > 0) {

                        setListCuentasPreSelec(listCuentasPre);
                        Presupuestos pre = detallePresupuestoBO.elementoPresupuesto(this);
                        if (pre != null) {
                            Map<Character, String> mapMoneda = new HashMap<>();
                            mapMoneda.put('$', "Dolar");
                            mapMoneda.put('Q', "Quetzal");
                            mapMoneda.put('L', "Lempira");
                            this.monedaPresupuesto = mapMoneda.get(pre.getMonedaPresupuesto());
                            this.fechaCreacionPresupuesto = pre.getFechaCreacionPresupuesto();
                        }
                    } else {
                        if (getListCuentasPreSelec() != null) {
                            if (getListCuentasPreSelec().size() > 0) {
                                getListCuentasPreSelec().clear();
                            }
                            this.monedaPresupuesto = null;
                            this.fechaCreacionPresupuesto = null;
                        }
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "No existe Presupuesto", ""));
                    }
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "No a Seleccionado el año del presupuesto", ""));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "No a Seleccionado la Unidad Ejecutora", ""));
        }
        RequestContext.getCurrentInstance().update("fromDetalle");
    }

    public void consultarCuenta(int codigoPresu, int codigoCuenta) {
        this.codigoPresup = codigoPresu;
        this.codigoCuent = codigoCuenta;
        DetalleCuentas dc = detallePresupuestoBO.consultarDetalleCuenta(codigoPresu, codigoCuenta);
        if (dc != null) {
//            setEnero(dc.getEnero());
//            setFebrero(dc.getFebrero());
//            setMarzo(dc.getMarzo());
//            setAbril(dc.getAbril());
//            setMayo(dc.getMayo());
//            setJunio(dc.getJunio());
//            setJulio(dc.getJulio());
//            setAgosto(dc.getAgosto());
//            setSeptiembre(dc.getSeptiembre());
//            setOctubre(dc.getOctubre());
//            setNoviembre(dc.getNoviembre());
//            setDiciembre(dc.getDiciembre());
            presupuestoAprobado = dc.getPresupuestoAprobado();
        }
        RequestContext.getCurrentInstance().update("fromDetalleCuenta");
    }

    public void inserUpdateDetalle() {
        if (detallePresupuestoBO.inserUpdateDetalleCuenta(this) == false) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Detalle No Agregado", ""));
        }
        RequestContext.getCurrentInstance().update("fromDetalleCuenta");
    }

    public void insertarMontos() {

        
        if (detalleDataTable != null) {

            
            if (detallePresupuestoBO.insertCuenta(detalleDataTable)) {
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Los montos se guardaron exitosamente", ""));
             limpiar();

             } else {
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error no se pudo guardar los montos", ""));
             }
             
        }
    }

    public void limpiar() {
//        enero = null;
//        febrero = null;
//        marzo = null;
//        abril = null;
//        mayo = null;
//        junio = null;
//        julio = null;
//        agosto = null;
//        septiembre = null;
//        octubre = null;
//        noviembre = null;
//        diciembre = null;
        presupuestoAprobado = null;
        RequestContext.getCurrentInstance().update("fromDetalleCuenta");
    }

    public void cargarPresupuestoAprobado() {
        pdfDoc = controlReporte.getPresupuestoAprobado(unidadesEjecutoras, anyoPresupuesto);
    }

    public void presupuestoAprobadoXLS() {
        controlReporte.exportarPresupuestoAprobado(unidadesEjecutoras, anyoPresupuesto, ReportePresupuesto.XLS);
    }

    public void presupuestoAprobadoPDF() {
        controlReporte.exportarPresupuestoAprobado(unidadesEjecutoras, anyoPresupuesto, ReportePresupuesto.PDF);
    }

    public void presupuestoAprobadoRTF() {
        controlReporte.exportarPresupuestoAprobado(unidadesEjecutoras, anyoPresupuesto, ReportePresupuesto.RTF);
    }

    public void presupuestoAprobadoPrintReport() {
        controlReporte.exportarPresupuestoAprobado(unidadesEjecutoras, anyoPresupuesto, ReportePresupuesto.PRINTER);
    }

    public StreamedContent getPdfDoc() {
        pdfDoc = controlReporte.getPresupuestoAprobado(unidadesEjecutoras, anyoPresupuesto);
        return pdfDoc;
    }

    public void setPdfDoc(StreamedContent pdfDoc) {
        this.pdfDoc = pdfDoc;
    }

    public ReportePresupuesto getControlReporte() {
        return controlReporte;
    }

    public void setControlReporte(ReportePresupuesto controlReporte) {
        this.controlReporte = controlReporte;
    }

    public DriverManagerDataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DriverManagerDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Set<DetalleCuentas> getListDetallePresupuesto() {
        return listDetallePresupuesto;
    }

    public void setListDetallePresupuesto(Set<DetalleCuentas> listDetallePresupuesto) {
        this.listDetallePresupuesto = listDetallePresupuesto;
    }

    public DetallePresupuestoBO getDetallePresupuestoBO() {
        return detallePresupuestoBO;
    }

    public void setDetallePresupuestoBO(DetallePresupuestoBO detallePresupuestoBO) {
        this.detallePresupuestoBO = detallePresupuestoBO;
    }

    public short getUnidadesEjecutoras() {
        return unidadesEjecutoras;
    }

    public void setUnidadesEjecutoras(short unidadesEjecutoras) {
        this.unidadesEjecutoras = unidadesEjecutoras;
    }

    public Map<String, Short> getMapaUnidadEjecutora() {
        return mapaUnidadEjecutora;
    }

    public void setMapaUnidadEjecutora(Map<String, Short> mapaUnidadEjecutora) {
        this.mapaUnidadEjecutora = mapaUnidadEjecutora;
    }

    public PresupuestoBO getPresupuestoBO() {
        return presupuestoBO;
    }

    public void setPresupuestoBO(PresupuestoBO presupuestoBO) {
        this.presupuestoBO = presupuestoBO;
    }

    public Date getFechaCreacionPresupuesto() {
        return fechaCreacionPresupuesto;
    }

    public void setFechaCreacionPresupuesto(Date fechaCreacionPresupuesto) {
        this.fechaCreacionPresupuesto = fechaCreacionPresupuesto;
    }

    public List<Integer> getListAnios() {
        return listAnios;
    }

    public void setListAnios(List<Integer> listAnios) {
        this.listAnios = listAnios;
    }

    public Integer getAnyoPresupuesto() {
        return anyoPresupuesto;
    }

    public void setAnyoPresupuesto(Integer anyoPresupuesto) {
        this.anyoPresupuesto = anyoPresupuesto;
    }

    public String getMonedaPresupuesto() {
        return monedaPresupuesto;
    }

    public void setMonedaPresupuesto(String monedaPresupuesto) {
        this.monedaPresupuesto = monedaPresupuesto;
    }

    public List<CuentasPresupuesto> getListCuentasPreSelec() {
        return listCuentasPreSelec;
    }

    public void setListCuentasPreSelec(List<CuentasPresupuesto> listCuentasPreSelec) {
        this.listCuentasPreSelec = listCuentasPreSelec;
    }

    public int getCodigoPresup() {
        return codigoPresup;
    }

    public void setCodigoPresup(int codigoPresup) {
        this.codigoPresup = codigoPresup;
    }

    public int getCodigoCuent() {
        return codigoCuent;
    }

    public void setCodigoCuent(int codigoCuent) {
        this.codigoCuent = codigoCuent;
    }

    public BigDecimal getPresupuestoAprobado() {
        return presupuestoAprobado;
    }

    public void setPresupuestoAprobado(BigDecimal presupuestoAprobado) {
        this.presupuestoAprobado = presupuestoAprobado;
    }

    public ReprogramarPreBO getReprogramarPreBO() {
        return reprogramarPreBO;
    }

    public void setReprogramarPreBO(ReprogramarPreBO reprogramarPreBO) {
        this.reprogramarPreBO = reprogramarPreBO;
    }

    public UIData getDetalleDataTable() {
        return detalleDataTable;
    }

    public void setDetalleDataTable(UIData detalleDataTable) {
        this.detalleDataTable = detalleDataTable;
    }

    public CuentasPresupuestoId getId() {
        return id;
    }

    public void setId(CuentasPresupuestoId id) {
        this.id = id;
    }

    public CatalogoCuentas getCatalogoCuentas() {
        return catalogoCuentas;
    }

    public void setCatalogoCuentas(CatalogoCuentas catalogoCuentas) {
        this.catalogoCuentas = catalogoCuentas;
    }

    public Presupuestos getPresupuestos() {
        return presupuestos;
    }

    public void setPresupuestos(Presupuestos presupuestos) {
        this.presupuestos = presupuestos;
    }

    public String getDescripcionCuentaMayor() {
        return descripcionCuentaMayor;
    }

    public void setDescripcionCuentaMayor(String descripcionCuentaMayor) {
        this.descripcionCuentaMayor = descripcionCuentaMayor;
    }

    public String getDependeDe() {
        return dependeDe;
    }

    public void setDependeDe(String dependeDe) {
        this.dependeDe = dependeDe;
    }

    public Boolean getConDetalle() {
        return conDetalle;
    }

    public void setConDetalle(Boolean conDetalle) {
        this.conDetalle = conDetalle;
    }

    public List<DetallePresupuestoBean> getListAllDetalle() {
        return listAllDetalle;
    }

    public void setListAllDetalle(List<DetallePresupuestoBean> listAllDetalle) {
        this.listAllDetalle = listAllDetalle;
    }

}
