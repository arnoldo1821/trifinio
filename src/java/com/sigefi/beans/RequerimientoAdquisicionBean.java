/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.RequerimientoAdquisicionBO;
import com.sigefi.clases.FechaSistema;
import com.sigefi.entity.DetalleCuentas;
import com.sigefi.entity.DetalleRequerimientoAdquisicion;
import com.sigefi.entity.DetalleRequerimientoAdquisicionId;
import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.UnidadesEjecutoras;
import com.sigefi.entity.VistoBuenoRequerimiento;
import com.sigefi.entity.VistoBuenoRequerimientoId;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.FlowEvent;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class RequerimientoAdquisicionBean implements Serializable{

    @ManagedProperty(value = "#{requerimientoAdquisicionBO}")
    private RequerimientoAdquisicionBO requerimientoAdquisicionBO;
    
    private List<UnidadesEjecutoras> listUniTodos = new ArrayList<>();
    private List<UnidadesEjecutoras> listUniOficina = new ArrayList<>();
    private short idUnidadTodos;
    private short idUnidadOficina;
    List<Short> listAnio = new ArrayList<>();
    private short anioSelecc;
    private boolean estadoAnio;
    private List<DetalleCuentas> listDetalleCuentas = new ArrayList<>();
    private int numCuenta;
    private String codRequerimiento;
    private String numProceso;
    private String actividad;
    private String unidadSolicitante;
    private String nombreSolicitante;
    private String nombreProceso;
    private BigDecimal presupuestoEstimado;
    private String metodoSeleccion;
    private Date fecha;
    
    /*TAB DETALLE*/
    private int cantidad;
    private String descripcion;
    private BigDecimal precioUnitario;
    private String unidadMedida;
    
    private boolean estadotabla;
    private boolean estadofinalizar;
    
    private boolean estadotabla2;
    private boolean estadofinalizar2;
    
    /*VISTO BUENO*/
    private String nombre;
    private String puesto;
    
    private List<DetalleRequerimientoAdquisicion> listDetalleReqAqui = new ArrayList<>();
    private List<VistoBuenoRequerimiento> listVistoBueno = new ArrayList<>();
        
    public RequerimientoAdquisicionBean() {
        FechaSistema anioHoy = new FechaSistema();
        short anio = (short) (anioHoy.getAnio() - 1);
        for (int i = 0; i <= 3; i++) {
            listAnio.add(anio);
            anio = (short) (anio + 1);
        }
        estadoAnio = true;
        estadotabla = true;
        estadofinalizar = true;
        estadotabla2 = true;
        estadofinalizar2 = true;
        
//        estadoAuxReque = true;
//        descAuxReque = "Se requiere una oficina";
    }
    
    @PostConstruct
    public void main(){
        List<UnidadesEjecutoras> listUni = requerimientoAdquisicionBO.listUnidades();
        if(listUni != null){
            for(UnidadesEjecutoras obj : listUni){
                if(!obj.getEsProyecto()){
                    listUniOficina.add(obj);
                }
            }
            setListUniTodos(listUni);
        }
    }
    
    public void unidadEjecutoraSeleccionada(){
        if(idUnidadTodos >0){
            estadoAnio = false;
        }else{
            estadoAnio = true;
        }
    }
    
    public void anioSeleccionado(){
        if(anioSelecc>0){
            Presupuestos pre = requerimientoAdquisicionBO.elementoPresupuesto(idUnidadTodos, anioSelecc);
            if(pre != null){
                listDetalleCuentas.addAll(pre.getDetalleCuentases());
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Presupuesto Seleccionado", ""));
            }else{
                if(listDetalleCuentas != null){
                    if(listDetalleCuentas.size()>0){
                        listDetalleCuentas.clear();
                    }
                }
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "No existe Presupuesto para esa unidad ejecutora y año seleccionado", ""));
            }
        }
    }
    
    /*TAB DETALLES*/
    public void agregrarElementoListaDetalleReq() {
            DetalleRequerimientoAdquisicion detaReqAdq = new DetalleRequerimientoAdquisicion();

            detaReqAdq.setCantidad(cantidad);
            detaReqAdq.setNombreBien(descripcion);
            detaReqAdq.setPrecioUnitario(precioUnitario);
            detaReqAdq.setUnidadMedida(unidadMedida);

            if (listDetalleReqAqui != null) {
                if (listDetalleReqAqui.size() > 0) {
                    if (comprobarValorTablaDetalle(listDetalleReqAqui, detaReqAdq)) {
                        limpiarDetalleReq();
                        listDetalleReqAqui.add(detaReqAdq);
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El elemento agregado correctamente", ""));
                        estadofinalizar = false;
                    } 
                }else {
                    limpiarDetalleReq();
                    listDetalleReqAqui.add(detaReqAdq);
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El elemento agregado correctamente", ""));
                    estadofinalizar = false;
                }
            }
        
    }
    
    public boolean comprobarValorTablaDetalle(List<DetalleRequerimientoAdquisicion> list, DetalleRequerimientoAdquisicion objA) {
        for (DetalleRequerimientoAdquisicion obj : list) {
            if (obj.getNombreBien().equals(objA.getNombreBien())) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "La descripcion ya existe", ""));
                return false;
            }
        }
        return true;
    }

    
    public void limpiarDetalleReq(){
        cantidad = 0;
        descripcion = null;
        precioUnitario = null;
        unidadMedida = null;
    }
    
    public void eliminarElementoListaDetalleReq(DetalleRequerimientoAdquisicion obj){
        listDetalleReqAqui.remove(obj);
        if(listDetalleReqAqui.size() == 0){
            estadofinalizar = true;
            estadotabla = true;
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El elemento eliminado correctamente", ""));
    }
    
    public void finalizarLista(){
        estadotabla = false;
    }
    
    /*TAB VISTO BUENO*/
    public void agregrarElementoListaVistoBueno() {
        
            VistoBuenoRequerimientoId vistoId = new VistoBuenoRequerimientoId();
            
            vistoId.setCodigoRequerimiento(codRequerimiento);
            vistoId.setNombre(nombre);
            
            VistoBuenoRequerimiento visto = new VistoBuenoRequerimiento();

            visto.setId(vistoId);
            visto.setPuesto(puesto);
            

            if (listVistoBueno != null) {
                if (listVistoBueno.size() > 0) {
                    if (comprobarValorTablaVistoBueno(listVistoBueno, visto)) {
                        limpiarVistoBueno();
                        listVistoBueno.add(visto);
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El elemento agregado correctamente", ""));
                        estadofinalizar2 = false;
                    } 
                }else {
                    limpiarVistoBueno();
                    listVistoBueno.add(visto);
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El elemento agregado correctamente", ""));
                    estadofinalizar2 = false;
                }
            }
        
    }
    
    public boolean comprobarValorTablaVistoBueno(List<VistoBuenoRequerimiento> list, VistoBuenoRequerimiento objA) {
        for (VistoBuenoRequerimiento obj : list) {
            if (obj.getId().getNombre().equals(objA.getId().getNombre())) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "El nombre ya existe", ""));
                return false;
            }
            if (obj.getPuesto().equals(objA.getPuesto())) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "El puesto ya existe", ""));
                return false;
            }
        }
        return true;
    }

    
    public void limpiarVistoBueno(){
        nombre = null;
        puesto = null;
    }
    
    public void eliminarElementoListaVistoBueno(VistoBuenoRequerimiento obj){
        listVistoBueno.remove(obj);
        if(listVistoBueno.size() == 0){
            estadofinalizar2 = true;
            estadotabla2 = true;
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El elemento eliminado correctamente", ""));
    }
    
    public void finalizarListaVistoBueno(){
        estadotabla2 = false;
    }
    
    public void comprobrarRequerimiento() {
        if (codRequerimiento != null) {
            if (!codRequerimiento.isEmpty()) {
                if (requerimientoAdquisicionBO.consultarCodigoRequerimiento(codRequerimiento)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Nuevo codigo de requerimiento", ""));
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "El " + codRequerimiento + " codigo del requerimiento  ya existe", ""));
                    codRequerimiento = null;
                }
            }
        }
    }
    
    public RequerimientoAdquisicionBO getRequerimientoAdquisicionBO() {
        return requerimientoAdquisicionBO;
    }

    public void setRequerimientoAdquisicionBO(RequerimientoAdquisicionBO requerimientoAdquisicionBO) {
        this.requerimientoAdquisicionBO = requerimientoAdquisicionBO;
    }

    public List<UnidadesEjecutoras> getListUniTodos() {
        return listUniTodos;
    }

    public void setListUniTodos(List<UnidadesEjecutoras> listUniTodos) {
        this.listUniTodos = listUniTodos;
    }

    public short getIdUnidadTodos() {
        return idUnidadTodos;
    }

    public void setIdUnidadTodos(short idUnidadTodos) {
        this.idUnidadTodos = idUnidadTodos;
    }

    public List<UnidadesEjecutoras> getListUniOficina() {
        return listUniOficina;
    }

    public void setListUniOficina(List<UnidadesEjecutoras> listUniOficina) {
        this.listUniOficina = listUniOficina;
    }

    public short getIdUnidadOficina() {
        return idUnidadOficina;
    }

    public void setIdUnidadOficina(short idUnidadOficina) {
        this.idUnidadOficina = idUnidadOficina;
    }

    public List<Short> getListAnio() {
        return listAnio;
    }

    public void setListAnio(List<Short> listAnio) {
        this.listAnio = listAnio;
    }

    public short getAnioSelecc() {
        return anioSelecc;
    }

    public void setAnioSelecc(short anioSelecc) {
        this.anioSelecc = anioSelecc;
    }

    public boolean isEstadoAnio() {
        return estadoAnio;
    }

    public void setEstadoAnio(boolean estadoAnio) {
        this.estadoAnio = estadoAnio;
    }

    public List<DetalleCuentas> getListDetalleCuentas() {
        return listDetalleCuentas;
    }

    public void setListDetalleCuentas(List<DetalleCuentas> listDetalleCuentas) {
        this.listDetalleCuentas = listDetalleCuentas;
    }

    public int getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(int numCuenta) {
        this.numCuenta = numCuenta;
    }

    public String getCodRequerimiento() {
        return codRequerimiento;
    }

    public void setCodRequerimiento(String codRequerimiento) {
        this.codRequerimiento = codRequerimiento;
    }

    public String getNumProceso() {
        return numProceso;
    }

    public void setNumProceso(String numProceso) {
        this.numProceso = numProceso;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getUnidadSolicitante() {
        return unidadSolicitante;
    }

    public void setUnidadSolicitante(String unidadSolicitante) {
        this.unidadSolicitante = unidadSolicitante;
    }

    public String getNombreSolicitante() {
        return nombreSolicitante;
    }

    public void setNombreSolicitante(String nombreSolicitante) {
        this.nombreSolicitante = nombreSolicitante;
    }

    public String getNombreProceso() {
        return nombreProceso;
    }

    public void setNombreProceso(String nombreProceso) {
        this.nombreProceso = nombreProceso;
    }

    public BigDecimal getPresupuestoEstimado() {
        return presupuestoEstimado;
    }

    public void setPresupuestoEstimado(BigDecimal presupuestoEstimado) {
        this.presupuestoEstimado = presupuestoEstimado;
    }

    public String getMetodoSeleccion() {
        return metodoSeleccion;
    }

    public void setMetodoSeleccion(String metodoSeleccion) {
        this.metodoSeleccion = metodoSeleccion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isEstadotabla() {
        return estadotabla;
    }

    public void setEstadotabla(boolean estadotabla) {
        this.estadotabla = estadotabla;
    }

    public boolean isEstadofinalizar() {
        return estadofinalizar;
    }

    public void setEstadofinalizar(boolean estadofinalizar) {
        this.estadofinalizar = estadofinalizar;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public BigDecimal getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(BigDecimal precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public List<DetalleRequerimientoAdquisicion> getListDetalleReqAqui() {
        return listDetalleReqAqui;
    }

    public void setListDetalleReqAqui(List<DetalleRequerimientoAdquisicion> listDetalleReqAqui) {
        this.listDetalleReqAqui = listDetalleReqAqui;
    }

    public List<VistoBuenoRequerimiento> getListVistoBueno() {
        return listVistoBueno;
    }

    public void setListVistoBueno(List<VistoBuenoRequerimiento> listVistoBueno) {
        this.listVistoBueno = listVistoBueno;
    }

    public boolean isEstadotabla2() {
        return estadotabla2;
    }

    public void setEstadotabla2(boolean estadotabla2) {
        this.estadotabla2 = estadotabla2;
    }

    public boolean isEstadofinalizar2() {
        return estadofinalizar2;
    }

    public void setEstadofinalizar2(boolean estadofinalizar2) {
        this.estadofinalizar2 = estadofinalizar2;
    }

    
}
