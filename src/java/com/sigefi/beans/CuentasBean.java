/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.CuentasBO;
import com.sigefi.entity.CatalogoCuentas;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.model.TreeNode;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class CuentasBean implements Serializable {

    @ManagedProperty(name = "cuentasBO", value = "#{cuentasBO}")
    private CuentasBO cuentasBO;

    private String nombreCuenta;
    private String descripcionCuenta;

    private String nombreSubCuenta;
    private String descripcionSubCuenta;
    private int idCuenta;

    private TreeNode root;
    private short unidadEjecutoraSeleccionada;
    private String codigoCuenta;
    private CatalogoCuentas cuentaSelec;
    private String codigoSubCuenta;
    private int anio;

    private boolean estado1;

    public CuentasBean() {

    }

    public void llenarTablaNodoCuentasCatalogo() {
//        setRoot(cuentasBO.listNodoCuentasCatalogo());
        root = cuentasBO.listNodoCuentasCatalogo(unidadEjecutoraSeleccionada);
        expandirAll(root);
        estado1 = false;
    }

    @PostConstruct
    public void main() {
        llenarTablaNodoCuentasCatalogo();
        estado1 = true;

    }

    public void cuentaSeleccionada(CatalogoCuentas cuenta) {
        idCuenta = cuenta.getIdCuenta();
        this.cuentaSelec = cuenta;
        RequestContext.getCurrentInstance().update("fromCrearSubCuenta");
    }

    private void expandirAll(TreeNode treeNode) {
        if (treeNode.getChildren().size() > 0) {
            treeNode.setExpanded(true);
            for (int i = 0; i < treeNode.getChildren().size(); i++) {
                expandirAll(treeNode.getChildren().get(i));
            }
        }
    }

    public void insertCuenta() {
        if (cuentasBO.insertCuenta(this)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cuenta agregada correctamente", ""));
            llenarTablaNodoCuentasCatalogo();
            limpiar();
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Cuenta Agregada No Agregada ya no hay espacio para cuentas del nivel 1", ""));
            RequestContext.getCurrentInstance().update("fromCuenta");
        }
    }

    public void insertSubCuenta() {
        if (idCuenta > 0) {
            if (cuentasBO.insertSubCuenta(this)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SubCuenta Agregada Correctamente", ""));
                llenarTablaNodoCuentasCatalogo();
                limpiar();
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Cuenta Agregada No Agregada ya no hay espacio para cuentas del nivel", ""));
                RequestContext.getCurrentInstance().update("fromCrearSubCuenta");
            }
            RequestContext.getCurrentInstance().update("fromCrearSubCuenta");
        }
        RequestContext.getCurrentInstance().update("formSubCuentas2");
    }

    public void limpiar() {
        idCuenta = 0;
        nombreCuenta = null;
        nombreSubCuenta = null;
        descripcionCuenta = null;
        descripcionSubCuenta = null;
        codigoCuenta = null;
        codigoSubCuenta = null;
        RequestContext.getCurrentInstance().update("fromCuenta");
        RequestContext.getCurrentInstance().update("fromCrearSubCuenta");
        RequestContext.getCurrentInstance().update("formSubCuentas2");
    }

    public void cancelar() {
        unidadEjecutoraSeleccionada = 0;
        llenarTablaNodoCuentasCatalogo();
        estado1 = true;
        limpiar();
    }

    public CuentasBO getCuentasBO() {
        return cuentasBO;
    }

    public void setCuentasBO(CuentasBO cuentasBO) {
        this.cuentasBO = cuentasBO;
    }

    public String getNombreCuenta() {
        return nombreCuenta;
    }

    public void setNombreCuenta(String nombreCuenta) {
        this.nombreCuenta = nombreCuenta;
    }

    public String getDescripcionCuenta() {
        return descripcionCuenta;
    }

    public void setDescripcionCuenta(String descripcionCuenta) {
        this.descripcionCuenta = descripcionCuenta;
    }

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public String getNombreSubCuenta() {
        return nombreSubCuenta;
    }

    public void setNombreSubCuenta(String nombreSubCuenta) {
        this.nombreSubCuenta = nombreSubCuenta;
    }

    public int getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(int idCuenta) {
        this.idCuenta = idCuenta;
    }

    public String getDescripcionSubCuenta() {
        return descripcionSubCuenta;
    }

    public void setDescripcionSubCuenta(String descripcionSubCuenta) {
        this.descripcionSubCuenta = descripcionSubCuenta;
    }

    public short getUnidadEjecutoraSeleccionada() {
        return unidadEjecutoraSeleccionada;
    }

    public void setUnidadEjecutoraSeleccionada(short unidadEjecutoraSeleccionada) {
        this.unidadEjecutoraSeleccionada = unidadEjecutoraSeleccionada;
    }

    public String getCodigoCuenta() {
        return codigoCuenta;
    }

    public void setCodigoCuenta(String codigoCuenta) {
        this.codigoCuenta = codigoCuenta;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public boolean isEstado1() {
        return estado1;
    }

    public void setEstado1(boolean estado1) {
        this.estado1 = estado1;
    }

    public String getCodigoSubCuenta() {
        return codigoSubCuenta;
    }

    public void setCodigoSubCuenta(String codigoSubCuenta) {
        this.codigoSubCuenta = codigoSubCuenta;
    }

    public CatalogoCuentas getCuentaSelec() {
        return cuentaSelec;
    }

    public void setCuentaSelec(CatalogoCuentas cuentaSelec) {
        this.cuentaSelec = cuentaSelec;
    }

}
