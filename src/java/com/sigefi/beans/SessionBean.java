/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.SessionBO;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;
/**
 *
 * @author AAldemaro
 */
@ManagedBean
@SessionScoped
public class SessionBean implements Serializable{
    
    @ManagedProperty(name = "sessionBO" , value = "#{sessionBO}")
    private transient SessionBO sessionBO;
    
    private String usu;
    private String nombreCompleto;
    private String nombreUsuario;
    private String apellidoUsuario;
    private String nuvaClave;
    private String unidadEjecutora;
    private short idUnidadEjecutora;
    
    /**
     * Creates a new instance of SessionBean
     */
    public SessionBean(){
        HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        if(httpSession.getAttribute("usuarioLogeado") != null){
            usu = httpSession.getAttribute("usuarioLogeado").toString();
        }
    }
    
    public SessionBO getSessionBO() {
        return sessionBO;
    }

    public void setSessionBO(SessionBO sessionBO) {
        this.sessionBO = sessionBO;
    }
    
    public String getUsu() {
        return usu;
    }

    public void setUsu(String usu) {
        this.usu = usu;
    }

    public String getNuvaClave() {
        return nuvaClave;
    }

    public void setNuvaClave(String nuvaClave) {
        this.nuvaClave = nuvaClave;
    }

    public String cerrarSession(){
        this.usu = null;
        this.nuvaClave = null;
        HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        httpSession.invalidate();
        return "/index.xhtml"+"?faces-redirect=true";
    }
    
    public void UpdateContra(){
        
        if(sessionBO.updateCambioContra(usu, this.nuvaClave)){
            nuvaClave = null;
            RequestContext.getCurrentInstance().update("cambioContraf");
//            RequestContext.getCurrentInstance().update("cambioContraPrimeraVez");
        }
        
//        RequestContext.getCurrentInstance().update("formGeneral");
    }
    
    @PostConstruct
    public void main() {
        sessionBO.elementoUsuario(this);
        if (usu != null) {
            if (sessionBO.esLaContrasena(this) == true) {
                RequestContext.getCurrentInstance().execute("PF('cambioContraPrimeraVez').show()");
            }
        }
    }
        
//        if(usu != null){
//           if(sessionBO.ConsultaUsuarioContra(usu)==true){
//                    RequestContext.getCurrentInstance().execute("PF('cambioContraPrimeraVez').show()");
//           }
//           
//           Iterator ite = sessionBO.consultaDirecUnid(usu);
//            Integer auxBool = 0;
//            while (ite.hasNext()) {
//                Integer next = (Integer) ite.next();
//                auxBool = next;
//            }
//            if(auxBool == 1){
//                esconderMenu = false;
//                RequestContext.getCurrentInstance().update("menuJefatura");
//                
//            }
//            if(auxBool == 3){
//                monitoreoDPMenu = false;
//                RequestContext.getCurrentInstance().update("menuJefatura");
//            }
//        }
//        
//        
//    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getUnidadEjecutora() {
        return unidadEjecutora;
    }

    public void setUnidadEjecutora(String unidadEjecutora) {
        this.unidadEjecutora = unidadEjecutora;
    }

    public short getIdUnidadEjecutora() {
        return idUnidadEjecutora;
    }

    public void setIdUnidadEjecutora(short idUnidadEjecutora) {
        this.idUnidadEjecutora = idUnidadEjecutora;
    }

    public String getApellidoUsuario() {
        return apellidoUsuario;
    }

    public void setApellidoUsuario(String apellidoUsuario) {
        this.apellidoUsuario = apellidoUsuario;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }
    
}
