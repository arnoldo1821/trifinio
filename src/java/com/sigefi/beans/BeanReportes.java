package com.sigefi.beans;

import com.sigefi.informe.ReportePresupuesto;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.PhaseId;
import javax.servlet.ServletContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author Oscar
 */
@ManagedBean(name = "reportesBean")
@RequestScoped
public class BeanReportes implements java.io.Serializable {

    @ManagedProperty("#{dataSource}")
    private DriverManagerDataSource dataSource;

    private ReportePresupuesto controlReporte;
    public StreamedContent pdfDocument;
    
    private String urlDoc;
    
    public BeanReportes() {
//        pdfDocument = new DefaultStreamedContent(new ByteArrayInputStream(new byte[1]));
    }

    @PostConstruct
    public void init() {
        
        System.err.println(".0. en PostConstruct");
        controlReporte = new ReportePresupuesto(dataSource);
    }

    public void cargarPresupuestoAprobado(ActionEvent event) {
        pdfDocument = (StreamedContent) event.getComponent().getAttributes().get("pdfFile");
        System.err.println(".2. en cargarPresupuestoAprobado "+pdfDocument);
    }

    public StreamedContent getPresupuestoAprobado(DetallePresupuestoBean obj) {
        System.err.println(".1. en getPresupuestoAprobado");
        return controlReporte.getPresupuestoAprobado(obj.getUnidadesEjecutoras(), obj.getAnyoPresupuesto());
    }

    public ReportePresupuesto getControlReporte() {
        return controlReporte;
    }

    public void setControlReporte(ReportePresupuesto controlReporte) {
        this.controlReporte = controlReporte;
    }

    public StreamedContent getPdfDocument() {
        FacesContext context = FacesContext.getCurrentInstance();
        System.err.println(".pdfDocument. "+pdfDocument);
        if (pdfDocument == null) {
            try {
                
                if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
                    pdfDocument = new DefaultStreamedContent();
                    System.err.println(".0.1. en RENDER_RESPONSE");
                } else {
                    ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
                    String realPath = (String) servletContext.getRealPath("/");
                    String url = realPath + "\\recursos\\imagenes\\otras\\NoHayDocumento.jpg";
                    if (!url.isEmpty()) {
                        File chartFile = new File(url);
                        if (!chartFile.isFile()) {
                            pdfDocument = new DefaultStreamedContent();
                        } else {
                            if (url.endsWith(".pdf")) {
                                pdfDocument = new DefaultStreamedContent(new FileInputStream(chartFile), "application/pdf");
                            } else {
                                pdfDocument = new DefaultStreamedContent(new FileInputStream(chartFile), "image/png");
                            }
                        }
                    }
                    System.err.println(".0.2. en cargar imagen por defecto");
                }
            } catch (Exception e) {
                System.err.println("Error en postContruct.");
            }
        }else{
            System.err.println(".0.3. en pdfDocument no nulo");
        }
        return pdfDocument;
    }

    public void setPdfDocument(StreamedContent pdfDocument) {
        this.pdfDocument = pdfDocument;
    }

    public DriverManagerDataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DriverManagerDataSource dataSource) {
        this.dataSource = dataSource;
    }

}
