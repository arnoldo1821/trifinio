/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.FirmasAutorizadasBO;
import com.sigefi.bo.PresupuestoBO;
import com.sigefi.clases.SubirArchivoServer;
import com.sigefi.entity.FirmasAutorizadas;
import com.sigefi.entity.FirmasAutorizadasProyecto;
import com.sigefi.entity.UnidadesEjecutoras;
import com.sigefi.entity.Usuarios;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import javax.faces.context.FacesContext;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class FirmasAutorizadasBean implements Serializable {

    @ManagedProperty(value = "#{firmasAutorizadasBO}")
    private FirmasAutorizadasBO firmasAutorizadasBO;

    @ManagedProperty(name = "presupuestoBO", value = "#{presupuestoBO}")
    private PresupuestoBO presupuestoBO;

    private String nombreUsuario;
    private Map<String, String> mapUsuarios;

    private String UsuarioLogeado;
    private FirmasAutorizadasBean selectFirma;
    private FirmasAutorizadasProyecto selectFirmaAuto;
    private List<FirmasAutorizadas> listAllFirmarAuto;
    private List<FirmasAutorizadasProyecto> listAllFirmarAutoProyec;

    private String Cargo;
    private String urlFirma;

    private Map<String, Short> mapaUnidadEjecutora;
    private short unidadesEjecutoras;

    public FirmasAutorizadasBean() {
    }

    @PostConstruct
    public void main() {
        List<Usuarios> listAllUsuarios = firmasAutorizadasBO.listAllUusarios();
        if (listAllUsuarios != null) {
            mapUsuarios = new HashMap<>();
            for (Usuarios obj : listAllUsuarios) {
                mapUsuarios.put(obj.getNombreUsuario() + " " + obj.getApellidosUsuario(), obj.getNombreUsuario() + " " + obj.getApellidosUsuario());
            }
        }

        SessionBean referenciaBeanSession = (SessionBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("sessionBean");

        UsuarioLogeado = referenciaBeanSession.getUsu();
//        idunidadEjecutora = referenciaBeanSession.getIdUnidadEjecutora();
//        unidadEjecutora = referenciaBeanSession.getUnidadEjecutora();

        llenarTablas();

        List<UnidadesEjecutoras> listUnidadesEjecutoras = presupuestoBO.listUnidadesEjecutoras();
        if (listUnidadesEjecutoras != null) {
            mapaUnidadEjecutora = new HashMap();
            for (UnidadesEjecutoras obj : listUnidadesEjecutoras) {
                mapaUnidadEjecutora.put(obj.getNombreUnidad(), obj.getCodigo());
            }
        }
    }

    public void llenarTablas() {
        setListAllFirmarAuto(firmasAutorizadasBO.listAllFirmasAutorizadas());
        setListAllFirmarAutoProyec(firmasAutorizadasBO.listAllFirmasAutorizadasProyecto());
    }

    public void verificarDatos() {
        if (this.UsuarioLogeado != null || this.Cargo != null) {
            RequestContext.getCurrentInstance().execute("PF('subirArchivo').show();");
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "No a digitado el cargo", ""));
        }
    }

    public void subirArchivoFirma(FileUploadEvent event) {
        if (this.UsuarioLogeado != null && this.Cargo != null) {
            SubirArchivoServer subir = new SubirArchivoServer();
            this.urlFirma = subir.SubirArchivo(event, "/SigefiDocumentos/ArchivosFirmas/", UsuarioLogeado + Cargo);
        }
    }

    public void deleteFirmaAutorizada(FirmasAutorizadasProyecto firma) {
        
        this.UsuarioLogeado = firma.getId().getUsuario();
        this.Cargo = firma.getId().getCargo();
        this.nombreUsuario = firma.getNombre();
        this.unidadesEjecutoras = firma.getUnidadesEjecutoras().getCodigo();
      
        if (!nombreUsuario.isEmpty()) {
            if (firmasAutorizadasBO.deleteFirmaAutorizada(this)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "La firma se elimino correctamente", ""));
                llenarTablas();
                limpiarFirma();
            }
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Error al eliminar la firma", ""));
        }
        
        RequestContext.getCurrentInstance().update("fromFirma");
        RequestContext.getCurrentInstance().update("formFirmaProyecto");
    }

    public void inserFirma() {
        
            
                if (firmasAutorizadasBO.insertFirmaAutorizada(this)) {
                    llenarTablas();
                    limpiarFirma();
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Firma Agregada Correctamente", ""));

                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Firma No Agregada ya exite", ""));
                }
           
        
        RequestContext.getCurrentInstance().update("fromFirma");
        RequestContext.getCurrentInstance().update("formFirmaProyecto");
    }

    public void limpiarFirma() {
        Cargo = null;
        nombreUsuario = null;
        urlFirma = null;
        RequestContext.getCurrentInstance().update("fromFirma");
        RequestContext.getCurrentInstance().update("formFirmaProyecto");
    }

    public FirmasAutorizadasBO getFirmasAutorizadasBO() {
        return firmasAutorizadasBO;
    }

    public void setFirmasAutorizadasBO(FirmasAutorizadasBO firmasAutorizadasBO) {
        this.firmasAutorizadasBO = firmasAutorizadasBO;
    }

    public Map<String, String> getMapUsuarios() {
        return mapUsuarios;
    }

    public void setMapUsuarios(Map<String, String> mapUsuarios) {
        this.mapUsuarios = mapUsuarios;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getUsuarioLogeado() {
        return UsuarioLogeado;
    }

    public void setUsuarioLogeado(String UsuarioLogeado) {
        this.UsuarioLogeado = UsuarioLogeado;
    }

    public List<FirmasAutorizadas> getListAllFirmarAuto() {
        return listAllFirmarAuto;
    }

    public void setListAllFirmarAuto(List<FirmasAutorizadas> listAllFirmarAuto) {
        this.listAllFirmarAuto = listAllFirmarAuto;
    }

    public List<FirmasAutorizadasProyecto> getListAllFirmarAutoProyec() {
        return listAllFirmarAutoProyec;
    }

    public void setListAllFirmarAutoProyec(List<FirmasAutorizadasProyecto> listAllFirmarAutoProyec) {
        this.listAllFirmarAutoProyec = listAllFirmarAutoProyec;
    }

    public String getCargo() {
        return Cargo;
    }

    public void setCargo(String Cargo) {
        this.Cargo = Cargo;
    }

    public String getUrlFirma() {
        return urlFirma;
    }

    public void setUrlFirma(String urlFirma) {
        this.urlFirma = urlFirma;
    }

    public Map<String, Short> getMapaUnidadEjecutora() {
        return mapaUnidadEjecutora;
    }

    public void setMapaUnidadEjecutora(Map<String, Short> mapaUnidadEjecutora) {
        this.mapaUnidadEjecutora = mapaUnidadEjecutora;
    }

    public short getUnidadesEjecutoras() {
        return unidadesEjecutoras;
    }

    public void setUnidadesEjecutoras(short unidadesEjecutoras) {
        this.unidadesEjecutoras = unidadesEjecutoras;
    }

    public PresupuestoBO getPresupuestoBO() {
        return presupuestoBO;
    }

    public void setPresupuestoBO(PresupuestoBO presupuestoBO) {
        this.presupuestoBO = presupuestoBO;
    }

    public FirmasAutorizadasBean getSelectFirma() {
        return selectFirma;
    }

    public void setSelectFirma(FirmasAutorizadasBean selectFirma) {
        this.selectFirma = selectFirma;
    }

    public FirmasAutorizadasProyecto getSelectFirmaAuto() {
        return selectFirmaAuto;
    }

    public void setSelectFirmaAuto(FirmasAutorizadasProyecto selectFirmaAuto) {
        this.selectFirmaAuto = selectFirmaAuto;
    }

}
