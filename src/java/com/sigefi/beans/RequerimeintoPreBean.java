/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.RequerimeintoPreBO;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class RequerimeintoPreBean implements Serializable{

    @ManagedProperty(value = "#{requerimeintoPreBO}")
    private RequerimeintoPreBO requerimeintoPreBO;
    
    public RequerimeintoPreBean() {
    }

    public RequerimeintoPreBO getRequerimeintoPreBO() {
        return requerimeintoPreBO;
    }

    public void setRequerimeintoPreBO(RequerimeintoPreBO requerimeintoPreBO) {
        this.requerimeintoPreBO = requerimeintoPreBO;
    }
    
    
}
