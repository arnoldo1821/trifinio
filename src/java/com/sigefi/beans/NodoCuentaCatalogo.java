/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author AAldemaro
 */
public class NodoCuentaCatalogo {
    
    private Map<Integer,TreeNode> mapNodo;

    public NodoCuentaCatalogo() {
        mapNodo = new HashMap<>();
    }

    public void listNodo(int numNode,TreeNode nodo){
        mapNodo.put(numNode,nodo);
    }

    public Map<Integer, TreeNode> getMapNodo() {
        return mapNodo;
    }

    public void setMapNodo(Map<Integer, TreeNode> mapNodo) {
        this.mapNodo = mapNodo;
    }
    
}
