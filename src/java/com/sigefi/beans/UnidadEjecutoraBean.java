/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.FirmasAutorizadasBO;
import com.sigefi.bo.UnidadEjecutoraBO;
import com.sigefi.clases.SubirArchivoServer;
import com.sigefi.entity.Ciudades;
import com.sigefi.entity.UnidadesEjecutoras;
import com.sigefi.entity.Usuarios;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class UnidadEjecutoraBean implements Serializable {

    /**
     * Creates a new instance of UnidadEjecutoraBean
     */
   // @ManagedProperty(name = "unidadBO", value = "#{unidadBO}")
    //  private UnidadEjecutoraBO unidadBO;
    @ManagedProperty(name = "unidadEjecutoraBO", value = "#{unidadEjecutoraBO}")
    private UnidadEjecutoraBO unidadEjecutoraBO;

    @ManagedProperty(value = "#{firmasAutorizadasBO}")
    private FirmasAutorizadasBO firmasAutorizadasBO;

    private String nombreUnidadEjecutora;
    private String director;
    private short codigo;
    private String reponsable;
    private String idciudad;
    private List<String> idCiudad;
    private String prefijoCodigo;
    private boolean tipo;
    private boolean tipo2;
    private List<UnidadEjecutoraBean> listAllUnid;
    private List<UnidadEjecutoraBean> filtroUnidad;

    private Map<String, String> mapCiudades;

    private String urlFirma;
    private String Cargo;

    private Map<String, String> mapUsuarios;
    private String nombreUsuario;
    private String UsuarioLogeado;

    private UnidadesEjecutoras unidadesEjecutoras;
    private boolean estadoFirma;
    private boolean esProyecto;
    private boolean esCheque;
    private boolean requiereCheque;

    private boolean estadoSelecProyecto;
    private boolean estadoSelecOficina;
    private boolean estadoChequeNo;
    private boolean estadoChequeSi;

    public UnidadEjecutoraBean() {
        estadoSelecProyecto = true;
        estadoSelecOficina = false;
        estadoChequeNo = false;
        estadoChequeSi = true;
    }

    @PostConstruct
    public void main() {
        mapCiudades = new HashMap<>();
        List<Ciudades> listCiudades = unidadEjecutoraBO.listAllCiudades();
        if (listCiudades != null) {
            for (Ciudades obj : listCiudades) {
                mapCiudades.put(obj.getNombreCiudad(), obj.getNombreCiudad());
            }
        }
        llenarListUnidadEjecutora();

        List<Usuarios> listAllUsuarios = firmasAutorizadasBO.listAllUusarios();
        if (listAllUsuarios != null) {
            mapUsuarios = new HashMap<>();
            for (Usuarios obj : listAllUsuarios) {
                mapUsuarios.put(obj.getNombreUsuario() + " " + obj.getApellidosUsuario(), obj.getNombreUsuario() + " " + obj.getApellidosUsuario());
            }
        }

        SessionBean referenciaBeanSession = (SessionBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("sessionBean");

        UsuarioLogeado = referenciaBeanSession.getUsu();
    }

    public void llenarListUnidadEjecutora() {
        setListAllUnid(unidadEjecutoraBO.listAllUnidadesEjecutoras());
    }

    public void insertUnidadEjecutora() {
        if (unidadEjecutoraBO.insertUnidadEjecutora(this)) {
            limpiar();
            llenarListUnidadEjecutora();
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Unidad Ejecutora ya existe con ese nombre", ""));
        }
        RequestContext.getCurrentInstance().update("fromUnidadEjecutora:unida");
    }

    public void tipoSeleccionado() {
        if (tipo == true) {
            estadoSelecOficina = true;
            estadoSelecProyecto = false;
        } else {
            estadoSelecOficina = false;
            estadoSelecProyecto = true;
        }
    }

    public void tipoChequeSeleccionado() {
        if (tipo2 == true) {
            estadoChequeNo = true;
            estadoChequeSi = false;
        } else {
            estadoChequeNo = false;
            estadoChequeSi = true;
        }
    }

    public void verificarDatos() {
        if (this.UsuarioLogeado != null || this.Cargo != null) {
            RequestContext.getCurrentInstance().execute("PF('subirArchivo').show();");
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "No a digitado el cargo", ""));
        }
    }

    public void subirArchivoFirma(FileUploadEvent event) {
        if (this.UsuarioLogeado != null && this.Cargo != null) {
            SubirArchivoServer subir = new SubirArchivoServer();
            this.urlFirma = subir.SubirArchivo(event, "/SigefiDocumentos/ArchivosFirmas/", UsuarioLogeado + Cargo);
        }
    }

    public void unidadEjecutoraSelecc(UnidadesEjecutoras uni) {
        this.unidadesEjecutoras = uni;
        setEsProyecto(uni.getEsProyecto());
    }

    public void unidadEjecutoraSelec(UnidadesEjecutoras uni) {
        this.unidadesEjecutoras = uni;
        setEsCheque(uni.getEsCheque());
    }

    public void inserFirma() {
        if (this.urlFirma != null) {
            if (!this.urlFirma.isEmpty()) {
                if (unidadEjecutoraBO.insertFirmaAutorizada(this)) {
                    limpiarFirma();
                    llenarListUnidadEjecutora();
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Firma Agregada Corectamente", ""));
                    RequestContext.getCurrentInstance().update("fromUnidadEjecutora:unida");
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Firma No Agregada ya exite", ""));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "No a Subido por lo menos un archivo", ""));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "No a Subido por lo menos un archivo", ""));
        }
        RequestContext.getCurrentInstance().update("fromDetalleCuenta");
    }

    public void actualizarUnidad(RowEditEvent event) {
        if (unidadEjecutoraBO.actualizaUnidadEjecutora((UnidadEjecutoraBean) event.getObject())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Registro actualizado correctamente "));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "No Se Puedo Realizar La Actualización", ""));
        }
    }// Fin de Actualizar Unidad Ejecutora

    public void cancelarActualizarUnidad(RowEditEvent event) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Operacion Cancelada"));
    }

    public void limpiarFirma() {
        Cargo = null;
        nombreUsuario = null;
        urlFirma = null;
    }

    public void limpiar() {
        nombreUnidadEjecutora = null;
        director = null;
        reponsable = null;
        idCiudad = null;
        prefijoCodigo = null;
        estadoSelecProyecto = true;
        estadoSelecOficina = false;
        estadoChequeNo = false;
        estadoChequeSi = true;
        if (idCiudad != null) {
            if (idCiudad.size() > 0) {
                idCiudad.clear();
            }
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Unidad Ejecutora Agregada Correctamente", ""));
        RequestContext.getCurrentInstance().update("fromUnidadEjecutora");
    }

    public UnidadEjecutoraBO getUnidadEjecutoraBO() {
        return unidadEjecutoraBO;
    }

    public void setUnidadEjecutoraBO(UnidadEjecutoraBO unidadEjecutoraBO) {
        this.unidadEjecutoraBO = unidadEjecutoraBO;
    }

    public Map<String, String> getMapCiudades() {
        return mapCiudades;
    }

    public void setMapCiudades(Map<String, String> mapCiudades) {
        this.mapCiudades = mapCiudades;
    }

    public String getNombreUnidadEjecutora() {
        return nombreUnidadEjecutora;
    }

    public void setNombreUnidadEjecutora(String nombreUnidadEjecutora) {
        this.nombreUnidadEjecutora = nombreUnidadEjecutora;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getReponsable() {
        return reponsable;
    }

    public void setReponsable(String reponsable) {
        this.reponsable = reponsable;
    }

    public List<String> getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(List<String> idCiudad) {
        this.idCiudad = idCiudad;
    }

    public boolean isTipo() {
        return tipo;
    }

    public void setTipo(boolean tipo) {
        this.tipo = tipo;
    }

    public String getPrefijoCodigo() {
        return prefijoCodigo;
    }

    public void setPrefijoCodigo(String prefijoCodigo) {
        this.prefijoCodigo = prefijoCodigo;
    }

    public boolean isTipo2() {
        return tipo2;
    }

    public void setTipo2(boolean tipo2) {
        this.tipo2 = tipo2;
    }

    public boolean isRequiereCheque() {
        return requiereCheque;
    }

    public void setRequiereCheque(boolean requiereCheque) {
        this.requiereCheque = requiereCheque;
    }

    public boolean isEstadoChequeNo() {
        return estadoChequeNo;
    }

    public void setEstadoChequeNo(boolean estadoChequeNo) {
        this.estadoChequeNo = estadoChequeNo;
    }

    public boolean isEstadoChequeSi() {
        return estadoChequeSi;
    }

    public void setEstadoChequeSi(boolean estadoChequeSi) {
        this.estadoChequeSi = estadoChequeSi;
    }

    public List<UnidadEjecutoraBean> getListAllUnid() {
        return listAllUnid;
    }

    public void setListAllUnid(List<UnidadEjecutoraBean> listAllUnid) {
        this.listAllUnid = listAllUnid;
    }

    public List<UnidadEjecutoraBean> getFiltroUnidad() {
        return filtroUnidad;
    }

    public void setFiltroUnidad(List<UnidadEjecutoraBean> filtroUnidad) {
        this.filtroUnidad = filtroUnidad;
    }

    public String getUrlFirma() {
        return urlFirma;
    }

    public void setUrlFirma(String urlFirma) {
        this.urlFirma = urlFirma;
    }

    public String getCargo() {
        return Cargo;
    }

    public void setCargo(String Cargo) {
        this.Cargo = Cargo;
    }

    public FirmasAutorizadasBO getFirmasAutorizadasBO() {
        return firmasAutorizadasBO;
    }

    public void setFirmasAutorizadasBO(FirmasAutorizadasBO firmasAutorizadasBO) {
        this.firmasAutorizadasBO = firmasAutorizadasBO;
    }

    public Map<String, String> getMapUsuarios() {
        return mapUsuarios;
    }

    public void setMapUsuarios(Map<String, String> mapUsuarios) {
        this.mapUsuarios = mapUsuarios;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getUsuarioLogeado() {
        return UsuarioLogeado;
    }

    public void setUsuarioLogeado(String UsuarioLogeado) {
        this.UsuarioLogeado = UsuarioLogeado;
    }

    public UnidadesEjecutoras getUnidadesEjecutoras() {
        return unidadesEjecutoras;
    }

    public void setUnidadesEjecutoras(UnidadesEjecutoras unidadesEjecutoras) {
        this.unidadesEjecutoras = unidadesEjecutoras;
    }

    public boolean isEstadoFirma() {
        return estadoFirma;
    }

    public void setEstadoFirma(boolean estadoFirma) {
        this.estadoFirma = estadoFirma;
    }

    public boolean isEsProyecto() {
        return esProyecto;
    }

    public void setEsProyecto(boolean esProyecto) {
        this.esProyecto = esProyecto;
    }

    public boolean isEstadoSelecProyecto() {
        return estadoSelecProyecto;
    }

    public void setEstadoSelecProyecto(boolean estadoSelecProyecto) {
        this.estadoSelecProyecto = estadoSelecProyecto;
    }

    public boolean isEstadoSelecOficina() {
        return estadoSelecOficina;
    }

    public void setEstadoSelecOficina(boolean estadoSelecOficina) {
        this.estadoSelecOficina = estadoSelecOficina;
    }

    public short getCodigo() {
        return codigo;
    }

    public void setCodigo(short codigo) {
        this.codigo = codigo;
    }

    public String getIdciudad() {
        return idciudad;
    }

    public void setIdciudad(String idciudad) {
        this.idciudad = idciudad;
    }

    public boolean isEsCheque() {
        return esCheque;
    }

    public void setEsCheque(boolean esCheque) {
        this.esCheque = esCheque;
    }

}
