/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.ReprogramarPreBO;
import com.sigefi.entity.DetalleCuentas;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIData;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class ReprogramarPreBean implements Serializable {

    @ManagedProperty(value = "#{reprogramarPreBO}")
    private ReprogramarPreBO reprogramarPreBO;

    private UIData reproDataTable;
    private UIData DataTable;
    private short unidadesEjecutoras;
    private int anyoPresupuesto;
    private List<DetalleCuentas> listDetalleCuentas;
    private List<DetalleCuentas> selectCuentas;
    private Map<Integer, DetalleCuentas> mapDetalleCuentas;

    private int codgioCuenta1;
    private int codgioCuenta2;
    private BigDecimal valor1;
    private BigDecimal valor2;
    private BigDecimal cargo;
    private BigDecimal abono;

    private int codigoPresupuesto;
    private int codigoCuenta;
    private BigDecimal presupuestoAprobado;
    private BigDecimal presupuestoComprometido;
    private BigDecimal ejecutadoContable;
    private BigDecimal ejecutadoPagado;
    private BigDecimal presupuestoReal;
    private BigDecimal presupuestoReprogramar;
    private List<ReprogramarPreBean> listAllDetalleCuentas;

    public ReprogramarPreBean() {
    }

    @PostConstruct
    public void main() {
        llenarListDetalleCuentas();
    }

    public void llenarListDetalleCuentas() {
        setListAllDetalleCuentas(reprogramarPreBO.listNodoDetalleCuentas(unidadesEjecutoras, anyoPresupuesto));

        List<DetalleCuentas> listC = reprogramarPreBO.listDetalleCuentasPre(unidadesEjecutoras, anyoPresupuesto);
        if (listC != null) {
            if (listC.size() > 0) {
                setListDetalleCuentas(listC);
                mapDetalleCuentas = new HashMap<>();
                for (DetalleCuentas obj : listC) {
                    mapDetalleCuentas.put(obj.getId().getCodgioCuenta(), obj);
                }
                RequestContext.getCurrentInstance().update("fromReprogramacion");
            }
        } else {
            if (listDetalleCuentas != null) {
                if (listDetalleCuentas.size() > 0) {
                    listDetalleCuentas.clear();
                }
            }
            if (mapDetalleCuentas != null) {
                if (mapDetalleCuentas.size() > 0) {
                    mapDetalleCuentas.clear();
                }
            }
        }
        RequestContext.getCurrentInstance().update("fromReprogramacion");
    }

    public void consultarPresupuesto() {
        List<DetalleCuentas> listC = reprogramarPreBO.listDetalleCuentasPre(unidadesEjecutoras, anyoPresupuesto);
        if (listC != null) {
            if (listC.size() > 0) {
                setListDetalleCuentas(listC);
                mapDetalleCuentas = new HashMap<>();
                for (DetalleCuentas obj : listC) {
                    mapDetalleCuentas.put(obj.getId().getCodgioCuenta(), obj);
                }
                RequestContext.getCurrentInstance().update("fromRepro");
                RequestContext.getCurrentInstance().execute("PF('reproPre').show();");
            }
        } else {
            if (listDetalleCuentas != null) {
                if (listDetalleCuentas.size() > 0) {
                    listDetalleCuentas.clear();
                }
            }
            if (mapDetalleCuentas != null) {
                if (mapDetalleCuentas.size() > 0) {
                    mapDetalleCuentas.clear();
                }
            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "No existe presupuesto para la Unidad Ejecutora y año seleccionado", ""));
        }
    }

    public void reprogramarPresupuesto() {

        if(reproDataTable != null){
        List<DetalleCuentas> listCuentas;
        listCuentas = (List<DetalleCuentas>) reproDataTable.getValue();
        
        BigDecimal auxCargo = new BigDecimal(0);
        BigDecimal auxAbono = new BigDecimal(0);
        
        for (DetalleCuentas obj : listCuentas) {
            auxCargo = obj.getCargo().add(auxCargo);
            auxAbono = obj.getAbono().add(auxAbono);
        }
        
       if(auxCargo.equals(auxAbono)){
        if (reprogramarPreBO.updateCuentas(reproDataTable)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Reprogramacion de cuentas exitosa", ""));
            limpiar();
            
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error no se pudo realizar la reprogramacion", ""));
        }
       }else{
           FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "El total de Cargo debe ser igual al total de Abono", ""));
           reproDataTable = null;
       }
        }
    }

    public void limpiar() {
        this.listDetalleCuentas.clear();
        this.listAllDetalleCuentas.clear();
        this.mapDetalleCuentas.clear();
        this.reproDataTable = null;
        this.unidadesEjecutoras = 0;
        this.anyoPresupuesto = 0;
        RequestContext.getCurrentInstance().update("fromReprogramacion");
    }

    public void cargarCuenta() {
        if (codgioCuenta1 >= 0) {
            if (codgioCuenta2 >= 0) {
                DetalleCuentas det1 = mapDetalleCuentas.get(codgioCuenta1);
                BigDecimal aux = det1.getPresupuestoReprogramar().subtract(valor1);
                det1.setPresupuestoReprogramar(aux);
                if (reprogramarPreBO.modificarCuenta(det1)) {
                    DetalleCuentas det2 = mapDetalleCuentas.get(codgioCuenta2);
                    BigDecimal aux2 = det2.getPresupuestoReprogramar().add(valor1);
                    det2.setPresupuestoReprogramar(aux2);
                    if (reprogramarPreBO.modificarCuenta(det2)) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cuenta reprogramada exitosamente", ""));
                    }
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "No a seleccionado la cuenta a abonar", ""));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "No a seleccionado la cuenta a cargar", ""));
        }
    }

    public ReprogramarPreBO getReprogramarPreBO() {
        return reprogramarPreBO;
    }

    public void setReprogramarPreBO(ReprogramarPreBO reprogramarPreBO) {
        this.reprogramarPreBO = reprogramarPreBO;
    }

    public short getUnidadesEjecutoras() {
        return unidadesEjecutoras;
    }

    public void setUnidadesEjecutoras(short unidadesEjecutoras) {
        this.unidadesEjecutoras = unidadesEjecutoras;
    }

    public int getAnyoPresupuesto() {
        return anyoPresupuesto;
    }

    public void setAnyoPresupuesto(int anyoPresupuesto) {
        this.anyoPresupuesto = anyoPresupuesto;
    }

    public List<DetalleCuentas> getListDetalleCuentas() {
        return listDetalleCuentas;
    }

    public void setListDetalleCuentas(List<DetalleCuentas> listDetalleCuentas) {
        this.listDetalleCuentas = listDetalleCuentas;
    }

    public int getCodgioCuenta1() {
        return codgioCuenta1;
    }

    public void setCodgioCuenta1(int codgioCuenta1) {
        this.codgioCuenta1 = codgioCuenta1;
    }

    public int getCodgioCuenta2() {
        return codgioCuenta2;
    }

    public void setCodgioCuenta2(int codgioCuenta2) {
        this.codgioCuenta2 = codgioCuenta2;
    }

    public BigDecimal getValor1() {
        return valor1;
    }

    public void setValor1(BigDecimal valor1) {
        this.valor1 = valor1;
    }

    public Map<Integer, DetalleCuentas> getMapDetalleCuentas() {
        return mapDetalleCuentas;
    }

    public void setMapDetalleCuentas(Map<Integer, DetalleCuentas> mapDetalleCuentas) {
        this.mapDetalleCuentas = mapDetalleCuentas;
    }

    public int getCodigoPresupuesto() {
        return codigoPresupuesto;
    }

    public void setCodigoPresupuesto(int codigoPresupuesto) {
        this.codigoPresupuesto = codigoPresupuesto;
    }

    public int getCodigoCuenta() {
        return codigoCuenta;
    }

    public void setCodigoCuenta(int codigoCuenta) {
        this.codigoCuenta = codigoCuenta;
    }

    public BigDecimal getPresupuestoAprobado() {
        return presupuestoAprobado;
    }

    public void setPresupuestoAprobado(BigDecimal presupuestoAprobado) {
        this.presupuestoAprobado = presupuestoAprobado;
    }

    public BigDecimal getPresupuestoComprometido() {
        return presupuestoComprometido;
    }

    public void setPresupuestoComprometido(BigDecimal presupuestoComprometido) {
        this.presupuestoComprometido = presupuestoComprometido;
    }

    public BigDecimal getEjecutadoContable() {
        return ejecutadoContable;
    }

    public void setEjecutadoContable(BigDecimal ejecutadoContable) {
        this.ejecutadoContable = ejecutadoContable;
    }

    public BigDecimal getEjecutadoPagado() {
        return ejecutadoPagado;
    }

    public void setEjecutadoPagado(BigDecimal ejecutadoPagado) {
        this.ejecutadoPagado = ejecutadoPagado;
    }

    public BigDecimal getPresupuestoReal() {
        return presupuestoReal;
    }

    public void setPresupuestoReal(BigDecimal presupuestoReal) {
        this.presupuestoReal = presupuestoReal;
    }

    public BigDecimal getPresupuestoReprogramar() {
        return presupuestoReprogramar;
    }

    public void setPresupuestoReprogramar(BigDecimal presupuestoReprogramar) {
        this.presupuestoReprogramar = presupuestoReprogramar;
    }

    public List<ReprogramarPreBean> getListAllDetalleCuentas() {
        return listAllDetalleCuentas;
    }

    public void setListAllDetalleCuentas(List<ReprogramarPreBean> listAllDetalleCuentas) {
        this.listAllDetalleCuentas = listAllDetalleCuentas;
    }

    public BigDecimal getValor2() {
        return valor2;
    }

    public void setValor2(BigDecimal valor2) {
        this.valor2 = valor2;
    }

    public List<DetalleCuentas> getSelectCuentas() {
        return selectCuentas;
    }

    public void setSelectCuentas(List<DetalleCuentas> selectCuentas) {
        this.selectCuentas = selectCuentas;
    }

    public UIData getReproDataTable() {
        return reproDataTable;
    }

    public void setReproDataTable(UIData reproDataTable) {
        this.reproDataTable = reproDataTable;
    }

    public BigDecimal getCargo() {
        return cargo;
    }

    public void setCargo(BigDecimal cargo) {
        this.cargo = cargo;
    }

    public BigDecimal getAbono() {
        return abono;
    }

    public void setAbono(BigDecimal abono) {
        this.abono = abono;
    }

    public UIData getDataTable() {
        return DataTable;
    }

    public void setDataTable(UIData DataTable) {
        this.DataTable = DataTable;
    }

}
