/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.LoginBO;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author alberto
 */

@ManagedBean
@RequestScoped
public class BeanLogin implements Serializable{

    private String usuario;
    private String clave;
    private String url;
    private Boolean status;
    
    @ManagedProperty (name = "loginBO" , value="#{loginBO}")
    private LoginBO loginBO;
    
    /**
     * Creates a new instance of BeanLogin
     */
    public BeanLogin() {
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getUrl() {
        return url;
    }
   
    public void setUrl(String url) {
        this.url = url;
    }
    
    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public LoginBO getLoginBO() {
        return loginBO;
    }

    public void setLoginBO(LoginBO loginBO) {
        this.loginBO = loginBO;
    }    
    
    public String validarLogin(){
        
        String remoteAddr = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr();       
        HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);

        loginBO.validaLogin(this);
        if(this.getStatus()){    
            httpSession.setAttribute("usuarioLogeado", this.usuario);
            loginBO.bitacora(usuario,remoteAddr , 1);
            return getUrl();
        }else{
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Usuario o Contraseña Incorrecto","Vualva a Digitarlo") );
            httpSession.invalidate();
            loginBO.bitacora(usuario,remoteAddr , 2);   
            this.usuario = null;
            this.clave = null;
            return "index.xhtml";
        }
    }
    
}
