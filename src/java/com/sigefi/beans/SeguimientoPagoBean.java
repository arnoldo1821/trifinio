/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.SeguimientoPagoBO;
import com.sigefi.entity.EstadoRequisicion;
import com.sigefi.entity.RequerimientoPagos;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class SeguimientoPagoBean implements Serializable{
    
    @ManagedProperty(value = "#{seguimientoPagoBO}")
    private SeguimientoPagoBO seguimientoPagoBO;
    
    private double opacidad1;
    private double opacidad2;
    private double opacidad3;
    private double opacidad4;
    private double opacidad5;
    private double opacidad6;
    private double opacidad7;
    private double opacidad8;
    
    private double opacidadAux1;
    private double opacidadAux2;
    private double opacidadAux3;
    private double opacidadAux4;
    private double opacidadAux5;
    private double opacidadAux6;
    private double opacidadAux7;
    private double opacidadAux8;
    
    private List<RequerimientoPagos> listReqUsuario = new ArrayList<>();
    private Map<Integer,RequerimientoPagos> mapReqUsuario = new HashMap<>();
    private String usuarioLogeado;
    private int reqSelec;
    
    public SeguimientoPagoBean() {
       
        opacidad1 = 0.3;
        opacidad2 = 0.3;
        opacidad3 = 0.3;
        opacidad4 = 0.3;
        opacidad5 = 0.3;
        opacidad6 = 0.3;
        opacidad7 = 0.3;
        opacidad8 = 0.3;
        
        opacidadAux1 = 0;
        opacidadAux2 = 0;
        opacidadAux3 = 0;
        opacidadAux4 = 0;
        opacidadAux5 = 0;
        opacidadAux6 = 0;
        opacidadAux7 = 0;
        opacidadAux8 = 0;
        
        SessionBean referenciaBeanSession = (SessionBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("sessionBean");
        this.usuarioLogeado = referenciaBeanSession.getNombreUsuario();
        
    }
    
    @PostConstruct
    public void main(){
        List<RequerimientoPagos> lisReqP = seguimientoPagoBO.listReqUsuario(usuarioLogeado);
        if(lisReqP != null){
            setListReqUsuario(lisReqP);
            for(RequerimientoPagos obj: lisReqP){
                mapReqUsuario.put(obj.getIncrReqPk(), obj);
            }
        }
    }
    
    public void requerimientoSeleccionado(){
        List<EstadoRequisicion> listReq = new ArrayList<>();
        listReq.addAll(mapReqUsuario.get(this.reqSelec).getEstadoRequisicions());
//        int estado = -1;
        for(EstadoRequisicion est: listReq){
            if(est.getControl()){
                estadoSeguimiento(est.getCodigoEstado()); 
            }
        }
//        estadoSeguimiento(listReq.get((listReq.size()-1)).getCodigoEstado());
    }
    
    public void estadoSeguimiento(int estaReq){
        
        if(estaReq > -1){
            switch(estaReq){
                case 0:
                    opacidad1 = 0.3;
                    opacidad2 = 0.3;
                    opacidad3 = 0.3;
                    opacidad4 = 0.3;
                    opacidad5 = 0.3;
                    opacidad6 = 0.3;
                    opacidad7 = 0.3;
                    opacidad8 = 0.3;
                    opacidadAux1 = 0;
                    opacidadAux2 = 0;
                    opacidadAux3 = 0;
                    opacidadAux4 = 0;
                    opacidadAux5 = 0;
                    opacidadAux6 = 0;
                    opacidadAux7 = 0;
                    opacidadAux8 = 0;
                break;
                case 1:
                    opacidad1 = 1;
                    opacidad2 = 0.3;
                    opacidad3 = 0.3;
                    opacidad4 = 0.3;
                    opacidad5 = 0.3;
                    opacidad6 = 0.3;
                    opacidad7 = 0.3;
                    opacidad8 = 0.3;
                    opacidadAux1 = 1;
                    opacidadAux2 = 0;
                    opacidadAux3 = 0;
                    opacidadAux4 = 0;
                    opacidadAux5 = 0;
                    opacidadAux6 = 0;
                    opacidadAux7 = 0;
                    opacidadAux8 = 0;
                break;
                case 2:
                    opacidad1 = 0.3;
                    opacidad2 = 1;
                    opacidad3 = 0.3;
                    opacidad4 = 0.3;
                    opacidad5 = 0.3;
                    opacidad6 = 0.3;
                    opacidad7 = 0.3;
                    opacidad8 = 0.3;
                    opacidadAux1 = 0;
                    opacidadAux2 = 1;
                    opacidadAux3 = 0;
                    opacidadAux4 = 0;
                    opacidadAux5 = 0;
                    opacidadAux6 = 0;
                    opacidadAux7 = 0;
                    opacidadAux8 = 0;
                break;
                case 3:
                    opacidad1 = 0.3;
                    opacidad2 = 0.3;
                    opacidad3 = 1;
                    opacidad4 = 0.3;
                    opacidad5 = 0.3;
                    opacidad6 = 0.3;
                    opacidad7 = 0.3;
                    opacidad8 = 0.3;
                    opacidadAux1 = 0;
                    opacidadAux2 = 0;
                    opacidadAux3 = 1;
                    opacidadAux4 = 0;
                    opacidadAux5 = 0;
                    opacidadAux6 = 0;
                    opacidadAux7 = 0;
                    opacidadAux8 = 0;
                break;
                case 4:
                    opacidad1 = 0.3;
                    opacidad2 = 0.3;
                    opacidad3 = 0.3;
                    opacidad4 = 1;
                    opacidad5 = 0.3;
                    opacidad6 = 0.3;
                    opacidad7 = 0.3;
                    opacidad8 = 0.3;
                    opacidadAux1 = 0;
                    opacidadAux2 = 0;
                    opacidadAux3 = 0;
                    opacidadAux4 = 1;
                    opacidadAux5 = 0;
                    opacidadAux6 = 0;
                    opacidadAux7 = 0;
                    opacidadAux8 = 0;
                break;
                case 5:
                    opacidad1 = 0.3;
                    opacidad2 = 0.3;
                    opacidad3 = 0.3;
                    opacidad4 = 0.3;
                    opacidad5 = 1;
                    opacidad6 = 0.3;
                    opacidad7 = 0.3;
                    opacidad8 = 0.3;
                    opacidadAux1 = 0;
                    opacidadAux2 = 0;
                    opacidadAux3 = 0;
                    opacidadAux4 = 0;
                    opacidadAux5 = 1;
                    opacidadAux6 = 0;
                    opacidadAux7 = 0;
                    opacidadAux8 = 0;
                break;
                case 6:
                    opacidad1 = 0.3;
                    opacidad2 = 0.3;
                    opacidad3 = 0.3;
                    opacidad4 = 0.3;
                    opacidad5 = 0.3;
                    opacidad6 = 1;
                    opacidad7 = 0.3;
                    opacidad8 = 0.3;
                    opacidadAux1 = 0;
                    opacidadAux2 = 0;
                    opacidadAux3 = 0;
                    opacidadAux4 = 0;
                    opacidadAux5 = 0;
                    opacidadAux6 = 1;
                    opacidadAux7 = 0;
                    opacidadAux8 = 0;
                break;
                case 7:
                    opacidad1 = 0.3;
                    opacidad2 = 0.3;
                    opacidad3 = 0.3;
                    opacidad4 = 0.3;
                    opacidad5 = 0.3;
                    opacidad6 = 0.3;
                    opacidad7 = 1;
                    opacidad8 = 0.3;
                    opacidadAux1 = 0;
                    opacidadAux2 = 0;
                    opacidadAux3 = 0;
                    opacidadAux4 = 0;
                    opacidadAux5 = 0;
                    opacidadAux6 = 0;
                    opacidadAux7 = 1;
                    opacidadAux8 = 0;
                break;
                case 8:
                    opacidad1 = 0.3;
                    opacidad2 = 0.3;
                    opacidad3 = 0.3;
                    opacidad4 = 0.3;
                    opacidad5 = 0.3;
                    opacidad6 = 0.3;
                    opacidad7 = 0.3;
                    opacidad8 = 1;
                    opacidadAux1 = 0;
                    opacidadAux2 = 0;
                    opacidadAux3 = 0;
                    opacidadAux4 = 0;
                    opacidadAux5 = 0;
                    opacidadAux6 = 0;
                    opacidadAux7 = 0;
                    opacidadAux8 = 1;
                break;
                
            }
        }
    }
    
    
    public SeguimientoPagoBO getSeguimientoPagoBO() {
        return seguimientoPagoBO;
    }

    public void setSeguimientoPagoBO(SeguimientoPagoBO seguimientoPagoBO) {
        this.seguimientoPagoBO = seguimientoPagoBO;
    }

    public double getOpacidad1() {
        return opacidad1;
    }

    public void setOpacidad1(double opacidad1) {
        this.opacidad1 = opacidad1;
    }

    public double getOpacidad2() {
        return opacidad2;
    }

    public void setOpacidad2(double opacidad2) {
        this.opacidad2 = opacidad2;
    }

    public double getOpacidad3() {
        return opacidad3;
    }

    public void setOpacidad3(double opacidad3) {
        this.opacidad3 = opacidad3;
    }

    public double getOpacidad4() {
        return opacidad4;
    }

    public void setOpacidad4(double opacidad4) {
        this.opacidad4 = opacidad4;
    }

    public double getOpacidad5() {
        return opacidad5;
    }

    public void setOpacidad5(double opacidad5) {
        this.opacidad5 = opacidad5;
    }

    public double getOpacidad6() {
        return opacidad6;
    }

    public void setOpacidad6(double opacidad6) {
        this.opacidad6 = opacidad6;
    }

    public double getOpacidad7() {
        return opacidad7;
    }

    public void setOpacidad7(double opacidad7) {
        this.opacidad7 = opacidad7;
    }

    public double getOpacidad8() {
        return opacidad8;
    }

    public void setOpacidad8(double opacidad8) {
        this.opacidad8 = opacidad8;
    }

    public double getOpacidadAux1() {
        return opacidadAux1;
    }

    public void setOpacidadAux1(double opacidadAux1) {
        this.opacidadAux1 = opacidadAux1;
    }

    public double getOpacidadAux2() {
        return opacidadAux2;
    }

    public void setOpacidadAux2(double opacidadAux2) {
        this.opacidadAux2 = opacidadAux2;
    }

    public double getOpacidadAux3() {
        return opacidadAux3;
    }

    public void setOpacidadAux3(double opacidadAux3) {
        this.opacidadAux3 = opacidadAux3;
    }

    public double getOpacidadAux4() {
        return opacidadAux4;
    }

    public void setOpacidadAux4(double opacidadAux4) {
        this.opacidadAux4 = opacidadAux4;
    }

    public double getOpacidadAux5() {
        return opacidadAux5;
    }

    public void setOpacidadAux5(double opacidadAux5) {
        this.opacidadAux5 = opacidadAux5;
    }

    public double getOpacidadAux6() {
        return opacidadAux6;
    }

    public void setOpacidadAux6(double opacidadAux6) {
        this.opacidadAux6 = opacidadAux6;
    }

    public double getOpacidadAux7() {
        return opacidadAux7;
    }

    public void setOpacidadAux7(double opacidadAux7) {
        this.opacidadAux7 = opacidadAux7;
    }

    public double getOpacidadAux8() {
        return opacidadAux8;
    }

    public void setOpacidadAux8(double opacidadAux8) {
        this.opacidadAux8 = opacidadAux8;
    }

    public String getUsuarioLogeado() {
        return usuarioLogeado;
    }

    public void setUsuarioLogeado(String usuarioLogeado) {
        this.usuarioLogeado = usuarioLogeado;
    }   

    public List<RequerimientoPagos> getListReqUsuario() {
        return listReqUsuario;
    }

    public void setListReqUsuario(List<RequerimientoPagos> listReqUsuario) {
        this.listReqUsuario = listReqUsuario;
    }

    public int getReqSelec() {
        return reqSelec;
    }

    public void setReqSelec(int reqSelec) {
        this.reqSelec = reqSelec;
    }

    public Map<Integer, RequerimientoPagos> getMapReqUsuario() {
        return mapReqUsuario;
    }

    public void setMapReqUsuario(Map<Integer, RequerimientoPagos> mapReqUsuario) {
        this.mapReqUsuario = mapReqUsuario;
    }
    
    
}
