/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.CambioEmpresaBO;
import com.sigefi.entity.Empresa;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class CambioEmpresaBean implements Serializable{

    @ManagedProperty(value = "#{cambioEmpresaBO}")
    private CambioEmpresaBO cambioEmpresaBO;
    
    private short unidadesEjecutoras;
    private int anio;
    private boolean estado1;
    private Empresa empSelecc;
    
    public CambioEmpresaBean() {
        empSelecc = new Empresa();
    }
    
    public void consultarEmpresa(){
        Empresa emp = cambioEmpresaBO.consultarEmpresa(unidadesEjecutoras, anio);
        if(emp == null){
            estado1  = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Empresa no encontrada", ""));
        }else{
            estado1 = true;
            empSelecc = emp;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Empresa Seleccionada correctamente", ""));
        }
    }

    public CambioEmpresaBO getCambioEmpresaBO() {
        return cambioEmpresaBO;
    }

    public void setCambioEmpresaBO(CambioEmpresaBO cambioEmpresaBO) {
        this.cambioEmpresaBO = cambioEmpresaBO;
    }

    public short getUnidadesEjecutoras() {
        return unidadesEjecutoras;
    }

    public void setUnidadesEjecutoras(short unidadesEjecutoras) {
        this.unidadesEjecutoras = unidadesEjecutoras;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public boolean isEstado1() {
        return estado1;
    }

    public void setEstado1(boolean estado1) {
        this.estado1 = estado1;
    }

    public Empresa getEmpSelecc() {
        return empSelecc;
    }

    public void setEmpSelecc(Empresa empSelecc) {
        this.empSelecc = empSelecc;
    }
    
    
}
