/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.ArchivosRequerimientoBO;
import com.sigefi.clases.Email;
import com.sigefi.clases.SubirArchivoServer;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.DocumentosRequerimientos;
import com.sigefi.entity.RequerimientoPagos;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class ArchivosRequerimientoBean implements Serializable{

    @ManagedProperty(value = "#{archivosRequerimientoBO}")
    private ArchivosRequerimientoBO archivosRequerimientoBO;
    
    private String usuario;
    private List<RequerimientoPagos> listAllReqUsuario;
    private List<DetalleRequerimientoPago> detalleAllRequerimiento;
    private List<DetalleRequerimientoPago> filtrodetalleRequerimiento;
    private String nomDoc;
    private String urlDoc;
    private int incre_reque;
    private String tipoDocumento;
    
    private List<DocumentosRequerimientos> listAllReqDocUsuario;
               
    private short unidadEjecutoraIdUsuarioLogeado;
    private List<String> listCorreos = new ArrayList<>();
    
    public ArchivosRequerimientoBean() {
        SessionBean referenciaBeanSession
                = (SessionBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("sessionBean");

        this.usuario = referenciaBeanSession.getUsu(); 
        this.unidadEjecutoraIdUsuarioLogeado = referenciaBeanSession.getIdUnidadEjecutora();
    }
    
    public void llenarTabla(){
        setListAllReqUsuario(archivosRequerimientoBO.listAllReqUsuario(usuario));
        setDetalleAllRequerimiento(archivosRequerimientoBO.listAllDetalleRequerimiento(usuario));
    }
    @PostConstruct
    public void main(){
        llenarTabla();
        List<String> listC = archivosRequerimientoBO.listCorreoUsuarios(unidadEjecutoraIdUsuarioLogeado);
        if(listC != null){
            setListCorreos(listC);
        }
    }
    
    public void requerimientoSeleccionado(RequerimientoPagos reque){
        this.nomDoc = reque.getCodigoRequerimiento()+"_"+reque.getAnioPresupuesto()+"_"+reque.getUnidadesEjecutoras().getCodigo()+"_"+reque.getUsuario();
        this.incre_reque = reque.getIncrReqPk();
        setListAllReqDocUsuario(archivosRequerimientoBO.listAllDocReqUsuario(this.getIncre_reque()));
        RequestContext.getCurrentInstance().update("fromAgregarDoc");
    }
    
    public void comprobarTipoDoc(){
        if(this.tipoDocumento != null){
            if(!this.tipoDocumento.isEmpty()){
                this.nomDoc = this.nomDoc+"_"+getTipoDocumento();
                RequestContext.getCurrentInstance().execute("PF('subirArchivo').show();");
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"No a seleccionado el tipo de documento",""));
            }
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"No a seleccionado el tipo de documento",""));
        }
    }
    
    public void subirArchivo(FileUploadEvent event) {
        if (archivosRequerimientoBO.conprobacionElemento(this) == false) {
            SubirArchivoServer subir = new SubirArchivoServer();
            this.urlDoc = subir.SubirArchivo(event, "/SigefiDocumentos/DocumentosRequemientoPago/", nomDoc);
            if (this.urlDoc != null) {
                if (!this.urlDoc.isEmpty()) {
                    if (archivosRequerimientoBO.insertDocReque(this)) {
                        tipoDocumento = null;
                        setListAllReqDocUsuario(archivosRequerimientoBO.listAllDocReqUsuario(this.getIncre_reque()));
                        if(getListAllReqDocUsuario() != null){
                            if(getListAllReqDocUsuario().size()>3){
                                if(getListCorreos() != null){
                                    if(getListCorreos().size()>0){
                                        for(String objs: getListCorreos()){
                                            Email email = new Email(objs, "Espera de Autorizacion", "Espera de Autorizacion");
                                            email.sendMail();
                                        }
                                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Correos Enviados Exitosamentes", ""));
                                    }
                                }
                                
                                
                            }
                        }
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Archivo Agregado Correctamente", ""));
                    }

                }
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ese tipo de documento ya esta agregado", ""));
        }
        RequestContext.getCurrentInstance().execute("PF('subirArchivo').hide();");
        RequestContext.getCurrentInstance().update("fromAgregarDoc");
    }
    
    public void enviar(DetalleRequerimientoPago obj){
        RequerimientoPagos obj1;
        obj1 = obj.getRequerimientoPagos();
        if(archivosRequerimientoBO.enviarRequerimiento(obj1)){
            llenarTabla();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Requerimiento enviado correctamente", ""));
        }
    }
    
    public ArchivosRequerimientoBO getArchivosRequerimientoBO() {
        return archivosRequerimientoBO;
    }

    public void setArchivosRequerimientoBO(ArchivosRequerimientoBO archivosRequerimientoBO) {
        this.archivosRequerimientoBO = archivosRequerimientoBO;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public List<RequerimientoPagos> getListAllReqUsuario() {
        return listAllReqUsuario;
    }

    public void setListAllReqUsuario(List<RequerimientoPagos> listAllReqUsuario) {
        this.listAllReqUsuario = listAllReqUsuario;
    }

    public String getNomDoc() {
        return nomDoc;
    }

    public void setNomDoc(String nomDoc) {
        this.nomDoc = nomDoc;
    }

    public String getUrlDoc() {
        return urlDoc;
    }

    public void setUrlDoc(String urlDoc) {
        this.urlDoc = urlDoc;
    }

    public int getIncre_reque() {
        return incre_reque;
    }

    public void setIncre_reque(int incre_reque) {
        this.incre_reque = incre_reque;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public List<DocumentosRequerimientos> getListAllReqDocUsuario() {
        return listAllReqDocUsuario;
    }

    public void setListAllReqDocUsuario(List<DocumentosRequerimientos> listAllReqDocUsuario) {
        this.listAllReqDocUsuario = listAllReqDocUsuario;
    }

    public short getUnidadEjecutoraIdUsuarioLogeado() {
        return unidadEjecutoraIdUsuarioLogeado;
    }

    public void setUnidadEjecutoraIdUsuarioLogeado(short unidadEjecutoraIdUsuarioLogeado) {
        this.unidadEjecutoraIdUsuarioLogeado = unidadEjecutoraIdUsuarioLogeado;
    }

    public List<String> getListCorreos() {
        return listCorreos;
    }

    public void setListCorreos(List<String> listCorreos) {
        this.listCorreos = listCorreos;
    }

    public List<DetalleRequerimientoPago> getDetalleAllRequerimiento() {
        return detalleAllRequerimiento;
    }

    public void setDetalleAllRequerimiento(List<DetalleRequerimientoPago> detalleAllRequerimiento) {
        this.detalleAllRequerimiento = detalleAllRequerimiento;
    }

    public List<DetalleRequerimientoPago> getFiltrodetalleRequerimiento() {
        return filtrodetalleRequerimiento;
    }

    public void setFiltrodetalleRequerimiento(List<DetalleRequerimientoPago> filtrodetalleRequerimiento) {
        this.filtrodetalleRequerimiento = filtrodetalleRequerimiento;
    }
    
}
