/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.CuentasBancariasBO;
import com.sigefi.bo.SeguimientoCuentasBancariasBO;
import com.sigefi.clases.ConvertirNumerosLetras;
import com.sigefi.clases.FechaSistema;
import com.sigefi.entity.CuentasBancarias;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.SegumientoCuentaBancaria;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Elmer
 */
@ManagedBean
@ViewScoped
public class SeguimientoCuentasBancariasBean implements Serializable {

    @ManagedProperty(value = "#{seguimientoCuentasBancariasBO}")
    private SeguimientoCuentasBancariasBO seguimientoCuentasBancariasBO;
    @ManagedProperty(value = "#{cuentasBancariasBO}")
    private CuentasBancariasBO cuentasBancariasBO;

    private String codigoCuenta;
    private String codigoAux;
    private String codigoRequerimientoPago;
    private String CuentasBancarias;
    private String nombreCuenta;
    private String codigoComprobante;
    private String codigoDocumento;
    private String documento;
    private String pongaseAlaCuentade;
    private BigDecimal valor;
    private String valorLetras;
    private String concepto;
    private Date fechaIngreso;
    private Date fechaCreacion;
    private Date fechaInicio;
    private Date fechaFin;
    private Boolean chequeContinuo;
    private Boolean cajaChica;
    private String tipoCargo;
    private BigDecimal saldoAnterior;
    private BigDecimal nuevoSaldo;
    private BigDecimal saldoInicial;
    private List<CuentasBancarias> listCuentasBanAll;
    private CuentasBancarias cuenta;
    private DetalleRequerimientoPago detalle;
    private CuentasBancarias cuentaBanSelecc;
    private List<SegumientoCuentaBancaria> listAllcuentaSeleccionada;

    private Map<String, String> mapaCuentasBancarias;
    private Map<String, String> mapaDetalleRequerimiento;

    public SeguimientoCuentasBancariasBean() {
        cuentaBanSelecc = new CuentasBancarias();
        FechaSistema hoy = new FechaSistema();
        fechaCreacion = hoy.getFecha();
        this.fechaIngreso = hoy.getFecha();
        cajaChica = false;
        chequeContinuo = true;
    }

    @PostConstruct
    public void main() {
        List<CuentasBancarias> listCuentasBancarias = cuentasBancariasBO.listAllCuentas();
        if (listCuentasBancarias != null) {
            mapaCuentasBancarias = new HashMap();
            for (CuentasBancarias obj : listCuentasBancarias) {
                mapaCuentasBancarias.put(obj.getCodigoCuenta(), obj.getCodigoCuenta());
            }
        }

        List<DetalleRequerimientoPago> listDetalleRequerimiento = cuentasBancariasBO.listRequerimientoPago();
        if (listDetalleRequerimiento != null) {
            mapaDetalleRequerimiento = new HashMap();
            for (DetalleRequerimientoPago obj : listDetalleRequerimiento) {
                mapaDetalleRequerimiento.put(obj.getRequerimientoPagos().getCodigoRequerimiento(), obj.getRequerimientoPagos().getCodigoRequerimiento());
            }
        }

        llenarTabla();
    }

    public void llenarTabla() {
        setListCuentasBanAll(cuentasBancariasBO.listAllCuentas());
    }

    public void llenarLista(SeguimientoCuentasBancariasBean cuenta) {
        List<SegumientoCuentaBancaria> listMovimientos = seguimientoCuentasBancariasBO.listAllSeguimientoCuentas(cuenta.codigoAux, cuenta.fechaInicio, cuenta.fechaFin);
        if (listMovimientos != null) {
            setListAllCuentaSeleccionada(listMovimientos);
        }
    }

    public void pasarCodigoRequerimiento(SeguimientoCuentasBancariasBean seguimiento) {
        this.detalle = cuentasBancariasBO.requerimientoElemento(this);
        this.cuenta = cuentasBancariasBO.cuentaElemento(this);
        if (cuenta == null) {
            if (detalle != null) {
                this.documento = detalle.getClaseDocumento();
                this.codigoDocumento = detalle.getNumeroDocumento();
                this.pongaseAlaCuentade = detalle.getNombreProveedor();
                this.valor = detalle.getMonto();
                String letras = ConvertirNumerosLetras.convertNumberToLetter(this.valor.toString());
                this.valorLetras = letras;
                this.concepto = detalle.getConcepto();
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El codigo del requerimiento es invalido", ""));
            }
        } else {
            if (detalle != null) {
                this.documento = detalle.getClaseDocumento();
                this.codigoDocumento = detalle.getNumeroDocumento();
                this.pongaseAlaCuentade = detalle.getNombreProveedor();
                this.valor = detalle.getMonto();
                String letras = ConvertirNumerosLetras.convertNumberToLetter(this.valor.toString());
                this.valorLetras = letras;
                this.concepto = detalle.getConcepto();
                this.saldoAnterior = cuenta.getSaldo();
                this.nuevoSaldo = cuenta.getSaldo().subtract(this.valor);
                
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El codigo del requerimiento es invalido", ""));
            }
        }

    }

    public void pasarCodigoCuenta2(SeguimientoCuentasBancariasBean seguimiento) {
        this.codigoAux = seguimiento.getCodigoCuenta();
        this.cuenta = cuentasBancariasBO.cuentaElemento(this);

        if (cuenta != null) {
            this.saldoAnterior = cuenta.getSaldo();
            this.nombreCuenta = cuenta.getNombreCuenta();
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El codigo de la cuenta es invalido", ""));
        }
    }

    public void calculoSaldo(SeguimientoCuentasBancariasBean seguimiento) {
        this.codigoAux = seguimiento.getCodigoCuenta();
        this.detalle = cuentasBancariasBO.requerimientoElemento(this);
        if (detalle == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Por favor seleccione primero el codigo del requerimiento", ""));
            limpiar();
            RequestContext.getCurrentInstance().update("formCheques");
        } else {
            this.cuenta = cuentasBancariasBO.cuentaElemento(this);
            if (cuenta != null) {
                this.nombreCuenta = cuenta.getNombreCuenta();
                this.saldoAnterior = cuenta.getSaldo();
                this.nuevoSaldo = cuenta.getSaldo().subtract(this.valor);
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El codigo de la cuenta es invalido", ""));
            }
        }
    }

    public void pasarCodigoCuenta(SeguimientoCuentasBancariasBean seguimiento) {
        this.codigoAux = seguimiento.getCodigoCuenta();
        this.cuenta = cuentasBancariasBO.cuentaElemento(this);
        if (cuenta != null) {
            this.nombreCuenta = cuenta.getNombreCuenta();
            this.saldoAnterior = cuenta.getSaldo();
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El codigo de la cuenta es invalido", ""));
        }
    }

    public void SeguimientoCuenta(SeguimientoCuentasBancariasBean seguimiento) {
        this.codigoAux = seguimiento.getCodigoCuenta();
        List<SegumientoCuentaBancaria> listMovimientos = seguimientoCuentasBancariasBO.listAllSeguimientoCuentas(codigoAux, this.fechaInicio, this.fechaFin);
        if (listMovimientos != null) {
            limpiar2();
            setListAllCuentaSeleccionada(listMovimientos);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "La cuenta no tiene movimientos registrados ", ""));
            limpiar2();
        }
    }

    public void conversion(SeguimientoCuentasBancariasBean seguimiento) {
        BigDecimal cantidad = seguimiento.getValor();
        String valorAux = cantidad.toString();
        String letras = ConvertirNumerosLetras.convertNumberToLetter(valorAux);
        seguimiento.setValorLetras(letras);
        this.cuenta = cuentasBancariasBO.cuentaElemento(this);
        if (cuenta != null) {
            this.saldoAnterior = cuenta.getSaldo();
            this.nuevoSaldo = cuenta.getSaldo().subtract(this.valor);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "LA CUENTA NO TIENE SALDO INICIAL ASIGNADO", ""));
        }
    }

    public void conversion2(SeguimientoCuentasBancariasBean seguimiento) {
        BigDecimal cantidad = seguimiento.getValor();
        String valorAux = cantidad.toString();
        String letras = ConvertirNumerosLetras.convertNumberToLetter(valorAux);
        seguimiento.setValorLetras(letras);
        this.cuenta = cuentasBancariasBO.cuentaElemento(this);
        if (cuenta != null) {
            this.saldoAnterior = cuenta.getSaldo();
            this.nuevoSaldo = cuenta.getSaldo().add(this.valor);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "LA CUENTA NO TIENE SALDO INICIAL ASIGNADO", ""));
        }
    }

    public void insertMovimientoAbono() {
        if (seguimientoCuentasBancariasBO.insertMovimientoCuenta(this)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Movimiento agregado correctamente ", ""));
            RequestContext.getCurrentInstance().update("formNotasAbono");
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error no se agrego el movimiento", ""));
            RequestContext.getCurrentInstance().update("formNotasAbono");
        }
        if (this.cuenta.getCodigoCuenta() != null) {
            CuentasBancarias aux = new CuentasBancarias();
            aux.setCodigoCuenta(this.cuenta.getCodigoCuenta());
            aux.setNombreBanco(this.cuenta.getNombreBanco());
            this.cuenta.setSaldo(this.nuevoSaldo);
            if (cuentasBancariasBO.updateCuentaBancaria(this.cuenta)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cuenta Bancaria modificada correctamente ", ""));
                llenarTabla();
                limpiar();
            }
        }
    }

    public void insertMovimientoCargo() {
        if (seguimientoCuentasBancariasBO.insertMovimeintoCargo(this)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Movimiento agregado correctamente ", ""));
            RequestContext.getCurrentInstance().update("formNotasAbono");
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error no se agrego el movimiento", ""));
            RequestContext.getCurrentInstance().update("formNotasAbono");
        }
        if (this.cuenta.getCodigoCuenta() != null) {
            CuentasBancarias aux = new CuentasBancarias();
            aux.setCodigoCuenta(this.cuenta.getCodigoCuenta());
            aux.setNombreBanco(this.cuenta.getNombreBanco());
            this.cuenta.setSaldo(this.nuevoSaldo);
            if (cuentasBancariasBO.updateCuentaBancaria(this.cuenta)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cuenta Bancaria modificada correctamente ", ""));
                llenarTabla();
                limpiar();
            }
        }
    }

    public void insertMovimientoCheque() {
        if (seguimientoCuentasBancariasBO.insertMovimientoCheque(this)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Movimiento agregado correctamente ", ""));
            RequestContext.getCurrentInstance().update("formCheque");
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error no se agrego el movimiento", ""));
            RequestContext.getCurrentInstance().update("formCheque");
        }
        if (this.cuenta.getCodigoCuenta() != null) {
            CuentasBancarias aux = new CuentasBancarias();
            aux.setCodigoCuenta(this.cuenta.getCodigoCuenta());
            aux.setNombreBanco(this.cuenta.getNombreBanco());
            this.cuenta.setSaldo(this.nuevoSaldo);
            if (cuentasBancariasBO.updateCuentaBancaria(this.cuenta)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cuenta Bancaria modificada correctamente ", ""));
                llenarTabla();
                limpiar();
            }
        }
    }
    
    public void insertMovimientoEmisionCheque() {
        if (seguimientoCuentasBancariasBO.insertMovimientoEmisionCheque(this)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Movimiento agregado correctamente ", ""));
            RequestContext.getCurrentInstance().update("formCheque");
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error no se agrego el movimiento", ""));
            RequestContext.getCurrentInstance().update("formCheque");
        }
        if (this.cuenta.getCodigoCuenta() != null) {
            CuentasBancarias aux = new CuentasBancarias();
            aux.setCodigoCuenta(this.cuenta.getCodigoCuenta());
            aux.setNombreBanco(this.cuenta.getNombreBanco());
            this.cuenta.setSaldo(this.nuevoSaldo);
            if (cuentasBancariasBO.updateCuentaBancaria(this.cuenta)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cuenta Bancaria modificada correctamente ", ""));
                llenarTabla();
                limpiar();
            }
        }
    }

    public void insertMovimientoNotaTesoreria() {
        if (seguimientoCuentasBancariasBO.insertMovimientoNotaTesoreria(this)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Movimiento agregado correctamente ", ""));
            RequestContext.getCurrentInstance().update("formNotasTesoreria");
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error no se agrego el movimiento", ""));
            RequestContext.getCurrentInstance().update("formNotasTesoreria");
        }
        if (this.cuenta.getCodigoCuenta() != null) {
            CuentasBancarias aux = new CuentasBancarias();
            aux.setCodigoCuenta(this.cuenta.getCodigoCuenta());
            aux.setNombreBanco(this.cuenta.getNombreBanco());
            this.cuenta.setSaldo(this.nuevoSaldo);
            if (cuentasBancariasBO.updateCuentaBancaria(this.cuenta)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cuenta Bancaria modificada correctamente ", ""));
                llenarTabla();
                limpiar();
            }
        }
    }

    public void deleteCuentaBan(CuentasBancarias obj) {
        if (cuentasBancariasBO.deleteCuentaBancaria(obj)) {
            llenarTabla();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cuenta Bancaria eliminada correctamente ", ""));
        }
    }

    public void cuentaSeleccionada(CuentasBancarias obj) {
        cuentaBanSelecc = new CuentasBancarias();
        this.setCuentaBanSelecc(obj);
        RequestContext.getCurrentInstance().update("fromModifiCuentaBan");
    }

    public void updateCuentaBan() {
        if (cuentaBanSelecc != null) {
            if (cuentaBanSelecc.getCodigoCuenta() != null) {
                if (cuentasBancariasBO.updateCuentaBancaria(this.cuentaBanSelecc)) {
                    llenarTabla();
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cuenta Bancaria modificada correctamente ", ""));
                }
            }
        }
    }

    public void limpiar() {
        codigoCuenta = "";
        codigoRequerimientoPago = "";
        CuentasBancarias = null;
        nombreCuenta = "";
        codigoComprobante = "";
        cajaChica = false;
        pongaseAlaCuentade = "";
        valor = null;
        valorLetras = "";
        concepto = "";
        tipoCargo = null;
        saldoAnterior = null;
        nuevoSaldo = null;
        saldoInicial = null;
        documento = null;
        codigoDocumento = null;
        listAllcuentaSeleccionada = null;
        RequestContext.getCurrentInstance().update("formNotasAbono");
        // RequestContext.getCurrentInstance().update("formSeguimiento");
    }

    public void limpiar2() {
        codigoComprobante = null;
        codigoRequerimientoPago = "";
        pongaseAlaCuentade = "";
        valor = null;
        valorLetras = null;
        concepto = "";
        tipoCargo = null;
        saldoAnterior = null;
        cajaChica = false;
        nuevoSaldo = null;
        saldoInicial = null;
        documento = null;
        codigoDocumento = null;
        listAllcuentaSeleccionada = null;
        RequestContext.getCurrentInstance().update("formNotasAbono");
        // RequestContext.getCurrentInstance().update("formSeguimiento");
    }

    public SeguimientoCuentasBancariasBO getSeguimientoCuentasBancariasBO() {
        return seguimientoCuentasBancariasBO;
    }

    public void setSeguimientoCuentasBancariasBO(SeguimientoCuentasBancariasBO seguimientoCuentasBancariasBO) {
        this.seguimientoCuentasBancariasBO = seguimientoCuentasBancariasBO;
    }

    public String getCodigoCuenta() {
        return codigoCuenta;
    }

    public void setCodigoCuenta(String codigoCuenta) {
        this.codigoCuenta = codigoCuenta;
    }

    public List<CuentasBancarias> getListCuentasBanAll() {
        return listCuentasBanAll;
    }

    public void setListCuentasBanAll(List<CuentasBancarias> listCuentasBanAll) {
        this.listCuentasBanAll = listCuentasBanAll;
    }

    public CuentasBancarias getCuentaBanSelecc() {
        return cuentaBanSelecc;
    }

    public void setCuentaBanSelecc(CuentasBancarias cuentaBanSelecc) {
        this.cuentaBanSelecc = cuentaBanSelecc;
    }

    public String getNombreCuenta() {
        return nombreCuenta;
    }

    public void setNombreCuenta(String nombreCuenta) {
        this.nombreCuenta = nombreCuenta;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getCuentasBancarias() {
        return CuentasBancarias;
    }

    public void setCuentasBancarias(String CuentasBancarias) {
        this.CuentasBancarias = CuentasBancarias;
    }

    public Map<String, String> getMapaCuentasBancarias() {
        return mapaCuentasBancarias;
    }

    public void setMapaCuentasBancarias(Map<String, String> mapaCuentasBancarias) {
        this.mapaCuentasBancarias = mapaCuentasBancarias;
    }

    public BigDecimal getSaldoInicial() {
        return saldoInicial;
    }

    public void setSaldoInicial(BigDecimal saldoInicial) {
        this.saldoInicial = saldoInicial;
    }

    public String getPongaseAlaCuentade() {
        return pongaseAlaCuentade;
    }

    public void setPongaseAlaCuentade(String pongaseAlaCuentade) {
        this.pongaseAlaCuentade = pongaseAlaCuentade;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getValorLetras() {
        return valorLetras;
    }

    public void setValorLetras(String valorLetras) {
        this.valorLetras = valorLetras;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getTipoCargo() {
        return tipoCargo;
    }

    public void setTipoCargo(String tipoCargo) {
        this.tipoCargo = tipoCargo;
    }

    public BigDecimal getSaldoAnterior() {
        return saldoAnterior;
    }

    public void setSaldoAnterior(BigDecimal saldoAnterior) {
        this.saldoAnterior = saldoAnterior;
    }

    public BigDecimal getNuevoSaldo() {
        return nuevoSaldo;
    }

    public void setNuevoSaldo(BigDecimal nuevoSaldo) {
        this.nuevoSaldo = nuevoSaldo;
    }

    public CuentasBancariasBO getCuentasBancariasBO() {
        return cuentasBancariasBO;
    }

    public void setCuentasBancariasBO(CuentasBancariasBO cuentasBancariasBO) {
        this.cuentasBancariasBO = cuentasBancariasBO;
    }

    public String getCodigoComprobante() {
        return codigoComprobante;
    }

    public void setCodigoComprobante(String codigoComprobante) {
        this.codigoComprobante = codigoComprobante;
    }

    public String getCodigoAux() {
        return codigoAux;
    }

    public void setCodigoAux(String codigoAux) {
        this.codigoAux = codigoAux;
    }

    public CuentasBancarias getCuenta() {
        return cuenta;
    }

    public void setCuenta(CuentasBancarias cuenta) {
        this.cuenta = cuenta;
    }

    public String getCodigoDocumento() {
        return codigoDocumento;
    }

    public void setCodigoDocumento(String codigoDocumento) {
        this.codigoDocumento = codigoDocumento;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public List<SegumientoCuentaBancaria> getListAllCuentaSeleccionada() {
        return listAllcuentaSeleccionada;
    }

    public void setListAllCuentaSeleccionada(List<SegumientoCuentaBancaria> listAllcuentaSeleccionada) {
        this.listAllcuentaSeleccionada = listAllcuentaSeleccionada;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public List<SegumientoCuentaBancaria> getListAllcuentaSeleccionada() {
        return listAllcuentaSeleccionada;
    }

    public void setListAllcuentaSeleccionada(List<SegumientoCuentaBancaria> listAllcuentaSeleccionada) {
        this.listAllcuentaSeleccionada = listAllcuentaSeleccionada;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Map<String, String> getMapaDetalleRequerimiento() {
        return mapaDetalleRequerimiento;
    }

    public void setMapaDetalleRequerimiento(Map<String, String> mapaDetalleRequerimiento) {
        this.mapaDetalleRequerimiento = mapaDetalleRequerimiento;
    }

    public String getCodigoRequerimientoPago() {
        return codigoRequerimientoPago;
    }

    public void setCodigoRequerimientoPago(String codigoRequerimientoPago) {
        this.codigoRequerimientoPago = codigoRequerimientoPago;
    }

    public DetalleRequerimientoPago getDetalle() {
        return detalle;
    }

    public void setDetalle(DetalleRequerimientoPago detalle) {
        this.detalle = detalle;
    }

    public Boolean getCajaChica() {
        return cajaChica;
    }

    public void setCajaChica(Boolean cajaChica) {
        this.cajaChica = cajaChica;
    }

    public Boolean getChequeContinuo() {
        return chequeContinuo;
    }

    public void setChequeContinuo(Boolean chequeContinuo) {
        this.chequeContinuo = chequeContinuo;
    }

}
