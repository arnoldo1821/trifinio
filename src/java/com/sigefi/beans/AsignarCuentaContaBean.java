/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.AsignarCuentaContaBO;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class AsignarCuentaContaBean implements Serializable{

    @ManagedProperty(value = "#{asignarCuentaContaBO}")
    private AsignarCuentaContaBO asignarCuentaContaBO;
            
    public AsignarCuentaContaBean() {
    }

    public AsignarCuentaContaBO getAsignarCuentaContaBO() {
        return asignarCuentaContaBO;
    }

    public void setAsignarCuentaContaBO(AsignarCuentaContaBO asignarCuentaContaBO) {
        this.asignarCuentaContaBO = asignarCuentaContaBO;
    }
    
    
}
