/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.CentroCostoBO;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Elmer
 */
@ManagedBean
@ViewScoped
public class CentroCostoBean implements Serializable {

    @ManagedProperty(name = "centroCostoBO", value = "#{centroCostoBO}")
    private CentroCostoBO centroCostoBO;

    private Integer idCentroCosto;
    private String centroCosto;
    private Boolean activa;
    private List<CentroCostoBean> listCentroCosto;
     private List<CentroCostoBean> filtroCentroCosto;
    private CentroCostoBean selectCentro;

    /**
     * Creates a new instance of CentroCostoBean
     */
    public CentroCostoBean() {
    }

    @PostConstruct
    public void main() {
        llenarLista();
    }

    public void llenarLista() {
        List<CentroCostoBean> listCentroC = centroCostoBO.listCentroCostoAll();
        if (listCentroC != null) {
            setListCentroCosto(listCentroC);
        }
    }

    public void limpiar() {
        centroCosto = null;
        RequestContext.getCurrentInstance().update("fromCentroCosto");
        RequestContext.getCurrentInstance().update("form");
    }

    public CentroCostoBO getCentroCostoBO() {
        return centroCostoBO;
    }

    public void insertCentroCosto() {
        if (centroCostoBO.insertCentroCosto(this)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Centro de costo agregado correctamente", ""));
            llenarLista();
            limpiar();
        }
        RequestContext.getCurrentInstance().update("fromCentroCosto");
        RequestContext.getCurrentInstance().update("form");
    }

    public void deleteCentroCosto(CentroCostoBean CentroC) {
        this.idCentroCosto = CentroC.getIdCentroCosto();
        this.centroCosto = CentroC.getCentroCosto();
        this.activa = CentroC.getActiva();
        if (!centroCosto.isEmpty()) {
            if (centroCostoBO.deleteCentroCosto(CentroC)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Centro de costo eliminado correctamente", ""));
                llenarLista();
                limpiar();
            }
        }
        RequestContext.getCurrentInstance().update("formCentroCosto");
    }

    public void updateCentroCosto(CentroCostoBean CentroC) {
        this.idCentroCosto = CentroC.getIdCentroCosto();
        this.centroCosto = CentroC.getCentroCosto();
        this.activa = CentroC.getActiva();
        if (!centroCosto.isEmpty()) {
            if (centroCostoBO.updateCentroCosto(CentroC)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Centro de costo actualizado correctamente", ""));
                llenarLista();
                limpiar();
            }
        }
        RequestContext.getCurrentInstance().update("formCentroCosto");
    }

    public void actualizarCentroCosto(RowEditEvent event) {
        if (centroCostoBO.updateCentroCosto((CentroCostoBean) event.getObject())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Registro actualizado correctamente "));
        }
    }

    public void cancelarActualizarCentroCosto(RowEditEvent event) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Operacion Cancelada"));
    }

    public void setCentroCostoBO(CentroCostoBO centroCostoBO) {
        this.centroCostoBO = centroCostoBO;
    }

    public Integer getIdCentroCosto() {
        return idCentroCosto;
    }

    public void setIdCentroCosto(Integer idCentroCosto) {
        this.idCentroCosto = idCentroCosto;
    }

    public String getCentroCosto() {
        return centroCosto;
    }

    public void setCentroCosto(String centroCosto) {
        this.centroCosto = centroCosto;
    }

    public List<CentroCostoBean> getListCentroCosto() {
        return listCentroCosto;
    }

    public void setListCentroCosto(List<CentroCostoBean> listCentroCosto) {
        this.listCentroCosto = listCentroCosto;
    }

    public Boolean getActiva() {
        return activa;
    }

    public void setActiva(Boolean activa) {
        this.activa = activa;
    }

    public CentroCostoBean getSelectCentro() {
        return selectCentro;
    }

    public void setSelectCentro(CentroCostoBean selectCentro) {
        this.selectCentro = selectCentro;
    }

    public List<CentroCostoBean> getFiltroCentroCosto() {
        return filtroCentroCosto;
    }

    public void setFiltroCentroCosto(List<CentroCostoBean> filtroCentroCosto) {
        this.filtroCentroCosto = filtroCentroCosto;
    }

}
