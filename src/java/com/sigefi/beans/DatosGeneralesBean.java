/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.DatosGeneralesBO;
import com.sigefi.bo.PresupuestoBO;
import com.sigefi.clases.FechaSistema;
import com.sigefi.entity.UnidadesEjecutoras;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class DatosGeneralesBean implements Serializable{

    @ManagedProperty(value = "#{datosGeneralesBO}")
    private DatosGeneralesBO datosGeneralesBO;
    
    @ManagedProperty(name = "presupuestoBO", value = "#{presupuestoBO}")
    private PresupuestoBO presupuestoBO;
    
    private short idunidadesEjecutoras;
    private UnidadesEjecutoras unidadesEjecutoras;
    private Map<String, Short> mapaUnidadEjecutora;
    private Map<Short, UnidadesEjecutoras> mapaUnidadEjecutora2;
    private Integer anyoEjecucion;
    private List<Integer> listAnios;
    
    private Character moneda;
    private String usuarioCreador;
    
    public DatosGeneralesBean() {
    }
    
    @PostConstruct
    public void main(){
        SessionBean referenciaBeanSession
                = (SessionBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("sessionBean");

        usuarioCreador = referenciaBeanSession.getNombreUsuario();
        
        List<UnidadesEjecutoras> listUnidadesEjecutoras = presupuestoBO.listUnidadesEjecutoras();
        if (listUnidadesEjecutoras != null) {
            mapaUnidadEjecutora = new HashMap();
            mapaUnidadEjecutora2 = new HashMap();
            for (UnidadesEjecutoras obj : listUnidadesEjecutoras) {
                mapaUnidadEjecutora.put(obj.getNombreUnidad(), obj.getCodigo());
                mapaUnidadEjecutora2.put(obj.getCodigo(), obj);
            }
        }
        
        FechaSistema anioHoy = new FechaSistema();
        int anio = anioHoy.getAnio();
        listAnios = new ArrayList<>();
        for (int i = 0; i <= 2; i++) {
            listAnios.add(anio);
            anio = anio + 1;
        }
    }
    
    public void insertEmpresa(){
        this.unidadesEjecutoras = mapaUnidadEjecutora2.get(idunidadesEjecutoras);
        if(datosGeneralesBO.insertEmpres(this)){
            
            limpiar();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Empresa agregada correctamente", ""));
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Error, Empresa No agregada ", ""));
        }
    }
    
    public void limpiar(){
        moneda = null;
    }
    
    public DatosGeneralesBO getDatosGeneralesBO() {
        return datosGeneralesBO;
    }

    public void setDatosGeneralesBO(DatosGeneralesBO datosGeneralesBO) {
        this.datosGeneralesBO = datosGeneralesBO;
    }

    public Map<String, Short> getMapaUnidadEjecutora() {
        return mapaUnidadEjecutora;
    }

    public void setMapaUnidadEjecutora(Map<String, Short> mapaUnidadEjecutora) {
        this.mapaUnidadEjecutora = mapaUnidadEjecutora;
    }

    public PresupuestoBO getPresupuestoBO() {
        return presupuestoBO;
    }

    public void setPresupuestoBO(PresupuestoBO presupuestoBO) {
        this.presupuestoBO = presupuestoBO;
    }

    public short getIdunidadesEjecutoras() {
        return idunidadesEjecutoras;
    }

    public void setIdunidadesEjecutoras(short idunidadesEjecutoras) {
        this.idunidadesEjecutoras = idunidadesEjecutoras;
    }

    public UnidadesEjecutoras getUnidadesEjecutoras() {
        return unidadesEjecutoras;
    }

    public void setUnidadesEjecutoras(UnidadesEjecutoras unidadesEjecutoras) {
        this.unidadesEjecutoras = unidadesEjecutoras;
    }

    public Integer getAnyoEjecucion() {
        return anyoEjecucion;
    }

    public void setAnyoEjecucion(Integer anyoEjecucion) {
        this.anyoEjecucion = anyoEjecucion;
    }

    public List<Integer> getListAnios() {
        return listAnios;
    }

    public void setListAnios(List<Integer> listAnios) {
        this.listAnios = listAnios;
    }

    public Character getMoneda() {
        return moneda;
    }

    public void setMoneda(Character moneda) {
        this.moneda = moneda;
    }   

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public Map<Short, UnidadesEjecutoras> getMapaUnidadEjecutora2() {
        return mapaUnidadEjecutora2;
    }

    public void setMapaUnidadEjecutora2(Map<Short, UnidadesEjecutoras> mapaUnidadEjecutora2) {
        this.mapaUnidadEjecutora2 = mapaUnidadEjecutora2;
    }    
}
