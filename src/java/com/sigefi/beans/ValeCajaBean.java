/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.ValeCajaBO;
import com.sigefi.clases.ConvertirNumerosLetras;
import com.sigefi.entity.ManualClasificatorio;
import com.sigefi.entity.ValeCaja;
import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.primefaces.context.RequestContext;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author Aldemaro Gonzalez
 */
@ManagedBean
@ViewScoped
public class ValeCajaBean implements Serializable {

    @ManagedProperty(value = "#{valeCajaBO}")
    private ValeCajaBO valeCajaBO;

    @ManagedProperty("#{dataSource}")
    private DriverManagerDataSource dataSource;

    
    private List<ManualClasificatorio> listManualAll;
    private ValeCaja valeCaja;

    public ValeCajaBean() {
        valeCaja = new ValeCaja();
        valeCaja.setManualClasificatorio(new ManualClasificatorio());
    }

    @PostConstruct
    public void main() {
        setListManualAll(valeCajaBO.listManualClasiAll());
    }

    public void conversion(ValeCaja vale){
        BigDecimal cantidad = vale.getCantidadNum();
        String valor = cantidad.toString();
        String letras = ConvertirNumerosLetras.convertNumberToLetter(valor);
        vale.setCantidadText(letras);
    }
    public void insetVale() {
        try {
            if (valeCajaBO.insertValeCaja(this)) {
                reporte(valeCaja.getNumero(), valeCaja.getFecha());
                limpiar();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Vale de Caja agregado correctamente", ""));
                RequestContext.getCurrentInstance().update("formValeCaja");
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Vale de Caja modificado correctamente", ""));
            }
        } catch (Exception e) {
        }
    }
    
    public void reporte(int numero,Date fecha) {
        try {
            Map<String, Object> mapParametros = new HashMap<>();
            mapParametros.put("NUMERO", numero);
            mapParametros.put("FECHA", fecha);
            FacesContext context = FacesContext.getCurrentInstance();
            File jasper = new File("/Sigefi/Reportes/" + "ValeCaja" + ".jasper");
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasper.getPath(), mapParametros, dataSource.getConnection());
            byte[] bytes = JasperExportManager.exportReportToPdf(jasperPrint);
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.setContentLength(bytes.length);
            response.getOutputStream().write(bytes);
            response.setContentType("application/pdf");
            context.responseComplete();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void limpiar() {
        valeCaja = new ValeCaja();
        valeCaja.setManualClasificatorio(new ManualClasificatorio());
    }

    public List<ManualClasificatorio> getListManualAll() {
        return listManualAll;
    }

    public void setListManualAll(List<ManualClasificatorio> listManualAll) {
        this.listManualAll = listManualAll;
    }

    public ValeCajaBO getValeCajaBO() {
        return valeCajaBO;
    }

    public void setValeCajaBO(ValeCajaBO valeCajaBO) {
        this.valeCajaBO = valeCajaBO;
    }

    public ValeCaja getValeCaja() {
        return valeCaja;
    }

    public void setValeCaja(ValeCaja valeCaja) {
        this.valeCaja = valeCaja;
    }

    public DriverManagerDataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DriverManagerDataSource dataSource) {
        this.dataSource = dataSource;
    }

    

    
}
