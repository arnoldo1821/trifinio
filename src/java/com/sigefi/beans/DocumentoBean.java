/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@RequestScoped
public class DocumentoBean implements Serializable {

    private StreamedContent documento;

    public DocumentoBean() {
    }

    public StreamedContent getDocumento() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
                return new DefaultStreamedContent();
            } else {
                String url = context.getExternalContext().getRequestParameterMap().get("agencyId");
                if (!url.isEmpty()) {
                    File chartFile = new File(url);
                    if (!chartFile.isFile()) {
                        return new DefaultStreamedContent();
                    } else {
                        if (url.endsWith(".pdf")) {
                            return new DefaultStreamedContent(new FileInputStream(chartFile), "application/pdf");
                        } else {
                            return new DefaultStreamedContent(new FileInputStream(chartFile), "image/png");
                        }
                    }
                } else {
                    ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
                    String realPath = (String) servletContext.getRealPath("/"); // Sustituye "/" por el directorio ej: "/upload"
                    String urld = realPath + "\\recursos\\imagenes\\otras\\NoHayDocumento.jpg";
                    File defectoFile = new File(urld);
                    return new DefaultStreamedContent(new FileInputStream(defectoFile), "image/png");
                }

            }
        } catch (Exception e) {
        }
//        return documento;
        return new DefaultStreamedContent();
    }

    public void setDocumento(StreamedContent documento) {
        this.documento = documento;
    }

}
