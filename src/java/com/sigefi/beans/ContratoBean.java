
package com.sigefi.beans;

import com.sigefi.bo.ContratoBO;
import com.sigefi.bo.ModeloContratoBO;
import com.sigefi.entity.ClausulasContrato;
import com.sigefi.entity.Contratos;
import java.io.Serializable;
import java.util.Set;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author SKAR
 */
@ManagedBean
@ViewScoped
public class ContratoBean implements Serializable {
    
    @ManagedProperty("#{contratoBO}")
    private ContratoBO contratoBO;
    @ManagedProperty(value="#{modeloContratoBO}")
    private ModeloContratoBO modeloContratoBO;
    
    private Contratos contrato;
    private ClausulasContrato clausulaSelected;
    
    private boolean renderNuevoContrato;
    private boolean renderModContrato;
    private boolean renderPanelBoton;

    /**
     * Creates a new instance of ContratoBean
     */
    public ContratoBean() {
        renderNuevoContrato = true;
        renderModContrato = false;
        renderPanelBoton = true;
        contrato = new Contratos();
    }
    
    public void onActionNuevoContrato(){
        renderNuevoContrato = true;
        renderNuevoContrato = false;
        
    }
    
    public void updateClausula(){
        Set<ClausulasContrato> setClausulas = contrato.getClausulasContratos();
        for (ClausulasContrato cla : setClausulas) {
            if (cla.getId().getClausula().equals(clausulaSelected.getId().getClausula())) {
                setClausulas.remove(cla);
                setClausulas.add(clausulaSelected);
                contrato.setClausulasContratos(setClausulas);
            }
        }
    }
    
    public void cancelarEditClausula(){
        clausulaSelected = null;
    }
    
//    GETTER AND SETTER

    public ContratoBO getContratoBO() {
        return contratoBO;
    }

    public void setContratoBO(ContratoBO contratoBO) {
        this.contratoBO = contratoBO;
    }

    public ClausulasContrato getClausulaSelected() {
        return clausulaSelected;
    }

    public void setClausulaSelected(ClausulasContrato clausulaSelected) {
        this.clausulaSelected = clausulaSelected;
    }

    public boolean isRenderPanelBoton() {
        return renderPanelBoton;
    }

    public void setRenderPanelBoton(boolean renderPanelBoton) {
        this.renderPanelBoton = renderPanelBoton;
    }

    public Contratos getContrato() {
        return contrato;
    }

    public void setContrato(Contratos contrato) {
        this.contrato = contrato;
    }

    public boolean isRenderNuevoContrato() {
        return renderNuevoContrato;
    }

    public void setRenderNuevoContrato(boolean renderNuevoContrato) {
        this.renderNuevoContrato = renderNuevoContrato;
    }

    public boolean isRenderModContrato() {
        return renderModContrato;
    }

    public void setRenderModContrato(boolean renderModContrato) {
        this.renderModContrato = renderModContrato;
    }

    public ModeloContratoBO getModeloContratoBO() {
        return modeloContratoBO;
    }

    public void setModeloContratoBO(ModeloContratoBO modeloContratoBO) {
        this.modeloContratoBO = modeloContratoBO;
    }
    
}
