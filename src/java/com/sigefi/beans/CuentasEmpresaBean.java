/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.CuentasEmpresaBO;
import com.sigefi.clases.FechaSistema;
import com.sigefi.entity.CatalogoCuentasConta;
import com.sigefi.entity.CatalogosConta;
import com.sigefi.entity.Empresa;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.model.TreeNode;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class CuentasEmpresaBean implements Serializable{

    @ManagedProperty(value = "#{cuentasEmpresaBO}")
    private CuentasEmpresaBO cuentasEmpresaBO;
    
    private short idunidadesEjecutoras;
    
    private int anio;
    private List<Integer> listAnios = new ArrayList<>();
    private boolean estado1;
    private Empresa empSelecc;
    private TreeNode[] treeNodeSeleccionado;
    private List<CatalogosConta> listCuentasAgre;
    
    public CuentasEmpresaBean() {
        FechaSistema fechaHoy = new  FechaSistema();
        int f = fechaHoy.getAnio()-1;
        for(int i=0;i<3;i++){
            f = f + i;
            listAnios.add(f);
        }
        estado1 = false;
    }
    
    
    public void consultarEmpresa(){
        Empresa emp = cuentasEmpresaBO.consultarEmpresa(idunidadesEjecutoras, anio);
        if(emp == null){
            estado1  = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Empresa no encontrada", ""));
        }else{
            estado1 = true;
            empSelecc = emp;
            setListCuentasAgre(cuentasEmpresaBO.listCuentasAgregadas(this.empSelecc));
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Empresa Seleccionada correctamente", ""));
        }
    }
    
    public void displaySelectedMultiple(TreeNode[] nodes) {
        if (nodes != null && nodes.length > 0) {
            int i = 0, tam = nodes.length;
            List<String> listNum = new ArrayList<>();
            for (TreeNode node : nodes) {
                listNum.add(((CatalogoCuentasConta) nodes[i].getData()).getCodCatalocuenta());
                if (i == (tam - 1)) {
                    listNum.add(((CatalogoCuentasConta) nodes[i].getData()).getCuentapadre());
                }
                i++;
            }
            if(cuentasEmpresaBO.inserCuentas(listNum, this) == 1){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cuentas Agregadas corectamente", ""));
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Cuentas No Agregadas ya estan agregadas", ""));
            }
        }
        
        setListCuentasAgre(cuentasEmpresaBO.listCuentasAgregadas(this.empSelecc));
        RequestContext.getCurrentInstance().update("fromCuentaEmpresa:cuentas");
    }
    
    public void finalzar(){
        idunidadesEjecutoras = 0;
        anio = 0;
        empSelecc = new Empresa();
        if(getListCuentasAgre() != null){
            if(getListCuentasAgre().size()>0){
                getListCuentasAgre().clear();
            }
        }
        estado1 = false;
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Proceso de asignar cuentas finalizado", ""));
    }
    
    public CuentasEmpresaBO getCuentasEmpresaBO() {
        return cuentasEmpresaBO;
    }

    public void setCuentasEmpresaBO(CuentasEmpresaBO cuentasEmpresaBO) {
        this.cuentasEmpresaBO = cuentasEmpresaBO;
    }

    public short getIdunidadesEjecutoras() {
        return idunidadesEjecutoras;
    }

    public void setIdunidadesEjecutoras(short idunidadesEjecutoras) {
        this.idunidadesEjecutoras = idunidadesEjecutoras;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public List<Integer> getListAnios() {
        return listAnios;
    }

    public void setListAnios(List<Integer> listAnios) {
        this.listAnios = listAnios;
    }

    public boolean isEstado1() {
        return estado1;
    }

    public void setEstado1(boolean estado1) {
        this.estado1 = estado1;
    }

    public Empresa getEmpSelecc() {
        return empSelecc;
    }

    public void setEmpSelecc(Empresa empSelecc) {
        this.empSelecc = empSelecc;
    }

    public TreeNode[] getTreeNodeSeleccionado() {
        return treeNodeSeleccionado;
    }

    public void setTreeNodeSeleccionado(TreeNode[] treeNodeSeleccionado) {
        this.treeNodeSeleccionado = treeNodeSeleccionado;
    }

    public List<CatalogosConta> getListCuentasAgre() {
        return listCuentasAgre;
    }

    public void setListCuentasAgre(List<CatalogosConta> listCuentasAgre) {
        this.listCuentasAgre = listCuentasAgre;
    }

    
    
    
}
