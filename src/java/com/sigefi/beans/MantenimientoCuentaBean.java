/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.CuentasBO;
import com.sigefi.bo.MantenimientoCuentaBO;
import com.sigefi.entity.CatalogoCuentas;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.model.TreeNode;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class MantenimientoCuentaBean implements Serializable{

    @ManagedProperty(value = "#{mantenimientoCuentaBO}")
    private MantenimientoCuentaBO mantenimientoCuentaBO;
    
    @ManagedProperty(value = "#{cuentasBO}")
    private CuentasBO cuentasBO;
    
    private TreeNode root;
    
    private int idCuenta;
    private String nombreCuenta;
    private String nombreNuevo;
    
    private CatalogoCuentas cuentaSelec;
    
    private short unidadEjecutoraSeleccionada;
    private int anio;
    
    public MantenimientoCuentaBean() {
    }
    
    public void llenarNodos(){
        root = cuentasBO.listNodoCuentasCatalogo(unidadEjecutoraSeleccionada);
//        root = new DefaultTreeNode("principal", null);
        expandirAll(root);
    }
    
    @PostConstruct
    public void main(){
        llenarNodos();
    }
    
    private void expandirAll(TreeNode treeNode) {
        if (treeNode.getChildren().size() > 0) {
            treeNode.setExpanded(true);
            for (int i = 0; i < treeNode.getChildren().size(); i++) {
                expandirAll(treeNode.getChildren().get(i));
            }
        }
    }
    
    public void cuentaSeleccionada(CatalogoCuentas cuenta){       
        this.idCuenta = cuenta.getIdCuenta();
        this.nombreCuenta = cuenta.getNombreCuenta();
        this.nombreNuevo = nombreCuenta;
        this.cuentaSelec = cuenta;
        RequestContext.getCurrentInstance().update("fromModificarCuenta");
    }
    
    public void cuentaSeleccionada2(CatalogoCuentas cuenta){       
        this.idCuenta = cuenta.getIdCuenta();
        this.nombreCuenta = cuenta.getNombreCuenta();
        this.nombreNuevo = nombreCuenta;
        this.cuentaSelec = cuenta;
        RequestContext.getCurrentInstance().update("fromEliminarCuenta");
    }
    
    public void eliminarCuenta(){
            if(!nombreCuenta.isEmpty()){
                cuentaSelec.setActiva(false);
            if(mantenimientoCuentaBO.deleteCuenta(cuentaSelec)){
                llenarNodos();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Cuenta Eliminada",""));
            }
        }
        RequestContext.getCurrentInstance().update("formManCuenta");
    }
    
    public void updateCuenta(){
        if(!nombreCuenta.isEmpty()){
            cuentaSelec.setNombreCuenta(nombreNuevo);
            if(mantenimientoCuentaBO.updateCuenta(cuentaSelec)){
                llenarNodos();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Cuenta Modificada",""));
            }
        }
        RequestContext.getCurrentInstance().update("formManCuenta");
    }
    
    public MantenimientoCuentaBO getMantenimientoCuentaBO() {
        return mantenimientoCuentaBO;
    }

    public void setMantenimientoCuentaBO(MantenimientoCuentaBO mantenimientoCuentaBO) {
        this.mantenimientoCuentaBO = mantenimientoCuentaBO;
    }

    public CuentasBO getCuentasBO() {
        return cuentasBO;
    }

    public void setCuentasBO(CuentasBO cuentasBO) {
        this.cuentasBO = cuentasBO;
    }

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public int getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(int idCuenta) {
        this.idCuenta = idCuenta;
    }

    public String getNombreCuenta() {
        return nombreCuenta;
    }

    public void setNombreCuenta(String nombreCuenta) {
        this.nombreCuenta = nombreCuenta;
    }

    public String getNombreNuevo() {
        return nombreNuevo;
    }

    public void setNombreNuevo(String nombreNuevo) {
        this.nombreNuevo = nombreNuevo;
    }

    public CatalogoCuentas getCuentaSelec() {
        return cuentaSelec;
    }

    public void setCuentaSelec(CatalogoCuentas cuentaSelec) {
        this.cuentaSelec = cuentaSelec;
    }

    public short getUnidadEjecutoraSeleccionada() {
        return unidadEjecutoraSeleccionada;
    }

    public void setUnidadEjecutoraSeleccionada(short unidadEjecutoraSeleccionada) {
        this.unidadEjecutoraSeleccionada = unidadEjecutoraSeleccionada;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }
    
    
}
