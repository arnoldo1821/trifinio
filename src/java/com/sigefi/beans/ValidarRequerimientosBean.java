/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.PresupuestoBO;
import com.sigefi.bo.RequisicionBO;
import com.sigefi.bo.ValidarRequerimientosBO;
import com.sigefi.clases.FechaSistema;
import com.sigefi.entity.CentroCosto;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.DocumentosRequerimientos;
import com.sigefi.entity.NoConformidades;
import com.sigefi.entity.PartesInteresadas;
import com.sigefi.entity.RequerimientoPagos;
import com.sigefi.entity.UnidadesEjecutoras;
import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class ValidarRequerimientosBean implements Serializable {

    @ManagedProperty(value = "#{validarRequerimientosBO}")
    private ValidarRequerimientosBO validarRequerimientosBO;

    @ManagedProperty(name = "requisicionBO", value = "#{requisicionBO}")
    private RequisicionBO requisicionBO;

    @ManagedProperty(name = "presupuestoBO", value = "#{presupuestoBO}")
    private PresupuestoBO presupuestoBO;

    private List<RequerimientoPagos> listAllReque;
    private List<DetalleRequerimientoPago> listAllDetalleRequeSelec;
    private List<DocumentosRequerimientos> listAllDocRequeSelec;
    private List<DetalleRequerimientoPago> detalleAllRequerimiento;
    private List<DetalleRequerimientoPago> filtrodetalleRequerimiento;
    private Map<String, Short> mapaUnidadEjecutora;
    private Map<String, String> mapProveedor;
    private List<Short> listAnios;
    private List<Character> listMoneda;

    private String nomUnidadEjecutora;
    private Date fechaPresentacion;
    private Date fechaEstimacion;
    private String codigoReq;
    private Character moneda;

    private int fkPkRequerimiento;
    private short correlativo = 0;
    private RequerimientoPagos requerimientoPagos;
    private String codigoProveedor;
    private String concepto;
    private String nombreProveedor;
    private String codigoCuenta;
    private String conceptoGasto;
    private BigDecimal monto;
    private String claseDocumento;
    private String numeroDocumento;
    private Date fechaDocumento;
    
    
    private int incrReqPk;
    private UnidadesEjecutoras unidadesEjecutoras;
    private CentroCosto centroCosto;
    private String codigoRequerimiento;
    private String codigoSiafi;
    private boolean siafi;
    private boolean afectacionPresupuestaria;
    private Date fechaPresent;
    private Date fechaEstimadaPago;
    private Character moneda1;
    private int anioPresupuesto;
    private String usuario;

    private int incre_req_px;

    private String url;

    private boolean validacion;
    private boolean estadoValidacion;
    private DetalleRequerimientoPago selectRequerimiento;

    private RequerimientoPagos reqSelecc;
    private String UsuarioLogeado;
    private String observaciones;

    private Map<String, Short> mapErrores;
    private short errorSeleccionado;

    public ValidarRequerimientosBean() {
        validacion = true;
        SessionBean referenciaBeanSession = (SessionBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("sessionBean");
        UsuarioLogeado = referenciaBeanSession.getNombreUsuario();
    }

    @PostConstruct
    public void main() {
        llenarTabla();
        List<NoConformidades> listAllErrores = validarRequerimientosBO.listAllErrores();
        if (listAllErrores != null) {
            mapErrores = new HashMap<>();
            for (NoConformidades objError : listAllErrores) {
                mapErrores.put(objError.getDescripcion(), objError.getCodigo());
            }
        }
        estadoValidacion = true;

        List<PartesInteresadas> listProv = requisicionBO.listProveedorAll();
        if (listProv != null) {
            mapProveedor = new HashMap<>();
            for (PartesInteresadas prove : listProv) {
                mapProveedor.put(prove.getNombreProveedor(), prove.getId().getCodigoProveedor());
            }
        }

        List<UnidadesEjecutoras> listUnidadesEjecutoras = presupuestoBO.listUnidadesEjecutoras();
        if (listUnidadesEjecutoras != null) {
            mapaUnidadEjecutora = new HashMap();
            for (UnidadesEjecutoras obj : listUnidadesEjecutoras) {
                mapaUnidadEjecutora.put(obj.getNombreUnidad(), obj.getCodigo());
            }
        }

        FechaSistema anioHoy = new FechaSistema();
        short anio = (short) (anioHoy.getAnio() - 1);
        listAnios = new ArrayList<>();
        for (int i = 0; i <= 3; i++) {
            listAnios.add(anio);
            anio = (short) (anio + 1);
        }

        listMoneda = new ArrayList<>();
        listMoneda.add('D');
        listMoneda.add('Q');
        listMoneda.add('L');
    }

    public void llenarTabla() {
        setListAllReque(validarRequerimientosBO.listAllRequePago());
        setDetalleAllRequerimiento(validarRequerimientosBO.listAllDetalleRequerimiento());
    }

    public void requeSelecc(DetalleRequerimientoPago obj) {
        RequerimientoPagos req = obj.getRequerimientoPagos();
        nomUnidadEjecutora = req.getUnidadesEjecutoras().getNombreUnidad();
        fechaPresentacion = req.getFechaPresentacion();
        fechaEstimacion = req.getFechaEstimadaPago();
        codigoReq = req.getCodigoRequerimiento();
        moneda = req.getMoneda();
        incre_req_px = req.getIncrReqPk();
        setListAllDetalleRequeSelec(validarRequerimientosBO.listDetalleRequePagoSelec(incre_req_px));
        setListAllDocRequeSelec(validarRequerimientosBO.listDocRequePagoSelec(incre_req_px));

        this.reqSelecc = req;
        RequestContext.getCurrentInstance().update("fromReque");
    }

    public void imagenSelecionada(String url) {
        try {
            setUrl(url);
            File chartFile = new File(getUrl());
            if (chartFile.isFile()) {
                if (getUrl().endsWith(".pdf")) {
                    RequestContext.getCurrentInstance().execute("PF('pdfReque').show();");
                    RequestContext.getCurrentInstance().update("fromPdf");
                } else {
                    RequestContext.getCurrentInstance().execute("PF('imgReque').show();");
                    RequestContext.getCurrentInstance().update("fromImagen");
                }
            } else {
                ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
                String realPath = (String) servletContext.getRealPath("/"); // Sustituye "/" por el directorio ej: "/upload"
                setUrl(realPath + "\\recursos\\imagenes\\otras\\NoHayDocumento.jpg");
                RequestContext.getCurrentInstance().execute("PF('imgReque').show();");
                RequestContext.getCurrentInstance().update("fromImagen");
            }

        } catch (Exception e) {
        }
    }

    public void validacionSeleccionada() {
        if (validacion) {
            estadoValidacion = true;
        } else {
            estadoValidacion = false;
        }
    }

    public void validarRequerimiento() {
        validarRequerimientosBO.validarRequerimiento(reqSelecc, this);
        llenarTabla();
        RequestContext.getCurrentInstance().update("formReqPrin");
    }

    public void updateRequerimiento(DetalleRequerimientoPago obj) {
        this.fkPkRequerimiento = obj.getId().getFkPkRequerimiento2();
        this.correlativo = obj.getId().getCorrelativo();
        this.requerimientoPagos = obj.getRequerimientoPagos();
        this.codigoProveedor = obj.getCodigoProveedor();
        this.concepto = obj.getConcepto();
        this.nombreProveedor = obj.getNombreProveedor();
        this.codigoCuenta = obj.getCodigoCuenta();
        this.conceptoGasto = obj.getConceptoGasto();
        this.monto = obj.getMonto();
        this.claseDocumento = obj.getClaseDocumento();
        this.numeroDocumento = obj.getNumeroDocumento();
        this.fechaDocumento = obj.getFechaDocumento();
        
        this.incrReqPk = obj.getRequerimientoPagos().getIncrReqPk();
        this.unidadesEjecutoras = obj.getRequerimientoPagos().getUnidadesEjecutoras();
        this.centroCosto = obj.getRequerimientoPagos().getCentroCosto();
        this.codigoRequerimiento = obj.getRequerimientoPagos().getCodigoRequerimiento();
        this.codigoSiafi = obj.getRequerimientoPagos().getCodigoSiafi();
        this.siafi = obj.getRequerimientoPagos().getSiafi();
        this.afectacionPresupuestaria = obj.getRequerimientoPagos().getAfectacionPresupuestaria();
        this.fechaPresent = obj.getRequerimientoPagos().getFechaPresentacion();
        this.fechaEstimadaPago = obj.getRequerimientoPagos().getFechaEstimadaPago();
        this.moneda1 = obj.getRequerimientoPagos().getMoneda();
        this.anioPresupuesto = obj.getRequerimientoPagos().getAnioPresupuesto();
        this.usuario = obj.getRequerimientoPagos().getUsuario();
        
        if (validarRequerimientosBO.updateRequerimiento(this)) {
            llenarTabla();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Requerimiento actualizado correctamente ", ""));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error al actualizar el requerimiento", ""));
        }
         
        RequestContext.getCurrentInstance().update("formReqModif");
    }

    public void eliminarRequerimiento(DetalleRequerimientoPago obj) {
        RequerimientoPagos re = obj.getRequerimientoPagos();
        if (validarRequerimientosBO.eliminarRequerimiento(re)) {
            llenarTabla();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Requerimiento eliminado correctamente ", ""));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error al eliminar el requerimiento tiene datos en otras tablas", ""));
        }
    }

    public ValidarRequerimientosBO getValidarRequerimientosBO() {
        return validarRequerimientosBO;
    }

    public void setValidarRequerimientosBO(ValidarRequerimientosBO validarRequerimientosBO) {
        this.validarRequerimientosBO = validarRequerimientosBO;
    }

    public List<RequerimientoPagos> getListAllReque() {
        return listAllReque;
    }

    public void setListAllReque(List<RequerimientoPagos> listAllReque) {
        this.listAllReque = listAllReque;
    }

    public Date getFechaPresentacion() {
        return fechaPresentacion;
    }

    public void setFechaPresentacion(Date fechaPresentacion) {
        this.fechaPresentacion = fechaPresentacion;
    }

    public Date getFechaEstimacion() {
        return fechaEstimacion;
    }

    public void setFechaEstimacion(Date fechaEstimacion) {
        this.fechaEstimacion = fechaEstimacion;
    }

    public String getCodigoReq() {
        return codigoReq;
    }

    public void setCodigoReq(String codigoReq) {
        this.codigoReq = codigoReq;
    }

    public Character getMoneda() {
        return moneda;
    }

    public void setMoneda(Character moneda) {
        this.moneda = moneda;
    }

    public int getIncre_req_px() {
        return incre_req_px;
    }

    public void setIncre_req_px(int incre_req_px) {
        this.incre_req_px = incre_req_px;
    }

    public String getNomUnidadEjecutora() {
        return nomUnidadEjecutora;
    }

    public void setNomUnidadEjecutora(String nomUnidadEjecutora) {
        this.nomUnidadEjecutora = nomUnidadEjecutora;
    }

    public List<DetalleRequerimientoPago> getListAllDetalleRequeSelec() {
        return listAllDetalleRequeSelec;
    }

    public void setListAllDetalleRequeSelec(List<DetalleRequerimientoPago> listAllDetalleRequeSelec) {
        this.listAllDetalleRequeSelec = listAllDetalleRequeSelec;
    }

    public List<DocumentosRequerimientos> getListAllDocRequeSelec() {
        return listAllDocRequeSelec;
    }

    public void setListAllDocRequeSelec(List<DocumentosRequerimientos> listAllDocRequeSelec) {
        this.listAllDocRequeSelec = listAllDocRequeSelec;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isValidacion() {
        return validacion;
    }

    public void setValidacion(boolean validacion) {
        this.validacion = validacion;
    }

    public boolean isEstadoValidacion() {
        return estadoValidacion;
    }

    public void setEstadoValidacion(boolean estadoValidacion) {
        this.estadoValidacion = estadoValidacion;
    }

    public RequerimientoPagos getReqSelecc() {
        return reqSelecc;
    }

    public void setReqSelecc(RequerimientoPagos reqSelecc) {
        this.reqSelecc = reqSelecc;
    }

    public String getUsuarioLogeado() {
        return UsuarioLogeado;
    }

    public void setUsuarioLogeado(String UsuarioLogeado) {
        this.UsuarioLogeado = UsuarioLogeado;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Map<String, Short> getMapErrores() {
        return mapErrores;
    }

    public void setMapErrores(Map<String, Short> mapErrores) {
        this.mapErrores = mapErrores;
    }

    public short getErrorSeleccionado() {
        return errorSeleccionado;
    }

    public void setErrorSeleccionado(short errorSeleccionado) {
        this.errorSeleccionado = errorSeleccionado;
    }

    public List<DetalleRequerimientoPago> getDetalleAllRequerimiento() {
        return detalleAllRequerimiento;
    }

    public void setDetalleAllRequerimiento(List<DetalleRequerimientoPago> detalleAllRequerimiento) {
        this.detalleAllRequerimiento = detalleAllRequerimiento;
    }

    public List<DetalleRequerimientoPago> getFiltrodetalleRequerimiento() {
        return filtrodetalleRequerimiento;
    }

    public void setFiltrodetalleRequerimiento(List<DetalleRequerimientoPago> filtrodetalleRequerimiento) {
        this.filtrodetalleRequerimiento = filtrodetalleRequerimiento;
    }

    public DetalleRequerimientoPago getSelectRequerimiento() {
        return selectRequerimiento;
    }

    public void setSelectRequerimiento(DetalleRequerimientoPago selectRequerimiento) {
        this.selectRequerimiento = selectRequerimiento;
    }

    public Map<String, Short> getMapaUnidadEjecutora() {
        return mapaUnidadEjecutora;
    }

    public void setMapaUnidadEjecutora(Map<String, Short> mapaUnidadEjecutora) {
        this.mapaUnidadEjecutora = mapaUnidadEjecutora;
    }

    public Map<String, String> getMapProveedor() {
        return mapProveedor;
    }

    public void setMapProveedor(Map<String, String> mapProveedor) {
        this.mapProveedor = mapProveedor;
    }

    public List<Short> getListAnios() {
        return listAnios;
    }

    public void setListAnios(List<Short> listAnios) {
        this.listAnios = listAnios;
    }

    public RequisicionBO getRequisicionBO() {
        return requisicionBO;
    }

    public void setRequisicionBO(RequisicionBO requisicionBO) {
        this.requisicionBO = requisicionBO;
    }

    public PresupuestoBO getPresupuestoBO() {
        return presupuestoBO;
    }

    public void setPresupuestoBO(PresupuestoBO presupuestoBO) {
        this.presupuestoBO = presupuestoBO;
    }

    public List<Character> getListMoneda() {
        return listMoneda;
    }

    public void setListMoneda(List<Character> listMoneda) {
        this.listMoneda = listMoneda;
    }

    public int getFkPkRequerimiento() {
        return fkPkRequerimiento;
    }

    public void setFkPkRequerimiento(int fkPkRequerimiento) {
        this.fkPkRequerimiento = fkPkRequerimiento;
    }

    public short getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(short correlativo) {
        this.correlativo = correlativo;
    }

    public RequerimientoPagos getRequerimientoPagos() {
        return requerimientoPagos;
    }

    public void setRequerimientoPagos(RequerimientoPagos requerimientoPagos) {
        this.requerimientoPagos = requerimientoPagos;
    }

    public String getCodigoProveedor() {
        return codigoProveedor;
    }

    public void setCodigoProveedor(String codigoProveedor) {
        this.codigoProveedor = codigoProveedor;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }

    public String getCodigoCuenta() {
        return codigoCuenta;
    }

    public void setCodigoCuenta(String codigoCuenta) {
        this.codigoCuenta = codigoCuenta;
    }

    public String getConceptoGasto() {
        return conceptoGasto;
    }

    public void setConceptoGasto(String conceptoGasto) {
        this.conceptoGasto = conceptoGasto;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public String getClaseDocumento() {
        return claseDocumento;
    }

    public void setClaseDocumento(String claseDocumento) {
        this.claseDocumento = claseDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public Date getFechaDocumento() {
        return fechaDocumento;
    }

    public void setFechaDocumento(Date fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    public int getIncrReqPk() {
        return incrReqPk;
    }

    public void setIncrReqPk(int incrReqPk) {
        this.incrReqPk = incrReqPk;
    }

    public UnidadesEjecutoras getUnidadesEjecutoras() {
        return unidadesEjecutoras;
    }

    public void setUnidadesEjecutoras(UnidadesEjecutoras unidadesEjecutoras) {
        this.unidadesEjecutoras = unidadesEjecutoras;
    }

    public CentroCosto getCentroCosto() {
        return centroCosto;
    }

    public void setCentroCosto(CentroCosto centroCosto) {
        this.centroCosto = centroCosto;
    }

    public String getCodigoRequerimiento() {
        return codigoRequerimiento;
    }

    public void setCodigoRequerimiento(String codigoRequerimiento) {
        this.codigoRequerimiento = codigoRequerimiento;
    }

    public String getCodigoSiafi() {
        return codigoSiafi;
    }

    public void setCodigoSiafi(String codigoSiafi) {
        this.codigoSiafi = codigoSiafi;
    }

    public boolean isSiafi() {
        return siafi;
    }

    public void setSiafi(boolean siafi) {
        this.siafi = siafi;
    }

    public boolean isAfectacionPresupuestaria() {
        return afectacionPresupuestaria;
    }

    public void setAfectacionPresupuestaria(boolean afectacionPresupuestaria) {
        this.afectacionPresupuestaria = afectacionPresupuestaria;
    }

    public Date getFechaPresent() {
        return fechaPresent;
    }

    public void setFechaPresent(Date fechaPresent) {
        this.fechaPresent = fechaPresent;
    }

    public Date getFechaEstimadaPago() {
        return fechaEstimadaPago;
    }

    public void setFechaEstimadaPago(Date fechaEstimadaPago) {
        this.fechaEstimadaPago = fechaEstimadaPago;
    }

    public Character getMoneda1() {
        return moneda1;
    }

    public void setMoneda1(Character moneda1) {
        this.moneda1 = moneda1;
    }

    public int getAnioPresupuesto() {
        return anioPresupuesto;
    }

    public void setAnioPresupuesto(int anioPresupuesto) {
        this.anioPresupuesto = anioPresupuesto;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    
    
}
