/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.CuentasBancariasBO;
import com.sigefi.bo.FuenteFinanciamientoBO;
import com.sigefi.clases.FechaSistema;
import com.sigefi.entity.CuentasBancarias;
import com.sigefi.entity.FuenteFinanciamiento;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class CuentasBancariasBean implements Serializable {

    @ManagedProperty(value = "#{cuentasBancariasBO}")
    private CuentasBancariasBO cuentasBancariasBO;
    
    @ManagedProperty(value = "#{fuenteFinanciamientoBO}")
    private FuenteFinanciamientoBO fuenteFinanciamientoBO;

    private String codigoCuenta;
    private String CuentasBancarias;
    private String nombreCuenta;
    private String pais;
    private Integer codigoFuenteFinanciamiento;
    private Date fechaCreacion;
    private String nombreBanco;
    private String tipoCuenta;
    private Character moneda;
    private Boolean revisadoContabilidad;
    private Integer cuentaContableAsociada;
    private BigDecimal saldoInicial;
    private List<CuentasBancarias> listCuentasBanAll;
    private List<FuenteFinanciamiento> listFuenteFinanciamientoAll;
    private CuentasBancarias cuentaBanSelecc;
    
    private Map<String, String> mapaCuentasBancarias;
    private Map<String, Integer> mapaFuenteFinanciamiento;

    public CuentasBancariasBean() {
        cuentaBanSelecc = new CuentasBancarias();
        FechaSistema hoy = new FechaSistema();
        fechaCreacion = hoy.getFecha();
    }

    @PostConstruct
    public void main() { 
        List<CuentasBancarias> listCuentasBancarias = cuentasBancariasBO.listAllCuentas();
        if (listCuentasBancarias != null) {
            mapaCuentasBancarias = new HashMap();
            for (CuentasBancarias obj : listCuentasBancarias) {
                mapaCuentasBancarias.put(obj.getNombreCuenta(), obj.getCodigoCuenta());
            }
        }
        
        List<FuenteFinanciamiento> listFuenteFinanciamiento = fuenteFinanciamientoBO.listAllFuenteFinanciamiento();
        if(listFuenteFinanciamiento != null){
           mapaFuenteFinanciamiento = new HashMap();
           for(FuenteFinanciamiento obj : listFuenteFinanciamiento){
               mapaFuenteFinanciamiento.put(obj.getNombreFuente(), obj.getCodigo());
           }
        }
        llenarTabla();
    }

    public void llenarTabla() {
        setListCuentasBanAll(cuentasBancariasBO.listAllCuentas());
        setListFuenteFinanciamientoAll(fuenteFinanciamientoBO.listAllFuenteFinanciamiento());
    }

    public void insertCuentaBan() {
        if (cuentasBancariasBO.insertCuentaBancaria(this)) {
            llenarTabla();
            limpiar();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cuenta Bancaria agregada correctamente ", ""));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error Cuenta Bancaria ya existe", ""));
        }
    }

    public void deleteCuentaBan(CuentasBancarias obj) {
        if (cuentasBancariasBO.deleteCuentaBancaria(obj)) {
            llenarTabla();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cuenta Bancaria eliminada correctamente ", ""));
        }
    }

    public void cuentaSeleccionada(CuentasBancarias obj) {
        cuentaBanSelecc = new CuentasBancarias();
        this.setCuentaBanSelecc(obj);
        RequestContext.getCurrentInstance().update("fromModifiCuentaBan");
    }

    public void updateCuentaBan() {
        if (cuentaBanSelecc != null) {
            if (cuentaBanSelecc.getCodigoCuenta() != null) {
                if (cuentasBancariasBO.updateCuentaBancaria(this.cuentaBanSelecc)) {
                    llenarTabla();
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cuenta Bancaria modificada correctamente ", ""));
                }
            }
        }
    }

    public void limpiar() {
        codigoCuenta = null;
        codigoFuenteFinanciamiento = null;
        nombreBanco = null;
        tipoCuenta = null;
        nombreCuenta = null;
        moneda = null;
        pais="";
        cuentaContableAsociada = null;
        saldoInicial = null;
    }

    public CuentasBancariasBO getCuentasBancariasBO() {
        return cuentasBancariasBO;
    }

    public void setCuentasBancariasBO(CuentasBancariasBO cuentasBancariasBO) {
        this.cuentasBancariasBO = cuentasBancariasBO;
    }

    public String getCodigoCuenta() {
        return codigoCuenta;
    }

    public void setCodigoCuenta(String codigoCuenta) {
        this.codigoCuenta = codigoCuenta;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public Character getMoneda() {
        return moneda;
    }

    public void setMoneda(Character moneda) {
        this.moneda = moneda;
    }

    public Integer getCuentaContableAsociada() {
        return cuentaContableAsociada;
    }

    public void setCuentaContableAsociada(Integer cuentaContableAsociada) {
        this.cuentaContableAsociada = cuentaContableAsociada;
    }

    public List<CuentasBancarias> getListCuentasBanAll() {
        return listCuentasBanAll;
    }

    public void setListCuentasBanAll(List<CuentasBancarias> listCuentasBanAll) {
        this.listCuentasBanAll = listCuentasBanAll;
    }

    public CuentasBancarias getCuentaBanSelecc() {
        return cuentaBanSelecc;
    }

    public void setCuentaBanSelecc(CuentasBancarias cuentaBanSelecc) {
        this.cuentaBanSelecc = cuentaBanSelecc;
    }

    public String getNombreCuenta() {
        return nombreCuenta;
    }

    public void setNombreCuenta(String nombreCuenta) {
        this.nombreCuenta = nombreCuenta;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Boolean getRevisadoContabilidad() {
        return revisadoContabilidad;
    }

    public void setRevisadoContabilidad(Boolean revisadoContabilidad) {
        this.revisadoContabilidad = revisadoContabilidad;
    }

    public String getCuentasBancarias() {
        return CuentasBancarias;
    }

    public void setCuentasBancarias(String CuentasBancarias) {
        this.CuentasBancarias = CuentasBancarias;
    }

    public Map<String, String> getMapaCuentasBancarias() {
        return mapaCuentasBancarias;
    }

    public void setMapaCuentasBancarias(Map<String, String> mapaCuentasBancarias) {
        this.mapaCuentasBancarias = mapaCuentasBancarias;
    }

    public BigDecimal getSaldoInicial() {
        return saldoInicial;
    }

    public void setSaldoInicial(BigDecimal saldoInicial) {
        this.saldoInicial = saldoInicial;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public FuenteFinanciamientoBO getFuenteFinanciamientoBO() {
        return fuenteFinanciamientoBO;
    }

    public void setFuenteFinanciamientoBO(FuenteFinanciamientoBO fuenteFinanciamientoBO) {
        this.fuenteFinanciamientoBO = fuenteFinanciamientoBO;
    }

    public List<FuenteFinanciamiento> getListFuenteFinanciamientoAll() {
        return listFuenteFinanciamientoAll;
    }

    public void setListFuenteFinanciamientoAll(List<FuenteFinanciamiento> listFuenteFinanciamientoAll) {
        this.listFuenteFinanciamientoAll = listFuenteFinanciamientoAll;
    }

    public Map<String, Integer> getMapaFuenteFinanciamiento() {
        return mapaFuenteFinanciamiento;
    }

    public void setMapaFuenteFinanciamiento(Map<String, Integer> mapaFuenteFinanciamiento) {
        this.mapaFuenteFinanciamiento = mapaFuenteFinanciamiento;
    }

    public Integer getCodigoFuenteFinanciamiento() {
        return codigoFuenteFinanciamiento;
    }

    public void setCodigoFuenteFinanciamiento(Integer codigoFuenteFinanciamiento) {
        this.codigoFuenteFinanciamiento = codigoFuenteFinanciamiento;
    }

}
