/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.UsuarioBO;
import com.sigefi.clases.SubirArchivoServer;
import com.sigefi.entity.Ciudades;
import com.sigefi.entity.Rol;
import com.sigefi.entity.UnidadesEjecutoras;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.imageio.stream.FileImageOutputStream;
import javax.servlet.ServletContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CaptureEvent;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author Administrador
 */
@ManagedBean
@ViewScoped
public class UsuarioBean implements Serializable {

    private Short idUsuario;
    private String nombre;
    private String apellido;
    private String email;
    private String usuario;
    private String clave;
    private String urlFoto;
    private String telefono;

    private Short rol;
    private Map<String, Short> mapaRol;
    private Map<String, String> mapaRolNombre;

    private Short ciudad;
    private Map<String, Short> mapaCiudad;

    private Short idUnidadEjecutora;
    private Map<String, Short> mapaUnidadEjecutora;

    @ManagedProperty(name = "usuarioBO", value = "#{usuarioBO}")
    private UsuarioBO usuarioBO;

    private List<UsuarioBean> listUsuarioAll;
    private List<UsuarioBean> filterListUsuarioAll;

    private String nombreRol;

    private Short estado;

    private boolean auxImagenSubir;
    private boolean auxImagenSubirUpdate;
    private boolean auxImagenSubirUpdate2;

    private String urlOriginal;

    /**
     * Creates a new instance of UsuarioBean
     */
    public UsuarioBean() {
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        String realPath = (String) servletContext.getRealPath("/"); // Sustituye "/" por el directorio ej: "/upload"
        String urld = realPath + "\\recursos\\imagenes\\otras\\usuario.jpg";
        this.urlFoto = urld;
        this.urlOriginal = urld;
        this.auxImagenSubir = false;
        this.auxImagenSubirUpdate = false;
        this.auxImagenSubirUpdate2 = false;

    }

    public void insertUsuario() {

        FacesContext context = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        String realPath = (String) servletContext.getRealPath("/"); // Sustituye "/" por el directorio ej: "/upload"
        String urld = realPath + "\\recursos\\imagenes\\otras\\usuario.jpg";

        SubirArchivoServer archivo = new SubirArchivoServer();

        if (!this.urlFoto.equalsIgnoreCase(urld)) {
            String ext = "";
            if (this.urlFoto.endsWith(".png") || this.urlFoto.endsWith(".PNG")) {
                ext = ".png";
            } else {
                ext = ".jpg";
            }
            String url2 = "/SigefiDocumentos/fotoUsuarios/" + usuario + ext;
            archivo.renombrarArchivo(this.urlFoto, url2);
            this.urlFoto = url2;
        }

        if (usuarioBO.insertUsuario(this)) {
            limpiar();
            llenarListaUsuarios();
            this.urlFoto = urld;
            context.addMessage(null, new FacesMessage("Usuario Agregado", "Correctamente"));
            context.addMessage(null, new FacesMessage("Datos Enviados al Usuario", "Correctamente"));

        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Usuario NO Agregado", "El usuario ya existe"));
        }

        RequestContext.getCurrentInstance().update("formularioUsuarioInsert");
        RequestContext.getCurrentInstance().update("principalUsuario");

    }

    public void consultarUsuario(String usu) {
//    public void consultarUsuario(UsuarioBean usu){
//        nombre = usu.nombre;
//        apellido = usu.apellido;
//        email = usu.email;
//        usuario = usu.usuario;
//        rol = usu.rol;
//        ciudad = usu.ciudad;
//        fotoUsuario = usu.fotoUsuario;
//        telefono = usu.telefono;
        usuarioBO.consultarUsuario(usu, this);
        RequestContext.getCurrentInstance().update("formularioUsuarioUpdate");
    }

    public void updateUsuario() {

        FacesContext context = FacesContext.getCurrentInstance();
        if (usuarioBO.updateUsuario(this)) {
            llenarListaUsuarios();
            auxImagenSubirUpdate = true;
            context.addMessage(null, new FacesMessage("Usuario Modificado", "Correctamente"));
            context.addMessage(null, new FacesMessage("Datos Enviados al Usuario", "Correctamente"));
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Usuario NO Modificado", "El Usuario No Existe"));
        }

        RequestContext.getCurrentInstance().update("principalUsuario");
    }

    public void deleteUsuario(String usu) {

        FacesContext context = FacesContext.getCurrentInstance();
        if (usuarioBO.deleteUsuario(usu)) {
            llenarListaUsuarios();
            context.addMessage(null, new FacesMessage("Usuario Eliminado", "Correctamente"));
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Usuario NO Eliminado", "El Usuario No Existe"));
        }

        RequestContext.getCurrentInstance().update("principalUsuario");
    }

    @PostConstruct
    public void usuarioMain() {

        List<Rol> listRoles = usuarioBO.listRolAll();
        if (listRoles != null) {
            mapaRol = new HashMap();
            mapaRolNombre = new HashMap();
            for (Rol obj : listRoles) {
                mapaRol.put(obj.getDescripcionRol(), obj.getIdRol());
                mapaRolNombre.put(obj.getDescripcionRol(), obj.getDescripcionRol());
            }
        }

        List<Ciudades> listCiudades = usuarioBO.listCiudades();
        if (listCiudades != null) {
            mapaCiudad = new HashMap();
            for (Ciudades obj : listCiudades) {
                mapaCiudad.put(obj.getNombreCiudad(), obj.getIdCiudad());
            }
        }

        List<UnidadesEjecutoras> listUnidadesEjecutoras = usuarioBO.listUnidadesEjecutoras();
        if (listUnidadesEjecutoras != null) {
            mapaUnidadEjecutora = new HashMap();
            for (UnidadesEjecutoras obj : listUnidadesEjecutoras) {
                mapaUnidadEjecutora.put(obj.getNombreUnidad(), obj.getCodigo());
            }
        }

        llenarListaUsuarios();
    }

    public void cerrarDialogo() {
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        String realPath = (String) servletContext.getRealPath("/"); // Sustituye "/" por el directorio ej: "/upload"
        String urld = realPath + "\\recursos\\imagenes\\otras\\usuario.jpg";
        if (!this.urlFoto.equalsIgnoreCase(urld)) {
            SubirArchivoServer borrar = new SubirArchivoServer();
            borrar.borrarArchivo(this.urlFoto);
        }
        limpiar();
        this.urlFoto = urld;
        RequestContext.getCurrentInstance().update("formularioUsuarioUpdate");
        RequestContext.getCurrentInstance().update("formularioUsuarioInsert");
        RequestContext.getCurrentInstance().execute("PF('agregarUsu').hide();");
        RequestContext.getCurrentInstance().update("panelT");
    }

    public void cerrarDialogo2() {
        if (auxImagenSubirUpdate == false && auxImagenSubirUpdate2) {
            if (!this.urlFoto.equalsIgnoreCase(urlOriginal)) {
                SubirArchivoServer borrar = new SubirArchivoServer();
                borrar.borrarArchivo(this.urlFoto);
            }
        }
        this.urlFoto = this.urlOriginal;
        auxImagenSubirUpdate = false;
        auxImagenSubirUpdate2 = false;
        RequestContext.getCurrentInstance().update("formularioUsuarioUpdate");
        RequestContext.getCurrentInstance().execute("PF('editarUsu').hide();");
        RequestContext.getCurrentInstance().update("panelT");
    }

    public void limpiar() {
        nombre = null;
        telefono = null;
        apellido = null;
        email = null;
        usuario = null;
        clave = null;
        rol = null;
        ciudad = null;
        auxImagenSubir = false;
    }

    public void llenarListaUsuarios() {
        try {
            setListUsuarioAll(usuarioBO.listUsuarioAll());
        } catch (Exception e) {
            System.err.println(e);
        }
    }
    /**
     * ***************************************************************************************
     */
    /**
     * ****************************************** EXCEL
     * **************************************
     */
    /**
     * ***************************************************************************************
     */

//    public void getReportData() throws IOException {
//        llenarListaUsuarios();
//        HSSFWorkbook workbook = new HSSFWorkbook();
//        HSSFSheet sheet = workbook.createSheet("CENSO");
//        
//        
//        sheet.setColumnWidth(0, 256*20);
//        sheet.setColumnWidth(1, 256*10);
//        sheet.setColumnWidth(2, 256*100);
//        
//        int numFilas = 1;
//        HSSFRow row = sheet.createRow(numFilas);
//        HSSFCell cell = row.createCell(0);
//        cell.setCellValue("REPORTE ");
//        HSSFCell cell2 = row.createCell(1);
//        cell2.setCellValue(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
//        
////        llenarCelda(numFilas, 0, sheet, "REPORTE ",new SimpleDateFormat("dd/MM/yyyy").format(new Date()),workbook);
//        numFilas++;
//        llenarCelda(numFilas, 0, sheet, "PACIENTES EN UCI :","",workbook);
//        numFilas++;
//        llenarCelda(numFilas, 0, sheet, "PACIENTES EN UCIN :","",workbook);
//        numFilas = numFilas +2;
//        
//        for (UsuarioBean obj : getListUsuarioAll()) {
//            for(int i=1;i<9;i++){
//                switch(i){
//                    case 1:
//                        llenarCelda(numFilas, 0, sheet, "HABITACION :",obj.getTelefono(),workbook);
//                    break;
//                    case 2:
//                        llenarCelda(numFilas, 0, sheet, "NOMBRE :",obj.getNombre(),workbook);
//                    break;
//                    case 3:
//                        llenarCelda(numFilas, 0, sheet, "FECHA DE INGRESO :",obj.getNombreRol(),workbook);
//                    break;
//                    case 4:
//                        llenarCelda(numFilas, 0, sheet, "FECHA DE EGRESO :",obj.getEmail(),workbook);
//                    break;
//                    case 5:
//                        llenarCelda(numFilas, 0, sheet, "RUTA DE INGRESO :",obj.getUrlFoto(),workbook);                        
//                    break;
//                    case 6:
//                        llenarCelda(numFilas, 0, sheet, "MEDICO TRATANTE :",obj.getApellido(),workbook);
//                    break;
//                    case 7:
//                        llenarCelda(numFilas, 0, sheet, "DIAGNOSTICO :",obj.getClave(),workbook);
//                    break;
//                    case 8:
//                        llenarCelda(numFilas, 0, sheet, "CIRUGIA :",obj.getClave(),workbook);
//                    break;
//                }
//                numFilas++;
//                
//            }
//            
//            numFilas = numFilas + 2;
//        }
////        HSSFRow row = sheet.createRow(1);
////        HSSFCell cell = row.createCell(0);
////        cell.setCellValue(1.1);
//        
//        
//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        ExternalContext externalContext = facesContext.getExternalContext();
//        externalContext.setResponseContentType("application/vnd.ms-excel");
//        externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"CENSO_"+new SimpleDateFormat("dd/MM/yyyy").format(new Date())+ ".xls\"");
//
//        workbook.write(externalContext.getResponseOutputStream());
//        facesContext.responseComplete();
//    }
//
//    public void llenarCelda(int numFilas,int numColumnas,HSSFSheet sheet,String value,String value2,HSSFWorkbook workbook) {  
//        HSSFRow row = sheet.createRow(numFilas);
//        row.createCell(0).setCellValue(value);
//        HSSFCell cell2 = row.createCell(1);
//        cell2.setCellValue(value2);
//        HSSFCellStyle estiloCelda = workbook.createCellStyle();
//        estiloCelda.setWrapText(true);
//        cell2.setCellStyle(estiloCelda);
//        sheet.addMergedRegion(new CellRangeAddress(numFilas,numFilas,1,2));
//    }

    /**
     * *******************************************************************************
     */
    /**
     * ********************************************************************************
     */
    /**
     * *******************************************************************************
     */
    public void subirFoto(FileUploadEvent event) {
        SubirArchivoServer subir = new SubirArchivoServer();
        if (auxImagenSubir == false) {
            this.urlFoto = subir.SubirArchivo(event, "/SigefiDocumentos/fotoUsuarios/", "temporal");

        } else {
            auxImagenSubir = true;
            subir.borrarArchivo(this.urlFoto);
            this.urlFoto = subir.SubirArchivo(event, "/SigefiDocumentos/fotoUsuarios/", "temporal");
        }
        RequestContext.getCurrentInstance().update("formularioUsuarioInsert:fotoImg");
    }

    public void modificarFoto(FileUploadEvent event) {
        SubirArchivoServer subir = new SubirArchivoServer();
        if (this.urlFoto.equalsIgnoreCase(urlOriginal)) {
            this.urlFoto = subir.SubirArchivo(event, "/SigefiDocumentos/fotoUsuarios/", this.usuario);
            auxImagenSubirUpdate2 = true;
        } else {
            subir.borrarArchivo(this.urlFoto);
            this.urlFoto = subir.SubirArchivo(event, "/SigefiDocumentos/fotoUsuarios/", this.usuario);
            auxImagenSubirUpdate2 = true;
        }
        RequestContext.getCurrentInstance().update("formularioUsuarioUpdate:fotoImg2");
    }

    private String filename;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void oncapture(CaptureEvent captureEvent) {
        filename = usuario;
        byte[] data = captureEvent.getData();

//        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
//        String newFileName = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "demo" +
//                                    File.separator + "images" + File.separator + "photocam" + File.separator + filename + ".jpeg";
        String newFileName = "/SigefiDocumentos/fotoUsuarios/" + filename + ".jpg";

        FileImageOutputStream imageOutput;
        try {
            imageOutput = new FileImageOutputStream(new File(newFileName));
            imageOutput.write(data, 0, data.length);
            imageOutput.close();
        } catch (IOException e) {
            throw new FacesException("Error in writing captured image.", e);
        }
    }

    public boolean isAuxImagenSubir() {
        return auxImagenSubir;
    }

    public void setAuxImagenSubir(boolean auxImagenSubir) {
        this.auxImagenSubir = auxImagenSubir;
    }

    public String getUrlOriginal() {
        return urlOriginal;
    }

    public void setUrlOriginal(String urlOriginal) {
        this.urlOriginal = urlOriginal;
    }

    public boolean isAuxImagenSubirUpdate() {
        return auxImagenSubirUpdate;
    }

    public void setAuxImagenSubirUpdate(boolean auxImagenSubirUpdate) {
        this.auxImagenSubirUpdate = auxImagenSubirUpdate;
    }

    public Short getEstado() {
        return estado;
    }

    public void setEstado(Short estado) {
        this.estado = estado;
    }

    public Short getIdUnidadEjecutora() {
        return idUnidadEjecutora;
    }

    public void setIdUnidadEjecutora(Short idUnidadEjecutora) {
        this.idUnidadEjecutora = idUnidadEjecutora;
    }

    public Map<String, Short> getMapaUnidadEjecutora() {
        return mapaUnidadEjecutora;
    }

    public void setMapaUnidadEjecutora(Map<String, Short> mapaUnidadEjecutora) {
        this.mapaUnidadEjecutora = mapaUnidadEjecutora;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public Short getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Short idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public Map<String, Short> getMapaRol() {
        return mapaRol;
    }

    public void setMapaRol(Map<String, Short> mapaRol) {
        this.mapaRol = mapaRol;
    }

    public Short getRol() {
        return rol;
    }

    public void setRol(Short rol) {
        this.rol = rol;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public UsuarioBO getUsuarioBO() {
        return usuarioBO;
    }

    public void setUsuarioBO(UsuarioBO usuarioBO) {
        this.usuarioBO = usuarioBO;
    }

    public List<UsuarioBean> getListUsuarioAll() {
        return listUsuarioAll;
    }

    public void setListUsuarioAll(List<UsuarioBean> listUsuarioAll) {
        this.listUsuarioAll = listUsuarioAll;
    }

    public String getNombreRol() {
        return nombreRol;
    }

    public void setNombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
    }

    public List<UsuarioBean> getFilterListUsuarioAll() {
        return filterListUsuarioAll;
    }

    public void setFilterListUsuarioAll(List<UsuarioBean> filterListUsuarioAll) {
        this.filterListUsuarioAll = filterListUsuarioAll;
    }

    public Map<String, String> getMapaRolNombre() {
        return mapaRolNombre;
    }

    public void setMapaRolNombre(Map<String, String> mapaRolNombre) {
        this.mapaRolNombre = mapaRolNombre;
    }

    public Short getCiudad() {
        return ciudad;
    }

    public void setCiudad(Short ciudad) {
        this.ciudad = ciudad;
    }

    public Map<String, Short> getMapaCiudad() {
        return mapaCiudad;
    }

    public void setMapaCiudad(Map<String, Short> mapaCiudad) {
        this.mapaCiudad = mapaCiudad;
    }

}
