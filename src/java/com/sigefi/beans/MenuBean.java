/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@SessionScoped
public class MenuBean implements Serializable{

    private List<String> pruebaList;
    private Map<String,String> pruebaMap;
    
    public List<String> getPruebaList() {
        return pruebaList;
    }

    public void setPruebaList(List<String> pruebaList) {
        this.pruebaList = pruebaList;
    }

    public Map<String, String> getPruebaMap() {
        return pruebaMap;
    }

    public void setPruebaMap(Map<String, String> pruebaMap) {
        this.pruebaMap = pruebaMap;
    }

    
    
    
    /**
     * Creates a new instance of MenuBean
     */
    public MenuBean() {
        pruebaList = new ArrayList<String>();
        pruebaList.add("uno");
        pruebaList.add("dos");
        pruebaList.add("tres");
        
        pruebaMap = new HashMap<>();
        
        pruebaMap.put("Uno", "1");
        pruebaMap.put("Dos", "2");
        pruebaMap.put("Tres", "3");
        pruebaMap.put("Cuatro", "4");
        
    }
    
}
