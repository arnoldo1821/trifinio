/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.FuenteFinanciamientoBO;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Elmer
 */
@ManagedBean
@ViewScoped
public class FuenteFinanciamientoBean implements Serializable {

    @ManagedProperty(name = "fuenteFinanciamientoBO", value = "#{fuenteFinanciamientoBO}")
    private FuenteFinanciamientoBO fuenteFinanciamientoBO;

    private Integer codigo;
    private String nombreFuente;
 
    private List<FuenteFinanciamientoBean> listFuenteFinanciamiento;
     private List<FuenteFinanciamientoBean> filtroFuenteFinanciamiento;
    private FuenteFinanciamientoBean selectFuente;

    /**
     * Creates a new instance of CentroCostoBean
     */
    public FuenteFinanciamientoBean() {
    }

    @PostConstruct
    public void main() {
       llenarLista();
    }

    public void llenarLista() {
        List<FuenteFinanciamientoBean> listfuenteF = fuenteFinanciamientoBO.listFuenteFinanciamientoAll();
        if (listfuenteF != null) {
            setListFuenteFinanciamiento(listfuenteF);
        }
    }

    public void limpiar() {
        nombreFuente = null;
        RequestContext.getCurrentInstance().update("formFuenteFinanciamiento");
        RequestContext.getCurrentInstance().update("form");
    }

    public FuenteFinanciamientoBO getFuenteFinanciamientoBO() {
        return fuenteFinanciamientoBO;
    }

    public void insertFuenteFinanciamiento() {
        if (fuenteFinanciamientoBO.insertFuenteFinanciamiento(this)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Fuente de financiamiento agregada correctamente", ""));
            llenarLista();
            limpiar();
        }
        RequestContext.getCurrentInstance().update("formFuenteFinanciamiento");
        RequestContext.getCurrentInstance().update("form");
    }

    public void deleteFuenteFinanciamiento(FuenteFinanciamientoBean FuenteF) {
        this.codigo = FuenteF.getCodigo();
        this.nombreFuente = FuenteF.getNombreFuente();
  
        if (!nombreFuente.isEmpty()) {
            if (fuenteFinanciamientoBO.deleteFuenteFinanciamiento(FuenteF)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Fuente de financiamiento eliminada correctamente", ""));
                llenarLista();
                limpiar();
            }
        }
        RequestContext.getCurrentInstance().update("formFuenteFinanciamiento");
    }

    public void updateFuenteFinanciamiento(FuenteFinanciamientoBean FuenteF) {
        this.codigo = FuenteF.getCodigo();
        this.nombreFuente = FuenteF.getNombreFuente();

        if (!nombreFuente.isEmpty()) {
            if (fuenteFinanciamientoBO.updateFuenteFinanciamiento(FuenteF)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Fuente de financiamiento actualizada correctamente", ""));
                llenarLista();
                limpiar();
            }
        }
        RequestContext.getCurrentInstance().update("formFuenteFinanciamiento");
    }

    public void actualizarFuenteFinanciamiento(RowEditEvent event) {
        if (fuenteFinanciamientoBO.updateFuenteFinanciamiento((FuenteFinanciamientoBean) event.getObject())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Registro actualizado correctamente "));
        }
    }

    public void cancelarActualizarFuenteFinanciamiento(RowEditEvent event) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Operacion Cancelada"));
    }
    
    public void setFuenteFinanciamientoBO(FuenteFinanciamientoBO fuenteFinanciamientoBO) {
        this.fuenteFinanciamientoBO = fuenteFinanciamientoBO;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNombreFuente() {
        return nombreFuente;
    }

    public void setNombreFuente(String nombreFuente) {
        this.nombreFuente = nombreFuente;
    }

    public List<FuenteFinanciamientoBean> getListFuenteFinanciamiento() {
        return listFuenteFinanciamiento;
    }

    public void setListFuenteFinanciamiento(List<FuenteFinanciamientoBean> listFuenteFinanciamiento) {
        this.listFuenteFinanciamiento = listFuenteFinanciamiento;
    }

    public List<FuenteFinanciamientoBean> getFiltroFuenteFinanciamiento() {
        return filtroFuenteFinanciamiento;
    }

    public void setFiltroFuenteFinanciamiento(List<FuenteFinanciamientoBean> filtroFuenteFinanciamiento) {
        this.filtroFuenteFinanciamiento = filtroFuenteFinanciamiento;
    }

    public FuenteFinanciamientoBean getSelectFuente() {
        return selectFuente;
    }

    public void setSelectFuente(FuenteFinanciamientoBean selectFuente) {
        this.selectFuente = selectFuente;
    }
    
}
