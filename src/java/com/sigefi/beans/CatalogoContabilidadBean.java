/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.CatalogoContabilidadBO;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.model.TreeNode;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class CatalogoContabilidadBean implements Serializable{

    @ManagedProperty(value = "#{catalogoContabilidadBO}")
    private CatalogoContabilidadBO catalogoContabilidadBO;
    
    private String nombreCuenta;
    private String descripcionCuenta;

    private String nombreSubCuenta;
    private String descripcionSubCuenta;
    private String idCuenta;

    private TreeNode root;
    
    private short tipo;
    private short naturaleza;
    
    public CatalogoContabilidadBO getCatalogoContabilidadBO() {
        return catalogoContabilidadBO;
    }

    public void setCatalogoContabilidadBO(CatalogoContabilidadBO catalogoContabilidadBO) {
        this.catalogoContabilidadBO = catalogoContabilidadBO;
    }
    
    public CatalogoContabilidadBean() {
    }
    
    @PostConstruct
    public void main() {
        llenarTablaNodoCuentasCatalogo();
    }

    public void llenarTablaNodoCuentasCatalogo() {
        root = catalogoContabilidadBO.listNodoCuentasCatalogo();
        expandirAll(root);
    }
    
    private void expandirAll(TreeNode treeNode) {
        if (treeNode.getChildren().size() > 0) {
            treeNode.setExpanded(true);
            for (int i = 0; i < treeNode.getChildren().size(); i++) {
                expandirAll(treeNode.getChildren().get(i));
            }
        }
    }
    
    
    public void insertCuenta() {
        if (catalogoContabilidadBO.insertCuenta(this)) {
            llenarTablaNodoCuentasCatalogo();
            limpiar();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cuenta Agregada Correctamente", ""));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Cuenta Agregada No Agregada ya no hay espacio pa cuentas del nivel 1", ""));
            RequestContext.getCurrentInstance().update("catalogoCuenta");
        }
    }


    public void cuentaSeleccionada(String idCuenta) {
//        this.idCuenta = selecCatal.getId().getIdCuenta();
        this.idCuenta = idCuenta;
    }

    public void insertSubCuenta() {
        if (Integer.valueOf(idCuenta) > 0) {
            if (catalogoContabilidadBO.insertSubCuenta(this)) {
                llenarTablaNodoCuentasCatalogo();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cuenta Agregada Correctamente", ""));
                limpiar();
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Cuenta Agregada No Agregada ya no hay espacio pa cuentas del nivel", ""));
                RequestContext.getCurrentInstance().update("formSubCuenta");
            }
        }
    }
    
    
    public void limpiar() {
        idCuenta = null; 
        nombreCuenta = null;
        nombreSubCuenta = null;
        descripcionCuenta = null;
        descripcionSubCuenta = null;
        naturaleza = 0;
        tipo = 0;
        RequestContext.getCurrentInstance().update("catalogoCuenta");
        RequestContext.getCurrentInstance().update("formSubCuenta");
    }

    public short getTipo() {
        return tipo;
    }

    public void setTipo(short tipo) {
        this.tipo = tipo;
    }

    public short getNaturaleza() {
        return naturaleza;
    }

    public void setNaturaleza(short naturaleza) {
        this.naturaleza = naturaleza;
    }
    
    public String getNombreCuenta() {
        return nombreCuenta;
    }

    public void setNombreCuenta(String nombreCuenta) {
        this.nombreCuenta = nombreCuenta;
    }

    public String getDescripcionCuenta() {
        return descripcionCuenta;
    }

    public void setDescripcionCuenta(String descripcionCuenta) {
        this.descripcionCuenta = descripcionCuenta;
    }

    public String getNombreSubCuenta() {
        return nombreSubCuenta;
    }

    public void setNombreSubCuenta(String nombreSubCuenta) {
        this.nombreSubCuenta = nombreSubCuenta;
    }

    public String getDescripcionSubCuenta() {
        return descripcionSubCuenta;
    }

    public void setDescripcionSubCuenta(String descripcionSubCuenta) {
        this.descripcionSubCuenta = descripcionSubCuenta;
    }

    public String getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(String idCuenta) {
        this.idCuenta = idCuenta;
    }

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }
    
    
}
