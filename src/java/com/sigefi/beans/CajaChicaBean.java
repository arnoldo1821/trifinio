/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.CajaChicaBO;
import com.sigefi.bo.PresupuestoBO;
import com.sigefi.bo.RequisicionBO;
import com.sigefi.clases.FechaSistema;
import com.sigefi.entity.DetalleCuentas;
import com.sigefi.entity.DetalleLiquidarCajaChica;
import com.sigefi.entity.LiquidarCajaChica;
import com.sigefi.entity.PartesInteresadas;
import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.UnidadesEjecutoras;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class CajaChicaBean implements Serializable{

    @ManagedProperty(value = "#{cajaChicaBO}")
    private CajaChicaBO cajaChicaBO;
    
    @ManagedProperty(name = "presupuestoBO", value = "#{presupuestoBO}")
    private PresupuestoBO presupuestoBO;
    
    
    /*SESSION*/
    private short idunidadEjecutora;
    private String nombreUnidadEjecutora;
    private String nomUsuario;
    private String usuario;
    
    private short unidadesEjecutoras;
    private Map<String, Short> mapaUnidadEjecutora;
    private int anyoPresupuesto;
    private List<Short> listAnios;
    
    /*PASO 2*/
    private String codigoRequerimiento;
    private Character moneda;
    private Date fechaPresentacion;
    private String nomProveedor;
    private String codigoProveedor;
    private Map<String, String> mapProveedor;
    private Map<String, String> mapProveedor2;
    private String concepto;
    
    /*PASO 3*/
    private short correlativo;
    private Date fechaDocumento;
    private String claseDocumento;
    private String numeroDocumento;
    private int detalleCuenta;
    private Map<String, Integer> mapDetalleCuenta;
    private Map<Integer, String> mapDetalleCuenta2;
    private String nombreGastoEspecifico;
    private BigDecimal totalPagar;
    private BigDecimal sumTotal;
    
    private boolean estado1;
    private boolean estado2;
    private boolean estado3;
    
    private int presupuesto;
    
    private int pkLiquiCaja;
    private LiquidarCajaChica liqCajaAux;
    
    private List<DetalleLiquidarCajaChica> listDetalleRequeCajaC;
    
    public CajaChicaBean() {
        SessionBean referenciaBeanSession
                = (SessionBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("sessionBean");

        usuario = referenciaBeanSession.getUsu();
        nomUsuario = referenciaBeanSession.getNombreUsuario();
        idunidadEjecutora = referenciaBeanSession.getIdUnidadEjecutora();
        nombreUnidadEjecutora = referenciaBeanSession.getUnidadEjecutora();
        estado1 = false;
        estado2 = true;
        estado3 = true;
    }
    
    @PostConstruct
    public void main(){
        List<PartesInteresadas> listProv = cajaChicaBO.listProveedorAll();
        if (listProv != null) {
            mapProveedor = new HashMap<>();
            mapProveedor2 = new HashMap<>();
            for (PartesInteresadas prove : listProv) {
                mapProveedor.put(prove.getNombreProveedor(), prove.getId().getCodigoProveedor());
                mapProveedor2.put(prove.getId().getCodigoProveedor(),prove.getNombreProveedor());
            }
        }

        List<UnidadesEjecutoras> listUnidadesEjecutoras = presupuestoBO.listUnidadesEjecutoras();
        if (listUnidadesEjecutoras != null) {
            mapaUnidadEjecutora = new HashMap();
            for (UnidadesEjecutoras obj : listUnidadesEjecutoras) {
                mapaUnidadEjecutora.put(obj.getNombreUnidad(), obj.getCodigo());
            }
        }

        FechaSistema anioHoy = new FechaSistema();
        short anio = (short) (anioHoy.getAnio() - 1);
        listAnios = new ArrayList<>();
        for (int i = 0; i <= 3; i++) {
            listAnios.add(anio);
            anio = (short) (anio + 1);
        }
    }
    
    /*PASO1*/
    public void buscarCuentasPre() {
        Presupuestos pre = cajaChicaBO.elementoPresupuesto(this);
        if (pre != null) {
            presupuesto = pre.getCodigoPresupuesto();
            estado1 = true;
            estado2 = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Presupuesto Seleccionado", ""));
            RequestContext.getCurrentInstance().update("formCajaChica1");
            RequestContext.getCurrentInstance().update("formCajaChica2");
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "No hay presupuesto para la Unidad Ejecutora y año seleccionado ", ""));
        }
    }
    
    /*PASO 2*/
    public void inserRequerimientoCaja() {
        if(mapProveedor2 != null){
            if(mapProveedor2.size()>0){
                this.nomProveedor = mapProveedor2.get(this.codigoProveedor);
            }
        }
         liqCajaAux = cajaChicaBO.insertLiquidacionCajaChica(this);
        if (liqCajaAux != null) {
            this.pkLiquiCaja =liqCajaAux.getPkIncreRequeCaja();
            llenarDetalleReqCajaC();
            presupuestoSeleccionado();
            estado2 = true;
            estado3 = false;
            RequestContext.getCurrentInstance().update("formCajaChica2");
            RequestContext.getCurrentInstance().update("formCajaChica3");
        }
    }
    
    public void llenarDetalleReqCajaC(){
        if(pkLiquiCaja >0){
        List<DetalleLiquidarCajaChica> listObj = cajaChicaBO.correlativo_Total_DetalleRequerimientoCaja(pkLiquiCaja);
            if (listObj != null) {
                correlativo = (short) (listObj.size()+1);
                sumTotal = BigDecimal.ZERO;
                for(DetalleLiquidarCajaChica objRe : listObj){
                    sumTotal = sumTotal.add(objRe.getMonto());
                }
                setListDetalleRequeCajaC(listObj);
            }else{
                correlativo = 1;
                sumTotal = BigDecimal.ZERO;
            }
        }
            
    }
    
    public void presupuestoSeleccionado() {
        if (presupuesto > 0) {
            List<DetalleCuentas> listDetaCue = cajaChicaBO.listDetalleCuentasAll(presupuesto);
            if (listDetaCue != null) {
                mapDetalleCuenta = new HashMap<>();
                mapDetalleCuenta2 = new HashMap<>();
                for (DetalleCuentas prove : listDetaCue) {
                    mapDetalleCuenta.put(prove.getCatalogoCuentas().getNombreCuenta(), prove.getId().getCodgioCuenta());
                    mapDetalleCuenta2.put(prove.getId().getCodgioCuenta(), prove.getCatalogoCuentas().getNombreCuenta());
                }
            }
        }
    }
    
    /*PASO 3*/
    public void insertDetalleRequerimientoCaja() {
        if (mapDetalleCuenta2 != null) {
            this.nombreGastoEspecifico = mapDetalleCuenta2.get(this.detalleCuenta);
            if (cajaChicaBO.insertDetalleRequisicionCaja(this)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Requerimiento agregado correctamente", ""));
                llenarDetalleReqCajaC();
                limpiar();
            }
        }
    }
    
    public void limpiar() {
        unidadesEjecutoras = 0;
        anyoPresupuesto = 0;

        fechaPresentacion = null;
        codigoRequerimiento = null;

        fechaDocumento = null;
        claseDocumento = null;
        numeroDocumento = null;
        codigoProveedor = null;
        nomProveedor = null;
        detalleCuenta = 0;
        nombreGastoEspecifico = null;
        
        totalPagar = null;
        concepto = null;
        
        RequestContext.getCurrentInstance().update("formCajaChica3");
    }
    
    public void finalizarRequicionCaja(){
        correlativo = 0;
        estado3 = true;
        estado1 = false;
        sumTotal = null;
        moneda = null;
        if(listDetalleRequeCajaC != null){
            if(listDetalleRequeCajaC.size()>0){
                listDetalleRequeCajaC.clear();
            }
        }
        limpiar();
        RequestContext.getCurrentInstance().update("formCajaChica1");
        RequestContext.getCurrentInstance().update("formCajaChica2");
    }
    
    public CajaChicaBO getCajaChicaBO() {
        return cajaChicaBO;
    }

    public void setCajaChicaBO(CajaChicaBO cajaChicaBO) {
        this.cajaChicaBO = cajaChicaBO;
    }

    public PresupuestoBO getPresupuestoBO() {
        return presupuestoBO;
    }

    public void setPresupuestoBO(PresupuestoBO presupuestoBO) {
        this.presupuestoBO = presupuestoBO;
    }

    public short getIdunidadEjecutora() {
        return idunidadEjecutora;
    }

    public void setIdunidadEjecutora(short idunidadEjecutora) {
        this.idunidadEjecutora = idunidadEjecutora;
    }

    public String getNombreUnidadEjecutora() {
        return nombreUnidadEjecutora;
    }

    public void setNombreUnidadEjecutora(String nombreUnidadEjecutora) {
        this.nombreUnidadEjecutora = nombreUnidadEjecutora;
    }

    public String getNomUsuario() {
        return nomUsuario;
    }

    public void setNomUsuario(String nomUsuario) {
        this.nomUsuario = nomUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Map<String, Short> getMapaUnidadEjecutora() {
        return mapaUnidadEjecutora;
    }

    public void setMapaUnidadEjecutora(Map<String, Short> mapaUnidadEjecutora) {
        this.mapaUnidadEjecutora = mapaUnidadEjecutora;
    }

    public Map<String, String> getMapProveedor() {
        return mapProveedor;
    }

    public void setMapProveedor(Map<String, String> mapProveedor) {
        this.mapProveedor = mapProveedor;
    }

    public List<Short> getListAnios() {
        return listAnios;
    }

    public void setListAnios(List<Short> listAnios) {
        this.listAnios = listAnios;
    }

    public short getUnidadesEjecutoras() {
        return unidadesEjecutoras;
    }

    public void setUnidadesEjecutoras(short unidadesEjecutoras) {
        this.unidadesEjecutoras = unidadesEjecutoras;
    }

    public int getAnyoPresupuesto() {
        return anyoPresupuesto;
    }

    public void setAnyoPresupuesto(int anyoPresupuesto) {
        this.anyoPresupuesto = anyoPresupuesto;
    }

    public String getCodigoRequerimiento() {
        return codigoRequerimiento;
    }

    public void setCodigoRequerimiento(String codigoRequerimiento) {
        this.codigoRequerimiento = codigoRequerimiento;
    }

    public Character getMoneda() {
        return moneda;
    }

    public void setMoneda(Character moneda) {
        this.moneda = moneda;
    }

    public Date getFechaPresentacion() {
        return fechaPresentacion;
    }

    public void setFechaPresentacion(Date fechaPresentacion) {
        this.fechaPresentacion = fechaPresentacion;
    }

    public String getNomProveedor() {
        return nomProveedor;
    }

    public void setNomProveedor(String nomProveedor) {
        this.nomProveedor = nomProveedor;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public short getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(short correlativo) {
        this.correlativo = correlativo;
    }

    public Date getFechaDocumento() {
        return fechaDocumento;
    }

    public void setFechaDocumento(Date fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    public String getClaseDocumento() {
        return claseDocumento;
    }

    public void setClaseDocumento(String claseDocumento) {
        this.claseDocumento = claseDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getNombreGastoEspecifico() {
        return nombreGastoEspecifico;
    }

    public void setNombreGastoEspecifico(String nombreGastoEspecifico) {
        this.nombreGastoEspecifico = nombreGastoEspecifico;
    }

    public BigDecimal getTotalPagar() {
        return totalPagar;
    }

    public void setTotalPagar(BigDecimal totalPagar) {
        this.totalPagar = totalPagar;
    }

    public boolean isEstado1() {
        return estado1;
    }

    public void setEstado1(boolean estado1) {
        this.estado1 = estado1;
    }

    public boolean isEstado2() {
        return estado2;
    }

    public void setEstado2(boolean estado2) {
        this.estado2 = estado2;
    }

    public boolean isEstado3() {
        return estado3;
    }

    public void setEstado3(boolean estado3) {
        this.estado3 = estado3;
    }

    public int getDetalleCuenta() {
        return detalleCuenta;
    }

    public void setDetalleCuenta(int detalleCuenta) {
        this.detalleCuenta = detalleCuenta;
    }

    public Map<String, Integer> getMapDetalleCuenta() {
        return mapDetalleCuenta;
    }

    public void setMapDetalleCuenta(Map<String, Integer> mapDetalleCuenta) {
        this.mapDetalleCuenta = mapDetalleCuenta;
    }

    public Map<Integer, String> getMapDetalleCuenta2() {
        return mapDetalleCuenta2;
    }

    public void setMapDetalleCuenta2(Map<Integer, String> mapDetalleCuenta2) {
        this.mapDetalleCuenta2 = mapDetalleCuenta2;
    }

    public BigDecimal getSumTotal() {
        return sumTotal;
    }

    public void setSumTotal(BigDecimal sumTotal) {
        this.sumTotal = sumTotal;
    }

    public int getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(int presupuesto) {
        this.presupuesto = presupuesto;
    }

    public int getPkLiquiCaja() {
        return pkLiquiCaja;
    }

    public void setPkLiquiCaja(int pkLiquiCaja) {
        this.pkLiquiCaja = pkLiquiCaja;
    }

    public List<DetalleLiquidarCajaChica> getListDetalleRequeCajaC() {
        return listDetalleRequeCajaC;
    }

    public void setListDetalleRequeCajaC(List<DetalleLiquidarCajaChica> listDetalleRequeCajaC) {
        this.listDetalleRequeCajaC = listDetalleRequeCajaC;
    }

    public LiquidarCajaChica getLiqCajaAux() {
        return liqCajaAux;
    }

    public void setLiqCajaAux(LiquidarCajaChica liqCajaAux) {
        this.liqCajaAux = liqCajaAux;
    }

    public String getCodigoProveedor() {
        return codigoProveedor;
    }

    public void setCodigoProveedor(String codigoProveedor) {
        this.codigoProveedor = codigoProveedor;
    }

    public Map<String, String> getMapProveedor2() {
        return mapProveedor2;
    }

    public void setMapProveedor2(Map<String, String> mapProveedor2) {
        this.mapProveedor2 = mapProveedor2;
    }
    
}
