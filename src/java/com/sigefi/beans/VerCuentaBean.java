/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.PresupuestoBO;
import com.sigefi.bo.VerCuentaBO;
import com.sigefi.clases.FechaSistema;
import com.sigefi.entity.Empresa;
import com.sigefi.entity.UnidadesEjecutoras;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class VerCuentaBean implements Serializable{

    @ManagedProperty(value = "#{verCuentaBO}")
    private VerCuentaBO verCuentaBO;
    
    
    private short unidadesEjecutoras;
    private int anio;
    private boolean estado1;
    private Empresa empSelecc;
    
    
    public VerCuentaBean() {
        estado1 = false;
    }
    
    @PostConstruct
    public void main() {
       
    }
    
    public void consultarEmpresa(){
        Empresa emp = verCuentaBO.consultarEmpresa(unidadesEjecutoras, anio);
        if(emp == null){
            estado1  = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Empresa no encontrada", ""));
        }else{
            estado1 = true;
            empSelecc = emp;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Empresa Seleccionada correctamente", ""));
        }
    }
    
    public VerCuentaBO getVerCuentaBO() {
        return verCuentaBO;
    }

    public void setVerCuentaBO(VerCuentaBO verCuentaBO) {
        this.verCuentaBO = verCuentaBO;
    }

    public short getUnidadesEjecutoras() {
        return unidadesEjecutoras;
    }

    public void setUnidadesEjecutoras(short unidadesEjecutoras) {
        this.unidadesEjecutoras = unidadesEjecutoras;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public boolean isEstado1() {
        return estado1;
    }

    public void setEstado1(boolean estado1) {
        this.estado1 = estado1;
    }

    public Empresa getEmpSelecc() {
        return empSelecc;
    }

    public void setEmpSelecc(Empresa empSelecc) {
        this.empSelecc = empSelecc;
    }
    
}
