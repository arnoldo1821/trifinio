/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.ValidarRequerimientosTesoreriaBO;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.DocumentosRequerimientos;
import com.sigefi.entity.NoConformidades;
import com.sigefi.entity.RequerimientoPagos;
import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class ValidarRequerimientosTesoreriaBean implements Serializable{

    @ManagedProperty(value = "#{validarRequerimientosTesoreriaBO}")
    private ValidarRequerimientosTesoreriaBO validarRequerimientosTesoreriaBO;
    
    private List<RequerimientoPagos> listAllReque;
    private List<DetalleRequerimientoPago> listAllDetalleRequeSelec;
    private List<DocumentosRequerimientos> listAllDocRequeSelec;
    private List<DetalleRequerimientoPago> detalleAllRequerimiento;
    private List<DetalleRequerimientoPago> filtrodetalleRequerimiento;
    private String nomUnidadEjecutora;
    private Date fechaPresentacion;
    private Date fechaEstimacion;
    private String codigoReq;
    private Character moneda; 
    
    private int incre_req_px;
 
    private String url;
    
    
    private boolean validacion;
    private boolean estadoValidacion;
    
    private RequerimientoPagos reqSelecc;
    private String UsuarioLogeado;
    private String observaciones;
    
    private Map<String,Short> mapErrores;
    private short errorSeleccionado;
    
    public ValidarRequerimientosTesoreriaBean() {   
        validacion = true;
        SessionBean referenciaBeanSession = (SessionBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("sessionBean");
        UsuarioLogeado = referenciaBeanSession.getNombreUsuario();
    }
    
    @PostConstruct
    public void main() {
        llenarTabla();
        List<NoConformidades> listAllErrores = validarRequerimientosTesoreriaBO.listAllErrores();
        if(listAllErrores != null){
            mapErrores = new HashMap<>();
            for(NoConformidades objError : listAllErrores){
                mapErrores.put(objError.getDescripcion(),objError.getCodigo());
            }
        }
        estadoValidacion = true;
    }
    
    public void llenarTabla(){
        setListAllReque(validarRequerimientosTesoreriaBO.listAllRequePago());
        setDetalleAllRequerimiento(validarRequerimientosTesoreriaBO.listAllDetalleRequerimiento());
    }
    
    public void requeSelecc(DetalleRequerimientoPago obj){
        RequerimientoPagos req = obj.getRequerimientoPagos();
        nomUnidadEjecutora = req.getUnidadesEjecutoras().getNombreUnidad();
        fechaPresentacion = req.getFechaPresentacion();
        fechaEstimacion = req.getFechaEstimadaPago();
        codigoReq = req.getCodigoRequerimiento();
        moneda = req.getMoneda();
        incre_req_px = req.getIncrReqPk();
        setListAllDetalleRequeSelec(validarRequerimientosTesoreriaBO.listDetalleRequePagoSelec(incre_req_px));
        setListAllDocRequeSelec(validarRequerimientosTesoreriaBO.listDocRequePagoSelec(incre_req_px));
        
        this.reqSelecc = req;
        RequestContext.getCurrentInstance().update("fromReque");
    }
    
    public void imagenSelecionada(String url) {
        try {
            setUrl(url);
            File chartFile = new File(getUrl());
            if(chartFile.isFile()){
            if(getUrl().endsWith(".pdf")){
                RequestContext.getCurrentInstance().execute("PF('pdfReque').show();");
                RequestContext.getCurrentInstance().update("fromPdf");
            }else{
                RequestContext.getCurrentInstance().execute("PF('imgReque').show();");
                RequestContext.getCurrentInstance().update("fromImagen");
            }     
            }else{
                ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
                String realPath = (String) servletContext.getRealPath("/"); // Sustituye "/" por el directorio ej: "/upload"
                setUrl(realPath + "\\recursos\\imagenes\\otras\\NoHayDocumento.jpg");
                RequestContext.getCurrentInstance().execute("PF('imgReque').show();");
                RequestContext.getCurrentInstance().update("fromImagen");
            }

        } catch (Exception e) {
        }
    }
    
    public void validacionSeleccionada(){
        if(validacion){
            estadoValidacion = true;
        }else{
            estadoValidacion = false;
        }
    }
    
    public void validarRequerimiento(){
        validarRequerimientosTesoreriaBO.validarRequerimiento(reqSelecc,this);
        llenarTabla();
        RequestContext.getCurrentInstance().update("formReqPrin");
    }

    public ValidarRequerimientosTesoreriaBO getValidarRequerimientosTesoreriaBO() {
        return validarRequerimientosTesoreriaBO;
    }

    public void setValidarRequerimientosTesoreriaBO(ValidarRequerimientosTesoreriaBO validarRequerimientosTesoreriaBO) {
        this.validarRequerimientosTesoreriaBO = validarRequerimientosTesoreriaBO;
    }

    public List<RequerimientoPagos> getListAllReque() {
        return listAllReque;
    }

    public void setListAllReque(List<RequerimientoPagos> listAllReque) {
        this.listAllReque = listAllReque;
    }

    public Date getFechaPresentacion() {
        return fechaPresentacion;
    }

    public void setFechaPresentacion(Date fechaPresentacion) {
        this.fechaPresentacion = fechaPresentacion;
    }

    public Date getFechaEstimacion() {
        return fechaEstimacion;
    }

    public void setFechaEstimacion(Date fechaEstimacion) {
        this.fechaEstimacion = fechaEstimacion;
    }

    public String getCodigoReq() {
        return codigoReq;
    }

    public void setCodigoReq(String codigoReq) {
        this.codigoReq = codigoReq;
    }

    public Character getMoneda() {
        return moneda;
    }

    public void setMoneda(Character moneda) {
        this.moneda = moneda;
    }

    public int getIncre_req_px() {
        return incre_req_px;
    }

    public void setIncre_req_px(int incre_req_px) {
        this.incre_req_px = incre_req_px;
    }

    public String getNomUnidadEjecutora() {
        return nomUnidadEjecutora;
    }

    public void setNomUnidadEjecutora(String nomUnidadEjecutora) {
        this.nomUnidadEjecutora = nomUnidadEjecutora;
    }

    public List<DetalleRequerimientoPago> getListAllDetalleRequeSelec() {
        return listAllDetalleRequeSelec;
    }

    public void setListAllDetalleRequeSelec(List<DetalleRequerimientoPago> listAllDetalleRequeSelec) {
        this.listAllDetalleRequeSelec = listAllDetalleRequeSelec;
    }

    public List<DocumentosRequerimientos> getListAllDocRequeSelec() {
        return listAllDocRequeSelec;
    }

    public void setListAllDocRequeSelec(List<DocumentosRequerimientos> listAllDocRequeSelec) {
        this.listAllDocRequeSelec = listAllDocRequeSelec;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isValidacion() {
        return validacion;
    }

    public void setValidacion(boolean validacion) {
        this.validacion = validacion;
    }

    public boolean isEstadoValidacion() {
        return estadoValidacion;
    }

    public void setEstadoValidacion(boolean estadoValidacion) {
        this.estadoValidacion = estadoValidacion;
    }

    public RequerimientoPagos getReqSelecc() {
        return reqSelecc;
    }

    public void setReqSelecc(RequerimientoPagos reqSelecc) {
        this.reqSelecc = reqSelecc;
    }

    public String getUsuarioLogeado() {
        return UsuarioLogeado;
    }

    public void setUsuarioLogeado(String UsuarioLogeado) {
        this.UsuarioLogeado = UsuarioLogeado;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Map<String, Short> getMapErrores() {
        return mapErrores;
    }

    public void setMapErrores(Map<String, Short> mapErrores) {
        this.mapErrores = mapErrores;
    }

    public short getErrorSeleccionado() {
        return errorSeleccionado;
    }

    public void setErrorSeleccionado(short errorSeleccionado) {
        this.errorSeleccionado = errorSeleccionado;
    }

    public List<DetalleRequerimientoPago> getDetalleAllRequerimiento() {
        return detalleAllRequerimiento;
    }

    public void setDetalleAllRequerimiento(List<DetalleRequerimientoPago> detalleAllRequerimiento) {
        this.detalleAllRequerimiento = detalleAllRequerimiento;
    }

    public List<DetalleRequerimientoPago> getFiltrodetalleRequerimiento() {
        return filtrodetalleRequerimiento;
    }

    public void setFiltrodetalleRequerimiento(List<DetalleRequerimientoPago> filtrodetalleRequerimiento) {
        this.filtrodetalleRequerimiento = filtrodetalleRequerimiento;
    }
    
    
}
