
package com.sigefi.beans;

import com.sigefi.bo.ClausulasModeloContratoBO;
import com.sigefi.bo.ModeloContratoBO;
import com.sigefi.entity.ClausulasModeloContrato;
import com.sigefi.entity.ClausulasModeloContratoId;
import com.sigefi.entity.ModeloContrato;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Oscar
 */
@ManagedBean
@ViewScoped
public class ModeloContratoBean implements Serializable{
    
    @ManagedProperty(value="#{modeloContratoBO}")
    private ModeloContratoBO modeloContratoBO;
    
    @ManagedProperty(value="#{clausulasModeloContratoBO}")
    private ClausulasModeloContratoBO clausulasModeloContratoBO;
    
    private ModeloContrato modeloContrato;
    private ModeloContrato modeloContratoSelected;
    private ClausulasModeloContrato clausulasModeloContrato;
    private ClausulasModeloContrato clausulasModeloContratoDlg;
    
    private List<ClausulasModeloContrato> lstClausulasModeloContrato;
    private List<ModeloContrato> lstModeloContrato;
    
    private boolean disableClausula;

    public ModeloContratoBean() {
        modeloContrato = new ModeloContrato();
        modeloContratoSelected = new ModeloContrato();
        
        clausulasModeloContrato = new ClausulasModeloContrato();
        clausulasModeloContrato.setId( new ClausulasModeloContratoId());
        clausulasModeloContrato.setModeloContrato(new ModeloContrato());
        
        clausulasModeloContratoDlg = new ClausulasModeloContrato();
        clausulasModeloContratoDlg.setId( new ClausulasModeloContratoId());
        
        disableClausula = true;
    }
    
    @PostConstruct
    public void init(){
//        lstClausulasModeloContrato = clausulasModeloContratoBO.selectAll(modeloContrato);
        lstModeloContrato = modeloContratoBO.selectAll();
    }
    
    public void guardar(){
        modeloContratoBO.insert(modeloContrato);
        lstModeloContrato.add(modeloContrato);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Modelo de contrato guardado.", ""));
    }
    
    public void guardarClausula(){
        clausulasModeloContrato.getModeloContrato().setCodigoContrato(clausulasModeloContrato.getId().getCodigoContrato());
        clausulasModeloContratoBO.insert(clausulasModeloContrato);
        lstClausulasModeloContrato.add(clausulasModeloContrato);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clausula guardada.", ""));
    }
    
    public void guardarClausulaTerminar(){
        clausulasModeloContratoBO.insert(clausulasModeloContrato);
        lstClausulasModeloContrato.add(clausulasModeloContrato);
        disableClausula = true;
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clausula guardada.", ""));
    }
    
    public void updateClausula(){
        clausulasModeloContratoBO.update(clausulasModeloContratoDlg);
        lstClausulasModeloContrato = clausulasModeloContratoBO.selectAll(clausulasModeloContratoDlg.getModeloContrato());
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Clausula guardada.", ""));
    }
    
    public void limpiar(){
        modeloContrato = new ModeloContrato();
    }
    
    public void limpiarModeloClausula(){
        clausulasModeloContrato = new ClausulasModeloContrato();
        clausulasModeloContrato.setId( new ClausulasModeloContratoId());
        clausulasModeloContrato.setModeloContrato(new ModeloContrato());
    }
    
    public void limpiarDlgModeloClausula(){
        clausulasModeloContratoDlg = new ClausulasModeloContrato();
        clausulasModeloContratoDlg.setId( new ClausulasModeloContratoId());
    }
    
    public void onSelectTipoContrato(){
        disableClausula = false;
        modeloContratoSelected = new ModeloContrato();
        modeloContratoSelected.setCodigoContrato(clausulasModeloContrato.getId().getCodigoContrato());
        lstClausulasModeloContrato = clausulasModeloContratoBO.selectAll(modeloContratoSelected);
    }
    
    public void onSelectClausula(){
        //some code
    }
    
//    GETTER AND SETTER

    public List<ModeloContrato> getLstModeloContrato() {
        return lstModeloContrato;
    }

    public void setLstModeloContrato(List<ModeloContrato> lstModeloContrato) {
        this.lstModeloContrato = lstModeloContrato;
    }

    public ModeloContrato getModeloContratoSelected() {
        return modeloContratoSelected;
    }

    public void setModeloContratoSelected(ModeloContrato modeloContratoSelected) {
        this.modeloContratoSelected = modeloContratoSelected;
    }

    public boolean isDisableClausula() {
        return disableClausula;
    }

    public void setDisableClausula(boolean disableClausula) {
        this.disableClausula = disableClausula;
    }

    public ClausulasModeloContratoBO getClausulasModeloContratoBO() {
        return clausulasModeloContratoBO;
    }

    public void setClausulasModeloContratoBO(ClausulasModeloContratoBO clausulasModeloContratoBO) {
        this.clausulasModeloContratoBO = clausulasModeloContratoBO;
    }

    public List<ClausulasModeloContrato> getLstClausulasModeloContrato() {
        return lstClausulasModeloContrato;
    }

    public ClausulasModeloContrato getClausulasModeloContratoDlg() {
        return clausulasModeloContratoDlg;
    }

    public void setClausulasModeloContratoDlg(ClausulasModeloContrato clausulasModeloContratoDlg) {
        this.clausulasModeloContratoDlg = clausulasModeloContratoDlg;
    }

    public void setLstClausulasModeloContrato(List<ClausulasModeloContrato> lstClausulasModeloContrato) {
        this.lstClausulasModeloContrato = lstClausulasModeloContrato;
    }

    public ClausulasModeloContrato getClausulasModeloContrato() {
        return clausulasModeloContrato;
    }

    public void setClausulasModeloContrato(ClausulasModeloContrato clausulasModeloContrato) {
        this.clausulasModeloContrato = clausulasModeloContrato;
    }

    public ModeloContrato getModeloContrato() {
        return modeloContrato;
    }

    public void setModeloContrato(ModeloContrato modeloContrato) {
        this.modeloContrato = modeloContrato;
    }

    public ModeloContratoBO getModeloContratoBO() {
        return modeloContratoBO;
    }

    public void setModeloContratoBO(ModeloContratoBO modeloContratoBO) {
        this.modeloContratoBO = modeloContratoBO;
    }
    
}
