/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.ProveedorBO;
import com.sigefi.entity.PartesInteresadas;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.model.TreeNode;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class ProveedorBean implements Serializable{

     @ManagedProperty(name = "proveedorBO", value = "#{proveedorBO}")
     private ProveedorBO proveedorBO;
     
     private String codigoProveedor;
     private String paisOrigen;
     private String nombreProveedor;
     private String telefonoProveedor;
     private String faxProveedor;
     private String EMailProveedor;
     private String representante;
     private String tipoDocumento;
     private String direccion;
     private String web;
     private Short metodoPago;
     private String condicionPago;
     private String nombreNuevo;
     private Boolean activo;
     
     
     private TreeNode root;
     private boolean estadoTipoPago;
     private String tipoProveedor;
     private String bien;
     private PartesInteresadas proveedor;
     private PartesInteresadas proveedorSelec;
     private ProveedorBean selectProveedor;
     private List<ProveedorBean> listProveedor;
     private List<ProveedorBean> listaPartesInteresadas;
     private List<ProveedorBean> filtroProveedor;
    public ProveedorBean() {
        estadoTipoPago = true;
    }
    
    public void llenarNodos(){
        root = proveedorBO.listNodoProveedores();
        expandirAll(root);
    }
    
    private void expandirAll(TreeNode treeNode) {
        if (treeNode.getChildren().size() > 0) {
            treeNode.setExpanded(true);
            for (int i = 0; i < treeNode.getChildren().size(); i++) {
                expandirAll(treeNode.getChildren().get(i));
            }
        }
    }
    
    public void llenarLista() {
        List<ProveedorBean> listP = proveedorBO.listProveedorAll();
        if (listP != null) {
            setListProveedor(listP);
        }
    }
    
    public void llenarListaPartes() {
        List<ProveedorBean> listPartes = proveedorBO.listPartesInteresadas();
        if (listPartes != null) {
            setListaPartesInteresadas(listPartes);
        }
    }
    
    
    
    @PostConstruct
    public void main() {
        llenarNodos();
        llenarLista();
    }
    public void inserProveedor(){
        if(proveedorBO.insertProveedor(this)){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Proveedor Agregado Correctamente",""));
            limpiar();
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Proveedor Ya Existe",""));
        }
    }
    
    public void proveedorSeleccionado(PartesInteresadas proveedor){
        this.tipoDocumento = proveedor.getTipoDocuemnto();
        this.paisOrigen = proveedor.getId().getPaisOrigen();
        this.nombreProveedor = proveedor.getNombreProveedor();
        this.telefonoProveedor = proveedor.getTelefonoProveedor();
        this.faxProveedor = proveedor.getFaxProveedor();
        this.EMailProveedor = proveedor.getEMailProveedor();
        this.representante = proveedor.getRepresentante();
        this.direccion = proveedor.getDireccion();
        this.web = proveedor.getWeb();
        this.condicionPago = proveedor.getCondicionPago();
        this.bien = proveedor.getBien();
        this.tipoProveedor = proveedor.getTipoProveedor();
        this.proveedorSelec = proveedor;
        RequestContext.getCurrentInstance().update("fromModificarProveedor");
        
    }
    
    public void updateProveedor(ProveedorBean prov){
        this.nombreProveedor = prov.getNombreProveedor();
        this.tipoDocumento = prov.getTipoDocumento();
        this.paisOrigen = prov.getPaisOrigen();
        this.telefonoProveedor = prov.getTelefonoProveedor();
        this.faxProveedor = prov.getFaxProveedor();
        this.EMailProveedor = prov.getEMailProveedor();
        this.representante = prov.getRepresentante();
        this.direccion = prov.getDireccion();
        this.web = prov.getWeb();
        this.condicionPago = prov.getCondicionPago();
        this.bien = prov.getBien();
        this.tipoProveedor = prov.getTipoProveedor();
        
        if(!nombreProveedor.isEmpty()){
            prov.setNombreNuevo(prov.getNombreProveedor());
            
            if(proveedorBO.updateProveedor(prov)){
                llenarLista();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Datos del Proveedor Modificados",""));
            }
        }
        RequestContext.getCurrentInstance().update("formProveedor");
    }
    
    
    public void deleteProveedor(ProveedorBean prov){
        this.nombreProveedor = prov.getNombreProveedor();
        this.tipoDocumento = prov.getTipoDocumento();
        this.paisOrigen = prov.getPaisOrigen();
        this.telefonoProveedor = prov.getTelefonoProveedor();
        this.faxProveedor = prov.getFaxProveedor();
        this.EMailProveedor = prov.getEMailProveedor();
        this.representante = prov.getRepresentante();
        this.direccion = prov.getDireccion();
        this.web = prov.getWeb();
        this.condicionPago = prov.getCondicionPago();
        this.bien = prov.getBien();
        this.tipoProveedor = prov.getTipoProveedor();
        
        if(!nombreProveedor.isEmpty()){
            prov.setNombreNuevo(prov.getNombreProveedor());
            
            if(proveedorBO.deleteProveedor(prov)){
                llenarLista();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Datos Eliminados Correctamente",""));
            }
        }
        RequestContext.getCurrentInstance().update("formProveedor");
    }
    
    
    public void tipoDePagoSeleccionado(){
        if(metodoPago == 1){
            estadoTipoPago = false;
        }else{
            condicionPago = "0";
            estadoTipoPago = true;
        }
        RequestContext.getCurrentInstance().update("formProveedor:condiPago");
    }
    
    public void limpiar() {
        codigoProveedor = null;
        paisOrigen = null;
        nombreProveedor = null;
        telefonoProveedor = null;
        faxProveedor = null;
        EMailProveedor = null;
        representante = null;
        tipoDocumento = null;
        direccion = null;
        web = null;
        metodoPago = null;
        condicionPago = null;
        RequestContext.getCurrentInstance().update("formProveedor");
    }
    
    public void limpiar2() {
        codigoProveedor = null;
        paisOrigen = null;
        nombreProveedor = null;
        telefonoProveedor = null;
        faxProveedor = null;
        EMailProveedor = null;
        representante = null;
        tipoDocumento = null;
        direccion = null;
        web = null;
        metodoPago = null;
        condicionPago = null;
        RequestContext.getCurrentInstance().update("fromModificarProveedor");
    }
    
    public String getCodigoProveedor() {
        return codigoProveedor;
    }

    public void setCodigoProveedor(String codigoProveedor) {
        this.codigoProveedor = codigoProveedor;
    }

    public String getPaisOrigen() {
        return paisOrigen;
    }

    public void setPaisOrigen(String paisOrigen) {
        this.paisOrigen = paisOrigen;
    }

    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }

    public String getTelefonoProveedor() {
        return telefonoProveedor;
    }

    public void setTelefonoProveedor(String telefonoProveedor) {
        this.telefonoProveedor = telefonoProveedor;
    }

    public String getFaxProveedor() {
        return faxProveedor;
    }

    public void setFaxProveedor(String faxProveedor) {
        this.faxProveedor = faxProveedor;
    }

    public String getEMailProveedor() {
        return EMailProveedor;
    }

    public void setEMailProveedor(String EMailProveedor) {
        this.EMailProveedor = EMailProveedor;
    }

    public String getRepresentante() {
        return representante;
    }

    public void setRepresentante(String representante) {
        this.representante = representante;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
        
    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public Short getMetodoPago() {
        return metodoPago;
    }

    public void setMetodoPago(Short metodoPago) {
        this.metodoPago = metodoPago;
    }

    public String getCondicionPago() {
        return condicionPago;
    }

    public void setCondicionPago(String condicionPago) {
        this.condicionPago = condicionPago;
    }

    public ProveedorBO getProveedorBO() {
        return proveedorBO;
    }

    public void setProveedorBO(ProveedorBO proveedorBO) {
        this.proveedorBO = proveedorBO;
    }

    public boolean isEstadoTipoPago() {
        return estadoTipoPago;
    }

    public void setEstadoTipoPago(boolean estadoTipoPago) {
        this.estadoTipoPago = estadoTipoPago;
    }

    public String getTipoProveedor() {
        return tipoProveedor;
    }

    public void setTipoProveedor(String tipoProveedor) {
        this.tipoProveedor = tipoProveedor;
    }

    public String getBien() {
        return bien;
    }

    public void setBien(String bien) {
        this.bien = bien;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public List<ProveedorBean> getListProveedor() {
        return listProveedor;
    }

    public void setListProveedor(List<ProveedorBean> listProveedor) {
        this.listProveedor = listProveedor;
    }

    public List<ProveedorBean> getFiltroProveedor() {
        return filtroProveedor;
    }

    public void setFiltroProveedor(List<ProveedorBean> filtroProveedor) {
        this.filtroProveedor = filtroProveedor;
    }

    public String getNombreNuevo() {
        return nombreNuevo;
    }

    public void setNombreNuevo(String nombreNuevo) {
        this.nombreNuevo = nombreNuevo;
    }

    public PartesInteresadas getProveedorSelec() {
        return proveedorSelec;
    }

    public void setProveedorSelec(PartesInteresadas proveedorSelec) {
        this.proveedorSelec = proveedorSelec;
    }

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public List<ProveedorBean> getListaPartesInteresadas() {
        return listaPartesInteresadas;
    }

    public void setListaPartesInteresadas(List<ProveedorBean> listaPartesInteresadas) {
        this.listaPartesInteresadas = listaPartesInteresadas;
    }

    public PartesInteresadas getProveedor() {
        return proveedor;
    }

    public void setProveedor(PartesInteresadas proveedor) {
        this.proveedor = proveedor;
    }

    public ProveedorBean getSelectProveedor() {
        return selectProveedor;
    }

    public void setSelectProveedor(ProveedorBean selectProveedor) {
        this.selectProveedor = selectProveedor;
    }
    
    
}
