/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.EjemploBO;
import com.sigefi.bo.UsuarioBO;
import com.sigefi.clases.SubirArchivoServer;
import com.sigefi.entity.Ciudades;
import com.sigefi.entity.Rol;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.imageio.stream.FileImageOutputStream;
import javax.servlet.ServletContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CaptureEvent;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Administrador
 */
@ManagedBean
@ViewScoped
public class EjemploBean implements Serializable{
     
    @ManagedProperty (name = "ejemploBO", value = "#{ejemploBO}")
    private EjemploBO ejemploBO;
  
    /**
     * Creates a new instance of UsuarioBean
     */
    public EjemploBean() {
    }

    public void insertEjemplo(){
        
    }
    public void consultarEjemplo(){
        
    }
    public void updateEjemplo(){
        
    }
    public void deleteEjemplo(){
        
    }
    
    public void limpiar(){
        
    }

    public EjemploBO getEjemploBO() {
        return ejemploBO;
    }

    public void setEjemploBO(EjemploBO ejemploBO) {
        this.ejemploBO = ejemploBO;
    }
    
    
}
