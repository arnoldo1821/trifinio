/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.PresupuestoBO;
import com.sigefi.bo.RequisicionBO;
import com.sigefi.clases.FechaSistema;
import com.sigefi.entity.CentroCosto;
import com.sigefi.entity.DetalleCuentas;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.PartesInteresadas;
import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.RequerimientoPagos;
import com.sigefi.entity.UnidadesEjecutoras;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Administrador
 */
@ManagedBean
@ViewScoped
public class RequisicionBean implements Serializable {

    @ManagedProperty(name = "requisicionBO", value = "#{requisicionBO}")
    private RequisicionBO requisicionBO;

    @ManagedProperty(name = "presupuestoBO", value = "#{presupuestoBO}")
    private PresupuestoBO presupuestoBO;
    /*SESSION*/
    private short idunidadEjecutora;
    private String nombreUnidadEjecutora;

    /*PASO 1 Y 2*/
    private String codigoRequerimiento;
    private String codigoSiafi;
    private Date fechaPresentacion;
    private Date fechaEstimadaPago;
    private Map<String, Short> mapaUnidadEjecutora;
    private short unidadesEjecutoras;
    private short codcentroCosto;
    private int anyoPresupuesto;
    private List<Short> listAnios;
    private String nomUsuario;
    private String usuario;
    private RequerimientoPagos requerimientoPagosAux;
    private short correlativo;
    /*PASO3*/
    private Date fechaDocumento;
    private String claseDocumento;
    private String numeroDocumento;
    private String concepto;

    private String nombreGastoEspecifico;
    private BigDecimal totalPagar;
    private Character moneda;

    private int presupuesto;

    private String proveedor;
    private String nombreProveedor;
    private Map<String, String> mapProveedor;

    private Map<String, Integer> mapCentroCosto;
    private int detalleCuenta;
    private Map<String, Integer> mapDetalleCuenta;
    private Map<Integer, String> mapDetalleCuenta2;

    private String urlFirma;

    private boolean estado1;
    private boolean estado2;
    private boolean estado3;
    private boolean estado4;
    private boolean tipo1;
    private boolean tipo2;
    private boolean estadoSiafiSi;
    private boolean estadoSiafiNo;
    private boolean estadoAfectacionSi;
    private boolean estadoAfectacionNo;

    private BigDecimal sumTotal;

    private int pkRequerimiento;

    private List<DetalleRequerimientoPago> listDetalleReque;

    public RequisicionBean() {
        estado1 = false;
        estado2 = true;
        estado3 = true;
        estado4 = true;
        estadoAfectacionNo = true;
        estadoAfectacionSi = false;
        estadoSiafiNo = false;
        estadoSiafiSi = true;
    }

    @PostConstruct
    public void main() {
        SessionBean referenciaBeanSession
                = (SessionBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("sessionBean");

        usuario = referenciaBeanSession.getUsu();
        nomUsuario = referenciaBeanSession.getNombreUsuario();
        idunidadEjecutora = referenciaBeanSession.getIdUnidadEjecutora();
        nombreUnidadEjecutora = referenciaBeanSession.getUnidadEjecutora();

        List<PartesInteresadas> listProv = requisicionBO.listProveedorAll();
        if (listProv != null) {
            mapProveedor = new HashMap<>();
            for (PartesInteresadas prove : listProv) {
                mapProveedor.put(prove.getNombreProveedor(), prove.getId().getCodigoProveedor());
            }
        }

        List<CentroCosto> listCentro = requisicionBO.listCentroAll();
        if (listCentro != null) {
            mapCentroCosto = new HashMap<>();
            for (CentroCosto centro : listCentro) {
                mapCentroCosto.put(centro.getCentroCosto(), centro.getIdCentroCosto());
            }
        }

        List<UnidadesEjecutoras> listUnidadesEjecutoras = presupuestoBO.listUnidadesEjecutoras();
        if (listUnidadesEjecutoras != null) {
            mapaUnidadEjecutora = new HashMap();
            for (UnidadesEjecutoras obj : listUnidadesEjecutoras) {
                mapaUnidadEjecutora.put(obj.getNombreUnidad(), obj.getCodigo());
            }
        }

        FechaSistema anioHoy = new FechaSistema();
        short anio = (short) (anioHoy.getAnio() - 1);
        listAnios = new ArrayList<>();
        for (int i = 0; i <= 3; i++) {
            listAnios.add(anio);
            anio = (short) (anio + 1);
        }

    }

    /*PASO1*/
    public void buscarCuentasPre() {
        Date fecha = new Date();
        String año = new SimpleDateFormat("yy").format(fecha);
        UnidadesEjecutoras unidad = requisicionBO.elementoUnidad(this);
        RequerimientoPagos req = requisicionBO.elementoRequerimiento();
        if(req == null){
            this.codigoRequerimiento = unidad.getPrefijoCodigo() + año + "0000" + 1;
        }
        else{
        if (req.getIncrReqPk() > 0 & req.getIncrReqPk() < 10) {
            this.codigoRequerimiento = unidad.getPrefijoCodigo() + año + "0000" + req.getIncrReqPk();
        } else {
            if (req.getIncrReqPk() > 9 & req.getIncrReqPk() < 100) {
                this.codigoRequerimiento = unidad.getPrefijoCodigo() + año + "000" + req.getIncrReqPk();
            } else {
                if (req.getIncrReqPk() > 99 & req.getIncrReqPk() < 1000) {
                    this.codigoRequerimiento = unidad.getPrefijoCodigo() + año + "00" + req.getIncrReqPk();
                } else {
                    if (req.getIncrReqPk() > 999 & req.getIncrReqPk() < 10000) {
                        this.codigoRequerimiento = unidad.getPrefijoCodigo() + año + "0" + req.getIncrReqPk();
                    } else {
                        if (req.getIncrReqPk() > 9999 & req.getIncrReqPk() < 100000) {
                            this.codigoRequerimiento = unidad.getPrefijoCodigo() + año + req.getIncrReqPk();
                        } else {
                            this.codigoRequerimiento = unidad.getPrefijoCodigo() + año + req.getIncrReqPk();
                        }
                    }
                }
            }
        }
        }

        Presupuestos pre = requisicionBO.elementoPresupuesto(this);
        if (pre != null) {
            presupuesto = pre.getCodigoPresupuesto();
            estado1 = true;
            estado2 = false;
            estado4 = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Presupuesto Seleccionado", ""));
            RequestContext.getCurrentInstance().update("formRequesicion1");
            RequestContext.getCurrentInstance().update("formRequesicion2");
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "No hay presupuesto para la Unidad Ejecutora y año seleccionado ", ""));
        }
    }

    /*PASO 2*/
    public void inserRequerimiento() {
        requerimientoPagosAux = requisicionBO.insertRequisicion(this);
        if (requerimientoPagosAux != null) {
            this.pkRequerimiento = requerimientoPagosAux.getIncrReqPk();
            llenarDetalleReq();
            presupuestoSeleccionado();
            estado2 = true;
            estado4 = true;
            estadoSiafiSi = true;
            estado3 = false;
            RequestContext.getCurrentInstance().update("formRequesicion2");
            RequestContext.getCurrentInstance().update("formRequesicion3");
        }
    }

    public void fechaEstimada(Date fecha) {
        Calendar calendar = Calendar.getInstance();
        int dias = 7;
        calendar.setTime(fecha);
        calendar.add(Calendar.DAY_OF_YEAR, dias);
        this.fechaEstimadaPago = calendar.getTime(); 
    }

    public void tipoSiafiSeleccionado() {
        if (tipo1 == true) {
            estadoSiafiNo = true;
            estadoSiafiSi = false;
        } else {
            estadoSiafiNo = false;
            estadoSiafiSi = true;
        }
    }

    public void tipoAfectacionSeleccionado() {
        if (tipo2 == true) {
            estadoAfectacionNo = true;
            estadoAfectacionSi = false;
        } else {
            estadoAfectacionNo = false;
            estadoAfectacionSi = true;
        }
    }

    public void llenarDetalleReq() {
        if (pkRequerimiento > 0) {
            List<DetalleRequerimientoPago> listObj = requisicionBO.correlativo_Total_DetalleRequerimiento(pkRequerimiento);
            if (listObj != null) {
                correlativo = (short) (listObj.size() + 1);
                sumTotal = BigDecimal.ZERO;
                for (DetalleRequerimientoPago objRe : listObj) {
                    sumTotal = sumTotal.add(objRe.getMonto());
                }
                setListDetalleReque(listObj);
            } else {
                correlativo = (short) (1);
                sumTotal = BigDecimal.ZERO;
            }
        }

    }

    public void presupuestoSeleccionado() {
        if (presupuesto > 0) {
            List<DetalleCuentas> listDetaCue = requisicionBO.listDetalleCuentasAll(presupuesto);
            if (listDetaCue != null) {
                mapDetalleCuenta = new HashMap<>();
                mapDetalleCuenta2 = new HashMap<>();
                for (DetalleCuentas prove : listDetaCue) {
                    mapDetalleCuenta.put(prove.getCatalogoCuentas().getNombreCuenta(), prove.getId().getCodgioCuenta());
                    mapDetalleCuenta2.put(prove.getId().getCodgioCuenta(), prove.getCatalogoCuentas().getNombreCuenta());
                }
            }
        }
    }

    /*PASO 3*/
    public void insertDetalleRequerimiento() {
        if (mapDetalleCuenta2 != null) {
            this.nombreGastoEspecifico = mapDetalleCuenta2.get(this.detalleCuenta);
            PartesInteresadas prov = requisicionBO.elementoProveedor(this);
            this.nombreProveedor = prov.getNombreProveedor();
            if (requisicionBO.insertDetalleRequisicion(this)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Requerimiento agregado correctamente", ""));
                llenarDetalleReq();
                limpiar();
            } else {
                correlativo = 1;
                sumTotal = BigDecimal.ZERO;
            }
        }
    }

    public void limpiar() {
        unidadesEjecutoras = 0;
        anyoPresupuesto = 0;

        fechaPresentacion = null;
        fechaEstimadaPago = null;
        codigoRequerimiento = null;

        fechaDocumento = null;
        claseDocumento = null;
        numeroDocumento = null;
        proveedor = null;
        detalleCuenta = 0;
        nombreGastoEspecifico = null;

        totalPagar = null;
        concepto = null;

        RequestContext.getCurrentInstance().update("formRequesicion3");
    }

    public void finalizarRequicion() {
        correlativo = 0;
        estado3 = true;
        estado1 = false;
        sumTotal = null;
        moneda = null;
        if (listDetalleReque != null) {
            if (listDetalleReque.size() > 0) {
                listDetalleReque.clear();
            }
        }
        limpiar();
        RequestContext.getCurrentInstance().update("formRequesicion1");
        RequestContext.getCurrentInstance().update("formRequesicion2");
    }

    public int getDetalleCuenta() {
        return detalleCuenta;
    }

    public void setDetalleCuenta(int detalleCuenta) {
        this.detalleCuenta = detalleCuenta;
    }

    public Map<String, Integer> getMapDetalleCuenta() {
        return mapDetalleCuenta;
    }

    public void setMapDetalleCuenta(Map<String, Integer> mapDetalleCuenta) {
        this.mapDetalleCuenta = mapDetalleCuenta;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public Map<String, String> getMapProveedor() {
        return mapProveedor;
    }

    public void setMapProveedor(Map<String, String> mapProveedor) {
        this.mapProveedor = mapProveedor;
    }

    public short getIdunidadEjecutora() {
        return idunidadEjecutora;
    }

    public void setIdunidadEjecutora(short idunidadEjecutora) {
        this.idunidadEjecutora = idunidadEjecutora;
    }

    public String getNombreUnidadEjecutora() {
        return nombreUnidadEjecutora;
    }

    public void setNombreUnidadEjecutora(String nombreUnidadEjecutora) {
        this.nombreUnidadEjecutora = nombreUnidadEjecutora;
    }

    public RequisicionBO getRequisicionBO() {
        return requisicionBO;
    }

    public void setRequisicionBO(RequisicionBO requisicionBO) {
        this.requisicionBO = requisicionBO;
    }

    public short getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(short correlativo) {
        this.correlativo = correlativo;
    }

    public String getNomUsuario() {
        return nomUsuario;
    }

    public void setNomUsuario(String nomUsuario) {
        this.nomUsuario = nomUsuario;
    }

    public String getCodigoRequerimiento() {
        return codigoRequerimiento;
    }

    public void setCodigoRequerimiento(String codigoRequerimiento) {
        this.codigoRequerimiento = codigoRequerimiento;
    }

    public Date getFechaPresentacion() {
        return fechaPresentacion;
    }

    public void setFechaPresentacion(Date fechaPresentacion) {
        this.fechaPresentacion = fechaPresentacion;
    }

    public Date getFechaEstimadaPago() {
        return fechaEstimadaPago;
    }

    public void setFechaEstimadaPago(Date fechaEstimadaPago) {
        this.fechaEstimadaPago = fechaEstimadaPago;
    }

    public Date getFechaDocumento() {
        return fechaDocumento;
    }

    public void setFechaDocumento(Date fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    public String getClaseDocumento() {
        return claseDocumento;
    }

    public void setClaseDocumento(String claseDocumento) {
        this.claseDocumento = claseDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getNombreGastoEspecifico() {
        return nombreGastoEspecifico;
    }

    public void setNombreGastoEspecifico(String nombreGastoEspecifico) {
        this.nombreGastoEspecifico = nombreGastoEspecifico;
    }

    public BigDecimal getTotalPagar() {
        return totalPagar;
    }

    public void setTotalPagar(BigDecimal totalPagar) {
        this.totalPagar = totalPagar;
    }

    public Character getMoneda() {
        return moneda;
    }

    public void setMoneda(Character moneda) {
        this.moneda = moneda;
    }

    public int getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(int presupuesto) {
        this.presupuesto = presupuesto;
    }

    public String getUrlFirma() {
        return urlFirma;
    }

    public void setUrlFirma(String urlFirma) {
        this.urlFirma = urlFirma;
    }

    public short getUnidadesEjecutoras() {
        return unidadesEjecutoras;
    }

    public void setUnidadesEjecutoras(short unidadesEjecutoras) {
        this.unidadesEjecutoras = unidadesEjecutoras;
    }

    public Map<String, Short> getMapaUnidadEjecutora() {
        return mapaUnidadEjecutora;
    }

    public void setMapaUnidadEjecutora(Map<String, Short> mapaUnidadEjecutora) {
        this.mapaUnidadEjecutora = mapaUnidadEjecutora;
    }

    public PresupuestoBO getPresupuestoBO() {
        return presupuestoBO;
    }

    public void setPresupuestoBO(PresupuestoBO presupuestoBO) {
        this.presupuestoBO = presupuestoBO;
    }

    public int getAnyoPresupuesto() {
        return anyoPresupuesto;
    }

    public void setAnyoPresupuesto(int anyoPresupuesto) {
        this.anyoPresupuesto = anyoPresupuesto;
    }

    public List<Short> getListAnios() {
        return listAnios;
    }

    public void setListAnios(List<Short> listAnios) {
        this.listAnios = listAnios;
    }

    public boolean isEstado1() {
        return estado1;
    }

    public void setEstado1(boolean estado1) {
        this.estado1 = estado1;
    }

    public boolean isEstado2() {
        return estado2;
    }

    public void setEstado2(boolean estado2) {
        this.estado2 = estado2;
    }

    public boolean isEstado3() {
        return estado3;
    }

    public void setEstado3(boolean estado3) {
        this.estado3 = estado3;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public RequerimientoPagos getRequerimientoPagosAux() {
        return requerimientoPagosAux;
    }

    public void setRequerimientoPagosAux(RequerimientoPagos requerimientoPagosAux) {
        this.requerimientoPagosAux = requerimientoPagosAux;
    }

    public Map<Integer, String> getMapDetalleCuenta2() {
        return mapDetalleCuenta2;
    }

    public void setMapDetalleCuenta2(Map<Integer, String> mapDetalleCuenta2) {
        this.mapDetalleCuenta2 = mapDetalleCuenta2;
    }

    public BigDecimal getSumTotal() {
        return sumTotal;
    }

    public void setSumTotal(BigDecimal sumTotal) {
        this.sumTotal = sumTotal;
    }

    public int getPkRequerimiento() {
        return pkRequerimiento;
    }

    public void setPkRequerimiento(int pkRequerimiento) {
        this.pkRequerimiento = pkRequerimiento;
    }

    public List<DetalleRequerimientoPago> getListDetalleReque() {
        return listDetalleReque;
    }

    public void setListDetalleReque(List<DetalleRequerimientoPago> listDetalleReque) {
        this.listDetalleReque = listDetalleReque;
    }

    public Map<String, Integer> getMapCentroCosto() {
        return mapCentroCosto;
    }

    public void setMapCentroCosto(Map<String, Integer> mapCentroCosto) {
        this.mapCentroCosto = mapCentroCosto;
    }

    public boolean isEstado4() {
        return estado4;
    }

    public void setEstado4(boolean estado4) {
        this.estado4 = estado4;
    }

    public short getCodcentroCosto() {
        return codcentroCosto;
    }

    public void setCodcentroCosto(short codcentroCosto) {
        this.codcentroCosto = codcentroCosto;
    }

    public boolean isTipo1() {
        return tipo1;
    }

    public void setTipo1(boolean tipo1) {
        this.tipo1 = tipo1;
    }

    public boolean isTipo2() {
        return tipo2;
    }

    public void setTipo2(boolean tipo2) {
        this.tipo2 = tipo2;
    }

    public boolean isEstadoSiafiSi() {
        return estadoSiafiSi;
    }

    public void setEstadoSiafiSi(boolean estadoSiafiSi) {
        this.estadoSiafiSi = estadoSiafiSi;
    }

    public boolean isEstadoSiafiNo() {
        return estadoSiafiNo;
    }

    public void setEstadoSiafiNo(boolean estadoSiafiNo) {
        this.estadoSiafiNo = estadoSiafiNo;
    }

    public boolean isEstadoAfectacionSi() {
        return estadoAfectacionSi;
    }

    public void setEstadoAfectacionSi(boolean estadoAfectacionSi) {
        this.estadoAfectacionSi = estadoAfectacionSi;
    }

    public boolean isEstadoAfectacionNo() {
        return estadoAfectacionNo;
    }

    public void setEstadoAfectacionNo(boolean estadoAfectacionNo) {
        this.estadoAfectacionNo = estadoAfectacionNo;
    }

    public String getCodigoSiafi() {
        return codigoSiafi;
    }

    public void setCodigoSiafi(String codigoSiafi) {
        this.codigoSiafi = codigoSiafi;
    }

    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }

}
