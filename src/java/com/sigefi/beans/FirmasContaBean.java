/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.FirmasContaBO;
import com.sigefi.entity.Empresa;
import com.sigefi.entity.FirmasConta;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class FirmasContaBean implements Serializable {

    @ManagedProperty(value = "#{firmasContaBO}")
    private FirmasContaBO firmasContaBO;

    private Empresa empresaSelec;
    private short codfirma = 1;
    private int numFirmas;
    private boolean estado0;
    private boolean estado1;
    private boolean estado2;
    private boolean estado3;
    private String nombre;
    private String titulo;
    private short idunidadesEjecutoras;
    private int anio;

    private List<FirmasConta> listFirmas = new ArrayList<>();

    public FirmasContaBean() {
        estado0 = false;
        estado1 = true;
        estado2 = true;
        estado3 = true;

        empresaSelec = new Empresa();
    }

    public void empresaSelecionada() {
        empresaSelec = firmasContaBO.elementoEmpresa(idunidadesEjecutoras, anio);
        if (empresaSelec != null) {
            if (empresaSelec.getCodigoEmpresaContable() > 0) {
                List<FirmasConta> l = firmasContaBO.elementoFirmas(empresaSelec);
                if (l != null) {
                    setListFirmas(l);
                } else {
                    if (getListFirmas() != null) {
                        if (getListFirmas().size() > 0) {
                            getListFirmas().clear();
                        }
                    }
                    estado0 = true;
                    estado1 = false;
                }
            }
        } else {
            if (getListFirmas() != null) {
                if (getListFirmas().size() > 0) {
                    getListFirmas().clear();
                }
            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "No existe empresa con la unidad ejecutora y año seleccionado", ""));
        }

    }

    public void numeroFirmasSelecc() {
        if (numFirmas > 0) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, numFirmas + " firmas seleccionadas", ""));
            estado1 = true;
            estado2 = false;
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "No a seleccionado el numero de fiirmas", ""));
        }
    }

    public void insertFirma() {
        if (firmasContaBO.insertFirma(this)) {
            limpiar();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Firma agregada correctamente", ""));
            setListFirmas(firmasContaBO.elementoFirmas(empresaSelec));
            if (numFirmas == codfirma) {
                estado2 = true;
                estado3 = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "todas las firmas fueron agregadas correctamente", ""));
            } else {
                codfirma = (short) (codfirma + 1);
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error al agregar la firma", ""));
        }
    }

    public void finalizar() {
        estado3 = true;
        estado0 = false;
        numFirmas = 0;
        idunidadesEjecutoras = 0;
        anio = 0;
        if (getListFirmas() != null) {
            if (getListFirmas().size() > 0) {
                getListFirmas().clear();
            }
        }
    }

    public void limpiar() {
        nombre = null;
        titulo = null;
    }

    public FirmasContaBO getFirmasContaBO() {
        return firmasContaBO;
    }

    public void setFirmasContaBO(FirmasContaBO firmasContaBO) {
        this.firmasContaBO = firmasContaBO;
    }

    public int getNumFirmas() {
        return numFirmas;
    }

    public void setNumFirmas(int numFirmas) {
        this.numFirmas = numFirmas;
    }

    public boolean isEstado1() {
        return estado1;
    }

    public void setEstado1(boolean estado1) {
        this.estado1 = estado1;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public short getIdunidadesEjecutoras() {
        return idunidadesEjecutoras;
    }

    public void setIdunidadesEjecutoras(short idunidadesEjecutoras) {
        this.idunidadesEjecutoras = idunidadesEjecutoras;
    }

    public List<FirmasConta> getListFirmas() {
        return listFirmas;
    }

    public void setListFirmas(List<FirmasConta> listFirmas) {
        this.listFirmas = listFirmas;
    }

    public short getCodfirma() {
        return codfirma;
    }

    public void setCodfirma(short codfirma) {
        this.codfirma = codfirma;
    }

    public boolean isEstado0() {
        return estado0;
    }

    public void setEstado0(boolean estado0) {
        this.estado0 = estado0;
    }

    public Empresa getEmpresaSelec() {
        return empresaSelec;
    }

    public void setEmpresaSelec(Empresa empresaSelec) {
        this.empresaSelec = empresaSelec;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public boolean isEstado2() {
        return estado2;
    }

    public void setEstado2(boolean estado2) {
        this.estado2 = estado2;
    }

    public boolean isEstado3() {
        return estado3;
    }

    public void setEstado3(boolean estado3) {
        this.estado3 = estado3;
    }

}
