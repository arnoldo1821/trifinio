/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.beans;

import com.sigefi.bo.LiquidarBO;
import com.sigefi.entity.Presupuestos;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author AAldemaro
 */
@ManagedBean
@ViewScoped
public class LiquidarBean implements Serializable{

    @ManagedProperty(value = "#{liquidarBO}")
    private LiquidarBO liquidarBO;
    
    private short unidadesEjecutoras;
    private int anyoPresupuesto;
    
    public LiquidarBean() {
    }

    public void liquidarYPasarRubros(){
        Presupuestos consul = liquidarBO.elementoPre(unidadesEjecutoras, anyoPresupuesto);
        if(consul != null){
            if(liquidarBO.insertPresupuesto(consul)){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Estructura del presupuesto creada correctamente", ""));
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "La Estructura del presupuesto ya existe", ""));
            }
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "El presupuesto ya fue liquidado o La Estructura del presupuesto ya existe", ""));
        }
    } 
    
    public void liquidarPre(){
        Presupuestos consul = liquidarBO.elementoPre(unidadesEjecutoras, anyoPresupuesto);
        if(consul != null){
            if(liquidarBO.liquidarPresupuesto(consul)){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El presupuesto se liquido correctamente", ""));
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "El presupuesto ya fue liquidado", ""));
            }
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "El presupuesto no existe o ya fue liquidado", ""));
        }
    }
    
    public void liquidarPasarRubrosCero(){
        Presupuestos consul = liquidarBO.elementoPre(unidadesEjecutoras, anyoPresupuesto);
        if(consul != null){
            if(liquidarBO.insertPresupuestoCero(consul)){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Estructura del presupuesto creada correctamente", ""));
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "La Estructura del presupuesto ya existe", ""));
            }
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "El presupuesto ya fue liquidado o La Estructura del presupuesto ya existe", ""));
        }
    }
    
    
    public LiquidarBO getLiquidarBO() {
        return liquidarBO;
    }

    public void setLiquidarBO(LiquidarBO liquidarBO) {
        this.liquidarBO = liquidarBO;
    }

    public short getUnidadesEjecutoras() {
        return unidadesEjecutoras;
    }

    public void setUnidadesEjecutoras(short unidadesEjecutoras) {
        this.unidadesEjecutoras = unidadesEjecutoras;
    }

    public int getAnyoPresupuesto() {
        return anyoPresupuesto;
    }

    public void setAnyoPresupuesto(int anyoPresupuesto) {
        this.anyoPresupuesto = anyoPresupuesto;
    }
    
    
}
