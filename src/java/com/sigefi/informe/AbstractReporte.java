/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.informe;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author Oscar
 */
public abstract class AbstractReporte implements java.io.Serializable {

    public static final int XLS = 1;
    public static final int PDF = 2;
    public static final int RTF = 3;
    public static final int PRINTER = 4;
    public static final int BYTE_ARRAY = 5;
    private final SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
    private Map params;

    private DriverManagerDataSource dataSource;

    public AbstractReporte() {
    }

    public AbstractReporte(DriverManagerDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public AbstractReporte(Map params, DriverManagerDataSource dataSource) {
        this.params = params;
        this.dataSource = dataSource;
    }

    protected void generarReporte(String archivo, String nombreCorto, int tipo) {
        String contentType = "octet-stream", extension = "";
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            File jasper = new File(context.getExternalContext().getRealPath("/reportes/sigefi/" + archivo + ".jasper"));
            JasperPrint jp = JasperFillManager.fillReport(jasper.getPath(), params, dataSource.getConnection());

            if (tipo == PRINTER) {
                JasperPrintManager.printReport(jp, false);
            } else {
                HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
                if (tipo == XLS) {
                    contentType = "excel";
                    extension = ".xls";
                } else if (tipo == PDF) {
                    contentType = "pdf";
                    extension = ".pdf";
                } else if (tipo == RTF) {
                    contentType = "rtf";
                    extension = ".rtf";
                }
                response.setContentType("application/" + contentType);
                response.addHeader("content-disposition", "attachment; filename=" + nombreCorto + formato.format(new Date()) + extension);
                OutputStream stream = response.getOutputStream();

                switch (tipo) {
                    case XLS:
                        JRXlsExporter xls = new JRXlsExporter();
                        xls.setExporterInput(new SimpleExporterInput(jp));
                        xls.setExporterOutput(new SimpleOutputStreamExporterOutput(stream));
                        SimpleXlsReportConfiguration configXls = new SimpleXlsReportConfiguration();
                        xls.setConfiguration(configXls);
                        xls.exportReport();
                        break;
                    case PDF:
                        JRPdfExporter pdf = new JRPdfExporter();
                        pdf.setExporterInput(new SimpleExporterInput(jp));
                        pdf.setExporterOutput(new SimpleOutputStreamExporterOutput(stream));
                        SimplePdfExporterConfiguration configPdf = new SimplePdfExporterConfiguration();
                        pdf.setConfiguration(configPdf);
                        pdf.exportReport();
                        break;
                    case RTF:
                        JRRtfExporter rtf = new JRRtfExporter();
                        rtf.setExporterInput(new SimpleExporterInput(jp));
                        rtf.setExporterOutput(new SimpleWriterExporterOutput(stream));
                        rtf.exportReport();
                        break;
                }

                stream.flush();
                stream.close();
                context.responseComplete();
            }
            System.err.println("Exito al generar el reporte.");
        } catch (JRException | IOException | SQLException ex) {
            System.err.println("Error al generar el reporte");
            Logger.getLogger(ReportePresupuesto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected StreamedContent generarReporte(String archivo, String nombreCorto) {
        String contentType = "application/pdf", extension = ".pdf";
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
                return new DefaultStreamedContent();
            } else {
                File jasper = new File(context.getExternalContext().getRealPath("/reportes/sigefi/" + archivo + ".jasper"));
                JasperPrint jp = JasperFillManager.fillReport(jasper.getPath(), params, dataSource.getConnection());

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                JasperExportManager.exportReportToPdfStream(jp, stream);
                stream.flush();
                stream.close();

                InputStream is = new ByteArrayInputStream(stream.toByteArray());
                String pdfName = nombreCorto + formato.format(new Date()) + extension;
                StreamedContent sc = new DefaultStreamedContent(is, contentType, pdfName);
                System.err.println("Exito al generar el reporte (a StreamedContent)");
                return sc;
            }
        } catch (JRException | IOException | SQLException ex) {
            System.err.println("Error al generar el reporte (a StreamedContent)");
            Logger.getLogger(ReportePresupuesto.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public Map getParams() {
        return params;
    }

    public void setParams(Map params) {
        this.params = params;
    }

    public DriverManagerDataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DriverManagerDataSource dataSource) {
        this.dataSource = dataSource;
    }
}
