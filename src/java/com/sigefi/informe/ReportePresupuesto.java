/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.informe;

import java.util.HashMap;
import java.util.Map;
import org.primefaces.model.StreamedContent;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author Oscar
 */
public class ReportePresupuesto extends AbstractReporte implements java.io.Serializable {

    public ReportePresupuesto() {
    }

    public ReportePresupuesto(DriverManagerDataSource dataSource) {
        super(dataSource);
    }

    public void exportarPresupuestoAprobado(int idUnidad, int anio, int tipo) {
        asignarParametros(idUnidad, anio);
        generarReporte("presupuesto_aprobado", "presupuesto_aprobado", tipo);
    }

    public void exportarPresupuestoReprogramado(int idUnidad, int anio, int tipo) {
        asignarParametros(idUnidad, anio);
        generarReporte("presupuesto_aprobado", "presupuesto_aprobado", tipo);
    }

    public void exportarPresupuestoEjecutado(int idUnidad, int anio, int tipo) {
        asignarParametros(idUnidad, anio);
        generarReporte("presupuesto_aprobado", "presupuesto_aprobado", tipo);
    }

    public void exportarPresupuestoComprometido(int idUnidad, int anio, int tipo) {
        asignarParametros(idUnidad, anio);
        generarReporte("presupuesto_aprobado", "presupuesto_aprobado", tipo);
    }
    
    public StreamedContent getPresupuestoAprobado(int idUnidad, int anio) {
        asignarParametros(idUnidad, anio);
        return generarReporte("presupuesto_aprobado", "presupuesto_aprobado");
    }

    public StreamedContent getPresupuestoReprogramado(int idUnidad, int anio) {
        asignarParametros(idUnidad, anio);
        return generarReporte("presupuesto_aprobado", "presupuesto_aprobado");
    }

    public StreamedContent getPresupuestoEjecutado(int idUnidad, int anio) {
        asignarParametros(idUnidad, anio);
        return generarReporte("presupuesto_aprobado", "presupuesto_aprobado");
    }

    public StreamedContent getPresupuestoComprometido(int idUnidad, int anio) {
        asignarParametros(idUnidad, anio);
        return generarReporte("presupuesto_aprobado", "presupuesto_aprobado");
    }

    private void asignarParametros(int idUnidad, int anio) {
        Map parametros = new HashMap();
        parametros.put("cod_unidad", idUnidad);
        parametros.put("anio", anio);
        setParams(parametros);
    }

}
