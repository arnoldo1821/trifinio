
package com.sigefi.informe;

import java.util.HashMap;
import java.util.Map;
import org.primefaces.model.StreamedContent;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author Oscar
 */
public class ReporteProveedores extends AbstractReporte implements java.io.Serializable {
    
    public ReporteProveedores() {
    }

    public ReporteProveedores(DriverManagerDataSource dataSource) {
        super(dataSource);
    }

    public void exportarProveedoresPorPais(String codigoPais, int tipo){
        asignarParametros(codigoPais);
        generarReporte("proveedoresPorPais", "proveedoresPorPais", tipo);
    }
    public StreamedContent getProveedoresPorPais(String codigoPais){
        asignarParametros(codigoPais);
        return generarReporte("proveedoresPorPais", "proveedoresPorPais");
    }
    
    private void asignarParametros(String codigoPais) {
        Map parametros = new HashMap();
        parametros.put("pais", codigoPais);
        setParams(parametros);
    }
    
}
