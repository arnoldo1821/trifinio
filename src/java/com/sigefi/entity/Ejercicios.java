package com.sigefi.entity;
// Generated 10-12-2015 09:40:19 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Ejercicios generated by hbm2java
 */
@Entity
@Table(name="ejercicios"
    ,schema="contabilidad"
)
public class Ejercicios  implements java.io.Serializable {


     private String codEjercicio;
     private Date aperturaEjercicio;
     private Date cierreEjercicio;
     private boolean ejercicioCerrado;
     private Short empresaFk;
     private Set<Diario> diarios = new HashSet<Diario>(0);
     private Set<Periodos> periodoses = new HashSet<Periodos>(0);

    public Ejercicios() {
    }

	
    public Ejercicios(String codEjercicio, Date aperturaEjercicio, Date cierreEjercicio, boolean ejercicioCerrado) {
        this.codEjercicio = codEjercicio;
        this.aperturaEjercicio = aperturaEjercicio;
        this.cierreEjercicio = cierreEjercicio;
        this.ejercicioCerrado = ejercicioCerrado;
    }
    public Ejercicios(String codEjercicio, Date aperturaEjercicio, Date cierreEjercicio, boolean ejercicioCerrado, Short empresaFk, Set<Diario> diarios, Set<Periodos> periodoses) {
       this.codEjercicio = codEjercicio;
       this.aperturaEjercicio = aperturaEjercicio;
       this.cierreEjercicio = cierreEjercicio;
       this.ejercicioCerrado = ejercicioCerrado;
       this.empresaFk = empresaFk;
       this.diarios = diarios;
       this.periodoses = periodoses;
    }
   
     @Id 

    
    @Column(name="cod_ejercicio", unique=true, nullable=false, length=20)
    public String getCodEjercicio() {
        return this.codEjercicio;
    }
    
    public void setCodEjercicio(String codEjercicio) {
        this.codEjercicio = codEjercicio;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="apertura_ejercicio", nullable=false, length=13)
    public Date getAperturaEjercicio() {
        return this.aperturaEjercicio;
    }
    
    public void setAperturaEjercicio(Date aperturaEjercicio) {
        this.aperturaEjercicio = aperturaEjercicio;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="cierre_ejercicio", nullable=false, length=13)
    public Date getCierreEjercicio() {
        return this.cierreEjercicio;
    }
    
    public void setCierreEjercicio(Date cierreEjercicio) {
        this.cierreEjercicio = cierreEjercicio;
    }

    
    @Column(name="ejercicio_cerrado", nullable=false)
    public boolean isEjercicioCerrado() {
        return this.ejercicioCerrado;
    }
    
    public void setEjercicioCerrado(boolean ejercicioCerrado) {
        this.ejercicioCerrado = ejercicioCerrado;
    }

    
    @Column(name="empresa_fk")
    public Short getEmpresaFk() {
        return this.empresaFk;
    }
    
    public void setEmpresaFk(Short empresaFk) {
        this.empresaFk = empresaFk;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="ejercicios")
    public Set<Diario> getDiarios() {
        return this.diarios;
    }
    
    public void setDiarios(Set<Diario> diarios) {
        this.diarios = diarios;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="ejercicios")
    public Set<Periodos> getPeriodoses() {
        return this.periodoses;
    }
    
    public void setPeriodoses(Set<Periodos> periodoses) {
        this.periodoses = periodoses;
    }




}


