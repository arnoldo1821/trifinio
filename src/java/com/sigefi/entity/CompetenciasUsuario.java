package com.sigefi.entity;
// Generated 10-12-2015 09:40:19 PM by Hibernate Tools 4.3.1


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * CompetenciasUsuario generated by hbm2java
 */
@Entity
@Table(name="competencias_usuario"
    ,schema="recuros_humanos"
)
public class CompetenciasUsuario  implements java.io.Serializable {


     private CompetenciasUsuarioId id;
     private Usuarios usuarios;

    public CompetenciasUsuario() {
    }

    public CompetenciasUsuario(CompetenciasUsuarioId id, Usuarios usuarios) {
       this.id = id;
       this.usuarios = usuarios;
    }
   
     @EmbeddedId

    
    @AttributeOverrides( {
        @AttributeOverride(name="idUsuario", column=@Column(name="id_usuario", nullable=false) ), 
        @AttributeOverride(name="competencia", column=@Column(name="competencia", nullable=false, length=150) ) } )
    public CompetenciasUsuarioId getId() {
        return this.id;
    }
    
    public void setId(CompetenciasUsuarioId id) {
        this.id = id;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_usuario", nullable=false, insertable=false, updatable=false)
    public Usuarios getUsuarios() {
        return this.usuarios;
    }
    
    public void setUsuarios(Usuarios usuarios) {
        this.usuarios = usuarios;
    }




}


