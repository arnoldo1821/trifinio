/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 *
 * @author Elmer
 */
@Entity
@Table(name="fuente_financiamiento"
    ,schema="tesoreria"
)
public class FuenteFinanciamiento implements java.io.Serializable {
     private int codigo;
     private String nombreFuente;

    public FuenteFinanciamiento() {
    }
     
     public FuenteFinanciamiento(int codigo,String nombreFuente){
         this.codigo = codigo;
         this.nombreFuente = nombreFuente;
     }
    
     @Id 

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="codigo", unique=true, nullable=false)
    public int getCodigo() {
        return this.codigo;
    }
    
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    @Column(name="nombre_fuente", length=300)
    public String getNombreFuente() {
        return this.nombreFuente;
    }
    
    public void setNombreFuente(String nombreFuente) {
        this.nombreFuente = nombreFuente;
    }
    
}
