package com.sigefi.entity;
// Generated 10-12-2015 09:40:19 PM by Hibernate Tools 4.3.1


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 * FirmasRequerimientoPago generated by hbm2java
 */
@Entity
@Table(name="firmas_requerimiento_pago"
    ,schema="asistente"
)
public class FirmasRequerimientoPago  implements java.io.Serializable {


     private int fkRequerimiento;
     private RequerimientoPagos requerimientoPagos;
     private Date fechaVerificacion;
     private Boolean validado;
     private String observacion;
     private String ipConexion;

    public FirmasRequerimientoPago() {
    }

	
    public FirmasRequerimientoPago(RequerimientoPagos requerimientoPagos, Date fechaVerificacion) {
        this.requerimientoPagos = requerimientoPagos;
        this.fechaVerificacion = fechaVerificacion;
    }
    public FirmasRequerimientoPago(RequerimientoPagos requerimientoPagos, Date fechaVerificacion, Boolean validado, String observacion, String ipConexion) {
       this.requerimientoPagos = requerimientoPagos;
       this.fechaVerificacion = fechaVerificacion;
       this.validado = validado;
       this.observacion = observacion;
       this.ipConexion = ipConexion;
    }
   
     @GenericGenerator(name="generator", strategy="foreign", parameters=@Parameter(name="property", value="requerimientoPagos"))@Id @GeneratedValue(generator="generator")

    
    @Column(name="fk_requerimiento", unique=true, nullable=false)
    public int getFkRequerimiento() {
        return this.fkRequerimiento;
    }
    
    public void setFkRequerimiento(int fkRequerimiento) {
        this.fkRequerimiento = fkRequerimiento;
    }

@OneToOne(fetch=FetchType.LAZY)@PrimaryKeyJoinColumn
    public RequerimientoPagos getRequerimientoPagos() {
        return this.requerimientoPagos;
    }
    
    public void setRequerimientoPagos(RequerimientoPagos requerimientoPagos) {
        this.requerimientoPagos = requerimientoPagos;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fecha_verificacion", nullable=false, length=29)
    public Date getFechaVerificacion() {
        return this.fechaVerificacion;
    }
    
    public void setFechaVerificacion(Date fechaVerificacion) {
        this.fechaVerificacion = fechaVerificacion;
    }

    
    @Column(name="validado")
    public Boolean getValidado() {
        return this.validado;
    }
    
    public void setValidado(Boolean validado) {
        this.validado = validado;
    }

    
    @Column(name="observacion")
    public String getObservacion() {
        return this.observacion;
    }
    
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    
    @Column(name="ip_conexion", length=18)
    public String getIpConexion() {
        return this.ipConexion;
    }
    
    public void setIpConexion(String ipConexion) {
        this.ipConexion = ipConexion;
    }




}


