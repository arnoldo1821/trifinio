package com.sigefi.entity;
// Generated 10-12-2015 09:40:19 PM by Hibernate Tools 4.3.1


import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * RequisicionCompra generated by hbm2java
 */
@Entity
@Table(name="requisicion_compra"
    ,schema="asistente"
)
public class RequisicionCompra  implements java.io.Serializable {


     private String codigoRequerimiento;
     private String oficinaRequisicion;
     private Date fechaRequisicion;
     private String programa;
     private String nombreProceso;
     private String numeroProceso;
     private BigDecimal presupuestoEstimado;
     private String idGasto;
     private String componente;
     private BigDecimal disponibilidadReal;
     private String actividad;
     private String unidadSolicitante;
     private String metodoSeleccion;
     private Set<ItemsRequerimiento> itemsRequerimientos = new HashSet<ItemsRequerimiento>(0);

    public RequisicionCompra() {
    }

	
    public RequisicionCompra(String codigoRequerimiento) {
        this.codigoRequerimiento = codigoRequerimiento;
    }
    public RequisicionCompra(String codigoRequerimiento, String oficinaRequisicion, Date fechaRequisicion, String programa, String nombreProceso, String numeroProceso, BigDecimal presupuestoEstimado, String idGasto, String componente, BigDecimal disponibilidadReal, String actividad, String unidadSolicitante, String metodoSeleccion, Set<ItemsRequerimiento> itemsRequerimientos) {
       this.codigoRequerimiento = codigoRequerimiento;
       this.oficinaRequisicion = oficinaRequisicion;
       this.fechaRequisicion = fechaRequisicion;
       this.programa = programa;
       this.nombreProceso = nombreProceso;
       this.numeroProceso = numeroProceso;
       this.presupuestoEstimado = presupuestoEstimado;
       this.idGasto = idGasto;
       this.componente = componente;
       this.disponibilidadReal = disponibilidadReal;
       this.actividad = actividad;
       this.unidadSolicitante = unidadSolicitante;
       this.metodoSeleccion = metodoSeleccion;
       this.itemsRequerimientos = itemsRequerimientos;
    }
   
     @Id 

    
    @Column(name="codigo_requerimiento", unique=true, nullable=false, length=75)
    public String getCodigoRequerimiento() {
        return this.codigoRequerimiento;
    }
    
    public void setCodigoRequerimiento(String codigoRequerimiento) {
        this.codigoRequerimiento = codigoRequerimiento;
    }

    
    @Column(name="oficina_requisicion", length=100)
    public String getOficinaRequisicion() {
        return this.oficinaRequisicion;
    }
    
    public void setOficinaRequisicion(String oficinaRequisicion) {
        this.oficinaRequisicion = oficinaRequisicion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="fecha_requisicion", length=13)
    public Date getFechaRequisicion() {
        return this.fechaRequisicion;
    }
    
    public void setFechaRequisicion(Date fechaRequisicion) {
        this.fechaRequisicion = fechaRequisicion;
    }

    
    @Column(name="programa", length=100)
    public String getPrograma() {
        return this.programa;
    }
    
    public void setPrograma(String programa) {
        this.programa = programa;
    }

    
    @Column(name="nombre_proceso")
    public String getNombreProceso() {
        return this.nombreProceso;
    }
    
    public void setNombreProceso(String nombreProceso) {
        this.nombreProceso = nombreProceso;
    }

    
    @Column(name="numero_proceso")
    public String getNumeroProceso() {
        return this.numeroProceso;
    }
    
    public void setNumeroProceso(String numeroProceso) {
        this.numeroProceso = numeroProceso;
    }

    
    @Column(name="presupuesto_estimado", precision=12)
    public BigDecimal getPresupuestoEstimado() {
        return this.presupuestoEstimado;
    }
    
    public void setPresupuestoEstimado(BigDecimal presupuestoEstimado) {
        this.presupuestoEstimado = presupuestoEstimado;
    }

    
    @Column(name="id_gasto", length=100)
    public String getIdGasto() {
        return this.idGasto;
    }
    
    public void setIdGasto(String idGasto) {
        this.idGasto = idGasto;
    }

    
    @Column(name="componente", length=150)
    public String getComponente() {
        return this.componente;
    }
    
    public void setComponente(String componente) {
        this.componente = componente;
    }

    
    @Column(name="disponibilidad_real", precision=15)
    public BigDecimal getDisponibilidadReal() {
        return this.disponibilidadReal;
    }
    
    public void setDisponibilidadReal(BigDecimal disponibilidadReal) {
        this.disponibilidadReal = disponibilidadReal;
    }

    
    @Column(name="actividad")
    public String getActividad() {
        return this.actividad;
    }
    
    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    
    @Column(name="unidad_solicitante", length=100)
    public String getUnidadSolicitante() {
        return this.unidadSolicitante;
    }
    
    public void setUnidadSolicitante(String unidadSolicitante) {
        this.unidadSolicitante = unidadSolicitante;
    }

    
    @Column(name="metodo_seleccion", length=10)
    public String getMetodoSeleccion() {
        return this.metodoSeleccion;
    }
    
    public void setMetodoSeleccion(String metodoSeleccion) {
        this.metodoSeleccion = metodoSeleccion;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="requisicionCompra")
    public Set<ItemsRequerimiento> getItemsRequerimientos() {
        return this.itemsRequerimientos;
    }
    
    public void setItemsRequerimientos(Set<ItemsRequerimiento> itemsRequerimientos) {
        this.itemsRequerimientos = itemsRequerimientos;
    }




}


