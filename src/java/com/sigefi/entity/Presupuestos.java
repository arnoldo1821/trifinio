package com.sigefi.entity;
// Generated 10-12-2015 09:40:19 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Presupuestos generated by hbm2java
 */
@Entity
@Table(name="presupuestos"
    ,schema="presupuestos"
)
public class Presupuestos  implements java.io.Serializable {


     private int codigoPresupuesto;
     private UnidadesEjecutoras unidadesEjecutoras;
     private Integer anyoPresupuesto;
     private Character monedaPresupuesto;
     private Date fechaCreacionPresupuesto;
     private String usuarioCreador;
     private Boolean liquidado;
     private Boolean esProyecto;
     private Set<CuentasPresupuesto> cuentasPresupuestos = new HashSet<CuentasPresupuesto>(0);
     private Set<SegumientoCuenta> segumientoCuentas = new HashSet<SegumientoCuenta>(0);
     private Set<DetalleCuentas> detalleCuentases = new HashSet<DetalleCuentas>(0);

    public Presupuestos() {
    }

	
    public Presupuestos(int codigoPresupuesto, Date fechaCreacionPresupuesto) {
        this.codigoPresupuesto = codigoPresupuesto;
        this.fechaCreacionPresupuesto = fechaCreacionPresupuesto;
    }
    public Presupuestos(int codigoPresupuesto, UnidadesEjecutoras unidadesEjecutoras, Integer anyoPresupuesto, Character monedaPresupuesto, Date fechaCreacionPresupuesto, String usuarioCreador, Boolean liquidado, Boolean esProyecto, Set<CuentasPresupuesto> cuentasPresupuestos, Set<SegumientoCuenta> segumientoCuentas, Set<DetalleCuentas> detalleCuentases) {
       this.codigoPresupuesto = codigoPresupuesto;
       this.unidadesEjecutoras = unidadesEjecutoras;
       this.anyoPresupuesto = anyoPresupuesto;
       this.monedaPresupuesto = monedaPresupuesto;
       this.fechaCreacionPresupuesto = fechaCreacionPresupuesto;
       this.usuarioCreador = usuarioCreador;
       this.liquidado = liquidado;
       this.esProyecto = esProyecto;
       this.cuentasPresupuestos = cuentasPresupuestos;
       this.segumientoCuentas = segumientoCuentas;
       this.detalleCuentases = detalleCuentases;
    }
   
     @Id 

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="codigo_presupuesto", unique=true, nullable=false)
    public int getCodigoPresupuesto() {
        return this.codigoPresupuesto;
    }
    
    public void setCodigoPresupuesto(int codigoPresupuesto) {
        this.codigoPresupuesto = codigoPresupuesto;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="unidad_ejecutora")
    public UnidadesEjecutoras getUnidadesEjecutoras() {
        return this.unidadesEjecutoras;
    }
    
    public void setUnidadesEjecutoras(UnidadesEjecutoras unidadesEjecutoras) {
        this.unidadesEjecutoras = unidadesEjecutoras;
    }

    
    @Column(name="anyo_presupuesto")
    public Integer getAnyoPresupuesto() {
        return this.anyoPresupuesto;
    }
    
    public void setAnyoPresupuesto(Integer anyoPresupuesto) {
        this.anyoPresupuesto = anyoPresupuesto;
    }

    
    @Column(name="moneda_presupuesto", length=1)
    public Character getMonedaPresupuesto() {
        return this.monedaPresupuesto;
    }
    
    public void setMonedaPresupuesto(Character monedaPresupuesto) {
        this.monedaPresupuesto = monedaPresupuesto;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="fecha_creacion_presupuesto", nullable=false, length=13)
    public Date getFechaCreacionPresupuesto() {
        return this.fechaCreacionPresupuesto;
    }
    
    public void setFechaCreacionPresupuesto(Date fechaCreacionPresupuesto) {
        this.fechaCreacionPresupuesto = fechaCreacionPresupuesto;
    }

    
    @Column(name="usuario_creador", length=50)
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }
    
    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    
    @Column(name="liquidado")
    public Boolean getLiquidado() {
        return this.liquidado;
    }
    
    public void setLiquidado(Boolean liquidado) {
        this.liquidado = liquidado;
    }

    
    @Column(name="es_proyecto")
    public Boolean getEsProyecto() {
        return this.esProyecto;
    }
    
    public void setEsProyecto(Boolean esProyecto) {
        this.esProyecto = esProyecto;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="presupuestos")
    public Set<CuentasPresupuesto> getCuentasPresupuestos() {
        return this.cuentasPresupuestos;
    }
    
    public void setCuentasPresupuestos(Set<CuentasPresupuesto> cuentasPresupuestos) {
        this.cuentasPresupuestos = cuentasPresupuestos;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="presupuestos")
    public Set<SegumientoCuenta> getSegumientoCuentas() {
        return this.segumientoCuentas;
    }
    
    public void setSegumientoCuentas(Set<SegumientoCuenta> segumientoCuentas) {
        this.segumientoCuentas = segumientoCuentas;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="presupuestos")
    public Set<DetalleCuentas> getDetalleCuentases() {
        return this.detalleCuentases;
    }
    
    public void setDetalleCuentases(Set<DetalleCuentas> detalleCuentases) {
        this.detalleCuentases = detalleCuentases;
    }




}


