/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 *
 * @author Elmer
 */
@Entity
@Table(name="centro_costo"
    ,schema="presupuestos"
)
public class CentroCosto implements java.io.Serializable {
     private int idCentroCosto;
     private String centroCosto;
     private boolean activa;

    public CentroCosto() {
    }
     
     public CentroCosto(int idCentroCosto,String centroCosto,boolean activa){
         this.idCentroCosto = idCentroCosto;
         this.centroCosto = centroCosto;
         this.activa = activa;
     }
    
     @Id 

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_centro_costo", unique=true, nullable=false)
    public int getIdCentroCosto() {
        return this.idCentroCosto;
    }
    
    public void setIdCentroCosto(int idCentroCosto) {
        this.idCentroCosto = idCentroCosto;
    }
    
    @Column(name="centro_costo", length=150)
    public String getCentroCosto() {
        return this.centroCosto;
    }
    
    public void setCentroCosto(String centroCosto) {
        this.centroCosto = centroCosto;
    }
    
    @Column(name="activa")
    public Boolean getActiva() {
        return this.activa;
    }
    
    public void setActiva(Boolean activa) {
        this.activa = activa;
    }
}
