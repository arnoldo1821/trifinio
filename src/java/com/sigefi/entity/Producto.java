package com.sigefi.entity;
// Generated 10-12-2015 09:40:19 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Producto generated by hbm2java
 */
@Entity
@Table(name="producto"
    ,schema="public"
)
public class Producto  implements java.io.Serializable {


     private int idProducto;
     private CategoriaProducto categoriaProducto;
     private String nombre;
     private String codigoProducto;
     private Set<DetalleVenta> detalleVentas = new HashSet<DetalleVenta>(0);

    public Producto() {
    }

	
    public Producto(int idProducto) {
        this.idProducto = idProducto;
    }
    public Producto(int idProducto, CategoriaProducto categoriaProducto, String nombre, String codigoProducto, Set<DetalleVenta> detalleVentas) {
       this.idProducto = idProducto;
       this.categoriaProducto = categoriaProducto;
       this.nombre = nombre;
       this.codigoProducto = codigoProducto;
       this.detalleVentas = detalleVentas;
    }
   
     @Id 

    
    @Column(name="id_producto", unique=true, nullable=false)
    public int getIdProducto() {
        return this.idProducto;
    }
    
    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_categoria_producto")
    public CategoriaProducto getCategoriaProducto() {
        return this.categoriaProducto;
    }
    
    public void setCategoriaProducto(CategoriaProducto categoriaProducto) {
        this.categoriaProducto = categoriaProducto;
    }

    
    @Column(name="nombre", length=500)
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    
    @Column(name="codigo_producto", length=50)
    public String getCodigoProducto() {
        return this.codigoProducto;
    }
    
    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="producto")
    public Set<DetalleVenta> getDetalleVentas() {
        return this.detalleVentas;
    }
    
    public void setDetalleVentas(Set<DetalleVenta> detalleVentas) {
        this.detalleVentas = detalleVentas;
    }




}


