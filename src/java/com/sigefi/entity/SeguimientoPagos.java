package com.sigefi.entity;
// Generated 10-12-2015 09:40:19 PM by Hibernate Tools 4.3.1


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * SeguimientoPagos generated by hbm2java
 */
@Entity
@Table(name="seguimiento_pagos"
    ,schema="calidad"
)
public class SeguimientoPagos  implements java.io.Serializable {


     private SeguimientoPagosId id;
     private Integer noConformidad;
     private String usuarioEstableceEstado;

    public SeguimientoPagos() {
    }

	
    public SeguimientoPagos(SeguimientoPagosId id) {
        this.id = id;
    }
    public SeguimientoPagos(SeguimientoPagosId id, Integer noConformidad, String usuarioEstableceEstado) {
       this.id = id;
       this.noConformidad = noConformidad;
       this.usuarioEstableceEstado = usuarioEstableceEstado;
    }
   
     @EmbeddedId

    
    @AttributeOverrides( {
        @AttributeOverride(name="codigoRequerimiento", column=@Column(name="codigo_requerimiento", nullable=false, length=25) ), 
        @AttributeOverride(name="fechaNoConformidad", column=@Column(name="fecha_no_conformidad", nullable=false, length=35) ) } )
    public SeguimientoPagosId getId() {
        return this.id;
    }
    
    public void setId(SeguimientoPagosId id) {
        this.id = id;
    }

    
    @Column(name="no_conformidad")
    public Integer getNoConformidad() {
        return this.noConformidad;
    }
    
    public void setNoConformidad(Integer noConformidad) {
        this.noConformidad = noConformidad;
    }

    
    @Column(name="usuario_establece_estado", length=35)
    public String getUsuarioEstableceEstado() {
        return this.usuarioEstableceEstado;
    }
    
    public void setUsuarioEstableceEstado(String usuarioEstableceEstado) {
        this.usuarioEstableceEstado = usuarioEstableceEstado;
    }




}


