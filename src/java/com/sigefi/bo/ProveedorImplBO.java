/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.ProveedorBean;
import com.sigefi.dao.ProveedorDAO;
import com.sigefi.entity.PartesInteresadas;
import com.sigefi.entity.PartesInteresadasId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "proveedorBO")
public class ProveedorImplBO implements ProveedorBO{
    @Autowired
    private ProveedorDAO proveedorDAO;

    public ProveedorDAO getProveedorDAO() {
        return proveedorDAO;
    }

    public void setProveedorDAO(ProveedorDAO proveedorDAO) {
        this.proveedorDAO = proveedorDAO;
    }

    @Override
    public boolean insertProveedor(ProveedorBean obj) {

        PartesInteresadas consulPartes = proveedorDAO.elementoProveedor(obj.getCodigoProveedor(),obj.getPaisOrigen());

        if (consulPartes == null) {
            
            PartesInteresadasId pId = new PartesInteresadasId();
            
            pId.setCodigoProveedor(obj.getCodigoProveedor());
            pId.setPaisOrigen(obj.getPaisOrigen());
            
            PartesInteresadas p = new PartesInteresadas();

            p.setId(pId);
            p.setNombreProveedor(obj.getNombreProveedor());
            p.setTelefonoProveedor(obj.getTelefonoProveedor());
            p.setFaxProveedor(obj.getFaxProveedor());
            p.setEMailProveedor(obj.getEMailProveedor());
            p.setRepresentante(obj.getRepresentante());
            p.setTipoDocuemnto(obj.getTipoDocumento());
            p.setDireccion(obj.getDireccion());
            p.setWeb(obj.getWeb());
            p.setMetodoPago(obj.getMetodoPago());
            p.setCondicionPago(obj.getCondicionPago());
            p.setTipoProveedor(obj.getTipoProveedor());
            p.setBien(obj.getBien());
            p.setActivo(true);

            proveedorDAO.insertProveedor(p);

            return true;
        }

        return false;
    }

    @Override
    public boolean updateProveedor(ProveedorBean obj) {
     PartesInteresadas consulPartes = proveedorDAO.elementoProveedor(obj.getCodigoProveedor(),obj.getPaisOrigen());

        if (consulPartes != null) {
            
            PartesInteresadasId pId = new PartesInteresadasId();
            
            pId.setCodigoProveedor(obj.getCodigoProveedor());
            pId.setPaisOrigen(obj.getPaisOrigen());
            
            PartesInteresadas p = new PartesInteresadas();

            p.setId(pId);
            p.setNombreProveedor(obj.getNombreProveedor());
            p.setTelefonoProveedor(obj.getTelefonoProveedor());
            p.setFaxProveedor(obj.getFaxProveedor());
            p.setEMailProveedor(obj.getEMailProveedor());
            p.setRepresentante(obj.getRepresentante());
            p.setTipoDocuemnto(obj.getTipoDocumento());
            p.setDireccion(obj.getDireccion());
            p.setWeb(obj.getWeb());
            p.setMetodoPago(obj.getMetodoPago());
            p.setCondicionPago(obj.getCondicionPago());
            p.setTipoProveedor(obj.getTipoProveedor());
            p.setBien(obj.getBien());
            p.setActivo(true);

            proveedorDAO.updateProveedor(p);

            return true;
        }

        return false;
    }

    @Override
    public boolean deleteProveedor(ProveedorBean obj) {
   PartesInteresadas consulPartes = proveedorDAO.elementoProveedor(obj.getCodigoProveedor(),obj.getPaisOrigen());

        if (consulPartes != null) {
            
            PartesInteresadasId pId = new PartesInteresadasId();
            
            pId.setCodigoProveedor(obj.getCodigoProveedor());
            pId.setPaisOrigen(obj.getPaisOrigen());
            
            PartesInteresadas p = new PartesInteresadas();

            p.setId(pId);
            p.setNombreProveedor(obj.getNombreProveedor());
            p.setTelefonoProveedor(obj.getTelefonoProveedor());
            p.setFaxProveedor(obj.getFaxProveedor());
            p.setEMailProveedor(obj.getEMailProveedor());
            p.setRepresentante(obj.getRepresentante());
            p.setTipoDocuemnto(obj.getTipoDocumento());
            p.setDireccion(obj.getDireccion());
            p.setWeb(obj.getWeb());
            p.setMetodoPago(obj.getMetodoPago());
            p.setCondicionPago(obj.getCondicionPago());
            p.setTipoProveedor(obj.getTipoProveedor());
            p.setBien(obj.getBien());
            p.setActivo(false);

            proveedorDAO.updateProveedor(p);

            return true;
        }

        return false;
    }

    @Override
    public List<ProveedorBean> listProveedorAll() {
     List<ProveedorBean> listBean = new ArrayList<>();
     List<PartesInteresadas> listProveedor = proveedorDAO.listProveedorAll();
     if(listProveedor != null){
            for(PartesInteresadas obj : listProveedor){
                ProveedorBean bean = new ProveedorBean();
                
                bean.setCodigoProveedor(obj.getId().getCodigoProveedor());
                bean.setPaisOrigen(obj.getId().getPaisOrigen());
                bean.setNombreProveedor(obj.getNombreProveedor());
                bean.setTelefonoProveedor(obj.getTelefonoProveedor());
                bean.setFaxProveedor(obj.getFaxProveedor());
                bean.setEMailProveedor(obj.getEMailProveedor());
                bean.setRepresentante(obj.getRepresentante());
                bean.setDireccion(obj.getDireccion());
                bean.setWeb(obj.getWeb());
                bean.setMetodoPago(obj.getMetodoPago());
                bean.setCondicionPago(obj.getCondicionPago());
                bean.setTipoDocumento(obj.getTipoDocuemnto());
                bean.setBien(obj.getBien());
                bean.setTipoProveedor(obj.getTipoProveedor());
                bean.setActivo(obj.isActivo());
                
                listBean.add(bean);
            }
            
            return listBean;
        }
        return null;
    }
    
    @Override
    public TreeNode listNodoProveedores() {

        TreeNode root = new DefaultTreeNode("principal", null);

        Map<String, TreeNode> mapNodo = new HashMap<>();
        
        List<PartesInteresadas> listAllProveedores = proveedorDAO.listProveedorAll();
        
        if(listAllProveedores != null){
            for(PartesInteresadas proveedores : listAllProveedores){
               
                    TreeNode nodo = new DefaultTreeNode(proveedores, root); 
                    mapNodo.put(proveedores.getId().getCodigoProveedor(), nodo);
                
            }
        }
        
        
        return root;
    }

    @Override
    public List<ProveedorBean> listPartesInteresadas() {
    
     List<PartesInteresadas> listProveedor = proveedorDAO.listProveedorAll();
     
     if(listProveedor != null){
            List<ProveedorBean> listBean = new ArrayList<>();
            for(PartesInteresadas partes : listProveedor){
            ProveedorBean prov = new ProveedorBean();
            prov.setProveedor(partes);
            listBean.add(prov);
            }
            return listBean;
        }
        return null;
    }

}
