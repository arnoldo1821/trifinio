
package com.sigefi.bo;

import com.sigefi.entity.Contratos;
import java.util.List;

/**
 *
 * @author SKAR
 */
public interface ContratoBO {
    
    void insert(Contratos obj);
    void update(Contratos obj);
    void delete(Contratos obj);
    List<Contratos> selectAll();
    
}
