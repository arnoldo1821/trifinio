/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.CatalogoContabilidadBean;
import com.sigefi.dao.CatalogoContabilidadDAO;
import com.sigefi.entity.CatalogoCuentasConta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "catalogoContabilidadBO")
public class CatalogoContabilidadImplBO implements CatalogoContabilidadBO{
    @Autowired
    private CatalogoContabilidadDAO catalogoContabilidadDAO;

    public CatalogoContabilidadDAO getCatalogoContabilidadDAO() {
        return catalogoContabilidadDAO;
    }

    public void setCatalogoContabilidadDAO(CatalogoContabilidadDAO catalogoContabilidadDAO) {
        this.catalogoContabilidadDAO = catalogoContabilidadDAO;
    }

    @Override
    public boolean insertCuenta(CatalogoContabilidadBean obj) {
        
        String idCuenta = catalogoContabilidadDAO.ultimoRegistroNivel(String.valueOf(0));
        
        if (idCuenta != null) {
            CatalogoCuentasConta ca = new CatalogoCuentasConta();
            ca.setCodCatalocuenta(idCuenta);
            ca.setCuentapadre(String.valueOf(0));
            ca.setNombreCuentacatalo(obj.getNombreCuenta());
            ca.setNaturaleza(obj.getNaturaleza());
            ca.setTipo(obj.getTipo());
            ca.setDescripcionCatalo(obj.getDescripcionCuenta());
            ca.setEsCuentadetalle(true);
            
            catalogoContabilidadDAO.insertCuenta(ca);

            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean insertSubCuenta(CatalogoContabilidadBean obj) {
        String idCuenta = catalogoContabilidadDAO.ultimoRegistroNivel(obj.getIdCuenta());
        
        if (idCuenta != null) {
            
            CatalogoCuentasConta ca = new CatalogoCuentasConta();
            ca.setCodCatalocuenta(idCuenta);
            ca.setCuentapadre(obj.getIdCuenta());
            ca.setNombreCuentacatalo(obj.getNombreSubCuenta());
            ca.setDescripcionCatalo(obj.getDescripcionSubCuenta());
            ca.setNaturaleza(obj.getNaturaleza());
            ca.setTipo(obj.getTipo());
            ca.setDescripcionCatalo(obj.getDescripcionSubCuenta());
            ca.setEsCuentadetalle(true);
            
            catalogoContabilidadDAO.insertCuenta(ca);
            
            CatalogoCuentasConta cuentaUpdate = catalogoContabilidadDAO.elementoCatalogoCuenta(obj.getIdCuenta());
            
            if(cuentaUpdate != null){
                cuentaUpdate.setEsCuentadetalle(false);
                catalogoContabilidadDAO.updateCuenta(cuentaUpdate);
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public TreeNode listNodoCuentasCatalogo() {
        TreeNode root = new DefaultTreeNode("principal", null);
        Map<String, TreeNode> mapNodo = new HashMap<>();
        List<CatalogoCuentasConta> listAllCatalogoCuenta = catalogoContabilidadDAO.listAllCatalogoCuenta();
        
        if(listAllCatalogoCuenta != null){
            for(CatalogoCuentasConta cuenta : listAllCatalogoCuenta){
                if(cuenta.getCuentapadre().equals("0")){
                    TreeNode nodo = new DefaultTreeNode(cuenta, root); 
                    mapNodo.put(cuenta.getCodCatalocuenta(), nodo);
                }else{
                    TreeNode nodo2 = new DefaultTreeNode(cuenta, mapNodo.get(cuenta.getCuentapadre())); 
                    mapNodo.put(cuenta.getCodCatalocuenta(), nodo2);
                }
            }
        }
        return root;
    }
    
    
}
