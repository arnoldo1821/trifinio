/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.CajaChicaBean;
import com.sigefi.dao.CajaChicaDAO;
import com.sigefi.entity.DetalleCuentas;
import com.sigefi.entity.DetalleLiquidarCajaChica;
import com.sigefi.entity.DetalleLiquidarCajaChicaId;
import com.sigefi.entity.LiquidarCajaChica;
import com.sigefi.entity.PartesInteresadas;
import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "cajaChicaBO")
public class CajaChicaImplBO implements CajaChicaBO{
    @Autowired
    private CajaChicaDAO cajaChicaDAO;

    public CajaChicaDAO getCajaChicaDAO() {
        return cajaChicaDAO;
    }

    public void setCajaChicaDAO(CajaChicaDAO cajaChicaDAO) {
        this.cajaChicaDAO = cajaChicaDAO;
    }

    @Override
    public Presupuestos elementoPresupuesto(CajaChicaBean obj) {
        return cajaChicaDAO.elementoPresupuesto(obj);
    }

    @Override
    public List<PartesInteresadas> listProveedorAll() {
        return cajaChicaDAO.allProveedores();
    }

    @Override
    public LiquidarCajaChica insertLiquidacionCajaChica(CajaChicaBean obj) {
        try {
            LiquidarCajaChica liqConsulta = cajaChicaDAO.elementoCajaChhica(obj);
            if(liqConsulta == null){
                LiquidarCajaChica newLiq = new LiquidarCajaChica();
                
                newLiq.setCodigoRequerimientoCajaChica(obj.getCodigoRequerimiento());
                newLiq.setFechaPresentacion(obj.getFechaPresentacion());
                newLiq.setMoneda(obj.getMoneda());
                UnidadesEjecutoras uni = new UnidadesEjecutoras();
                uni.setCodigo(obj.getUnidadesEjecutoras());
                newLiq.setUnidadesEjecutoras(uni);
                newLiq.setAnioPresupuesto((short)obj.getAnyoPresupuesto());
                newLiq.setUsuario(obj.getUsuario());
                newLiq.setCodigoProveedor(obj.getNomProveedor());
                newLiq.setNombreProveedor(obj.getCodigoProveedor());
                
                
                cajaChicaDAO.inserReqCajaChic(newLiq);
                
                return newLiq;
            }else{
                return liqConsulta;
            }
        } catch (Exception e) {
        }
        
        return null;
    }

    @Override
    public List<DetalleCuentas> listDetalleCuentasAll(int idPresupuesto) {
        return cajaChicaDAO.allDetalleCuentas(idPresupuesto);
    }

    @Override
    public List<DetalleLiquidarCajaChica> correlativo_Total_DetalleRequerimientoCaja(int codigoRequerimientoIncre) {
        return cajaChicaDAO.correlativo_Total_DetalleRequerimientoCaja(codigoRequerimientoIncre);
    }

    @Override
    public boolean insertDetalleRequisicionCaja(CajaChicaBean obj) {
        
        DetalleLiquidarCajaChicaId detalleCajaId = new DetalleLiquidarCajaChicaId();
        
        detalleCajaId.setFkRequeCaja(obj.getLiqCajaAux().getPkIncreRequeCaja());
        detalleCajaId.setCorrelativo(obj.getCorrelativo());
        
        DetalleLiquidarCajaChica detalleCaja = new DetalleLiquidarCajaChica();
        
        detalleCaja.setId(detalleCajaId);
        detalleCaja.setCodigoCuenta(obj.getDetalleCuenta());
        detalleCaja.setNombreCuenta(obj.getNombreGastoEspecifico());
        detalleCaja.setMonto(obj.getTotalPagar());
        detalleCaja.setTipoDocumento(obj.getClaseDocumento());
        detalleCaja.setNumeroDocumento(obj.getNumeroDocumento());
        detalleCaja.setFechaDocumentoLiquidacion(obj.getFechaDocumento());
        
        cajaChicaDAO.insertDetalleRequerimientoCaja(detalleCaja);
        
        return true;
        
    }
    
    
    
}
