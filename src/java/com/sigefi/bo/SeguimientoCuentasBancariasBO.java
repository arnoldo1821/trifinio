/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.SeguimientoCuentasBancariasBean;
import com.sigefi.entity.SegumientoCuentaBancaria;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Elmer
 */
public interface SeguimientoCuentasBancariasBO {
    List<SegumientoCuentaBancaria> listAllSeguimientoCuentas(String codigoCuenta,Date inicio, Date fin);
    SegumientoCuentaBancaria seguimientoElemento(String codigoCuenta);
    boolean insertMovimeintoCargo(SeguimientoCuentasBancariasBean bean);
    boolean insertMovimientoCuenta(SeguimientoCuentasBancariasBean bean);
    boolean insertMovimientoNotaTesoreria(SeguimientoCuentasBancariasBean bean);
    boolean insertMovimientoEmisionCheque(SeguimientoCuentasBancariasBean bean);
    boolean insertMovimientoCheque(SeguimientoCuentasBancariasBean bean);
    boolean updateMovimientoCuenta(SegumientoCuentaBancaria obj);
    boolean deleteMovimientoCuenta(SegumientoCuentaBancaria obj);
}
