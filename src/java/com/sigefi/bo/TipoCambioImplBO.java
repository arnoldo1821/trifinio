/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.TipoCambioBean;
import com.sigefi.dao.TipoCambioDAO;
import com.sigefi.entity.TipoCambio;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "tipoCambioBO")
public class TipoCambioImplBO implements TipoCambioBO{
    
    @Autowired
    private TipoCambioDAO tipoCambioDAO;

    public TipoCambioDAO getTipoCambioDAO() {
        return tipoCambioDAO;
    }

    public void setTipoCambioDAO(TipoCambioDAO tipoCambioDAO) {
        this.tipoCambioDAO = tipoCambioDAO;
    }

    @Override
    public boolean insertTipoCambio(TipoCambioBean obj) {
        
        TipoCambio tipoCambio = tipoCambioDAO.elementoTipoCambio(obj.getFechaCambio());
        
        if(tipoCambio == null){   
            
            TipoCambio tipoC = new TipoCambio();
            
            tipoC.setFechaCambio(obj.getFechaCambio());
            tipoC.setQuetzales(obj.getQuetzales());
            tipoC.setLempiras(obj.getLempiras());
            tipoC.setDolar(obj.getDolar());
            
            tipoCambioDAO.insertTipoCambio(tipoC);
            return true;
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Tipo de cambio ya existe con dicha fecha" , ""));
            return false;
        }
    }

    @Override
    public boolean updateTipoCambio(TipoCambioBean obj) {
       
        TipoCambio tipoCambio = tipoCambioDAO.elementoTipoCambio(obj.getFechaCambio());
        
        if(tipoCambio != null){   
            
            TipoCambio tipoC = new TipoCambio();
            
            tipoC.setFechaCambio(obj.getFechaCambio());
            tipoC.setQuetzales(obj.getQuetzales());
            tipoC.setLempiras(obj.getLempiras());
            tipoC.setDolar(obj.getDolar());
           
            tipoCambioDAO.updateTipoCambio(tipoC);
            return true;
        }//Fin del if
        else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"No se pudo actualizar el registro" , ""));
            return false;
        }
    }

    @Override
    public boolean deleteTipoCambio(Date fechaCambio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<TipoCambioBean> listTipoCambioAll() {
        List<TipoCambioBean> listBean = new ArrayList<>();
        List<TipoCambio> lisTipo = tipoCambioDAO.listTipoCambioAll();
        if(lisTipo != null){
            for(TipoCambio obj : lisTipo){
                
                TipoCambioBean bean = new TipoCambioBean();
                
                bean.setFechaCambio(obj.getFechaCambio());
                bean.setQuetzales(obj.getQuetzales());
                bean.setLempiras(obj.getLempiras());
                bean.setDolar(obj.getDolar());
                
                listBean.add(bean);
            }
            
            return listBean;
        }
        return null;
    }
    
    
}