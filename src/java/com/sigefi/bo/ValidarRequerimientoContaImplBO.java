/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.ValidarRequerimientoContaBean;
import com.sigefi.clases.FechaSistema;
import com.sigefi.clases.SubirArchivoServer;
import com.sigefi.dao.RequisicionDAO;
import com.sigefi.dao.ValidarRequerimientoContaDAO;
import com.sigefi.entity.CatalogoCuentasConta;
import com.sigefi.entity.CatalogosConta;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.DocumentosRequerimientos;
import com.sigefi.entity.Empresa;
import com.sigefi.entity.EstadoRequisicion;
import com.sigefi.entity.EstadoRequisicionId;
import com.sigefi.entity.NoConformidades;
import com.sigefi.entity.RequerimientoPagos;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.List;
import java.util.Set;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "validarRequerimientoContaBO")
public class ValidarRequerimientoContaImplBO implements ValidarRequerimientoContaBO {

    @Autowired
    private ValidarRequerimientoContaDAO validarRequerimientoContaDAO;

    public ValidarRequerimientoContaDAO getValidarRequerimientoContaDAO() {
        return validarRequerimientoContaDAO;
    }

    public void setValidarRequerimientoContaDAO(ValidarRequerimientoContaDAO validarRequerimientoContaDAO) {
        this.validarRequerimientoContaDAO = validarRequerimientoContaDAO;
    }

    @Autowired
    private RequisicionDAO requisicionDAO;

    public RequisicionDAO getRequisicionDAO() {
        return requisicionDAO;
    }

    public void setRequisicionDAO(RequisicionDAO requisicionDAO) {
        this.requisicionDAO = requisicionDAO;
    }

    @Override
    public List<RequerimientoPagos> listAllRequePago() {
        return validarRequerimientoContaDAO.listAllRequePago();
    }

    @Override
    public List<DetalleRequerimientoPago> listDetalleRequePagoSelec(int incre_req_pk) {
        return validarRequerimientoContaDAO.listDetalleRequePagoSelec(incre_req_pk);
    }

    @Override
    public List<DocumentosRequerimientos> listDocRequePagoSelec(int incre_req_pk) {
        return validarRequerimientoContaDAO.listDocRequePagoSelec(incre_req_pk);
    }

    @Override
    public List<NoConformidades> listAllErrores() {
        return validarRequerimientoContaDAO.listAllErrores();
    }

    @Override
    public boolean validarRequerimiento(RequerimientoPagos req, ValidarRequerimientoContaBean obj) {
        try {
            EstadoRequisicion consul = requisicionDAO.elementoEstadoRequisicion(req);
            if (consul != null) {
                consul.setControl(false);
                requisicionDAO.updateEstadoRequi(consul);
                
                if (obj.isEstadoValidacion()) {
                    EstadoRequisicionId estaId = new EstadoRequisicionId();
                    FechaSistema f = new FechaSistema();
                    estaId.setFechaEstado(f.getFechaCompleta());
                    estaId.setIncreReqPk3(req.getIncrReqPk());

                    EstadoRequisicion esta1 = new EstadoRequisicion();

                    esta1.setId(estaId);
                    esta1.setCodigoEstado((short) 6);
                    esta1.setUsuarioEstableceEstado(obj.getUsuarioLogeado());
                    esta1.setControl(true);

                    validarRequerimientoContaDAO.insertEstadoRequisicion(esta1);

                    RequestContext.getCurrentInstance().execute("PF('validarReq').hide()");
                    RequestContext.getCurrentInstance().execute("PF('reuerimiento').hide()");
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Enviado a Tesoreria correctamente", ""));
                    return true;
                } else {
                    EstadoRequisicionId estaId = new EstadoRequisicionId();
                    FechaSistema f = new FechaSistema();
                    estaId.setFechaEstado(f.getFechaCompleta());
                    estaId.setIncreReqPk3(req.getIncrReqPk());

                    EstadoRequisicion esta = new EstadoRequisicion();

                    esta.setId(estaId);
                    esta.setCodigoEstado((short) 3);
                    esta.setUsuarioEstableceEstado(obj.getUsuarioLogeado());
                    esta.setObservacionEstado(obj.getObservaciones());
                    NoConformidades nc = new NoConformidades(obj.getErrorSeleccionado());
                    esta.setNoConformidades(nc);
                    esta.setControl(true);

                    validarRequerimientoContaDAO.insertEstadoRequisicion(esta);

                    RequestContext.getCurrentInstance().execute("PF('noValidarReq').hide()");
                    RequestContext.getCurrentInstance().execute("PF('reuerimiento').hide()");
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Enviado a Corrección", ""));
                    return true;
                }
            }else{
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean eliminarRequerimiento(RequerimientoPagos req) {
        try {
            Set<DetalleRequerimientoPago> detalle = req.getDetalleRequerimientoPagos();
            if (detalle != null) {
                for (DetalleRequerimientoPago de : detalle) {
                    validarRequerimientoContaDAO.deleteDetalleRequerimiento(de);
                }
            }

            Set<DocumentosRequerimientos> docSet = req.getDocumentosRequerimientoses();
            if (docSet != null) {
                SubirArchivoServer su = new SubirArchivoServer();
                for (DocumentosRequerimientos doc : docSet) {
                    su.borrarArchivo(doc.getUrlDocumento());
                    validarRequerimientoContaDAO.deleteDocumentoRequerimiento(doc);
                }
            }

            Set<EstadoRequisicion> estdoSet = req.getEstadoRequisicions();
            if (estdoSet != null) {
                for (EstadoRequisicion estado : estdoSet) {
                    validarRequerimientoContaDAO.deleteEstadoRequerimiento(estado);
                }
            }

            validarRequerimientoContaDAO.deleteRequerimiento(req);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<CatalogosConta> listCataloCuentaConta(Empresa emp, short anio) {
        return validarRequerimientoContaDAO.listCataloCuentaConta(emp, anio);
    }

    @Override
    public Empresa elementoEmpresa(UnidadesEjecutoras unidadEjecutora, int anio) {
        return validarRequerimientoContaDAO.elementoEmpresa(unidadEjecutora, anio);
    }

}
