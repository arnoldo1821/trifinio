/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.RequisicionBean;
import com.sigefi.clases.FechaSistema;
import com.sigefi.dao.RequisicionDAO;
import com.sigefi.entity.CentroCosto;
import com.sigefi.entity.DetalleCuentas;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.DetalleRequerimientoPagoId;
import com.sigefi.entity.EstadoRequisicion;
import com.sigefi.entity.EstadoRequisicionId;
import com.sigefi.entity.PartesInteresadas;
import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.RequerimientoPagos;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrador
 */
@Service(value = "requisicionBO")
public class RequisicionImplBO implements RequisicionBO {

    @Autowired
    private RequisicionDAO requisicionDAO;

    public RequisicionDAO getRequisicionDAO() {
        return requisicionDAO;
    }

    public void setRequisicionDAO(RequisicionDAO requisicionDAO) {
        this.requisicionDAO = requisicionDAO;
    }

    @Override
    public RequerimientoPagos insertRequisicion(RequisicionBean obj) {
//        try {
//            RequerimientoPagos requePaConsul = requisicionDAO.elementoRequerimientoPagos(obj.getCodigoRequerimiento(),obj.getUnidadesEjecutoras(),obj.getAnyoPresupuesto());
            RequerimientoPagos requePaConsul = requisicionDAO.elementoRequerimientoPagos(obj);

            if (requePaConsul == null) {

                RequerimientoPagos requePa = new RequerimientoPagos();

                requePa.setCodigoRequerimiento(obj.getCodigoRequerimiento());
                requePa.setFechaPresentacion(obj.getFechaPresentacion());
                requePa.setFechaEstimadaPago(obj.getFechaEstimadaPago());
                requePa.setMoneda(obj.getMoneda());
                requePa.setSiafi(obj.isTipo1());
                requePa.setAfectacionPresupuestaria(obj.isTipo2());
                requePa.setCodigoSiafi(obj.getCodigoSiafi());
                UnidadesEjecutoras uni = new UnidadesEjecutoras();
                uni.setCodigo(obj.getUnidadesEjecutoras());
                requePa.setUnidadesEjecutoras(uni);
                CentroCosto centro = new CentroCosto();
                centro.setIdCentroCosto(obj.getCodcentroCosto());
                requePa.setCentroCosto(centro);
                requePa.setAnioPresupuesto(obj.getAnyoPresupuesto());
                requePa.setUsuario(obj.getUsuario());

                requisicionDAO.insertRequerimientoPagos(requePa);

                EstadoRequisicion consul = requisicionDAO.elementoEstadoRequisicion(requePa);
                
                if (consul == null) {
                    EstadoRequisicionId estaId = new EstadoRequisicionId();
                    estaId.setIncreReqPk3(requePa.getIncrReqPk());
                    FechaSistema fs = new FechaSistema();
                    estaId.setFechaEstado(fs.getFechaCompleta());
                    EstadoRequisicion esta = new EstadoRequisicion();
                    esta.setId(estaId);
                    esta.setCodigoEstado((short) 1);
                    esta.setUsuarioEstableceEstado(obj.getUsuario());
                    esta.setControl(true);
                    requisicionDAO.insertEstadoRequi(esta);
                }
                
                return requePa;

            } else {

                return requePaConsul;
            }

//        } catch (Exception e) {
////            return false;
//        }

//        return null;

    }

    @Override
    public boolean updateRequisicion(RequisicionBean obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean deleteRequisicion(String pkRequisicion) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<RequisicionBean> listRequisicionAll() {
        List<RequerimientoPagos> listPagos = requisicionDAO.allRequerimientoPagos();
        List<RequisicionBean> listBean = new ArrayList<>();
        if (listPagos != null) {
            int i = 0;
            for (RequerimientoPagos obj : listPagos) {
                i++;
                RequisicionBean bean = new RequisicionBean();

//                bean.setCorrelativo(String.valueOf(i));
                bean.setFechaPresentacion(obj.getFechaPresentacion());
//                bean.setTotalPagar(obj.getNumeroDocumento());

            }
        }
        return listBean;
    }

    @Override
    public Boolean consultarRequisicion(String usuario, RequisicionBean obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PartesInteresadas> listProveedorAll() {
        return requisicionDAO.allProveedores();
    }

    @Override
    public List<DetalleCuentas> listDetalleCuentasAll(int idPresupuesto) {
        return requisicionDAO.allDetalleCuentas(idPresupuesto);
    }

    @Override
    public Presupuestos elementoPresupuesto(RequisicionBean obj) {
        return requisicionDAO.elementoPresupuesto(obj);
    }

    @Override
    public List<DetalleRequerimientoPago> correlativo_Total_DetalleRequerimiento(int pkRequerimiento) {
        return requisicionDAO.correlativo_Total_DetalleRequerimiento(pkRequerimiento);
    }

    @Override
    public boolean insertDetalleRequisicion(RequisicionBean obj) {

        DetalleRequerimientoPagoId detallePagoId = new DetalleRequerimientoPagoId();

        detallePagoId.setFkPkRequerimiento2(obj.getRequerimientoPagosAux().getIncrReqPk());
        detallePagoId.setCorrelativo(obj.getCorrelativo());

        DetalleRequerimientoPago detallePago = new DetalleRequerimientoPago();

        detallePago.setId(detallePagoId);
        detallePago.setCodigoProveedor(obj.getProveedor());
        detallePago.setNombreProveedor(obj.getNombreProveedor());
        detallePago.setConcepto(obj.getConcepto());
        detallePago.setCodigoCuenta(String.valueOf(obj.getDetalleCuenta()));
        detallePago.setConceptoGasto(obj.getNombreGastoEspecifico());
        detallePago.setMonto(obj.getTotalPagar());

        detallePago.setClaseDocumento(obj.getClaseDocumento());
        detallePago.setNumeroDocumento(obj.getNumeroDocumento());
        detallePago.setFechaDocumento(obj.getFechaDocumento());

        requisicionDAO.insertDetalleRequerimientoPagos(detallePago);

        return true;

    }

    @Override
    public List<CentroCosto> listCentroAll() {
        return requisicionDAO.allCentro();
    }

    @Override
    public UnidadesEjecutoras elementoUnidad(RequisicionBean obj) {
        return requisicionDAO.elementoUnidadEjecutora(obj);
    }

    @Override
    public RequerimientoPagos elementoRequerimiento() {
        return requisicionDAO.elementoRequerimiento();
    }

    @Override
    public PartesInteresadas elementoProveedor(RequisicionBean obj) {
        return requisicionDAO.elementoProveedor(obj);
    }

}
