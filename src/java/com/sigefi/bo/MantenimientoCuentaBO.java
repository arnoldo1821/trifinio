/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.entity.CatalogoCuentas;

/**
 *
 * @author AAldemaro
 */
public interface MantenimientoCuentaBO {
    boolean updateCuenta(CatalogoCuentas obj); 
    boolean deleteCuenta(CatalogoCuentas obj);
}
