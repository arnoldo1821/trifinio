/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.TipoCambioBean;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface TipoCambioBO {
    boolean insertTipoCambio(TipoCambioBean obj);
    boolean updateTipoCambio(TipoCambioBean obj);
    boolean deleteTipoCambio(Date fechaCambio);
    List<TipoCambioBean> listTipoCambioAll();
}
