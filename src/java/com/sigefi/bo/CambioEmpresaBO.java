/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.entity.Empresa;

/**
 *
 * @author AAldemaro
 */
public interface CambioEmpresaBO {
    Empresa consultarEmpresa(short idUnidadEje,int anio);
}
