/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.BeanLogin;

/**
 *
 * @author alberto
 */
public interface LoginBO {
    void validaLogin(BeanLogin obj);  
    void bitacora(String usuario,String ip,int val);
}
