/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.TipoCambioContaBean;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface TipoCambioContaBO {
    boolean insertTipoCambioConta(TipoCambioContaBean obj);
    boolean updateTipoCambioConta(TipoCambioContaBean obj);
    boolean deleteTipoCambioConta(Date fechaCambio);
    List<TipoCambioContaBean> listTipoCambioContaAll();
}
