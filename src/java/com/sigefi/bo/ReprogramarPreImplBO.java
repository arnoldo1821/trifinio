/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.ReprogramarPreBean;
import com.sigefi.dao.ReprogramarPreDAO;
import com.sigefi.entity.DetalleCuentas;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.faces.component.UIData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "reprogramarPreBO")
public class ReprogramarPreImplBO implements ReprogramarPreBO {

    @Autowired
    private ReprogramarPreDAO reprogramarPreDAO;

    public ReprogramarPreDAO getReprogramarPreDAO() {
        return reprogramarPreDAO;
    }

    public void setReprogramarPreDAO(ReprogramarPreDAO reprogramarPreDAO) {
        this.reprogramarPreDAO = reprogramarPreDAO;
    }

    @Override
    public List<DetalleCuentas> listDetalleCuentasPre(short unidadE, int anio) {
        return reprogramarPreDAO.listDetalleCuentasPre(unidadE, anio);
    }

    @Override
    public boolean modificarCuenta(DetalleCuentas obj) {
        try {
            reprogramarPreDAO.modificarCuenta(obj);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<ReprogramarPreBean> listNodoDetalleCuentas(short unidad, int anio) {
        List<ReprogramarPreBean> listBean = new ArrayList();
        List<DetalleCuentas> listCuenta = reprogramarPreDAO.listDetalleCuentasPre(unidad, anio);
        if (listCuenta != null) {
            for (DetalleCuentas obj : listCuenta) {
                ReprogramarPreBean bean = new ReprogramarPreBean();

                bean.setCodigoCuenta(obj.getId().getCodgioCuenta());
                bean.setCodigoPresupuesto(obj.getPresupuestos().getCodigoPresupuesto());
                bean.setPresupuestoAprobado(obj.getPresupuestoAprobado());
                bean.setPresupuestoComprometido(obj.getPresupuestoComprometido());
                bean.setPresupuestoReal(obj.getPresupuestoReal());
                bean.setPresupuestoReprogramar(obj.getPresupuestoReprogramar());
                bean.setCargo(obj.getCargo());
                bean.setAbono(obj.getAbono());

                listBean.add(bean);
            }
            return listBean;
        }
        return null;
    }

    @Override
    public boolean updateCuentas(UIData list) {
        try {
            BigDecimal aux = new BigDecimal(0);
            BigDecimal aux1;
            BigDecimal aux2;
            List<DetalleCuentas> listCuentas;
            listCuentas = (List<DetalleCuentas>) list.getValue();
            for (DetalleCuentas obj : listCuentas) {
                aux1 = aux.add(obj.getAbono());
                aux2 = aux1.subtract(obj.getCargo());

                obj.setPresupuestoReprogramar(aux2);
                reprogramarPreDAO.modificarCuenta(obj);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
