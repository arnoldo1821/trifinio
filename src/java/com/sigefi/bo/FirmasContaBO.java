/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.FirmasContaBean;
import com.sigefi.entity.Empresa;
import com.sigefi.entity.FirmasConta;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface FirmasContaBO {
    Empresa elementoEmpresa(short idUnidadEjecutora,int anio);
    boolean insertFirma(FirmasContaBean obj);
    List<FirmasConta> elementoFirmas(Empresa empresaFirma);
}
