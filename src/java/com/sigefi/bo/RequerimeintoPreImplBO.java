/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.dao.RequerimeintoPreDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "requerimeintoPreBO")
public class RequerimeintoPreImplBO implements RequerimeintoPreBO{
    @Autowired
    private RequerimeintoPreDAO requerimeintoPreDAO;

    public RequerimeintoPreDAO getRequerimeintoPreDAO() {
        return requerimeintoPreDAO;
    }

    public void setRequerimeintoPreDAO(RequerimeintoPreDAO requerimeintoPreDAO) {
        this.requerimeintoPreDAO = requerimeintoPreDAO;
    }
    
}
