/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.ValeCajaBean;
import com.sigefi.entity.ManualClasificatorio;
import java.util.List;

/**
 *
 * @author Aldemaro Gonzalez
 */
public interface ValeCajaBO {
    List<ManualClasificatorio> listManualClasiAll();
    boolean insertValeCaja(ValeCajaBean bean);
}
