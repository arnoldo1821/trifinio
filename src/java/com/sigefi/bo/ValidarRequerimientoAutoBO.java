/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.ValidarRequerimientoAutoBean;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.DocumentosRequerimientos;
import com.sigefi.entity.NoConformidades;
import com.sigefi.entity.RequerimientoPagos;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface ValidarRequerimientoAutoBO {
    List<RequerimientoPagos> listAllRequePago();
    List<DetalleRequerimientoPago> listDetalleRequePagoSelec(int incre_req_pk);
    List<DocumentosRequerimientos> listDocRequePagoSelec(int incre_req_pk);
    List<NoConformidades> listAllErrores();
    boolean validarRequerimiento(RequerimientoPagos req,ValidarRequerimientoAutoBean obj);
}
