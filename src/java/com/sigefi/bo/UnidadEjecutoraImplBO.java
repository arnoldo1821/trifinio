/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.UnidadEjecutoraBean;
import com.sigefi.dao.UnidadEjecutoraDAO;
import com.sigefi.entity.Ciudades;
import com.sigefi.entity.FirmasAutorizadas;
import com.sigefi.entity.FirmasAutorizadasId;
import com.sigefi.entity.FirmasAutorizadasProyecto;
import com.sigefi.entity.FirmasAutorizadasProyectoId;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "unidadEjecutoraBO")
public class UnidadEjecutoraImplBO implements UnidadEjecutoraBO {

    @Autowired
    private UnidadEjecutoraDAO unidadEjecutoraDAO;

    public UnidadEjecutoraDAO getUnidadEjecutoraDAO() {
        return unidadEjecutoraDAO;
    }

    public void setUnidadEjecutoraDAO(UnidadEjecutoraDAO unidadEjecutoraDAO) {
        this.unidadEjecutoraDAO = unidadEjecutoraDAO;
    }

    @Override
    public boolean insertUnidadEjecutora(UnidadEjecutoraBean obj) {

        UnidadesEjecutoras consulUnidad = unidadEjecutoraDAO.elementoUnidadEjecutora(obj.getNombreUnidadEjecutora());

        if (consulUnidad == null) {

            UnidadesEjecutoras ue = new UnidadesEjecutoras();
            
            String auxPaisesCiudad = "";
            int i=0,aux = obj.getIdCiudad().size();
          
            for (String objCP : obj.getIdCiudad()) {
                i++;
                if(i==aux){
                    auxPaisesCiudad = auxPaisesCiudad + objCP;
                }else{
                    auxPaisesCiudad = auxPaisesCiudad + objCP + ",";
                }
            }

            ue.setIdCiudad(auxPaisesCiudad);
            ue.setDirector(obj.getDirector());
            ue.setNombreUnidad(obj.getNombreUnidadEjecutora());
            ue.setReponsable(obj.getReponsable());
            ue.setPrefijoCodigo(obj.getPrefijoCodigo());

            if (obj.isTipo()) {
                ue.setEsProyecto(true);
            } else {
                ue.setEsProyecto(false);
            }
            
            if (obj.isTipo2()){
                ue.setEsCheque(true);
            }else{
                ue.setEsCheque(false);
            }

            unidadEjecutoraDAO.insertUnidad(ue);

            return true;

        }
        
        

        return false;
    }

    @Override
    public List<Ciudades> listAllCiudades() {
        return unidadEjecutoraDAO.listAllCiudades();
    }

    @Override
    public List<UnidadEjecutoraBean> listAllUnidadesEjecutoras() {
       /* List<UnidadesEjecutoras> listU = unidadEjecutoraDAO.listAllUnidadEjecutoras();
        if (listU != null) {
            List<UnidadEjecutoraBean> listBean = new ArrayList<>();
            for (UnidadesEjecutoras uni : listU) {
                UnidadEjecutoraBean bean = new UnidadEjecutoraBean();
                bean.setUnidadesEjecutoras(uni);
                if (uni.getFirmasAutorizadases().size() >= 4) {
                    bean.setEstadoFirma(true);
                } else {
                    if (uni.getFirmasAutorizadasProyectos().size() >= 4) {
                        bean.setEstadoFirma(true);
                    } else {
                        bean.setEstadoFirma(false);
                    }
                }

                listBean.add(bean);
            }

            return listBean;
        }

        return null;*/
        List<UnidadEjecutoraBean> listBean = new ArrayList();
        List<UnidadesEjecutoras> listUnidad = unidadEjecutoraDAO.listAllUnidadEjecutoras();
        if(listUnidad!=null){
            for(UnidadesEjecutoras obj : listUnidad){
                UnidadEjecutoraBean bean = new UnidadEjecutoraBean();
                bean.setCodigo(obj.getCodigo());
                bean.setEsProyecto(obj.getEsProyecto());
                bean.setIdciudad(obj.getIdCiudad());
                bean.setNombreUnidadEjecutora(obj.getNombreUnidad());
                bean.setPrefijoCodigo(obj.getPrefijoCodigo());
                bean.setDirector(obj.getDirector());
                bean.setReponsable(obj.getReponsable());
                bean.setEsCheque(obj.getEsCheque());
                 if (obj.getFirmasAutorizadases().size() >= 4) {
                    bean.setEstadoFirma(true);
                } else {
                    if (obj.getFirmasAutorizadasProyectos().size() >= 4) {
                        bean.setEstadoFirma(true);
                    } else {
                        bean.setEstadoFirma(false);
                    }
                }
                listBean.add(bean);
            }
            return listBean;
        }
    return null;
    }

    @Override
    public boolean insertFirmaAutorizada(UnidadEjecutoraBean obj) {

        if (obj.isEsProyecto() == false) {
            FirmasAutorizadas firmaConsultaA = unidadEjecutoraDAO.elementoFirmaAutorizadas(obj.getNombreUsuario(), obj.getCargo());

            if (firmaConsultaA == null) {
                FirmasAutorizadasId firmaId = new FirmasAutorizadasId();

                firmaId.setCargo(obj.getCargo());
                firmaId.setUsuario(obj.getUsuarioLogeado());

                FirmasAutorizadas firma = new FirmasAutorizadas();

                firma.setId(firmaId);
                firma.setNombre(obj.getNombreUsuario());
                firma.setUnidadesEjecutoras(obj.getUnidadesEjecutoras());

                firma.setUrlArchivo(obj.getUrlFirma());

                unidadEjecutoraDAO.insertFirmaAutorizada(firma);
                return true;
            }
        } else {
            FirmasAutorizadasProyecto firmaConsultaAP = unidadEjecutoraDAO.elementoFirmaAutorizadasProyecto(obj.getNombreUsuario(), obj.getCargo());

            if (firmaConsultaAP == null) {
                FirmasAutorizadasProyectoId firmaProyecId = new FirmasAutorizadasProyectoId();

                firmaProyecId.setCargo(obj.getCargo());
                firmaProyecId.setUsuario(obj.getUsuarioLogeado());

                FirmasAutorizadasProyecto firmaProyec = new FirmasAutorizadasProyecto();

                firmaProyec.setId(firmaProyecId);
                firmaProyec.setNombre(obj.getNombreUsuario());
                firmaProyec.setUnidadesEjecutoras(obj.getUnidadesEjecutoras());

                firmaProyec.setUrlArchivo(obj.getUrlFirma());

                unidadEjecutoraDAO.insertFirmaAutorizadaProyecto(firmaProyec);
                return true;
            }
        }

        return false;

    }

    @Override
    public boolean actualizaUnidadEjecutora(UnidadEjecutoraBean obj) {
        UnidadesEjecutoras consulUnidad = unidadEjecutoraDAO.elementoUnidadEjecutora2(obj.getCodigo());
       if (consulUnidad != null) {

            UnidadesEjecutoras ue = new UnidadesEjecutoras();
         
            ue.setCodigo(obj.getCodigo());
            ue.setDirector(obj.getDirector());
            ue.setNombreUnidad(obj.getNombreUnidadEjecutora());
            ue.setReponsable(obj.getReponsable());
            ue.setEsProyecto(obj.isEsProyecto());
            ue.setIdCiudad(obj.getIdciudad());
            ue.setEsCheque(obj.isEsCheque());
            ue.setPrefijoCodigo(obj.getPrefijoCodigo());
            
            unidadEjecutoraDAO.actualizaUnidad(ue);
           
            return true;
    }
        return false;
    }

    @Override
    public List<UnidadEjecutoraBean> listAllUnidades() {
        List<UnidadEjecutoraBean> listBean = new ArrayList();
        List<UnidadesEjecutoras> listUnidad = unidadEjecutoraDAO.listAllUnidadEjecutoras();
        if(listUnidad!=null){
            for(UnidadesEjecutoras obj : listUnidad){
                UnidadEjecutoraBean bean = new UnidadEjecutoraBean();
                bean.setNombreUnidadEjecutora(obj.getNombreUnidad());
                bean.setPrefijoCodigo(obj.getPrefijoCodigo());
                bean.setDirector(obj.getDirector());
                bean.setReponsable(obj.getReponsable());
                bean.setEsCheque(obj.getEsCheque());
                 if (obj.getFirmasAutorizadases().size() >= 4) {
                    bean.setEstadoFirma(true);
                } else {
                    if (obj.getFirmasAutorizadasProyectos().size() >= 4) {
                        bean.setEstadoFirma(true);
                    } else {
                        bean.setEstadoFirma(false);
                    }
                }
                listBean.add(bean);
            }
            return listBean;
        }
    return null;
    }

}
