/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.CuentasBancariasBean;
import com.sigefi.beans.SeguimientoCuentasBancariasBean;
import com.sigefi.dao.CuentasBancariasDAO;
import com.sigefi.entity.CuentasBancarias;
import com.sigefi.entity.DetalleRequerimientoPago;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "cuentasBancariasBO")
public class CuentasBancariasImplBO implements CuentasBancariasBO{
    @Autowired
    private CuentasBancariasDAO cuentasBancariasDAO;
    
    public CuentasBancariasDAO getCuentasBancariasDAO() {
        return cuentasBancariasDAO;
    }

    public void setCuentasBancariasDAO(CuentasBancariasDAO cuentasBancariasDAO) {
        this.cuentasBancariasDAO = cuentasBancariasDAO;
    }

    @Override
    public List<CuentasBancarias> listAllCuentas() {
        return cuentasBancariasDAO.listAllCuentas();
    }

    @Override
    public boolean insertCuentaBancaria(CuentasBancariasBean bean) {
        CuentasBancarias consul = cuentasBancariasDAO.elemntoCuentaBancaria(bean.getCodigoCuenta());
        if(consul == null){
            CuentasBancarias cb = new CuentasBancarias();
            cb.setCodigoCuenta(bean.getCodigoCuenta());
            cb.setCodigo(bean.getCodigoFuenteFinanciamiento());
            cb.setNombreCuenta(bean.getNombreCuenta());
            cb.setFechaCreacion(bean.getFechaCreacion());
            cb.setCuentaContableAsociada(bean.getCuentaContableAsociada());
            cb.setMoneda(bean.getMoneda());
            cb.setPais(bean.getPais());
            cb.setNombreBanco(bean.getNombreBanco());
            cb.setTipoCuenta(bean.getTipoCuenta());
            cb.setRevisadoContabilidad(Boolean.FALSE);
            cb.setSaldo(bean.getSaldoInicial());
            
            cuentasBancariasDAO.insertCuentaBancaria(cb);
         
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean updateCuentaBancaria(CuentasBancarias obj) {
        try {
            cuentasBancariasDAO.updateCuentaBancaria(obj);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean deleteCuentaBancaria(CuentasBancarias obj) {
        try {
            cuentasBancariasDAO.deleteCuentaBancaria(obj);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public CuentasBancarias cuentaElemento(SeguimientoCuentasBancariasBean obj) {
        return cuentasBancariasDAO.elemntoCuentaBancaria(obj);
    }

    @Override
    public List<DetalleRequerimientoPago> listRequerimientoPago() {
        return cuentasBancariasDAO.listRequerimientoPago();
    }

    @Override
    public DetalleRequerimientoPago requerimientoElemento(SeguimientoCuentasBancariasBean obj) {
       return cuentasBancariasDAO.elementoRequerimiento(obj);
    }
    
    
    
}
