/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.PresupuestoBean;
import com.sigefi.entity.CuentasPresupuesto;
import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface PresupuestoBO {
    boolean insertPresupuesto(PresupuestoBean obj);
    List<UnidadesEjecutoras> listUnidadesEjecutoras();
    List<Presupuestos> listAllPresupuestos();
    List<CuentasPresupuesto> listCuentasAgregadas(int codigoPresupuesto);
    int inserCuentas(List<Integer>  listNum,PresupuestoBean obj);
    
}
