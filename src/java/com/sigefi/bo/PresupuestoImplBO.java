/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.PresupuestoBean;
import com.sigefi.dao.PresupuestoDAO;
import com.sigefi.dao.UsuarioDAO;
import com.sigefi.entity.CatalogoCuentas;
import com.sigefi.entity.CuentasPresupuesto;
import com.sigefi.entity.CuentasPresupuestoId;
import com.sigefi.entity.Empresa;
import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.HashSet;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "presupuestoBO")
public class PresupuestoImplBO implements PresupuestoBO {

    @Autowired
    private PresupuestoDAO presupuestoDAO;

    public PresupuestoDAO getPresupuestoDAO() {
        return presupuestoDAO;
    }

    public void setPresupuestoDAO(PresupuestoDAO presupuestoDAO) {
        this.presupuestoDAO = presupuestoDAO;
    }

    @Autowired
    private UsuarioDAO usuarioDAO;

    public UsuarioDAO getUsuarioDAO() {
        return usuarioDAO;
    }

    public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
        this.usuarioDAO = usuarioDAO;
    }

    @Override
    public boolean insertPresupuesto(PresupuestoBean obj) {
        try {

            Presupuestos preConsulta = presupuestoDAO.elementoPresupuesto(obj.getUnidadesEjecutoras(), obj.getFechaCreacionPresupuesto(), obj.getAnyoPresupuesto());

            if (preConsulta == null) {

                Presupuestos pre = new Presupuestos();

                pre.setAnyoPresupuesto(obj.getAnyoPresupuesto());
                pre.setMonedaPresupuesto(obj.getMonedaPresupuesto());
                pre.setFechaCreacionPresupuesto(obj.getFechaCreacionPresupuesto());
                pre.setUsuarioCreador(obj.getUsuarioCreador());
                UnidadesEjecutoras uni = new UnidadesEjecutoras();
                uni.setCodigo(obj.getUnidadesEjecutoras());
                pre.setUnidadesEjecutoras(uni);
                pre.setLiquidado(false);

                presupuestoDAO.insertPresupuesto(pre);

                obj.setPkPresupuesto(pre.getCodigoPresupuesto());

                Empresa emp = new Empresa();

                emp.setAnyoContable(obj.getAnyoPresupuesto());
                emp.setFechaCreacionContable(obj.getFechaCreacionPresupuesto());
                emp.setMonedaContable(obj.getMonedaPresupuesto());
                emp.setUnidadesEjecutoras(new UnidadesEjecutoras(obj.getUnidadesEjecutoras()));
                emp.setUsuarioCreador(obj.getUsuarioCreador());

                presupuestoDAO.insertEmpresa(emp);

                List<CatalogoCuentas> listCata = presupuestoDAO.listCatalogoCuentas(uni, obj.getAnyoPresupuesto());

                if (listCata != null) {
                    for (CatalogoCuentas cat : listCata) {

                        CuentasPresupuesto cuentaPre = new CuentasPresupuesto();

                        CuentasPresupuestoId cuentaPreId = new CuentasPresupuestoId();
                        cuentaPreId.setCodigoCuenta(cat.getIdCuenta());
                        cuentaPreId.setCodigoPresupuesto(obj.getPkPresupuesto());

                        cuentaPre.setId(cuentaPreId);

                        cuentaPre.setDependeDe(String.valueOf(obj.getUnidadesEjecutoras()));

                        cuentaPre.setDescripcionCuentaMayor(cat.getNombreCuenta());

                        cuentaPre.setConDetalle(false);

                        presupuestoDAO.insertCatalogoCuenta(cuentaPre);
                    }
                }

                return true;
            } else {
                obj.setPkPresupuesto(preConsulta.getCodigoPresupuesto());
                return false;
            }

        } catch (Exception e) {
            return false;
        }

    }

    @Override
    public List<UnidadesEjecutoras> listUnidadesEjecutoras() {
        return usuarioDAO.listUnidadesEjecutoras();
    }

    @Override
    public List<CuentasPresupuesto> listCuentasAgregadas(int codigoPresupuesto) {
        return presupuestoDAO.listCuentasPreAgregadas(codigoPresupuesto);
    }

    @Override
    public int inserCuentas(List<Integer> listNum, PresupuestoBean obj) {
        int aux, comprobarRegistro = 0;
        CatalogoCuentas c = presupuestoDAO.elementoCatalogoCuentas(listNum.get((listNum.size() - 1)));
        aux = c.getSubcuentaDe();

        while (aux > 0) {
            CatalogoCuentas cs = presupuestoDAO.elementoCatalogoCuentas(aux);
            listNum.add(cs.getIdCuenta());
            aux = cs.getSubcuentaDe();

        }

        HashSet<Integer> hashSet = new HashSet<Integer>(listNum);
        listNum.clear();
        listNum.addAll(hashSet);

        for (Integer num : listNum) {
            CatalogoCuentas cuenta = presupuestoDAO.elementoCatalogoCuentas(num);

            CuentasPresupuesto cuentaPre = new CuentasPresupuesto();

            CuentasPresupuestoId cuentaPreId = new CuentasPresupuestoId();
            cuentaPreId.setCodigoCuenta(cuenta.getIdCuenta());
            cuentaPreId.setCodigoPresupuesto(obj.getPkPresupuesto());

            cuentaPre.setId(cuentaPreId);

            cuentaPre.setDependeDe(String.valueOf(obj.getUnidadesEjecutoras()));

            cuentaPre.setDescripcionCuentaMayor(cuenta.getNombreCuenta());

            cuentaPre.setConDetalle(false);

            CuentasPresupuesto cuentaConsulta = presupuestoDAO.CuentasPreElemento(obj.getPkPresupuesto(), cuenta.getIdCuenta());

            if (cuentaConsulta == null) {
                presupuestoDAO.insertCatalogoCuenta(cuentaPre);
                comprobarRegistro = 1;
            }
        }

        return comprobarRegistro;
    }

    @Override
    public List<Presupuestos> listAllPresupuestos() {
        return usuarioDAO.listAllPresupuestos();
    }

}
