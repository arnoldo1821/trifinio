/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.DetallePresupuestoBean;
import com.sigefi.entity.CuentasPresupuesto;
import com.sigefi.entity.DetalleCuentas;
import com.sigefi.entity.Presupuestos;
import java.util.List;
import java.util.Set;
import javax.faces.component.UIData;

/**
 *
 * @author AAldemaro
 */
public interface DetallePresupuestoBO {
    Presupuestos elementoPresupuesto(DetallePresupuestoBean obj);
    List<CuentasPresupuesto> listCuentasSelec(DetallePresupuestoBean obj);
    DetalleCuentas consultarDetalleCuenta(int codigoPresupuesto,int codigoCuenta);
    List<DetallePresupuestoBean> listAllDetallePresupuesto(DetallePresupuestoBean obj);
    boolean inserUpdateDetalleCuenta(DetallePresupuestoBean obj);
    boolean insertCuenta(UIData list);
    Set<DetalleCuentas> consultarDetallePresupuesto(DetallePresupuestoBean obj);
}
