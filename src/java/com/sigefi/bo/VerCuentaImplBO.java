/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.dao.VerCuentaDAO;
import com.sigefi.entity.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "verCuentaBO")
public class VerCuentaImplBO implements VerCuentaBO{
    @Autowired
    private VerCuentaDAO verCuentaDAO;

    public VerCuentaDAO getVerCuentaDAO() {
        return verCuentaDAO;
    }

    public void setVerCuentaDAO(VerCuentaDAO verCuentaDAO) {
        this.verCuentaDAO = verCuentaDAO;
    }

    @Override
    public Empresa consultarEmpresa(short idUnidadEje, int anio) {
        return verCuentaDAO.consultarEmpresa(idUnidadEje, anio);
    }
    
        
}
