/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.FuenteFinanciamientoBean;
import com.sigefi.entity.FuenteFinanciamiento;
import java.util.List;

/**
 *
 * @author Elmer
 */
public interface FuenteFinanciamientoBO {
    boolean insertFuenteFinanciamiento(FuenteFinanciamientoBean obj);
    boolean updateFuenteFinanciamiento(FuenteFinanciamientoBean obj);
    boolean deleteFuenteFinanciamiento(FuenteFinanciamientoBean obj);
    List<FuenteFinanciamientoBean> listFuenteFinanciamientoAll();
    List<FuenteFinanciamiento> listAllFuenteFinanciamiento();
}
