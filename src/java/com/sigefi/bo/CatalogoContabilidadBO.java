/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.CatalogoContabilidadBean;
import org.primefaces.model.TreeNode;

/**
 *
 * @author AAldemaro
 */
public interface CatalogoContabilidadBO {
    boolean insertCuenta(CatalogoContabilidadBean obj);
    boolean insertSubCuenta(CatalogoContabilidadBean obj);
    TreeNode listNodoCuentasCatalogo();
}
