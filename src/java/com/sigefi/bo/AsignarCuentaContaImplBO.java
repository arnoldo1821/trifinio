/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.dao.AsignarCuentaContaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "asignarCuentaContaBO")
public class AsignarCuentaContaImplBO implements AsignarCuentaContaBO{
    @Autowired
    private AsignarCuentaContaDAO asignarCuentaContaDAO;

    public AsignarCuentaContaDAO getAsignarCuentaContaDAO() {
        return asignarCuentaContaDAO;
    }

    public void setAsignarCuentaContaDAO(AsignarCuentaContaDAO asignarCuentaContaDAO) {
        this.asignarCuentaContaDAO = asignarCuentaContaDAO;
    }
    
    
}
