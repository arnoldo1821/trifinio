/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.TipoCambioContaBean;
import com.sigefi.dao.TipoCambioContaDAO;
import com.sigefi.entity.TipoCambioConta;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "tipoCambioContaBO")
public class TipoCambioContaImplBO implements TipoCambioContaBO{
    
    @Autowired
    private TipoCambioContaDAO tipoCambioContaDAO;

    public TipoCambioContaDAO getTipoCambioContaDAO() {
        return tipoCambioContaDAO;
    }

    public void setTipoCambioContaDAO(TipoCambioContaDAO tipoCambioContaDAO) {
        this.tipoCambioContaDAO = tipoCambioContaDAO;
    }

    @Override
    public boolean insertTipoCambioConta(TipoCambioContaBean obj) {
        
        TipoCambioConta tipoCambio = tipoCambioContaDAO.elementoTipoCambioConta(obj.getFechaCambio());
        
        if(tipoCambio == null){   
            
            TipoCambioConta tipoC = new TipoCambioConta();
            
            tipoC.setFechaCambio(obj.getFechaCambio());
            tipoC.setQuetzales(obj.getQuetzales());
            tipoC.setLempiras(obj.getLempiras());
            tipoC.setDolar(obj.getDolar());
            
            tipoCambioContaDAO.insertTipoCambioConta(tipoC);
            return true;
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Tipo de cambio ya existe con dicha fecha" , ""));
            return false;
        }
    }

    @Override
    public boolean updateTipoCambioConta(TipoCambioContaBean obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean deleteTipoCambioConta(Date fechaCambio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<TipoCambioContaBean> listTipoCambioContaAll() {
        List<TipoCambioContaBean> listBean = new ArrayList<>();
        List<TipoCambioConta> lisTipo = tipoCambioContaDAO.listTipoCambioContaAll();
        if(lisTipo != null){
            for(TipoCambioConta obj : lisTipo){
                
                TipoCambioContaBean bean = new TipoCambioContaBean();
                
                bean.setFechaCambio(obj.getFechaCambio());
                bean.setQuetzales(obj.getQuetzales());
                bean.setLempiras(obj.getLempiras());
                bean.setDolar(obj.getDolar());
                
                listBean.add(bean);
            }
            
            return listBean;
        }
        return null;
    }
    
    
}
