/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.EjemploBean;
import com.sigefi.beans.UsuarioBean;
import com.sigefi.clases.FechaSistema;
import com.sigefi.dao.UsuarioDAO;
import com.sigefi.clases.Encrypt;
import com.sigefi.clases.SubirArchivoServer;
import com.sigefi.dao.EjemploDAO;
import com.sigefi.entity.Ciudades;
import com.sigefi.entity.Usuarios;
import com.sigefi.entity.Rol;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrador
 */
@Service(value = "ejemploBO")
public class EjemploImplBO implements EjemploBO{
    
    @Autowired
    private EjemploDAO ejemploDAO;

    public EjemploDAO getEjemploDAO() {
        return ejemploDAO;
    }

    public void setEjemploDAO(EjemploDAO ejemploDAO) {
        this.ejemploDAO = ejemploDAO;
    }

    
}
