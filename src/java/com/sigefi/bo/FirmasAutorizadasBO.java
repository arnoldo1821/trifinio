/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.FirmasAutorizadasBean;
import com.sigefi.entity.FirmasAutorizadas;
import com.sigefi.entity.FirmasAutorizadasProyecto;
import com.sigefi.entity.Usuarios;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface FirmasAutorizadasBO {
    List<Usuarios> listAllUusarios();
    List<FirmasAutorizadas> listAllFirmasAutorizadas();
    List<FirmasAutorizadasProyecto> listAllFirmasAutorizadasProyecto();
    boolean insertFirmaAutorizada(FirmasAutorizadasBean obj);
    boolean deleteFirmaAutorizada(FirmasAutorizadasBean obj);
}
