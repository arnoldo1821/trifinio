/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.CuentasEmpresaBean;
import com.sigefi.dao.CuentasEmpresaDAO;
import com.sigefi.entity.CatalogoCuentasConta;
import com.sigefi.entity.CatalogosConta;
import com.sigefi.entity.Empresa;
import java.util.HashSet;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "cuentasEmpresaBO")
public class CuentasEmpresaImplBO implements CuentasEmpresaBO {

    @Autowired
    private CuentasEmpresaDAO cuentasEmpresaDAO;

    public CuentasEmpresaDAO getCuentasEmpresaDAO() {
        return cuentasEmpresaDAO;
    }

    public void setCuentasEmpresaDAO(CuentasEmpresaDAO cuentasEmpresaDAO) {
        this.cuentasEmpresaDAO = cuentasEmpresaDAO;
    }

    @Override
    public Empresa consultarEmpresa(short idUnidadEje, int anio) {
        return cuentasEmpresaDAO.consultarEmpresa(idUnidadEje, anio);
    }

    @Override
    public int inserCuentas(List<String> listNum, CuentasEmpresaBean obj) {
        String aux;
        int comprobarRegistro = 0;
        CatalogoCuentasConta c = cuentasEmpresaDAO.elementoCatalogoCuentas(listNum.get((listNum.size() - 1)));
        aux = c.getCuentapadre();
        while (!aux.equals("0")) {
            CatalogoCuentasConta cs = cuentasEmpresaDAO.elementoCatalogoCuentas(aux);
            listNum.add(cs.getCodCatalocuenta());
            aux = cs.getCuentapadre();
        }

        HashSet<String> hashSet = new HashSet<String>(listNum);
        listNum.clear();
        listNum.addAll(hashSet);

        for (String num : listNum) {
            CatalogoCuentasConta cuenta = cuentasEmpresaDAO.elementoCatalogoCuentas(num);

            if (cuentasEmpresaDAO.elementoCatalogoConta(num, obj.getEmpSelecc()) == null) {
                CatalogosConta cuentaPre = new CatalogosConta();

                cuentaPre.setAnioEjecucion((short) obj.getAnio());
                cuentaPre.setEmpresa(obj.getEmpSelecc());
                cuentaPre.setCodCatalogo(cuenta.getCodCatalocuenta());
//            cuentaPre.setTipoCatalogo(cuenta.getCodCatalocuenta());

                cuentasEmpresaDAO.insertCatalogoCuenta(cuentaPre);
                comprobarRegistro = 1;
            }
        }

        return comprobarRegistro;
    }

    @Override
    public List<CatalogosConta> listCuentasAgregadas(Empresa empresa) {
        return cuentasEmpresaDAO.listCuentasAgregadas(empresa);
    }

}
