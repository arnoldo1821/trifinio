/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.CentroCostoBean;
import java.util.List;

/**
 *
 * @author Elmer
 */
public interface CentroCostoBO {
    boolean insertCentroCosto(CentroCostoBean obj);
    boolean updateCentroCosto(CentroCostoBean obj);
    boolean deleteCentroCosto(CentroCostoBean obj);
    List<CentroCostoBean> listCentroCostoAll();
}
