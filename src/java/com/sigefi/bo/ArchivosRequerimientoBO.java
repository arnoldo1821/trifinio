/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.ArchivosRequerimientoBean;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.DocumentosRequerimientos;
import com.sigefi.entity.RequerimientoPagos;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface ArchivosRequerimientoBO {
    List<RequerimientoPagos> listAllReqUsuario(String usuario);
    List<DetalleRequerimientoPago> listAllDetalleRequerimiento (String usuario);
    boolean insertDocReque(ArchivosRequerimientoBean obj);
    boolean conprobacionElemento(ArchivosRequerimientoBean obj);
    List<DocumentosRequerimientos> listAllDocReqUsuario(int pkIncreReque);
    List<String> listCorreoUsuarios(short idUnidadEjecutora);
    public boolean enviarRequerimiento(RequerimientoPagos obj);
}
