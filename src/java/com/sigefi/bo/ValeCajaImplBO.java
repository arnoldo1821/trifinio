/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.ValeCajaBean;
import com.sigefi.dao.ValeCajaDAO;
import com.sigefi.entity.ManualClasificatorio;
import com.sigefi.entity.ValeCaja;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Aldemaro Gonzalez
 */
@Service(value = "valeCajaBO")
public class ValeCajaImplBO implements ValeCajaBO{
    @Autowired
    private ValeCajaDAO valeCajaDAO;

    public ValeCajaDAO getValeCajaDAO() {
        return valeCajaDAO;
    }

    public void setValeCajaDAO(ValeCajaDAO valeCajaDAO) {
        this.valeCajaDAO = valeCajaDAO;
    }

    @Override
    public List<ManualClasificatorio> listManualClasiAll() {
        return valeCajaDAO.listManualClasiAll();
    }

    @Override
    public boolean insertValeCaja(ValeCajaBean bean) {
        ValeCaja consul = valeCajaDAO.elementoValeCaja(bean.getValeCaja().getFecha(), bean.getValeCaja().getNumero());
        if(consul == null){
            valeCajaDAO.insertValeCaja(bean.getValeCaja());
            return true;
        }else{
            consul.setCantidadNum(bean.getValeCaja().getCantidadNum());
            consul.setCantidadText(bean.getValeCaja().getCantidadText());
            consul.setConcepto(bean.getValeCaja().getConcepto());
            consul.setFecha(bean.getValeCaja().getFecha());
            consul.setFehcaMod(new Date());
            consul.setManualClasificatorio(bean.getValeCaja().getManualClasificatorio());
            consul.setNumero(bean.getValeCaja().getNumero());
            consul.setOrdenDe(bean.getValeCaja().getOrdenDe());
            valeCajaDAO.updateValeCaja(consul);
            return false;
        }
    }
    
    
}
