/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.ValidarRequerimientoAutoBean;
import com.sigefi.clases.FechaSistema;
import com.sigefi.dao.RequisicionDAO;
import com.sigefi.dao.ValidarRequerimientoAutoDAO;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.DocumentosRequerimientos;
import com.sigefi.entity.EstadoRequisicion;
import com.sigefi.entity.EstadoRequisicionId;
import com.sigefi.entity.NoConformidades;
import com.sigefi.entity.RequerimientoPagos;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "validarRequerimientoAutoBO")
public class ValidarRequerimientoAutoImplBO implements ValidarRequerimientoAutoBO {

    @Autowired
    private ValidarRequerimientoAutoDAO validarRequerimientoAutoDAO;

    public ValidarRequerimientoAutoDAO getValidarRequerimientoAutoDAO() {
        return validarRequerimientoAutoDAO;
    }

    public void setValidarRequerimientoAutoDAO(ValidarRequerimientoAutoDAO validarRequerimientoAutoDAO) {
        this.validarRequerimientoAutoDAO = validarRequerimientoAutoDAO;
    }

    @Autowired
    private RequisicionDAO requisicionDAO;

    public RequisicionDAO getRequisicionDAO() {
        return requisicionDAO;
    }

    public void setRequisicionDAO(RequisicionDAO requisicionDAO) {
        this.requisicionDAO = requisicionDAO;
    }

    @Override
    public List<RequerimientoPagos> listAllRequePago() {
        return validarRequerimientoAutoDAO.listAllRequePago();
    }

    @Override
    public List<DetalleRequerimientoPago> listDetalleRequePagoSelec(int incre_req_pk) {
        return validarRequerimientoAutoDAO.listDetalleRequePagoSelec(incre_req_pk);
    }

    @Override
    public List<DocumentosRequerimientos> listDocRequePagoSelec(int incre_req_pk) {
        return validarRequerimientoAutoDAO.listDocRequePagoSelec(incre_req_pk);
    }

    @Override
    public List<NoConformidades> listAllErrores() {
        return validarRequerimientoAutoDAO.listAllErrores();
    }

    @Override
    public boolean validarRequerimiento(RequerimientoPagos req, ValidarRequerimientoAutoBean obj) {
        try {
            EstadoRequisicion consul = requisicionDAO.elementoEstadoRequisicion(req);
            if (consul != null) {
                
                consul.setControl(false);
                requisicionDAO.updateEstadoRequi(consul);
                
                if (obj.isEstadoValidacion()) {
                    
                    EstadoRequisicionId estaId = new EstadoRequisicionId();
                    FechaSistema f = new FechaSistema();
                    estaId.setFechaEstado(f.getFechaCompleta());
                    estaId.setIncreReqPk3(req.getIncrReqPk());

                    EstadoRequisicion esta1 = new EstadoRequisicion();

                    esta1.setId(estaId);
                    esta1.setCodigoEstado((short) 4);
                    esta1.setUsuarioEstableceEstado(obj.getUsuarioLogeado());
                    esta1.setControl(true);
                    
                    validarRequerimientoAutoDAO.insertEstadoRequisicion(esta1);

                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Enviado al encargado de presupuesto", ""));
                    return true;
                } else {
                    
                    EstadoRequisicionId estaId = new EstadoRequisicionId();
                    FechaSistema f = new FechaSistema();
                    estaId.setFechaEstado(f.getFechaCompleta());
                    estaId.setIncreReqPk3(req.getIncrReqPk());

                    EstadoRequisicion esta = new EstadoRequisicion();

                    esta.setId(estaId);
                    esta.setCodigoEstado((short) 3);
                    esta.setUsuarioEstableceEstado(obj.getUsuarioLogeado());
                    esta.setObservacionEstado(obj.getObservaciones());
                    NoConformidades nc = new NoConformidades(obj.getErrorSeleccionado());
                    esta.setNoConformidades(nc);
                    esta.setControl(true);
                    
                    validarRequerimientoAutoDAO.insertEstadoRequisicion(esta);

                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Enviado a Corrección", ""));
                    return true;

                }
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

}
