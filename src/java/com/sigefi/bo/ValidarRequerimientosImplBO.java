/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.ValidarRequerimientosBean;
import com.sigefi.clases.FechaSistema;
import com.sigefi.clases.SubirArchivoServer;
import com.sigefi.dao.RequisicionDAO;
import com.sigefi.dao.ValidarRequerimientosDAO;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.DetalleRequerimientoPagoId;
import com.sigefi.entity.DocumentosRequerimientos;
import com.sigefi.entity.EstadoRequisicion;
import com.sigefi.entity.EstadoRequisicionId;
import com.sigefi.entity.NoConformidades;
import com.sigefi.entity.RequerimientoPagos;
import java.util.List;
import java.util.Set;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "validarRequerimientosBO")
public class ValidarRequerimientosImplBO implements ValidarRequerimientosBO {

    @Autowired
    private ValidarRequerimientosDAO validarRequerimientosDAO;

    public ValidarRequerimientosDAO getValidarRequerimientosDAO() {
        return validarRequerimientosDAO;
    }

    public void setValidarRequerimientosDAO(ValidarRequerimientosDAO validarRequerimientosDAO) {
        this.validarRequerimientosDAO = validarRequerimientosDAO;
    }

    @Autowired
    private RequisicionDAO requisicionDAO;

    public RequisicionDAO getRequisicionDAO() {
        return requisicionDAO;
    }

    public void setRequisicionDAO(RequisicionDAO requisicionDAO) {
        this.requisicionDAO = requisicionDAO;
    }

    @Override
    public List<RequerimientoPagos> listAllRequePago() {
        return validarRequerimientosDAO.listAllRequePago();
    }

    @Override
    public List<DetalleRequerimientoPago> listDetalleRequePagoSelec(int incre_req_pk) {
        return validarRequerimientosDAO.listDetalleRequePagoSelec(incre_req_pk);
    }

    @Override
    public List<DocumentosRequerimientos> listDocRequePagoSelec(int incre_req_pk) {
        return validarRequerimientosDAO.listDocRequePagoSelec(incre_req_pk);
    }

    @Override
    public List<NoConformidades> listAllErrores() {
        return validarRequerimientosDAO.listAllErrores();
    }

    @Override
    public void validarRequerimiento(RequerimientoPagos req, ValidarRequerimientosBean obj) {
        try {
            EstadoRequisicion consul = requisicionDAO.elementoEstadoRequisicion(req);
            if (consul != null) {
                
                consul.setControl(false);
                requisicionDAO.updateEstadoRequi(consul);
                
                if (obj.isEstadoValidacion()) {

                    EstadoRequisicionId estaId = new EstadoRequisicionId();
                    FechaSistema f = new FechaSistema();
                    estaId.setFechaEstado(f.getFechaCompleta());
                    estaId.setIncreReqPk3(req.getIncrReqPk());

                    EstadoRequisicion esta1 = new EstadoRequisicion();

                    esta1.setId(estaId);
                    esta1.setCodigoEstado((short) 5);
                    esta1.setUsuarioEstableceEstado(obj.getUsuarioLogeado());
                    esta1.setControl(true);
                    
                    requisicionDAO.insertEstadoRequi(esta1);

                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Enviado a Contabilidad correctamente", ""));

                } else {
                    EstadoRequisicionId estaId = new EstadoRequisicionId();
                    FechaSistema f = new FechaSistema();
                    estaId.setFechaEstado(f.getFechaCompleta());
                    estaId.setIncreReqPk3(req.getIncrReqPk());

                    EstadoRequisicion esta = new EstadoRequisicion();

                    esta.setId(estaId);
                    esta.setCodigoEstado((short) 3);
                    esta.setUsuarioEstableceEstado(obj.getUsuarioLogeado());
                    esta.setObservacionEstado(obj.getObservaciones());
                    NoConformidades nc = new NoConformidades(obj.getErrorSeleccionado());
                    esta.setNoConformidades(nc);
                    esta.setControl(true);
                    requisicionDAO.insertEstadoRequi(esta);

                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Enviado a Corrección", ""));
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    public boolean eliminarRequerimiento(RequerimientoPagos req) {
        try {
            Set<DetalleRequerimientoPago> detalle = req.getDetalleRequerimientoPagos();
            if (detalle != null) {
                for (DetalleRequerimientoPago de : detalle) {
                    validarRequerimientosDAO.deleteDetalleRequerimiento(de);
                }
            }

            Set<DocumentosRequerimientos> docSet = req.getDocumentosRequerimientoses();
            if (docSet != null) {
                SubirArchivoServer su = new SubirArchivoServer();
                for (DocumentosRequerimientos doc : docSet) {
                    su.borrarArchivo(doc.getUrlDocumento());
                    validarRequerimientosDAO.deleteDocumentoRequerimiento(doc);
                }
            }

            Set<EstadoRequisicion> estdoSet = req.getEstadoRequisicions();
            if (estdoSet != null) {
                for (EstadoRequisicion estado : estdoSet) {
                    validarRequerimientosDAO.deleteEstadoRequerimiento(estado);
                }
            }

            validarRequerimientosDAO.deleteRequerimiento(req);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<DetalleRequerimientoPago> listAllDetalleRequerimiento() {
       return validarRequerimientosDAO.listAllDetalleRequerimiento();
    }

    @Override
    public boolean updateRequerimiento(ValidarRequerimientosBean obj) {
         DetalleRequerimientoPago requerimiento = validarRequerimientosDAO.elementoRequerimiento(obj.getFkPkRequerimiento(), obj.getCorrelativo());
        
        if(requerimiento != null){
            DetalleRequerimientoPagoId reqId = new DetalleRequerimientoPagoId();
            
            reqId.setFkPkRequerimiento2(obj.getFkPkRequerimiento());
            reqId.setCorrelativo(obj.getCorrelativo());
            
            DetalleRequerimientoPago req = new DetalleRequerimientoPago();
            
            req.setId(reqId);
            req.setClaseDocumento(obj.getClaseDocumento());
            req.setCodigoCuenta(obj.getCodigoCuenta());
            req.setCodigoProveedor(obj.getCodigoProveedor());
            req.setConcepto(obj.getConcepto());
            req.setConceptoGasto(obj.getConceptoGasto());
            req.setFechaDocumento(obj.getFechaDocumento());
            req.setMonto(obj.getMonto());
            req.setNombreProveedor(obj.getNombreProveedor());
            req.setNumeroDocumento(obj.getNumeroDocumento());
            req.setRequerimientoPagos(obj.getRequerimientoPagos());
            
            validarRequerimientosDAO.updateDetalleRequerimiento(req);
            
            RequerimientoPagos reqPagos = new RequerimientoPagos();
            
            reqPagos.setIncrReqPk(obj.getIncrReqPk());
            reqPagos.setUnidadesEjecutoras(obj.getUnidadesEjecutoras());
            reqPagos.setCentroCosto(obj.getCentroCosto());
            reqPagos.setCodigoRequerimiento(obj.getCodigoRequerimiento());
            reqPagos.setSiafi(obj.isSiafi());
            reqPagos.setCodigoSiafi(obj.getCodigoSiafi());
            reqPagos.setAfectacionPresupuestaria(obj.isAfectacionPresupuestaria());
            reqPagos.setFechaPresentacion(obj.getFechaPresent());
            reqPagos.setFechaEstimadaPago(obj.getFechaEstimadaPago());
            reqPagos.setMoneda(obj.getMoneda1());
            reqPagos.setAnioPresupuesto(obj.getAnioPresupuesto());
            reqPagos.setUsuario(obj.getUsuario());
            
            validarRequerimientosDAO.updateRequerimientoPago(reqPagos);
            
            return true;
        }
        else{
            return false;
        }
    }
}
