/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.dao.validacionContableDAO;
import com.sigefi.entity.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "validacionContableBO")
public class validacionContableImplBO implements validacionContableBO{
    @Autowired
    private validacionContableDAO validacionContableDAO;

    public validacionContableDAO getValidacionContableDAO() {
        return validacionContableDAO;
    }

    public void setValidacionContableDAO(validacionContableDAO validacionContableDAO) {
        this.validacionContableDAO = validacionContableDAO;
    }

    @Override
    public Empresa consultarEmpresa(short idUnidadEje, int anio) {
        return validacionContableDAO.consultarEmpresa(idUnidadEje, anio);
    }
    
    
}
