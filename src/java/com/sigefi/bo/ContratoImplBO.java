
package com.sigefi.bo;

import com.sigefi.dao.ContratoDAO;
import com.sigefi.entity.Contratos;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author SKAR
 */
@Service(value = "contratoBO")
public class ContratoImplBO implements ContratoBO {
    
    @Autowired
    private ContratoDAO contratoDAO;

    @Override
    public void insert(Contratos obj) {
        contratoDAO.insert(obj);
    }

    @Override
    public void update(Contratos obj) {
        contratoDAO.update(obj);
    }

    @Override
    public void delete(Contratos obj) {
        contratoDAO.delete(obj);
    }

    @Override
    public List<Contratos> selectAll() {
        return contratoDAO.selectAll();
    }    

    public ContratoDAO getContratoDAO() {
        return contratoDAO;
    }

    public void setContratoDAO(ContratoDAO contratoDAO) {
        this.contratoDAO = contratoDAO;
    }
}
