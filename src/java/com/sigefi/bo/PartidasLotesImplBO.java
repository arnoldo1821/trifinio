/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.dao.PartidasLotesDAO;
import com.sigefi.entity.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "partidasLotesBO")
public class PartidasLotesImplBO implements PartidasLotesBO{
    @Autowired
    private PartidasLotesDAO partidasLotesDAO;

    public PartidasLotesDAO getPartidasLotesDAO() {
        return partidasLotesDAO;
    }

    public void setPartidasLotesDAO(PartidasLotesDAO partidasLotesDAO) {
        this.partidasLotesDAO = partidasLotesDAO;
    }

    @Override
    public Empresa consultarEmpresa(short idUnidadEje, int anio) {
        return partidasLotesDAO.consultarEmpresa(idUnidadEje, anio);
    }
    
    
}
