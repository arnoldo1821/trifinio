/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.entity.RequerimientoPagos;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface SeguimientoPagoBO {
    List<RequerimientoPagos> listReqUsuario(String usuario);
}
