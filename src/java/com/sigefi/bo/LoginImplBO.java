/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.BeanLogin;
import com.sigefi.dao.LoginDAO;
import com.sigefi.clases.Encrypt;
import com.sigefi.clases.FechaSistema;
import com.sigefi.entity.Bitacora;
import com.sigefi.entity.Usuarios;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author alberto
 */

@Service (value = "loginBO")
public class LoginImplBO implements LoginBO{
    
    @Autowired
    private LoginDAO loginDAO;

    public LoginDAO getLoginDAO() {
        return loginDAO;
    }
    
    public void setLoginDAO(LoginDAO loginDAO) {
        this.loginDAO = loginDAO;
    }

    @Override
    public void validaLogin(BeanLogin obj) {
        
        Usuarios login = new Usuarios();
        login.setUsuario(obj.getUsuario());
        login.setClave(Encrypt.sha512(obj.getClave()));
        login = loginDAO.validaLogin(login);
        if(login != null){
            obj.setStatus(true);
            obj.setUrl(loginDAO.view(login.getRol().getIdRol()).getUrlRol()+"?faces-redirect=true");
        }else{
            obj.setStatus(false);
        }
    }

    @Override
    public void bitacora(String usuario, String ip, int val) {
      
        try {
            Bitacora bit = new Bitacora();

            bit.setUsuario(usuario);
            FechaSistema fecha = new FechaSistema();
            bit.setFecha(fecha.getFechaString());
            bit.setHost(ip);

            if (val == 1) {
                bit.setAccion(true);
            } else {
                bit.setAccion(false);
            }
            loginDAO.bitacora(bit);
        } catch (Exception e) {
            
        }
    }
    
}
