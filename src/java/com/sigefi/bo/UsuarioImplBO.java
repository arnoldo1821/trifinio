/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.UsuarioBean;
import com.sigefi.clases.FechaSistema;
import com.sigefi.dao.UsuarioDAO;
import com.sigefi.clases.Encrypt;
import com.sigefi.clases.SubirArchivoServer;
import com.sigefi.entity.Ciudades;
import com.sigefi.entity.Usuarios;
import com.sigefi.entity.Rol;
import com.sigefi.entity.UnidadesEjecutoras;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrador
 */
@Service(value = "usuarioBO")
public class UsuarioImplBO implements UsuarioBO{
    
    @Autowired
    private UsuarioDAO usuarioDAO;

    public UsuarioDAO getUsuarioDAO() {
        return usuarioDAO;
    }

    public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
        this.usuarioDAO = usuarioDAO;
    }

    @Override
    public Boolean insertUsuario(UsuarioBean obj) {
        
        Usuarios usu = usuarioDAO.ElementoUsuario(obj.getUsuario());
        
        if (usu == null) {
            try {
                Usuarios usuario = new Usuarios();
               
                usuario.setNombreUsuario(obj.getNombre());
                usuario.setApellidosUsuario(obj.getApellido());
                usuario.setUsuario(obj.getUsuario());
                if (!obj.getEmail().isEmpty()) {
                    usuario.setEMail(obj.getEmail());
                }
                usuario.setClave(Encrypt.sha512(obj.getClave()));
                
                Rol rol = new Rol();
                rol.setIdRol(obj.getRol());
                usuario.setRol(rol);
                
                FechaSistema f = new FechaSistema();
                usuario.setFechaCreacion(f.getFecha());
                
                usuario.setIdCiudad(obj.getCiudad());
                
                if(!obj.getUrlFoto().contains("recursos\\imagenes\\otras\\usuario.jpg")){
                    usuario.setUrlFoto(obj.getUrlFoto());
                }
                
                short stado = 1;
                usuario.setEstatus(stado);
                
                usuario.setTelefono(obj.getTelefono());
                
                UnidadesEjecutoras unidadE = new UnidadesEjecutoras();
                unidadE.setCodigo(obj.getIdUnidadEjecutora());
                
                usuario.setUnidadesEjecutoras(unidadE);
                
                usuarioDAO.insertUsuario(usuario);
                
                return true;
            } catch (Exception e) {
                System.out.println("***** "+e);
                return false;
            }
        }else{
            return false;
        }
    }

    @Override
    public Boolean updateUsuario(UsuarioBean obj) {
        
        Usuarios usuario = usuarioDAO.ElementoUsuario(obj.getUsuario());
        
        if (usuario != null) {
            try {
                usuario.setNombreUsuario(obj.getNombre());
                usuario.setApellidosUsuario(obj.getApellido());
                if (!obj.getEmail().isEmpty()) {
                    usuario.setEMail(obj.getEmail());
                }
                usuario.setClave(Encrypt.sha512(obj.getClave()));
                
                Rol rol = new Rol();
                rol.setIdRol(obj.getRol());
                usuario.setRol(rol);
                
                usuario.setIdCiudad(obj.getCiudad());
                
                ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
                String realPath = (String) servletContext.getRealPath("/"); // Sustituye "/" por el directorio ej: "/upload"
                String urld = realPath + "\\recursos\\imagenes\\otras\\usuario.jpg";
                
                if(!obj.getUrlFoto().equalsIgnoreCase(urld)){
                    usuario.setUrlFoto(obj.getUrlFoto());
                }
                
                usuario.setEstatus(obj.getEstado());
                
                usuario.setTelefono(obj.getTelefono());
                
                UnidadesEjecutoras unidadesE = new UnidadesEjecutoras();
                unidadesE.setCodigo(obj.getIdUnidadEjecutora());
                usuario.setUnidadesEjecutoras(unidadesE);
                
                usuarioDAO.updateUsuario(usuario);
                
                return true;
            } catch (Exception e) {
                return false;
            }
        }else{
            return false;
        }
    }

    @Override
    public Boolean deleteUsuario(String usuario) {
        Usuarios usu = usuarioDAO.ElementoUsuario(usuario);        
        if (usu != null) {
            try {
                if(usu.getUrlFoto() != null){
                    ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
                    String realPath = (String) servletContext.getRealPath("/"); // Sustituye "/" por el directorio ej: "/upload"
                    String urld = realPath + "\\recursos\\imagenes\\otras\\usuario.jpg";
                    if(!usu.getUrlFoto().equalsIgnoreCase(urld)){
                        SubirArchivoServer archivo = new SubirArchivoServer();
                        archivo.borrarArchivo(usu.getUrlFoto());
                    }
                }
                usuarioDAO.deleteUsuario(usu);
                return true;
            } catch (Exception e) {
                return false;
            }
        }else{
            return false;
        }
    }

    @Override
    public List<Rol> listRolAll() {
        return usuarioDAO.listRol();
    }


    @Override
    public List listUsuarioAll() {
        
        List<UsuarioBean> listUsuario = new ArrayList();
        
        try {

            for (Usuarios obj : usuarioDAO.listUsuario()) {
                
                UsuarioBean bean = new UsuarioBean();

                bean.setIdUsuario(obj.getIdUsuario());
                bean.setNombre(obj.getNombreUsuario());
                bean.setApellido(obj.getApellidosUsuario());
                bean.setEmail(obj.getEMail());
                bean.setUsuario(obj.getUsuario());
                bean.setRol(obj.getRol().getIdRol());
                bean.setNombreRol(usuarioDAO.ElementoRol(obj.getRol().getIdRol()));
                bean.setCiudad(obj.getIdCiudad());
                bean.setTelefono(obj.getTelefono());
                bean.setEstado(obj.getEstatus());
                
//                SubirArchivoServer foto = new SubirArchivoServer();
//                
//                if(obj.getUrlFoto() != null){
//                    bean.setFotoUsuario(foto.descargarArchivo(obj.getUrlFoto()));
//                }
                listUsuario.add(bean);
            }             
        }catch (Exception e){
            
        }
        return listUsuario;
    }

    @Override
    public Boolean consultarUsuario(String usuario, UsuarioBean obj) {
        try {

            Usuarios usu = usuarioDAO.ElementoUsuario(usuario);

            if (usu != null) {

                obj.setNombre(usu.getNombreUsuario());
                obj.setApellido(usu.getApellidosUsuario());
                obj.setEmail(usu.getEMail());
                obj.setUsuario(usu.getUsuario());
                obj.setClave(usu.getClave());
                obj.setRol(usu.getRol().getIdRol());
                obj.setCiudad(usu.getIdCiudad());
                
                ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
                String realPath = (String) servletContext.getRealPath("/"); // Sustituye "/" por el directorio ej: "/upload"
                String urld = realPath + "\\recursos\\imagenes\\otras\\usuario.jpg";
                String u = usu.getUrlFoto();
                if(u != null){
                    if(!u.isEmpty()){
                        File file = new File(u);
                            if (file.exists()) {
                                obj.setUrlFoto(u);
                            }else{
                                obj.setUrlFoto(urld);
                            }
                    }else{
                        obj.setUrlFoto(urld);
                    }
                }else{
                    obj.setUrlFoto(urld);
                }
                
                obj.setEstado(usu.getEstatus());
                obj.setTelefono(usu.getTelefono());
                obj.setIdUnidadEjecutora(usu.getUnidadesEjecutoras().getCodigo());
                return true;
            
            } else {
                return false;
            }
        } catch (Exception e) {
        }
        return false;
    }
    
    @Override
    public List<Ciudades> listCiudades() {
        return usuarioDAO.listCiudades();
    }

    @Override
    public List<UnidadesEjecutoras> listUnidadesEjecutoras() {
        return usuarioDAO.listUnidadesEjecutoras();
    }
    
}
