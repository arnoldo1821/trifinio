/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.FirmasAutorizadasBean;
import com.sigefi.dao.FirmasAutorizadasDAO;
import com.sigefi.entity.FirmasAutorizadas;
import com.sigefi.entity.FirmasAutorizadasId;
import com.sigefi.entity.FirmasAutorizadasProyecto;
import com.sigefi.entity.FirmasAutorizadasProyectoId;
import com.sigefi.entity.UnidadesEjecutoras;
import com.sigefi.entity.Usuarios;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "firmasAutorizadasBO")
public class FirmasAutorizadasImplBO implements FirmasAutorizadasBO {

    @Autowired
    private FirmasAutorizadasDAO firmasAutorizadasDAO;

    public FirmasAutorizadasDAO getFirmasAutorizadasDAO() {
        return firmasAutorizadasDAO;
    }

    public void setFirmasAutorizadasDAO(FirmasAutorizadasDAO firmasAutorizadasDAO) {
        this.firmasAutorizadasDAO = firmasAutorizadasDAO;
    }

    @Override
    public List<Usuarios> listAllUusarios() {
        return firmasAutorizadasDAO.listAllUsuarios();
    }

    @Override
    public List<FirmasAutorizadas> listAllFirmasAutorizadas() {
        return firmasAutorizadasDAO.listAllFirmasAutorizadas();
    }

    @Override
    public List<FirmasAutorizadasProyecto> listAllFirmasAutorizadasProyecto() {
        return firmasAutorizadasDAO.listAllFirmasAutorizadasProyecto();
    }

    @Override
    public boolean insertFirmaAutorizada(FirmasAutorizadasBean obj) {

        UnidadesEjecutoras unidConsulta = firmasAutorizadasDAO.elementoUnidadEjecutora(obj.getUnidadesEjecutoras());

        if (unidConsulta.getEsProyecto() == false) {
            FirmasAutorizadas firmaConsultaA = firmasAutorizadasDAO.elementoFirmaAutorizadas(obj.getNombreUsuario(), obj.getCargo());

            if (firmaConsultaA == null) {
                FirmasAutorizadasId firmaId = new FirmasAutorizadasId();

                firmaId.setCargo(obj.getCargo());
                firmaId.setUsuario(obj.getUsuarioLogeado());

                FirmasAutorizadas firma = new FirmasAutorizadas();

                firma.setId(firmaId);
                firma.setNombre(obj.getNombreUsuario());
                firma.setUnidadesEjecutoras(unidConsulta);

                firma.setUrlArchivo(obj.getUrlFirma());

                firmasAutorizadasDAO.insertFirmaAutorizada(firma);

                return true;
            }
        } else {
            FirmasAutorizadasProyecto firmaConsultaAP = firmasAutorizadasDAO.elementoFirmaAutorizadasProyecto(obj.getNombreUsuario(), obj.getCargo());

            if (firmaConsultaAP == null) {
                FirmasAutorizadasProyectoId firmaProyecId = new FirmasAutorizadasProyectoId();

                firmaProyecId.setCargo(obj.getCargo());
                firmaProyecId.setUsuario(obj.getUsuarioLogeado());

                FirmasAutorizadasProyecto firmaProyec = new FirmasAutorizadasProyecto();

                firmaProyec.setId(firmaProyecId);
                firmaProyec.setNombre(obj.getNombreUsuario());
                firmaProyec.setUnidadesEjecutoras(unidConsulta);

                firmaProyec.setUrlArchivo(obj.getUrlFirma());

                firmasAutorizadasDAO.insertFirmaAutorizadaProyecto(firmaProyec);

                return true;
            }
        }

        return false;
    }

    @Override
    public boolean deleteFirmaAutorizada(FirmasAutorizadasBean obj) {
        UnidadesEjecutoras unidConsulta = firmasAutorizadasDAO.elementoUnidadEjecutora(obj.getUnidadesEjecutoras());

          FirmasAutorizadasProyectoId firmaProyecId = new FirmasAutorizadasProyectoId();

            firmaProyecId.setCargo(obj.getCargo());
            firmaProyecId.setUsuario(obj.getUsuarioLogeado());

            FirmasAutorizadasProyecto firmaProyec = new FirmasAutorizadasProyecto();

            firmaProyec.setId(firmaProyecId);
            firmaProyec.setNombre(obj.getNombreUsuario());
            firmaProyec.setUnidadesEjecutoras(unidConsulta);

            firmaProyec.setUrlArchivo(obj.getUrlFirma());

            firmasAutorizadasDAO.deleteFirmaAutorizadaProyecto(firmaProyec);
            return true;
        
    }

}
