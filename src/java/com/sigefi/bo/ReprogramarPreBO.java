/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.ReprogramarPreBean;
import com.sigefi.entity.DetalleCuentas;
import java.util.List;
import javax.faces.component.UIData;

/**
 *
 * @author AAldemaro
 */
public interface ReprogramarPreBO {
    List<DetalleCuentas> listDetalleCuentasPre(short unidadE,int anio);
    boolean modificarCuenta(DetalleCuentas obj);
    boolean updateCuentas(UIData list);
    List<ReprogramarPreBean> listNodoDetalleCuentas(short unidad, int anio);
}
