/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.ValidarRequerimientosTesoreriaBean;
import com.sigefi.clases.FechaSistema;
import com.sigefi.dao.RequisicionDAO;
import com.sigefi.dao.ValidarRequerimientosTesoreriaDAO;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.DocumentosRequerimientos;
import com.sigefi.entity.EstadoRequisicion;
import com.sigefi.entity.EstadoRequisicionId;
import com.sigefi.entity.NoConformidades;
import com.sigefi.entity.RequerimientoPagos;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "validarRequerimientosTesoreriaBO")
public class ValidarRequerimientosTesoreriaImplBO implements ValidarRequerimientosTesoreriaBO {

    @Autowired
    private ValidarRequerimientosTesoreriaDAO validarRequerimientosTesoreriaDAO;

    public ValidarRequerimientosTesoreriaDAO getValidarRequerimientosTesoreriaDAO() {
        return validarRequerimientosTesoreriaDAO;
    }

    public void setValidarRequerimientosTesoreriaDAO(ValidarRequerimientosTesoreriaDAO validarRequerimientosTesoreriaDAO) {
        this.validarRequerimientosTesoreriaDAO = validarRequerimientosTesoreriaDAO;
    }

    @Autowired
    private RequisicionDAO requisicionDAO;

    public RequisicionDAO getRequisicionDAO() {
        return requisicionDAO;
    }

    public void setRequisicionDAO(RequisicionDAO requisicionDAO) {
        this.requisicionDAO = requisicionDAO;
    }

    @Override
    public List<RequerimientoPagos> listAllRequePago() {
        return validarRequerimientosTesoreriaDAO.listAllRequePago();
    }

    @Override
    public List<DetalleRequerimientoPago> listDetalleRequePagoSelec(int incre_req_pk) {
        return validarRequerimientosTesoreriaDAO.listDetalleRequePagoSelec(incre_req_pk);
    }

    @Override
    public List<DocumentosRequerimientos> listDocRequePagoSelec(int incre_req_pk) {
        return validarRequerimientosTesoreriaDAO.listDocRequePagoSelec(incre_req_pk);
    }

    @Override
    public List<NoConformidades> listAllErrores() {
        return validarRequerimientosTesoreriaDAO.listAllErrores();
    }

    @Override
    public void validarRequerimiento(RequerimientoPagos req, ValidarRequerimientosTesoreriaBean obj) {
        try {
            EstadoRequisicion consul = requisicionDAO.elementoEstadoRequisicion(req);
            if (consul != null) {
                
                consul.setControl(false);
                requisicionDAO.updateEstadoRequi(consul);
                
                if (obj.isEstadoValidacion()) {

                    EstadoRequisicionId estaId = new EstadoRequisicionId();
                    FechaSistema f = new FechaSistema();
                    estaId.setFechaEstado(f.getFechaCompleta());
                    estaId.setIncreReqPk3(req.getIncrReqPk());

                    EstadoRequisicion esta1 = new EstadoRequisicion();

                    esta1.setId(estaId);
                    esta1.setCodigoEstado((short) 7);
                    esta1.setUsuarioEstableceEstado(obj.getUsuarioLogeado());
                    esta1.setControl(true);
                    
                    requisicionDAO.insertEstadoRequi(esta1);

                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Proceso terminado", ""));

                } else {
                    EstadoRequisicionId estaId = new EstadoRequisicionId();
                    FechaSistema f = new FechaSistema();
                    estaId.setFechaEstado(f.getFechaCompleta());
                    estaId.setIncreReqPk3(req.getIncrReqPk());

                    EstadoRequisicion esta = new EstadoRequisicion();

                    esta.setId(estaId);
                    esta.setCodigoEstado((short) 5);
                    esta.setUsuarioEstableceEstado(obj.getUsuarioLogeado());
                    esta.setObservacionEstado(obj.getObservaciones());
                    NoConformidades nc = new NoConformidades(obj.getErrorSeleccionado());
                    esta.setNoConformidades(nc);
                    esta.setControl(true);
                    requisicionDAO.insertEstadoRequi(esta);

                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Enviado a Corrección", ""));
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    public List<DetalleRequerimientoPago> listAllDetalleRequerimiento() {
        return validarRequerimientosTesoreriaDAO.listAllDetalleRequerimiento();
    }

}
