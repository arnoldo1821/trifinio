/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.dao.SeguimientoPagoDAO;
import com.sigefi.entity.RequerimientoPagos;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "seguimientoPagoBO")
public class SeguimientoPagoImplBO implements SeguimientoPagoBO{
    @Autowired
    private SeguimientoPagoDAO seguimientoPagoDAO;

    public SeguimientoPagoDAO getSeguimientoPagoDAO() {
        return seguimientoPagoDAO;
    }

    public void setSeguimientoPagoDAO(SeguimientoPagoDAO seguimientoPagoDAO) {
        this.seguimientoPagoDAO = seguimientoPagoDAO;
    }

    @Override
    public List<RequerimientoPagos> listReqUsuario(String usuario) {
        return seguimientoPagoDAO.listReqUsuario(usuario);
    }
    
    
}
