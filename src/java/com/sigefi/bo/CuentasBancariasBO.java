/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.CuentasBancariasBean;
import com.sigefi.beans.SeguimientoCuentasBancariasBean;
import com.sigefi.entity.CuentasBancarias;
import com.sigefi.entity.DetalleRequerimientoPago;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface CuentasBancariasBO {
    List<CuentasBancarias> listAllCuentas();
    List<DetalleRequerimientoPago> listRequerimientoPago();
    CuentasBancarias cuentaElemento(SeguimientoCuentasBancariasBean obj);
    DetalleRequerimientoPago requerimientoElemento(SeguimientoCuentasBancariasBean obj);
    boolean insertCuentaBancaria(CuentasBancariasBean bean);
    boolean updateCuentaBancaria(CuentasBancarias obj);
    boolean deleteCuentaBancaria(CuentasBancarias obj);
}
