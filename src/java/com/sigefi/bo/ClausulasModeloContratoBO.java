/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.ModeloContratoBean;
import com.sigefi.entity.ClausulasModeloContrato;
import com.sigefi.entity.ModeloContrato;
import java.util.List;

/**
 *
 * @author SKAR
 */
public interface ClausulasModeloContratoBO {
    
    void insert(ClausulasModeloContrato obj);
    void update(ClausulasModeloContrato obj);
    void delete(ClausulasModeloContrato obj);
    List<ClausulasModeloContrato> selectAll();
    List<ClausulasModeloContrato> selectAll(ModeloContrato obj);
    ClausulasModeloContrato select(ModeloContratoBean obj);
    
}
