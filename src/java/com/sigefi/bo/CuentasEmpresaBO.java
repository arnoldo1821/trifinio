/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.CuentasEmpresaBean;
import com.sigefi.entity.CatalogoCuentasConta;
import com.sigefi.entity.CatalogosConta;
import com.sigefi.entity.Empresa;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface CuentasEmpresaBO {
    Empresa consultarEmpresa(short idUnidadEje,int anio);
    int inserCuentas(List<String>  listNum,CuentasEmpresaBean obj);
    List<CatalogosConta> listCuentasAgregadas(Empresa empresa);
}
