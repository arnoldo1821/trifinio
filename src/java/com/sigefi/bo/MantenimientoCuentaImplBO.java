/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.dao.MantenimientoCuentaDAO;
import com.sigefi.entity.CatalogoCuentas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "mantenimientoCuentaBO")
public class MantenimientoCuentaImplBO implements MantenimientoCuentaBO{
    @Autowired
    private MantenimientoCuentaDAO mantenimientoCuentaDAO;

    public MantenimientoCuentaDAO getMantenimientoCuentaDAO() {
        return mantenimientoCuentaDAO;
    }

    public void setMantenimientoCuentaDAO(MantenimientoCuentaDAO mantenimientoCuentaDAO) {
        this.mantenimientoCuentaDAO = mantenimientoCuentaDAO;
    }

    @Override
    public boolean updateCuenta(CatalogoCuentas obj) {
        mantenimientoCuentaDAO.updateCuenta(obj);
        return true;
    }
    
    @Override
    public boolean deleteCuenta(CatalogoCuentas obj) {
        mantenimientoCuentaDAO.deleteCuenta(obj);
        return true;
    }
    
    
}
