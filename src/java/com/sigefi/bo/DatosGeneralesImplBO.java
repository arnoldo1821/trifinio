/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.DatosGeneralesBean;
import com.sigefi.clases.FechaSistema;
import com.sigefi.dao.DatosGeneralesDAO;
import com.sigefi.entity.Empresa;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "datosGeneralesBO")
public class DatosGeneralesImplBO implements DatosGeneralesBO {

    @Autowired
    private DatosGeneralesDAO datosGeneralesDAO;

    public DatosGeneralesDAO getDatosGeneralesDAO() {
        return datosGeneralesDAO;
    }

    public void setDatosGeneralesDAO(DatosGeneralesDAO datosGeneralesDAO) {
        this.datosGeneralesDAO = datosGeneralesDAO;
    }

    @Override
    public boolean insertEmpres(DatosGeneralesBean obj) {
        try {
            Empresa emp = new Empresa();
            emp.setAnyoContable(obj.getAnyoEjecucion());
            emp.setMonedaContable(obj.getMoneda());
            emp.setUnidadesEjecutoras(obj.getUnidadesEjecutoras());
            emp.setUsuarioCreador(obj.getUsuarioCreador());
            emp.setEsProyecto(obj.getUnidadesEjecutoras().getEsProyecto());
            emp.setFechaCreacionContable(new Date());
            datosGeneralesDAO.inserEmpresa(emp);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
