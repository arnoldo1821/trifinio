/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.FuenteFinanciamientoBean;
import com.sigefi.dao.FuenteFinanciamientoDAO;
import com.sigefi.entity.FuenteFinanciamiento;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Elmer
 */
@Service(value = "fuenteFinanciamientoBO")
public class FuenteFinanciamientoImplBO implements FuenteFinanciamientoBO {
    
    @Autowired
    private FuenteFinanciamientoDAO fuenteFinanciamientoDAO;

    @Override
    public boolean insertFuenteFinanciamiento(FuenteFinanciamientoBean obj) {
         FuenteFinanciamiento fuenteFinanciamiento = fuenteFinanciamientoDAO.elementoFuenteFinanciamiento(obj.getCodigo());
        
        if(fuenteFinanciamiento == null){   
            
            FuenteFinanciamiento fuenteF = new FuenteFinanciamiento();
            
            fuenteF.setNombreFuente(obj.getNombreFuente());
      
            fuenteFinanciamientoDAO.insertFuenteFinanciamiento(fuenteF);
            return true;
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"La Fuente de Financiamiento Ya Existe" , ""));
            return false;
        }
    }

    @Override
    public boolean updateFuenteFinanciamiento(FuenteFinanciamientoBean obj) {
        FuenteFinanciamiento fuenteFinanciamiento = fuenteFinanciamientoDAO.elementoFuenteFinanciamiento(obj.getCodigo());
        
        if(fuenteFinanciamiento != null){   
            
            FuenteFinanciamiento fuenteF = new FuenteFinanciamiento();
            
            fuenteF.setCodigo(obj.getCodigo());
            fuenteF.setNombreFuente(obj.getNombreFuente());
           
            fuenteFinanciamientoDAO.updateFuenteFinanciamiento(fuenteF);
            return true;
        }//Fin del if
        else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"No se pudo actualizar el registro" , ""));
            return false;
        }
    }

    @Override
    public boolean deleteFuenteFinanciamiento(FuenteFinanciamientoBean obj) {
        FuenteFinanciamiento fuenteFinanciamiento = fuenteFinanciamientoDAO.elementoFuenteFinanciamiento(obj.getCodigo());
        
        if(fuenteFinanciamiento != null){   
            
            FuenteFinanciamiento fuenteF = new FuenteFinanciamiento();
            
            fuenteF.setCodigo(obj.getCodigo());
            fuenteF.setNombreFuente(obj.getNombreFuente());
           
            fuenteFinanciamientoDAO.deleteFuenteFinanciamiento(fuenteF);
            return true;
        }//Fin del if
        else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"No se pudo eliminar el registro" , ""));
            return false;
        }
    }

    
    @Override
    public List<FuenteFinanciamientoBean> listFuenteFinanciamientoAll() {
        List<FuenteFinanciamientoBean> listBean = new ArrayList<>();
        List<FuenteFinanciamiento> listFuente = fuenteFinanciamientoDAO.listFuenteFinanciamientoAll();
        if(listFuente != null){
            for(FuenteFinanciamiento obj : listFuente){
                
                FuenteFinanciamientoBean bean = new FuenteFinanciamientoBean();
                
                bean.setCodigo(obj.getCodigo());
                bean.setNombreFuente(obj.getNombreFuente());
                
                listBean.add(bean);
            }
            
            return listBean;
        }
        return null;
    }

    @Override
    public List<FuenteFinanciamiento> listAllFuenteFinanciamiento() {
        return fuenteFinanciamientoDAO.listAllFuenteFinanciamiento();
    }
    
}
