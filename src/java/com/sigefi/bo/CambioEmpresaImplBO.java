/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.dao.CambioEmpresaDAO;
import com.sigefi.entity.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "cambioEmpresaBO")
public class CambioEmpresaImplBO implements CambioEmpresaBO{
    @Autowired
    private CambioEmpresaDAO cambioEmpresaDAO;

    public CambioEmpresaDAO getCambioEmpresaDAO() {
        return cambioEmpresaDAO;
    }

    public void setCambioEmpresaDAO(CambioEmpresaDAO cambioEmpresaDAO) {
        this.cambioEmpresaDAO = cambioEmpresaDAO;
    }

    @Override
    public Empresa consultarEmpresa(short idUnidadEje, int anio) {
        return cambioEmpresaDAO.consultarEmpresa(idUnidadEje, anio);
    }
    
    
}
