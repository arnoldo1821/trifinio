/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.dao.LiquidarDAO;
import com.sigefi.entity.CuentasPresupuesto;
import com.sigefi.entity.CuentasPresupuestoId;
import com.sigefi.entity.DetalleCuentas;
import com.sigefi.entity.DetalleCuentasId;
import com.sigefi.entity.Presupuestos;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "liquidarBO")
public class LiquidarImplBO implements LiquidarBO {

    @Autowired
    private LiquidarDAO liquidarDAO;

    public LiquidarDAO getLiquidarDAO() {
        return liquidarDAO;
    }

    public void setLiquidarDAO(LiquidarDAO liquidarDAO) {
        this.liquidarDAO = liquidarDAO;
    }

    @Override
    public Presupuestos elementoPre(short unidad, int anio) {
        return liquidarDAO.elementoPre(unidad, anio);
    }

    @Override
    public boolean insertPresupuesto(Presupuestos obj) {
        try {
            Presupuestos consulPreAntiguo = liquidarDAO.elementoPre(obj.getUnidadesEjecutoras().getCodigo(), (obj.getAnyoPresupuesto()));

            if (consulPreAntiguo != null) {
                Presupuestos antiguoPre = new Presupuestos();

                antiguoPre.setCodigoPresupuesto(obj.getCodigoPresupuesto());
                antiguoPre.setMonedaPresupuesto(obj.getMonedaPresupuesto());
                antiguoPre.setUsuarioCreador(obj.getUsuarioCreador());
                antiguoPre.setAnyoPresupuesto((obj.getAnyoPresupuesto()));
                antiguoPre.setFechaCreacionPresupuesto(obj.getFechaCreacionPresupuesto());
                antiguoPre.setUnidadesEjecutoras(obj.getUnidadesEjecutoras());
                antiguoPre.setLiquidado(true);

                liquidarDAO.updatePresupuesto(antiguoPre);
            }
            
            Presupuestos consul = liquidarDAO.elementoPre(obj.getUnidadesEjecutoras().getCodigo(), (obj.getAnyoPresupuesto() + 1));
            if (consul == null) {
               
                Presupuestos nuevoPre = new Presupuestos();

                nuevoPre.setMonedaPresupuesto(obj.getMonedaPresupuesto());
                nuevoPre.setUsuarioCreador(obj.getUsuarioCreador());
                nuevoPre.setAnyoPresupuesto((obj.getAnyoPresupuesto() + 1));
                nuevoPre.setFechaCreacionPresupuesto(new Date());
                nuevoPre.setUnidadesEjecutoras(obj.getUnidadesEjecutoras());
                if (obj.getLiquidado() != null) {
                    nuevoPre.setLiquidado(obj.getLiquidado());
                }
                if (obj.getEsProyecto() != null) {
                    nuevoPre.setEsProyecto(obj.getEsProyecto());
                }

                liquidarDAO.insertPresupuesto(nuevoPre);

                /*CuentasPresupuesto*/
                Set<CuentasPresupuesto> listCuen = obj.getCuentasPresupuestos();
                if (listCuen != null) {
                    if (listCuen.size() > 0) {
                        for (CuentasPresupuesto cuenPre : listCuen) {
                            CuentasPresupuestoId cuenId = new CuentasPresupuestoId();
                            cuenId.setCodigoCuenta(cuenPre.getId().getCodigoCuenta());
                            cuenId.setCodigoPresupuesto(nuevoPre.getCodigoPresupuesto());
                            CuentasPresupuesto cuen = new CuentasPresupuesto();
                            cuen.setId(cuenId);
                            cuen.setDescripcionCuentaMayor(cuenPre.getDescripcionCuentaMayor());
                            cuen.setDependeDe(cuenPre.getDependeDe());
                            cuen.setConDetalle(cuenPre.getConDetalle());
                            liquidarDAO.insertCuentasPresupuesto(cuen);
                        }
                    }
                }

                /*DetalleCuentas*/
                Set<DetalleCuentas> listDetalle = obj.getDetalleCuentases();
                if (listDetalle != null) {
                    if (listDetalle.size() > 0) {
                        for (DetalleCuentas detaCons : listDetalle) {
                            DetalleCuentasId detaId = new DetalleCuentasId();
                            detaId.setCodgioCuenta(detaCons.getId().getCodgioCuenta());
                            detaId.setCodigoPresupuesto(nuevoPre.getCodigoPresupuesto());

                            DetalleCuentas deta = new DetalleCuentas();
                            deta.setId(detaId);
                            deta.setEnero(BigDecimal.ZERO);
                            deta.setFebrero(BigDecimal.ZERO);
                            deta.setMarzo(BigDecimal.ZERO);
                            deta.setAbril(BigDecimal.ZERO);
                            deta.setMayo(BigDecimal.ZERO);
                            deta.setJunio(BigDecimal.ZERO);
                            deta.setJulio(BigDecimal.ZERO);
                            deta.setAgosto(BigDecimal.ZERO);
                            deta.setSeptiembre(BigDecimal.ZERO);
                            deta.setOctubre(BigDecimal.ZERO);
                            deta.setNoviembre(BigDecimal.ZERO);
                            deta.setDiciembre(BigDecimal.ZERO);
                            deta.setCargo(BigDecimal.ZERO);
                            deta.setAbono(BigDecimal.ZERO);
                            deta.setPresupuestoAprobado(detaCons.getPresupuestoAprobado());
                            deta.setPresupuestoComprometido(detaCons.getPresupuestoAprobado());
                            deta.setPresupuestoReal(detaCons.getPresupuestoReal());
                            deta.setEjecutadoContable(detaCons.getEjecutadoContable());
                            deta.setEjecutadoPagado(detaCons.getEjecutadoPagado());
                            deta.setPresupuestoReprogramar(detaCons.getPresupuestoReprogramar());

                            liquidarDAO.insertDetalleCuentas(deta);

                        }
                    }
                }
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean liquidarPresupuesto(Presupuestos obj) {
        try {
            Presupuestos consulPreAntiguo = liquidarDAO.elementoPre(obj.getUnidadesEjecutoras().getCodigo(), (obj.getAnyoPresupuesto()));

            if (consulPreAntiguo != null) {
                Presupuestos antiguoPre = new Presupuestos();

                antiguoPre.setCodigoPresupuesto(obj.getCodigoPresupuesto());
                antiguoPre.setMonedaPresupuesto(obj.getMonedaPresupuesto());
                antiguoPre.setUsuarioCreador(obj.getUsuarioCreador());
                antiguoPre.setAnyoPresupuesto((obj.getAnyoPresupuesto()));
                antiguoPre.setFechaCreacionPresupuesto(obj.getFechaCreacionPresupuesto());
                antiguoPre.setUnidadesEjecutoras(obj.getUnidadesEjecutoras());
                antiguoPre.setLiquidado(true);

                liquidarDAO.updatePresupuesto(antiguoPre);
                 return true;
            }
             else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean insertPresupuestoCero(Presupuestos obj) {
       try {
            Presupuestos consulPreAntiguo = liquidarDAO.elementoPre(obj.getUnidadesEjecutoras().getCodigo(), (obj.getAnyoPresupuesto()));

            if (consulPreAntiguo != null) {
                Presupuestos antiguoPre = new Presupuestos();

                antiguoPre.setCodigoPresupuesto(obj.getCodigoPresupuesto());
                antiguoPre.setMonedaPresupuesto(obj.getMonedaPresupuesto());
                antiguoPre.setUsuarioCreador(obj.getUsuarioCreador());
                antiguoPre.setAnyoPresupuesto((obj.getAnyoPresupuesto()));
                antiguoPre.setFechaCreacionPresupuesto(obj.getFechaCreacionPresupuesto());
                antiguoPre.setUnidadesEjecutoras(obj.getUnidadesEjecutoras());
                antiguoPre.setLiquidado(true);

                liquidarDAO.updatePresupuesto(antiguoPre);
            }
            
            Presupuestos consul = liquidarDAO.elementoPre(obj.getUnidadesEjecutoras().getCodigo(), (obj.getAnyoPresupuesto() + 1));
            if (consul == null) {
               
                Presupuestos nuevoPre = new Presupuestos();

                nuevoPre.setMonedaPresupuesto(obj.getMonedaPresupuesto());
                nuevoPre.setUsuarioCreador(obj.getUsuarioCreador());
                nuevoPre.setAnyoPresupuesto((obj.getAnyoPresupuesto() + 1));
                nuevoPre.setFechaCreacionPresupuesto(new Date());
                nuevoPre.setUnidadesEjecutoras(obj.getUnidadesEjecutoras());
                if (obj.getLiquidado() != null) {
                    nuevoPre.setLiquidado(obj.getLiquidado());
                }
                if (obj.getEsProyecto() != null) {
                    nuevoPre.setEsProyecto(obj.getEsProyecto());
                }

                liquidarDAO.insertPresupuesto(nuevoPre);

                /*CuentasPresupuesto*/
                Set<CuentasPresupuesto> listCuen = obj.getCuentasPresupuestos();
                if (listCuen != null) {
                    if (listCuen.size() > 0) {
                        for (CuentasPresupuesto cuenPre : listCuen) {
                            CuentasPresupuestoId cuenId = new CuentasPresupuestoId();
                            cuenId.setCodigoCuenta(cuenPre.getId().getCodigoCuenta());
                            cuenId.setCodigoPresupuesto(nuevoPre.getCodigoPresupuesto());
                            CuentasPresupuesto cuen = new CuentasPresupuesto();
                            cuen.setId(cuenId);
                            cuen.setDescripcionCuentaMayor(cuenPre.getDescripcionCuentaMayor());
                            cuen.setDependeDe(cuenPre.getDependeDe());
                            cuen.setConDetalle(cuenPre.getConDetalle());
                            liquidarDAO.insertCuentasPresupuesto(cuen);
                        }
                    }
                }

                /*DetalleCuentas*/
                Set<DetalleCuentas> listDetalle = obj.getDetalleCuentases();
                if (listDetalle != null) {
                    if (listDetalle.size() > 0) {
                        for (DetalleCuentas detaCons : listDetalle) {
                            DetalleCuentasId detaId = new DetalleCuentasId();
                            detaId.setCodgioCuenta(detaCons.getId().getCodgioCuenta());
                            detaId.setCodigoPresupuesto(nuevoPre.getCodigoPresupuesto());

                            DetalleCuentas deta = new DetalleCuentas();
                            deta.setId(detaId);
                            deta.setEnero(BigDecimal.ZERO);
                            deta.setFebrero(BigDecimal.ZERO);
                            deta.setMarzo(BigDecimal.ZERO);
                            deta.setAbril(BigDecimal.ZERO);
                            deta.setMayo(BigDecimal.ZERO);
                            deta.setJunio(BigDecimal.ZERO);
                            deta.setJulio(BigDecimal.ZERO);
                            deta.setAgosto(BigDecimal.ZERO);
                            deta.setSeptiembre(BigDecimal.ZERO);
                            deta.setOctubre(BigDecimal.ZERO);
                            deta.setNoviembre(BigDecimal.ZERO);
                            deta.setDiciembre(BigDecimal.ZERO);
                            deta.setPresupuestoAprobado(BigDecimal.ZERO);
                            deta.setPresupuestoComprometido(BigDecimal.ZERO);
                            deta.setPresupuestoReal(BigDecimal.ZERO);
                            deta.setEjecutadoContable(BigDecimal.ZERO);
                            deta.setEjecutadoPagado(BigDecimal.ZERO);
                            deta.setPresupuestoReprogramar(BigDecimal.ZERO);
                            deta.setCargo(BigDecimal.ZERO);
                            deta.setAbono(BigDecimal.ZERO);

                            liquidarDAO.insertDetalleCuentas(deta);

                        }
                    }
                }
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

}
