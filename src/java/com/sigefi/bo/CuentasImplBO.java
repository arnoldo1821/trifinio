/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.CuentasBean;
import com.sigefi.beans.NodoCuentaCatalogo;
import com.sigefi.dao.CuentasDAO;
import com.sigefi.entity.CatalogoCuentas;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.primefaces.model.DefaultTreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.primefaces.model.TreeNode;

/**
 *
 * @author AAldemaro
 */
@Service(value = "cuentasBO")
public class CuentasImplBO implements CuentasBO {

    @Autowired
    private CuentasDAO cuentasDAO;

    public CuentasDAO getCuentasDAO() {
        return cuentasDAO;
    }

    public void setCuentasDAO(CuentasDAO cuentasDAO) {
        this.cuentasDAO = cuentasDAO;
    }

    @Override
    public boolean insertCuenta(CuentasBean obj) {
        
        int idCuenta = cuentasDAO.ultimoRegistroNivel(0);
        
        if (idCuenta > 0) {
            CatalogoCuentas ca = new CatalogoCuentas();
            ca.setIdCuenta(idCuenta);
            ca.setSubcuentaDe(0);
            ca.setUnidadesEjecutoras(new UnidadesEjecutoras(obj.getUnidadEjecutoraSeleccionada()));
            ca.setCodigoCuenta(obj.getCodigoCuenta());
            ca.setNombreCuenta(obj.getNombreCuenta());
            ca.setDescripcionCuenta(obj.getDescripcionCuenta());
            ca.setEsDetalle(true);
            ca.setActiva(true);
            
            cuentasDAO.insertCuenta(ca);

            return true;
        } else {
            return false;
        }
    }

    @Override
    public TreeNode listNodoCuentasCatalogo(short unidad) {

        TreeNode root = new DefaultTreeNode("principal", null);

        Map<Integer, TreeNode> mapNodo = new HashMap<>();
        
        List<CatalogoCuentas> listAllCatalogoCuenta = cuentasDAO.listAllCatalogoCuenta(unidad);
        
        if(listAllCatalogoCuenta != null){
            for(CatalogoCuentas cuenta : listAllCatalogoCuenta){
                if(cuenta.getSubcuentaDe() == 0){
                    TreeNode nodo = new DefaultTreeNode(cuenta, root); 
                    mapNodo.put(cuenta.getIdCuenta(), nodo);
                }else{
                    TreeNode nodo2 = new DefaultTreeNode(cuenta, mapNodo.get(cuenta.getSubcuentaDe())); 
                    mapNodo.put(cuenta.getIdCuenta(), nodo2);
                }
            }
        }
        
        
        return root;
    }

    @Override
    public boolean insertSubCuenta(CuentasBean obj) {
        
        int idCuenta = cuentasDAO.ultimoRegistroNivel(obj.getIdCuenta());
        
        if (idCuenta != 0) {
            
            CatalogoCuentas ca = new CatalogoCuentas();
          //  ca.setIdCuenta(idCuenta);
            ca.setCodigoCuenta(obj.getCodigoSubCuenta());
            ca.setUnidadesEjecutoras(new UnidadesEjecutoras(obj.getUnidadEjecutoraSeleccionada()));
            ca.setSubcuentaDe(obj.getIdCuenta());
            ca.setNombreCuenta(obj.getNombreSubCuenta());
            ca.setDescripcionCuenta(obj.getDescripcionSubCuenta());
            ca.setEsDetalle(true);
            ca.setActiva(true);
            
            cuentasDAO.insertCuenta(ca);
            
            CatalogoCuentas cuentaUpdate = cuentasDAO.elementoCatalogoCuenta(obj.getIdCuenta());
            
            if(cuentaUpdate != null){
                cuentaUpdate.setEsDetalle(false);
                cuentasDAO.updateCuenta(cuentaUpdate);
            }
            return true;
        } else {
            return false;
        }
    }

}
