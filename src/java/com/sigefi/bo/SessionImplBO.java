/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.SessionBean;
import com.sigefi.dao.SessionDAO;
import com.sigefi.clases.Encrypt;
import com.sigefi.entity.UnidadesEjecutoras;
import com.sigefi.entity.Usuarios;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */

@Service (value = "sessionBO")
public class SessionImplBO implements SessionBO{
   
    @Autowired
    private SessionDAO sessionDao;

    public SessionDAO getSessionDao() {
        return sessionDao;
    }

    public void setSessionDao(SessionDAO sessionDao) {
        this.sessionDao = sessionDao;
    }
    
    @Override
    public boolean updateCambioContra(String usuario, String contra) {
        
        try {

            FacesContext context = FacesContext.getCurrentInstance();

            Usuarios usu = sessionDao.getElemtoUsuario(usuario);

            usu.setClave(Encrypt.sha512(contra));
            sessionDao.updateCambioContra(usu);

            context.addMessage(null, new FacesMessage("Contraseña Actualizada", "Correctamente"));

            return true;

        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void elementoUsuario(SessionBean obj) {
        try {
            if (obj.getUsu() != null) {
                Usuarios usu = sessionDao.getElemtoUsuario(obj.getUsu());
                if (usu != null) {
                    obj.setNombreUsuario(usu.getNombreUsuario());
                    obj.setApellidoUsuario(usu.getApellidosUsuario());
                    obj.setNombreCompleto(usu.getNombreUsuario()+" "+usu.getApellidosUsuario());
                    UnidadesEjecutoras uni = sessionDao.getElemtoUnidadEjecutora(usu.getUnidadesEjecutoras().getCodigo());
                    if (uni != null) {
                        obj.setIdUnidadEjecutora(uni.getCodigo());
                        obj.setUnidadEjecutora(uni.getNombreUnidad());
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    public boolean esLaContrasena(SessionBean obj) {
        boolean isPassword = false ;
        try {
            FacesContext context = FacesContext.getCurrentInstance();

            Usuarios usu = sessionDao.getElemtoUsuario(obj.getUsu());

            String clave = Encrypt.sha512(obj.getUsu());
            if (clave.equals(usu.getClave())) {
                isPassword = true ;
            }

            return isPassword;

        } catch (Exception e) {
            return false;
        }
    }
    
}
