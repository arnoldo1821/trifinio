/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.EjemploBean;
import com.sigefi.beans.RequisicionBean;
import com.sigefi.entity.CentroCosto;
import com.sigefi.entity.Ciudades;
import com.sigefi.entity.CuentasPresupuesto;
import com.sigefi.entity.DetalleCuentas;
import com.sigefi.entity.DetalleCuentasId;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.PartesInteresadas;
import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.RequerimientoPagos;
import com.sigefi.entity.Rol;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.List;

/**
 *
 * @author Administrador
 */
public interface RequisicionBO {
    List<PartesInteresadas> listProveedorAll();
    List<CentroCosto> listCentroAll();
    Presupuestos elementoPresupuesto(RequisicionBean obj);
    UnidadesEjecutoras elementoUnidad(RequisicionBean obj);
    PartesInteresadas elementoProveedor(RequisicionBean obj);
    RequerimientoPagos elementoRequerimiento();
    RequerimientoPagos insertRequisicion(RequisicionBean obj);
//    List<Object> correlativo_Total_DetalleRequerimiento(int codigoRequerimiento);
    List<DetalleRequerimientoPago> correlativo_Total_DetalleRequerimiento(int codigoRequerimiento);
    boolean insertDetalleRequisicion(RequisicionBean obj);

    
    boolean updateRequisicion(RequisicionBean obj);
    boolean deleteRequisicion(String pkRequisicion);
    List<RequisicionBean> listRequisicionAll();
    Boolean consultarRequisicion(String usuario,RequisicionBean obj);
    
    List<DetalleCuentas> listDetalleCuentasAll(int idPresupuesto);
    
}
