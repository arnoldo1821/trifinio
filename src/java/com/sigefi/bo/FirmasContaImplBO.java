/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.FirmasContaBean;
import com.sigefi.dao.FirmasContaDAO;
import com.sigefi.entity.Empresa;
import com.sigefi.entity.FirmasConta;
import com.sigefi.entity.FirmasContaId;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "firmasContaBO")
public class FirmasContaImplBO implements FirmasContaBO {

    @Autowired
    private FirmasContaDAO firmasContaDAO;

    public FirmasContaDAO getFirmasContaDAO() {
        return firmasContaDAO;
    }

    public void setFirmasContaDAO(FirmasContaDAO firmasContaDAO) {
        this.firmasContaDAO = firmasContaDAO;
    }

    @Override
    public boolean insertFirma(FirmasContaBean obj) {
        try {
            FirmasContaId fcId = new FirmasContaId();
            fcId.setEmpresaFirmas(obj.getEmpresaSelec().getCodigoEmpresaContable());
            fcId.setCodFirmas(obj.getCodfirma());
            
            FirmasConta fc = new FirmasConta();
            fc.setId(fcId);
            fc.setSeNombre(obj.getNombre());
            fc.setSeTitulo(obj.getTitulo());
            firmasContaDAO.insertFirma(fc);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<FirmasConta> elementoFirmas(Empresa empresaFirma) {
        return firmasContaDAO.listFirmas(empresaFirma);
    }

    @Override
    public Empresa elementoEmpresa(short idUnidadEjecutora,int anio) {
        return firmasContaDAO.elementoEmpresa(idUnidadEjecutora,anio);
    }

}
