/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.dao.DetallesCuentasDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "detallesCuentasBO")
public class DetallesCuentasImplBO implements DetallesCuentasBO{
    
    @Autowired
    private DetallesCuentasDAO detallesCuentasDAO;

    public DetallesCuentasDAO getDetallesCuentasDAO() {
        return detallesCuentasDAO;
    }

    public void setDetallesCuentasDAO(DetallesCuentasDAO detallesCuentasDAO) {
        this.detallesCuentasDAO = detallesCuentasDAO;
    }

    

    
    
}
