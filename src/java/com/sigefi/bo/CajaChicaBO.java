/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.CajaChicaBean;
import com.sigefi.entity.DetalleCuentas;
import com.sigefi.entity.DetalleLiquidarCajaChica;
import com.sigefi.entity.LiquidarCajaChica;
import com.sigefi.entity.PartesInteresadas;
import com.sigefi.entity.Presupuestos;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface CajaChicaBO {
    List<PartesInteresadas> listProveedorAll();
    Presupuestos elementoPresupuesto(CajaChicaBean obj);
    LiquidarCajaChica insertLiquidacionCajaChica(CajaChicaBean obj);
    List<DetalleCuentas> listDetalleCuentasAll(int idPresupuesto);
    List<DetalleLiquidarCajaChica> correlativo_Total_DetalleRequerimientoCaja(int codigoRequerimientoIncre);
    boolean insertDetalleRequisicionCaja(CajaChicaBean obj);
}
