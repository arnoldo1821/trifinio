/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.ModeloContratoBean;
import com.sigefi.dao.ClausulasModeloContratoDAO;
import com.sigefi.entity.ClausulasModeloContrato;
import com.sigefi.entity.ModeloContrato;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author SKAR
 */
@Service(value="clausulasModeloContratoBO")
public class ClausulasModeloContratoImplBO implements ClausulasModeloContratoBO {

    @Autowired
    ClausulasModeloContratoDAO clausulasModeloContratoDAO;
    
    @Override
    public void insert(ClausulasModeloContrato obj) {
        clausulasModeloContratoDAO.insert(obj);
    }

    @Override
    public void update(ClausulasModeloContrato obj) {
        clausulasModeloContratoDAO.update(obj);
    }

    @Override
    public void delete(ClausulasModeloContrato obj) {
        clausulasModeloContratoDAO.delete(obj);
    }

    @Override
    public List<ClausulasModeloContrato> selectAll() {
        return clausulasModeloContratoDAO.selectAll();
    }

    @Override
    public List<ClausulasModeloContrato> selectAll(ModeloContrato obj) {
        return clausulasModeloContratoDAO.selectAll(obj);
    }

    @Override
    public ClausulasModeloContrato select(ModeloContratoBean obj) {
        return clausulasModeloContratoDAO.select(obj);
    }

    public ClausulasModeloContratoDAO getClausulasModeloContratoDAO() {
        return clausulasModeloContratoDAO;
    }

    public void setClausulasModeloContratoDAO(ClausulasModeloContratoDAO clausulasModeloContratoDAO) {
        this.clausulasModeloContratoDAO = clausulasModeloContratoDAO;
    }
    
}
