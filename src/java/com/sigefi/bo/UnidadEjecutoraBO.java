/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.UnidadEjecutoraBean;
import com.sigefi.entity.Ciudades;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface UnidadEjecutoraBO {
    boolean insertUnidadEjecutora(UnidadEjecutoraBean obj);
    boolean actualizaUnidadEjecutora(UnidadEjecutoraBean obj);
    List<Ciudades> listAllCiudades();
    List<UnidadEjecutoraBean> listAllUnidadesEjecutoras();
    List<UnidadEjecutoraBean> listAllUnidades();
    boolean insertFirmaAutorizada(UnidadEjecutoraBean obj);
}
