/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.SeguimientoCuentasBancariasBean;
import com.sigefi.dao.SeguimientoCuentasBancariasDAO;
import com.sigefi.entity.SegumientoCuentaBancaria;
//import com.sigefi.entity.SegumientoCuentaBancariaId;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Elmer
 */
@Service(value = "seguimientoCuentasBancariasBO")
public class SeguimientosCuentasBancariasImplBO implements SeguimientoCuentasBancariasBO{
    @Autowired
    private SeguimientoCuentasBancariasDAO seguimientoCuentasBancariasDAO;

    public SeguimientoCuentasBancariasDAO getSeguimientoCuentasBancariasDAO() {
        return seguimientoCuentasBancariasDAO;
    }

    public void setSeguimientoCuentasBancariasDAO(SeguimientoCuentasBancariasDAO seguimientoCuentasBancariasDAO) {
        this.seguimientoCuentasBancariasDAO = seguimientoCuentasBancariasDAO;
    }

    @Override
    public List<SegumientoCuentaBancaria> listAllSeguimientoCuentas(String codigoCuenta,Date inicio, Date fin) {
        return seguimientoCuentasBancariasDAO.listAllSeguimientoCuentas(codigoCuenta,inicio,fin);
    }

    @Override
    public boolean insertMovimientoCuenta(SeguimientoCuentasBancariasBean bean) {
        SegumientoCuentaBancaria consul = null;//seguimientoCuentasBancariasDAO.elemntoSeguimientoCuentaBancaria(bean.getCuentasBancarias());
        if(consul == null){
            //SegumientoCuentaBancariaId scbId = new SegumientoCuentaBancariaId();
            SegumientoCuentaBancaria scb = new SegumientoCuentaBancaria();
    
            //scbId.setCodigoCuenta(bean.getCodigoCuenta());
            //scb.setId(scbId);
            scb.setCodigoCuenta(bean.getCodigoCuenta());
            scb.setCodigoComprobante(bean.getCodigoComprobante());
            scb.setDocumentoTrasaccion(null);
            scb.setFechaTransaccion(bean.getFechaIngreso());
            scb.setAbonoTransaccion(bean.getValor());
            scb.setPongaseAlaordende(bean.getPongaseAlaCuentade());
            scb.setCargoTransaccion(BigDecimal.ZERO);
            scb.setSaldoTransaccion(bean.getNuevoSaldo());
            scb.setTransaccion("Nota de Abono");
            scb.setConcepto(bean.getConcepto());
            scb.setRevisadoContabilidad(Boolean.FALSE);
            scb.setTipoTransaccion(null);
            scb.setValorLetras(bean.getValorLetras());
            
            seguimientoCuentasBancariasDAO.insertMovimiento(scb);
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean updateMovimientoCuenta(SegumientoCuentaBancaria obj) {
        try {
            seguimientoCuentasBancariasDAO.updateMovimiento(obj);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean deleteMovimientoCuenta(SegumientoCuentaBancaria obj) {
        try {
            seguimientoCuentasBancariasDAO.deleteMovimiento(obj);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public SegumientoCuentaBancaria seguimientoElemento(String codigoCuenta) {
        return seguimientoCuentasBancariasDAO.elemntoSeguimientoCuentaBancaria(codigoCuenta);
    }

    @Override
    public boolean insertMovimeintoCargo(SeguimientoCuentasBancariasBean bean) {
        SegumientoCuentaBancaria consul = null;//seguimientoCuentasBancariasDAO.elemntoSeguimientoCuentaBancaria(bean.getCuentasBancarias());
        if(consul == null){
           // SegumientoCuentaBancariaId scbId = new SegumientoCuentaBancariaId();
            SegumientoCuentaBancaria scb = new SegumientoCuentaBancaria();
    
           // scbId.setCodigoCuenta(bean.getCodigoCuenta());
           // scb.setId(scbId);
            scb.setCodigoCuenta(bean.getCodigoCuenta());
            scb.setCodigoComprobante(bean.getCodigoComprobante());
            scb.setDocumentoTrasaccion(bean.getDocumento());
            scb.setCodigoDocumento(bean.getCodigoDocumento());
            scb.setFechaTransaccion(bean.getFechaIngreso());
            scb.setAbonoTransaccion(BigDecimal.ZERO);
            scb.setPongaseAlaordende(bean.getPongaseAlaCuentade());
            scb.setCargoTransaccion(bean.getValor());
            scb.setSaldoTransaccion(bean.getNuevoSaldo());
            scb.setTransaccion("Nota de Cargo");
            scb.setConcepto(bean.getConcepto());
            scb.setRevisadoContabilidad(Boolean.FALSE);
            scb.setTipoTransaccion(bean.getTipoCargo());
            scb.setValorLetras(bean.getValorLetras());
            
            seguimientoCuentasBancariasDAO.insertMovimiento(scb);
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean insertMovimientoCheque(SeguimientoCuentasBancariasBean bean) {
        SegumientoCuentaBancaria consul = null;//seguimientoCuentasBancariasDAO.elemntoSeguimientoCuentaBancaria(bean.getCuentasBancarias());
        if(consul == null){
            //SegumientoCuentaBancariaId scbId = new SegumientoCuentaBancariaId();
            SegumientoCuentaBancaria scb = new SegumientoCuentaBancaria();
    
            //scbId.setCodigoCuenta(bean.getCodigoCuenta());
            //scb.setId(scbId);
            scb.setCodigoCuenta(bean.getCodigoCuenta());
            scb.setCodigoComprobante(bean.getCodigoComprobante());
            scb.setFechaTransaccion(bean.getFechaIngreso());
            scb.setAbonoTransaccion(bean.getValor());
            scb.setPongaseAlaordende(bean.getPongaseAlaCuentade());
            scb.setCargoTransaccion(BigDecimal.ZERO);
            scb.setSaldoTransaccion(bean.getNuevoSaldo());
            scb.setTransaccion("Cheque de Tesoreria");
            scb.setConcepto(bean.getConcepto());
            scb.setRevisadoContabilidad(Boolean.FALSE);
            scb.setValorLetras(bean.getValorLetras());
            
            seguimientoCuentasBancariasDAO.insertMovimiento(scb);
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean insertMovimientoNotaTesoreria(SeguimientoCuentasBancariasBean bean) {
        SegumientoCuentaBancaria consul = null;//seguimientoCuentasBancariasDAO.elemntoSeguimientoCuentaBancaria(bean.getCuentasBancarias());
        if(consul == null){
            //SegumientoCuentaBancariaId scbId = new SegumientoCuentaBancariaId();
            SegumientoCuentaBancaria scb = new SegumientoCuentaBancaria();
    
            //scbId.setCodigoCuenta(bean.getCodigoCuenta());
            //scb.setId(scbId);
            scb.setCodigoCuenta(bean.getCodigoCuenta());
            scb.setCodigoComprobante(bean.getCodigoComprobante());
            scb.setDocumentoTrasaccion(null);
            scb.setFechaTransaccion(bean.getFechaIngreso());
            scb.setAbonoTransaccion(bean.getValor());
            scb.setPongaseAlaordende(bean.getPongaseAlaCuentade());
            scb.setCargoTransaccion(BigDecimal.ZERO);
            scb.setSaldoTransaccion(bean.getNuevoSaldo());
            scb.setTransaccion("Nota de Tesoreria");
            scb.setConcepto(bean.getConcepto());
            scb.setRevisadoContabilidad(Boolean.FALSE);
            scb.setTipoTransaccion(null);
            scb.setValorLetras(bean.getValorLetras());
            
            seguimientoCuentasBancariasDAO.insertMovimiento(scb);
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean insertMovimientoEmisionCheque(SeguimientoCuentasBancariasBean bean) {
        SegumientoCuentaBancaria consul = null;//seguimientoCuentasBancariasDAO.elemntoSeguimientoCuentaBancaria(bean.getCuentasBancarias());
        if(consul == null){
            //SegumientoCuentaBancariaId scbId = new SegumientoCuentaBancariaId();
            SegumientoCuentaBancaria scb = new SegumientoCuentaBancaria();
    
            //scbId.setCodigoCuenta(bean.getCodigoCuenta());
            //scb.setId(scbId);
            scb.setCodigoCuenta(bean.getCodigoCuenta());
            scb.setCodigoComprobante(bean.getCodigoComprobante());
            scb.setDocumentoTrasaccion(bean.getDocumento());
            scb.setCodigoDocumento(bean.getCodigoDocumento());
            scb.setFechaTransaccion(bean.getFechaIngreso());
            scb.setAbonoTransaccion(bean.getValor());
            scb.setPongaseAlaordende(bean.getPongaseAlaCuentade());
            scb.setCargoTransaccion(BigDecimal.ZERO);
            scb.setSaldoTransaccion(bean.getNuevoSaldo());
            scb.setTransaccion("Emision de Cheque");
            scb.setConcepto(bean.getConcepto());
            scb.setRevisadoContabilidad(Boolean.FALSE);
            scb.setValorLetras(bean.getValorLetras());
            scb.setCajaChica(bean.getCajaChica());
            scb.setCodigoRequerimiento(bean.getCodigoRequerimientoPago());
            
            seguimientoCuentasBancariasDAO.insertMovimiento(scb);
            return true;
        }else{
            return false;
        }
    }
    
    
    
}
