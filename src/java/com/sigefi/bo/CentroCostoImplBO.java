/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.CentroCostoBean;
import com.sigefi.dao.CentroCostoDAO;
import com.sigefi.entity.CentroCosto;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Elmer
 */
@Service(value = "centroCostoBO")
public class CentroCostoImplBO implements CentroCostoBO {
    
    @Autowired
    private CentroCostoDAO centroCostoDAO;

    @Override
    public boolean insertCentroCosto(CentroCostoBean obj) {
         CentroCosto centroCosto = centroCostoDAO.elementoCentroCosto(obj.getIdCentroCosto());
        
        if(centroCosto == null){   
            
            CentroCosto centroC = new CentroCosto();
            
            centroC.setCentroCosto(obj.getCentroCosto());
            centroC.setActiva(true);
            
            centroCostoDAO.insertCentroCosto(centroC);
            return true;
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"El centro de costo ya existe" , ""));
            return false;
        }
    }

    @Override
    public boolean updateCentroCosto(CentroCostoBean obj) {
        CentroCosto centroCosto = centroCostoDAO.elementoCentroCosto(obj.getIdCentroCosto());
        
        if(centroCosto != null){   
            
            CentroCosto centroC = new CentroCosto();
            
            centroC.setIdCentroCosto(obj.getIdCentroCosto());
            centroC.setCentroCosto(obj.getCentroCosto());
            centroC.setActiva(obj.getActiva());
           
            centroCostoDAO.updateCentroCosto(centroC);
            return true;
        }//Fin del if
        else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"No se pudo actualizar el registro" , ""));
            return false;
        }
    }

    @Override
    public boolean deleteCentroCosto(CentroCostoBean obj) {
        CentroCosto centroCosto = centroCostoDAO.elementoCentroCosto(obj.getIdCentroCosto());
        
        if(centroCosto != null){   
            
            CentroCosto centroC = new CentroCosto();
            
            centroC.setIdCentroCosto(obj.getIdCentroCosto());
            centroC.setCentroCosto(obj.getCentroCosto());
            centroC.setActiva(false);
           
            centroCostoDAO.updateCentroCosto(centroC);
            return true;
        }//Fin del if
        else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"No se pudo eliminar el registro" , ""));
            return false;
        }
    }

    
    @Override
    public List<CentroCostoBean> listCentroCostoAll() {
        List<CentroCostoBean> listBean = new ArrayList<>();
        List<CentroCosto> listCentro = centroCostoDAO.listCentroCostoAll();
        if(listCentro != null){
            for(CentroCosto obj : listCentro){
                
                CentroCostoBean bean = new CentroCostoBean();
                
                bean.setIdCentroCosto(obj.getIdCentroCosto());
                bean.setCentroCosto(obj.getCentroCosto());
                bean.setActiva(obj.getActiva());
                
                listBean.add(bean);
            }
            
            return listBean;
        }
        return null;
    }
    
}
