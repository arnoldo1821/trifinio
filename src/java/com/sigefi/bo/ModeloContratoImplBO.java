/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.dao.ModeloContratoDAO;
import com.sigefi.entity.ModeloContrato;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Oscar
 */
@Service(value = "modeloContratoBO")
public class ModeloContratoImplBO implements ModeloContratoBO{
    
    @Autowired
    private ModeloContratoDAO modeloContratoDAO;

    @Override
    public void insert(ModeloContrato obj) {
        modeloContratoDAO.insert(obj);
    }

    @Override
    public List<ModeloContrato> selectAll() {
        return modeloContratoDAO.selectAll();
    }

    @Override
    public List<ModeloContrato> selectAll(ModeloContrato obj) {
        return modeloContratoDAO.selectAll(obj);
    }

    public ModeloContratoDAO getModeloContratoDAO() {
        return modeloContratoDAO;
    }

    public void setModeloContratoDAO(ModeloContratoDAO modeloContratoDAO) {
        this.modeloContratoDAO = modeloContratoDAO;
    }
    
}
