/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.ProveedorBean;
import com.sigefi.entity.PartesInteresadas;
import java.util.List;
import org.primefaces.model.TreeNode;

/**
 *
 * @author AAldemaro
 */
public interface ProveedorBO {
    boolean insertProveedor(ProveedorBean obj);
    boolean updateProveedor(ProveedorBean obj);
    boolean deleteProveedor(ProveedorBean obj);
    List <ProveedorBean> listProveedorAll();
    List <ProveedorBean> listPartesInteresadas();
    TreeNode listNodoProveedores();
}
