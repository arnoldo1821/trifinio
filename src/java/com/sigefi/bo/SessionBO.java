/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.SessionBean;

/**
 *
 * @author AAldemaro
 */
public interface SessionBO {
    boolean updateCambioContra(String usuario,String contra);
    void elementoUsuario(SessionBean obj);
    boolean esLaContrasena(SessionBean obj);
    
}
