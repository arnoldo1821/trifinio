/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.DetallePresupuestoBean;
import com.sigefi.beans.UnidadEjecutoraBean;
import com.sigefi.dao.DetallePresupuestoDAO;
import com.sigefi.entity.CuentasPresupuesto;
import com.sigefi.entity.DetalleCuentas;
import com.sigefi.entity.DetalleCuentasId;
import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.UnidadesEjecutoras;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIData;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "detallePresupuestoBO")
public class DetallePresupuestoImplBO implements DetallePresupuestoBO {

    @Autowired
    private DetallePresupuestoDAO detallePresupuestoDAO;

    public DetallePresupuestoDAO getDetallePresupuestoDAO() {
        return detallePresupuestoDAO;
    }

    public void setDetallePresupuestoDAO(DetallePresupuestoDAO detallePresupuestoDAO) {
        this.detallePresupuestoDAO = detallePresupuestoDAO;
    }

    @Override
    public List<CuentasPresupuesto> listCuentasSelec(DetallePresupuestoBean obj) {
        return detallePresupuestoDAO.listCuentasSelec(obj);
    }

    @Override
    public DetalleCuentas consultarDetalleCuenta(int codigoPresupuesto, int codigoCuenta) {
        return detallePresupuestoDAO.consultarDetalleCuenta(codigoPresupuesto, codigoCuenta);
    }

    @Override
    public boolean inserUpdateDetalleCuenta(DetallePresupuestoBean obj) {

        DetalleCuentas dc = detallePresupuestoDAO.consultarDetalleCuenta(obj.getCodigoPresup(), obj.getCodigoCuent());
        
        if (dc == null) {
            DetalleCuentas dcn = new DetalleCuentas();

            DetalleCuentasId dcnid = new DetalleCuentasId();
            dcnid.setCodigoPresupuesto(obj.getCodigoPresup());
            dcnid.setCodgioCuenta(obj.getCodigoCuent());

            dcn.setId(dcnid);

            return inserModificarDetalle(1, obj, dcn);

        } else {
            return inserModificarDetalle(2, obj, dc);
        }
    }

    public boolean inserModificarDetalle(int val, DetallePresupuestoBean obj, DetalleCuentas dcn) {

//            dcn.setEnero(obj.getEnero());
//            dcn.setFebrero(obj.getFebrero());
//            dcn.setMarzo(obj.getMarzo());
//            dcn.setAbril(obj.getAbril());
//            dcn.setMayo(obj.getMayo());
//            dcn.setJunio(obj.getJunio());
//            dcn.setJulio(obj.getJulio());
//            dcn.setAgosto(obj.getAgosto());
//            dcn.setSeptiembre(obj.getSeptiembre());
//            dcn.setOctubre(obj.getOctubre());
//            dcn.setNoviembre(obj.getNoviembre());
//            dcn.setDiciembre(obj.getDiciembre());
        dcn.setPresupuestoAprobado(obj.getPresupuestoAprobado());
        dcn.setPresupuestoReal(obj.getPresupuestoAprobado());
        dcn.setEjecutadoPagado(BigDecimal.ZERO);
        dcn.setEjecutadoContable(BigDecimal.ZERO);
        dcn.setPresupuestoComprometido(BigDecimal.ZERO);
        dcn.setPresupuestoReprogramar(BigDecimal.ZERO);
        dcn.setCargo(BigDecimal.ZERO);
        dcn.setAbono(BigDecimal.ZERO);
        if (val == 1) {
         //   FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Detalle Agregado Correctamente", ""));
            detallePresupuestoDAO.insertDetalleCuenta(dcn);
        } else {
         //   FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Detalle Modificado Correctamente", ""));
            detallePresupuestoDAO.updateDetalleCuenta(dcn);
        }

        return true;
    }
    
    public boolean insertarDetalleCuenta(DetallePresupuestoBean obj, DetalleCuentas dcn) {


        dcn.setPresupuestoAprobado(obj.getPresupuestoAprobado());
        dcn.setPresupuestoReal(obj.getPresupuestoAprobado());
        dcn.setEjecutadoPagado(BigDecimal.ZERO);
        dcn.setEjecutadoContable(BigDecimal.ZERO);
        dcn.setPresupuestoComprometido(BigDecimal.ZERO);
        dcn.setPresupuestoReprogramar(BigDecimal.ZERO);
        dcn.setCargo(BigDecimal.ZERO);
        dcn.setAbono(BigDecimal.ZERO);
            detallePresupuestoDAO.insertarDetalleCuenta(dcn);
        return true;
    }

    @Override
    public Presupuestos elementoPresupuesto(DetallePresupuestoBean obj) {
        return detallePresupuestoDAO.elementoPresupuesto(obj);
    }

    @Override
    public Set<DetalleCuentas> consultarDetallePresupuesto(DetallePresupuestoBean obj) {
        return detallePresupuestoDAO.consultarDetallePresupuesto(obj);
    }

    @Override
    public List<DetallePresupuestoBean> listAllDetallePresupuesto(DetallePresupuestoBean detalle) {
        List<DetallePresupuestoBean> listBean = new ArrayList();
        List<CuentasPresupuesto> listUnidad = detallePresupuestoDAO.listAllCuentasPresupuesto(detalle);
        //List<DetalleCuentas> listDetalle = detallePresupuestoDAO.listAllDetalleCuentas(detalle);
        if (listUnidad != null) {
            for (CuentasPresupuesto obj : listUnidad) {
                DetallePresupuestoBean bean = new DetallePresupuestoBean();
                DetalleCuentas cuenta = detallePresupuestoDAO.listAllDetalleCuentas(detalle, obj.getPresupuestos().getCodigoPresupuesto(), obj.getCatalogoCuentas().getIdCuenta());
                bean.setId(obj.getId());
                bean.setCatalogoCuentas(obj.getCatalogoCuentas());
                bean.setPresupuestos(obj.getPresupuestos());
                bean.setDescripcionCuentaMayor(obj.getDescripcionCuentaMayor());
                bean.setDependeDe(obj.getDependeDe());
                bean.setConDetalle(obj.getConDetalle());
                if(cuenta !=null){
                bean.setPresupuestoAprobado(cuenta.getPresupuestoAprobado());
                }
                listBean.add(bean);
                
            }
            
            return listBean;
        }
        return null;
    }

    @Override
    public boolean insertCuenta(UIData list) {
        try {
            List<DetallePresupuestoBean> listCuentas;
            listCuentas = (List<DetallePresupuestoBean>) list.getValue();
            for (DetallePresupuestoBean obj : listCuentas) {
                    DetalleCuentas dcn = new DetalleCuentas();

                    DetalleCuentasId dcnid = new DetalleCuentasId();
                    dcnid.setCodigoPresupuesto(obj.getPresupuestos().getCodigoPresupuesto());
                    dcnid.setCodgioCuenta(obj.getId().getCodigoCuenta());

                    dcn.setId(dcnid);
                    insertarDetalleCuenta(obj,dcn);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
