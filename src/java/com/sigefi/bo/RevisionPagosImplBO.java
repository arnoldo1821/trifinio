/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.dao.RevisionPagosDAO;
import com.sigefi.entity.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "revisionPagosBO")
public class RevisionPagosImplBO implements RevisionPagosBO{
    @Autowired
    private RevisionPagosDAO revisionPagosDAO;

    public RevisionPagosDAO getRevisionPagosDAO() {
        return revisionPagosDAO;
    }

    public void setRevisionPagosDAO(RevisionPagosDAO revisionPagosDAO) {
        this.revisionPagosDAO = revisionPagosDAO;
    }

    @Override
    public Empresa consultarEmpresa(short idUnidadEje, int anio) {
        return revisionPagosDAO.consultarEmpresa(idUnidadEje, anio);
    }
    
    
}
