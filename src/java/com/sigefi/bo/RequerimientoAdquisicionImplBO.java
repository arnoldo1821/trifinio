/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.RequerimientoAdquisicionBean;
import com.sigefi.dao.RequerimientoAdquisicionDAO;
import com.sigefi.entity.DetalleRequerimientoAdquisicion;
import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.RequerimientoAdquisicion;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "requerimientoAdquisicionBO")
public class RequerimientoAdquisicionImplBO implements RequerimientoAdquisicionBO{
    @Autowired
    private RequerimientoAdquisicionDAO requerimientoAdquisicionDAO;

    public RequerimientoAdquisicionDAO getRequerimientoAdquisicionDAO() {
        return requerimientoAdquisicionDAO;
    }

    public void setRequerimientoAdquisicionDAO(RequerimientoAdquisicionDAO requerimientoAdquisicionDAO) {
        this.requerimientoAdquisicionDAO = requerimientoAdquisicionDAO;
    }

    @Override
    public List<UnidadesEjecutoras> listUnidades() {
        return requerimientoAdquisicionDAO.listUnidades();
    }

    @Override
    public Presupuestos elementoPresupuesto(short idUnidadEjec, int anioPre) {
        return requerimientoAdquisicionDAO.elementoPresupuesto(idUnidadEjec, anioPre);
    }

    @Override
    public boolean consultarCodigoRequerimiento(String codigoReq) {
        RequerimientoAdquisicion consulta = requerimientoAdquisicionDAO.consultarCodigoRequerimiento(codigoReq);
        if(consulta == null){
            return true;
        }else{
            return false;
        }
    }
    
}
