/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.ArchivosRequerimientoBean;
import com.sigefi.clases.FechaSistema;
import com.sigefi.dao.ArchivosRequerimientoDAO;
import com.sigefi.dao.RequisicionDAO;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.DocumentosRequerimientos;
import com.sigefi.entity.DocumentosRequerimientosId;
import com.sigefi.entity.EstadoRequisicion;
import com.sigefi.entity.EstadoRequisicionId;
import com.sigefi.entity.RequerimientoPagos;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AAldemaro
 */
@Service(value = "archivosRequerimientoBO")
public class ArchivosRequerimientoImplBO implements ArchivosRequerimientoBO {

    @Autowired
    private ArchivosRequerimientoDAO archivosRequerimientoDAO;

    public ArchivosRequerimientoDAO getArchivosRequerimientoDAO() {
        return archivosRequerimientoDAO;
    }

    public void setArchivosRequerimientoDAO(ArchivosRequerimientoDAO archivosRequerimientoDAO) {
        this.archivosRequerimientoDAO = archivosRequerimientoDAO;
    }

    @Autowired
    private RequisicionDAO requisicionDAO;

    public RequisicionDAO getRequisicionDAO() {
        return requisicionDAO;
    }

    public void setRequisicionDAO(RequisicionDAO requisicionDAO) {
        this.requisicionDAO = requisicionDAO;
    }

    @Override
    public List<RequerimientoPagos> listAllReqUsuario(String usuario) {
        return archivosRequerimientoDAO.listAllReqUsuario(usuario);
    }

    @Override
    public boolean insertDocReque(ArchivosRequerimientoBean obj) {
        try {
//            DocumentosRequerimientos docReqCons = archivosRequerimientoDAO.elementoDocReq(obj.getTipoDocumento(), obj.getIncre_reque());
//            if (docReqCons == null) {
            DocumentosRequerimientosId docReqId = new DocumentosRequerimientosId();
            docReqId.setFkRequerimiento1(obj.getIncre_reque());
            docReqId.setTipoDocumento(obj.getTipoDocumento());

            DocumentosRequerimientos docReq = new DocumentosRequerimientos();
            docReq.setId(docReqId);
            docReq.setUrlDocumento(obj.getUrlDoc());

            archivosRequerimientoDAO.insertDocReque(docReq);

            return true;
//            } else {
//                return false;
//            }
        } catch (Exception e) {
            return false;
        }

    }

    @Override
    public boolean conprobacionElemento(ArchivosRequerimientoBean obj) {
        DocumentosRequerimientos docConsulta = archivosRequerimientoDAO.elementoDocReq(obj.getTipoDocumento(), obj.getIncre_reque());
        if (docConsulta == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public List<DocumentosRequerimientos> listAllDocReqUsuario(int pkIncreReque) {
        return archivosRequerimientoDAO.listAllDocReqUsuario(pkIncreReque);
    }

    @Override
    public List<String> listCorreoUsuarios(short idUnidadEjecutora) {
        return archivosRequerimientoDAO.listCorreoUsuarios(idUnidadEjecutora);
    }

    @Override
    public boolean enviarRequerimiento(RequerimientoPagos obj) {
        EstadoRequisicion consul = requisicionDAO.elementoEstadoRequisicion(obj);
        if (consul != null) {
            
            consul.setControl(false);
            requisicionDAO.updateEstadoRequi(consul);
            
            EstadoRequisicionId estaId = new EstadoRequisicionId();
            estaId.setIncreReqPk3(obj.getIncrReqPk());
            FechaSistema fs = new FechaSistema();
            estaId.setFechaEstado(fs.getFechaCompleta());
            
            EstadoRequisicion esta = new EstadoRequisicion();
            esta.setId(estaId);
            esta.setCodigoEstado((short) 2);
            esta.setUsuarioEstableceEstado(obj.getUsuario());
            esta.setControl(true);
            requisicionDAO.insertEstadoRequi(esta);
            return true;
        }
        return false;
    }

    @Override
    public List<DetalleRequerimientoPago> listAllDetalleRequerimiento(String usuario) {
        return archivosRequerimientoDAO.listAllDetalleRequerimiento(usuario);
    }

}
