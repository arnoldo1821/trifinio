/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.UsuarioBean;
import com.sigefi.entity.Ciudades;
import com.sigefi.entity.Rol;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.List;

/**
 *
 * @author Administrador
 */
public interface UsuarioBO {
    Boolean insertUsuario(UsuarioBean obj);
    Boolean updateUsuario(UsuarioBean obj);
    Boolean deleteUsuario(String usuario);
    List<Rol> listRolAll();
    List<UsuarioBean> listUsuarioAll();
    Boolean consultarUsuario(String usuario,UsuarioBean obj);
    List<Ciudades> listCiudades();
    List<UnidadesEjecutoras> listUnidadesEjecutoras();
}
