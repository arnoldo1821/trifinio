
package com.sigefi.bo;

import com.sigefi.entity.ModeloContrato;
import java.util.List;

/**
 *
 * @author Oscar
 */
public interface ModeloContratoBO {
    
    void insert(ModeloContrato obj);
    List<ModeloContrato> selectAll();
    List<ModeloContrato> selectAll(ModeloContrato obj);
    
}
