/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.entity.Presupuestos;

/**
 *
 * @author AAldemaro
 */
public interface LiquidarBO {
    Presupuestos elementoPre(short unidad,int anio);
    boolean insertPresupuestoCero(Presupuestos obj);
    boolean insertPresupuesto(Presupuestos obj);
    boolean liquidarPresupuesto(Presupuestos obj);
}
