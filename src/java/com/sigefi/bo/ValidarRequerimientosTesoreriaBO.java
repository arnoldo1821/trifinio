/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.bo;

import com.sigefi.beans.ValidarRequerimientosTesoreriaBean;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.DocumentosRequerimientos;
import com.sigefi.entity.NoConformidades;
import com.sigefi.entity.RequerimientoPagos;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface ValidarRequerimientosTesoreriaBO {
    List<RequerimientoPagos> listAllRequePago();
    List<DetalleRequerimientoPago> listAllDetalleRequerimiento ();
    List<DetalleRequerimientoPago> listDetalleRequePagoSelec(int incre_req_pk);
    List<DocumentosRequerimientos> listDocRequePagoSelec(int incre_req_pk);
    List<NoConformidades> listAllErrores();
    void validarRequerimiento(RequerimientoPagos req,ValidarRequerimientosTesoreriaBean obj);
}
