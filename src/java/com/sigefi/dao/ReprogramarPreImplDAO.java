/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.DetalleCuentas;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "reprogramarPreDAO")
public class ReprogramarPreImplDAO extends HibernateDaoSupport implements ReprogramarPreDAO{
    @Autowired
    public ReprogramarPreImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public List<DetalleCuentas> listDetalleCuentasPre(short unidadE, int anio) {
        DetachedCriteria dc = DetachedCriteria.forClass(DetalleCuentas.class);
        dc.createAlias("presupuestos", "pre");
        dc.createAlias("pre.unidadesEjecutoras", "uni");
        dc.add(Restrictions.eq("uni.codigo", unidadE));
        dc.add(Restrictions.eq("pre.anyoPresupuesto", anio));
        dc.addOrder(Order.asc("id.codgioCuenta"));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return l;
        }
        return null;
    }

    @Override
    public void modificarCuenta(DetalleCuentas obj) {
        getHibernateTemplate().update(obj);
    }

    @Override
    public void insertarCuenta(DetalleCuentas obj) {
        getHibernateTemplate().save(obj);
    }
    
}
