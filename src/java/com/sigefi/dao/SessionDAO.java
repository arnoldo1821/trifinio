/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.UnidadesEjecutoras;
import com.sigefi.entity.Usuarios;

/**
 *
 * @author AAldemaro
 */
public interface SessionDAO {
    void updateCambioContra(Usuarios obj);
    Usuarios getElemtoUsuario(String usu );
    UnidadesEjecutoras getElemtoUnidadEjecutora(short idUnidadEjecutora);
}
