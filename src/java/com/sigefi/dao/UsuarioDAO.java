/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.Ciudades;
import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.Rol;
import com.sigefi.entity.UnidadesEjecutoras;
import com.sigefi.entity.Usuarios;
import java.util.List;

/**
 *
 * @author Administrador
 */
public interface UsuarioDAO {
    Usuarios ElementoUsuario(String usuario);
    String ElementoRol(Short idRol);
    void insertUsuario(Usuarios obj);
    void updateUsuario(Usuarios obj);
    void deleteUsuario(Usuarios obj);
    List<Rol> listRol();
    List<Usuarios> listUsuario();
    List<Ciudades> listCiudades();
    List<UnidadesEjecutoras> listUnidadesEjecutoras();
    List<Presupuestos> listAllPresupuestos();
}
