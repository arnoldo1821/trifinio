/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.CatalogoCuentasConta;
import com.sigefi.entity.CatalogosConta;
import com.sigefi.entity.Empresa;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface CuentasEmpresaDAO {
    Empresa consultarEmpresa(short idUnidadEje,int anio);
    CatalogoCuentasConta elementoCatalogoCuentas(String idCuenta);
    CatalogosConta elementoCatalogoConta(String idCuenta,Empresa empresa);
    void insertCatalogoCuenta(CatalogosConta obj);
    List<CatalogosConta> listCuentasAgregadas(Empresa empresa);
}
