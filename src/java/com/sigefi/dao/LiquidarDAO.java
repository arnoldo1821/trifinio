/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.CuentasPresupuesto;
import com.sigefi.entity.DetalleCuentas;
import com.sigefi.entity.Presupuestos;

/**
 *
 * @author AAldemaro
 */
public interface LiquidarDAO {
    Presupuestos elementoPre(short unidad,int anio);
    void insertPresupuesto(Presupuestos obj);
    void updatePresupuesto(Presupuestos obj);
    void insertCuentasPresupuesto(CuentasPresupuesto obj);
    void insertDetalleCuentas(DetalleCuentas obj);
}
