
package com.sigefi.dao;

import com.sigefi.entity.Contratos;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author SKAR
 */
@Transactional
@Service(value = "contratoDAO")
public class ContratoImplDAO extends HibernateDaoSupport implements ContratoDAO {
    
    @Autowired
    public ContratoImplDAO(SessionFactory sessionFactory){
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public void insert(Contratos obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public void update(Contratos obj) {
        getHibernateTemplate().update(obj);
    }

    @Override
    public void delete(Contratos obj) {
        getHibernateTemplate().delete(obj);
    }

    @Override
    public List selectAll() {
        DetachedCriteria dc = DetachedCriteria.forClass(Contratos.class);
        dc.addOrder(Order.asc("tipoContrato"));
        return getHibernateTemplate().findByCriteria(dc);
    }
    
}
