/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.CentroCosto;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Elmer
 */
@Transactional
@Service(value = "centroCostoImplDAO")
public class CentroCostoImplDAO extends HibernateDaoSupport implements CentroCostoDAO {

    @Autowired
    public CentroCostoImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
    
    @Override
    public void insertCentroCosto(CentroCosto obj) {
      getHibernateTemplate().save(obj);
    }

    @Override
    public void updateCentroCosto(CentroCosto obj) {
        getHibernateTemplate().update(obj);
    }

    @Override
    public void deleteCentroCosto(CentroCosto obj) {
        getHibernateTemplate().delete(obj);
    }

    @Override
    public List<CentroCosto> listCentroCostoAll() {
        Criteria c = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(CentroCosto.class);
        c.add(Restrictions.eq("activa", true));
        c.addOrder(Order.asc("centroCosto"));
        return c.list();
    }

    @Override
    public CentroCosto elementoCentroCosto(Integer idcentroCosto) {
       DetachedCriteria dc = DetachedCriteria.forClass(CentroCosto.class);
        dc.add(Restrictions.eq("idCentroCosto", idcentroCosto));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (CentroCosto) l.get(0);
        }    
        return null;
    }
    
}
