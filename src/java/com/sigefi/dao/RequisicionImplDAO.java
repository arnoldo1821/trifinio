/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.beans.RequisicionBean;
import com.sigefi.entity.CentroCosto;
import com.sigefi.entity.DetalleCuentas;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.EstadoRequisicion;
import com.sigefi.entity.PartesInteresadas;
import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.RequerimientoPagos;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrador
 */
@Transactional
@Service(value = "requisicionDAO")
public class RequisicionImplDAO extends HibernateDaoSupport implements RequisicionDAO {

    @Autowired
    public RequisicionImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public RequerimientoPagos elementoRequerimientoPagos(RequisicionBean obj) {
        DetachedCriteria dc = DetachedCriteria.forClass(RequerimientoPagos.class);
        dc.add(Restrictions.eq("codigoRequerimiento", obj.getCodigoRequerimiento()));
        dc.add(Restrictions.eq("unidadesEjecutoras.codigo", obj.getUnidadesEjecutoras()));
        dc.add(Restrictions.eq("anioPresupuesto", obj.getAnyoPresupuesto()));
        List l = getHibernateTemplate().findByCriteria(dc);
        if (l.size() > 0) {
            return (RequerimientoPagos) l.get(0);
        }

        return null;
    }

    @Override
    public void insertRequerimientoPagos(RequerimientoPagos obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public void updateRequerimientoPagos(RequerimientoPagos obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteRequerimientoPagos(RequerimientoPagos obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<RequerimientoPagos> allRequerimientoPagos() {
        List list = getHibernateTemplate().find("from RequerimientoPagos ");
        if (list.size() > 0) {
            return list;
        }
        return null;
    }

    @Override
    public void insertDetalleRequerimientoPagos(DetalleRequerimientoPago obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public List<PartesInteresadas> allProveedores() {
        return getHibernateTemplate().loadAll(PartesInteresadas.class);
    }

    @Override
    public List<DetalleCuentas> allDetalleCuentas(int idPresupuesto) {
        List list = getHibernateTemplate().find("from DetalleCuentas where id.codigoPresupuesto = ?", idPresupuesto);
        if (list.size() > 0) {
            return list;
        }
        return null;
    }

    @Override
    public Presupuestos elementoPresupuesto(RequisicionBean obj) {
        DetachedCriteria dc = DetachedCriteria.forClass(Presupuestos.class);
        dc.add(Restrictions.eq("anyoPresupuesto", obj.getAnyoPresupuesto()));
        dc.add(Restrictions.eq("unidadesEjecutoras.codigo", obj.getUnidadesEjecutoras()));
        List l = getHibernateTemplate().findByCriteria(dc);
        if (l.size() > 0) {
            return (Presupuestos) l.get(0);
        }

        return null;
    }

    @Override
    public List<DetalleRequerimientoPago> correlativo_Total_DetalleRequerimiento(int pkRequerimiento) {
        DetachedCriteria dc = DetachedCriteria.forClass(DetalleRequerimientoPago.class);
        dc.add(Restrictions.eq("id.fkPkRequerimiento2", pkRequerimiento));
        dc.addOrder(Order.asc("id.correlativo"));
        List l = getHibernateTemplate().findByCriteria(dc);
//        List<Object> listRe = new ArrayList<>();
        if (l.size() > 0) {
//            List<DetalleRequerimientoPago> listD = l;
//            BigDecimal sumTota = BigDecimal.ZERO;
//            for (DetalleRequerimientoPago obj : listD) {
//                if (obj.getMonto() != null) {
//                    sumTota = sumTota.add(obj.getMonto());
//                }
//            }                      
//            listRe.add((short) (((DetalleRequerimientoPago) l.get(0)).getId().getCorrelativo() + 1));
//            listRe.add(sumTota);
//            
            return l;
        } else {
//            listRe.add((short)1);
//            listRe.add(BigDecimal.ZERO);
//            return listRe;
            return null;
        }
    }

    @Override
    public void insertEstadoRequi(EstadoRequisicion obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public EstadoRequisicion elementoEstadoRequisicion(RequerimientoPagos req) {
        DetachedCriteria dc = DetachedCriteria.forClass(EstadoRequisicion.class);
        dc.add(Restrictions.eq("requerimientoPagos", req));
        dc.add(Restrictions.eq("control", true));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (EstadoRequisicion) l.get(0);
        }
        return null;
    }

    @Override
    public void updateEstadoRequi(EstadoRequisicion obj) {
        getHibernateTemplate().update(obj);
    }

    @Override
    public List<CentroCosto> allCentro() {
        return getHibernateTemplate().loadAll(CentroCosto.class);
    }

    @Override
    public UnidadesEjecutoras elementoUnidadEjecutora(RequisicionBean obj) {
        DetachedCriteria dc = DetachedCriteria.forClass(UnidadesEjecutoras.class);
        dc.add(Restrictions.eq("codigo", obj.getUnidadesEjecutoras()));
        List list = getHibernateTemplate().findByCriteria(dc);
        
        if(list.size()>0){
            return (UnidadesEjecutoras) list.get(0);
        }
        
        return null;
    }

    @Override
    public RequerimientoPagos elementoRequerimiento() {
        DetachedCriteria dc = DetachedCriteria.forClass(RequerimientoPagos.class);
        List list = getHibernateTemplate().findByCriteria(dc);
        
       if(list.size()>0){
            return (RequerimientoPagos) list.get(list.size()-1);
        }
        
        return null;
    }

    @Override
    public PartesInteresadas elementoProveedor(RequisicionBean obj) {
        DetachedCriteria dc = DetachedCriteria.forClass(PartesInteresadas.class);
        dc.add(Restrictions.eq("id.codigoProveedor", obj.getProveedor()));
        List list = getHibernateTemplate().findByCriteria(dc);
        
        if(list.size()>0){
            return (PartesInteresadas) list.get(0);
        }
        
        return null;
    }
    
    

}
