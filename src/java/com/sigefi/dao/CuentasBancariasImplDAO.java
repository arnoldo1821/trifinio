/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.beans.SeguimientoCuentasBancariasBean;
import com.sigefi.entity.CuentasBancarias;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.SegumientoCuentaBancaria;
//import com.sigefi.entity.SegumientoCuentaBancariaId;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "cuentasBancariasDAO")
public class CuentasBancariasImplDAO extends HibernateDaoSupport implements CuentasBancariasDAO{
    @Autowired
    public CuentasBancariasImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public List<CuentasBancarias> listAllCuentas() {
        DetachedCriteria dc  = DetachedCriteria.forClass(CuentasBancarias.class);
        dc.addOrder(Order.asc("codigoCuenta"));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return l;
        }
        return null;
    }

    @Override
    public CuentasBancarias elemntoCuentaBancaria(String codigoCuenta) {
        DetachedCriteria dc  = DetachedCriteria.forClass(CuentasBancarias.class);
        dc.add(Restrictions.eq("codigoCuenta", codigoCuenta));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (CuentasBancarias) l.get(0);
        }
        return null;
    }
    
    @Override
    public CuentasBancarias elemntoCuentaBancaria(SeguimientoCuentasBancariasBean obj) {
        DetachedCriteria dc  = DetachedCriteria.forClass(CuentasBancarias.class);
        //SegumientoCuentaBancariaId id = new SegumientoCuentaBancariaId();
        SegumientoCuentaBancaria cuenta = new SegumientoCuentaBancaria();
        //id.setCodigoCuenta(obj.getCodigoCuenta());
        //cuenta.setId(id);
        dc.add(Restrictions.eq("codigoCuenta",obj.getCodigoCuenta()));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (CuentasBancarias) l.get(0);
        }
        return null;
    }

    @Override
    public void insertCuentaBancaria(CuentasBancarias obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public void updateCuentaBancaria(CuentasBancarias obj) {
        getHibernateTemplate().update(obj);
    }

    @Override
    public void deleteCuentaBancaria(CuentasBancarias obj) {
        getHibernateTemplate().delete(obj);
    }

    @Override
    public List<DetalleRequerimientoPago> listRequerimientoPago() {
        DetachedCriteria dc = DetachedCriteria.forClass(DetalleRequerimientoPago.class);
        dc.createAlias("requerimientoPagos", "req");
        dc.createAlias("req.estadoRequisicions", "estado");
        dc.add(Restrictions.eq("estado.codigoEstado",(short) 7));
        dc.add(Restrictions.eq("estado.control", true));
        dc.addOrder(Order.desc("req.fechaPresentacion"));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return l;
        }
        return null;
    }

    @Override
    public DetalleRequerimientoPago elementoRequerimiento(SeguimientoCuentasBancariasBean obj) {
        DetachedCriteria dc = DetachedCriteria.forClass(DetalleRequerimientoPago.class);
        dc.createAlias("requerimientoPagos", "req");
        dc.add(Restrictions.eq("req.codigoRequerimiento", obj.getCodigoRequerimientoPago()));
        dc.createAlias("req.estadoRequisicions", "estado");
        dc.add(Restrictions.eq("estado.codigoEstado",(short) 7));
        dc.add(Restrictions.eq("estado.control", true));
        dc.addOrder(Order.desc("req.fechaPresentacion"));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (DetalleRequerimientoPago) l.get(0);
        }
        return null;
    }
    
}
