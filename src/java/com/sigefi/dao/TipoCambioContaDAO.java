/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.TipoCambioConta;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface TipoCambioContaDAO {
    TipoCambioConta elementoTipoCambioConta(Date fechaCambio);
    void insertTipoCambioConta(TipoCambioConta obj);
    void updateTipoCambioConta(TipoCambioConta obj);
    void deleteTipoCambioConta(TipoCambioConta obj);
    List<TipoCambioConta> listTipoCambioContaAll();
}
