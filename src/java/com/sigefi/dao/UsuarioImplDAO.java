/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.Ciudades;
import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.Usuarios;
import com.sigefi.entity.Rol;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrador
 */
@Transactional
@Service(value = "usuarioDAO")
public class UsuarioImplDAO extends HibernateDaoSupport implements UsuarioDAO{
    
    @Autowired
    public UsuarioImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
    
    @Override
    public Usuarios ElementoUsuario(String usuario) {
        List list = getHibernateTemplate().find("from Usuarios where usuario = ?",
                usuario);
        if (list.size() > 0) {
            return ((Usuarios) list.get(0));
        }
        return null;
    }
    
    @Override
    public String ElementoRol(Short idRol) {
        List list = getHibernateTemplate().find("from Rol where idRol = ?",
                idRol);
        if (list.size() > 0) {
            return ((Rol) list.get(0)).getDescripcionRol();
        }
        return null;
    }
    
    @Override
    public void insertUsuario(Usuarios obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public void updateUsuario(Usuarios obj) {
        getHibernateTemplate().update(obj);
    }

    @Override
    public void deleteUsuario(Usuarios obj) {
        getHibernateTemplate().delete(obj);
    }

    @Override
    public List<Rol> listRol() {
        return getHibernateTemplate().loadAll(Rol.class);
    }

    @Override
    public List<Usuarios> listUsuario() {
        return getHibernateTemplate().loadAll(Usuarios.class);
    }
    
    @Override
    public List<Ciudades> listCiudades() {
        return getHibernateTemplate().loadAll(Ciudades.class);
    }

    @Override
    public List<UnidadesEjecutoras> listUnidadesEjecutoras() {
        return getHibernateTemplate().loadAll(UnidadesEjecutoras.class);
    }

    @Override
    public List<Presupuestos> listAllPresupuestos() {
        return getHibernateTemplate().loadAll(Presupuestos.class);
    }
}
