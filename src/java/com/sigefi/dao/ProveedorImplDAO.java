/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.PartesInteresadas;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "proveedorDAO")
public class ProveedorImplDAO extends HibernateDaoSupport implements ProveedorDAO{
    
    @Autowired
    public ProveedorImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public void insertProveedor(PartesInteresadas obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public PartesInteresadas elementoProveedor(String codigoProveedor,String paisOrigen) {
        Criteria c = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(PartesInteresadas.class);
        
        c.add(Restrictions.eq("id.codigoProveedor", codigoProveedor));
        c.add(Restrictions.eq("id.paisOrigen", paisOrigen));
        
        if(c.list().size()>0){
            return (PartesInteresadas) c.list().get(0);
        }
        
        return null;
    }

    @Override
    public void updateProveedor(PartesInteresadas obj) {
    getHibernateTemplate().update(obj);
    }

    @Override
    public void deleteProveedor(PartesInteresadas obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PartesInteresadas> listProveedorAll() {
     Criteria c = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(PartesInteresadas.class);
     c.add(Restrictions.eq("activo", true));    
     c.addOrder(Order.asc("nombreProveedor"));
        return c.list();
        
    }
    
    
}
