/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.TipoCambio;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface TipoCambioDAO {
    TipoCambio elementoTipoCambio(Date fechaCambio);
    void insertTipoCambio(TipoCambio obj);
    void updateTipoCambio(TipoCambio obj);
    void deleteTipoCambio(TipoCambio obj);
    List<TipoCambio> listTipoCambioAll();
}
