/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.PartesInteresadas;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface ProveedorDAO {
    PartesInteresadas elementoProveedor(String codigoProveedor,String paisOrigen);
    void insertProveedor(PartesInteresadas obj);
    void updateProveedor(PartesInteresadas obj);
    void deleteProveedor(PartesInteresadas obj);
    List<PartesInteresadas> listProveedorAll();
}
