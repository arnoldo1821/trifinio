/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.beans.CajaChicaBean;
import com.sigefi.entity.DetalleCuentas;
import com.sigefi.entity.DetalleLiquidarCajaChica;
import com.sigefi.entity.LiquidarCajaChica;
import com.sigefi.entity.PartesInteresadas;
import com.sigefi.entity.Presupuestos;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "cajaChicaDAO")
public class CajaChicaImplDAO extends HibernateDaoSupport implements CajaChicaDAO{
    @Autowired
    public CajaChicaImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public Presupuestos elementoPresupuesto(CajaChicaBean obj) {
        DetachedCriteria dc = DetachedCriteria.forClass(Presupuestos.class);
        dc.add(Restrictions.eq("anyoPresupuesto", obj.getAnyoPresupuesto()));
        dc.add(Restrictions.eq("unidadesEjecutoras.codigo", obj.getUnidadesEjecutoras()));
        List l = getHibernateTemplate().findByCriteria(dc);
        if (l.size() > 0) {
            return (Presupuestos) l.get(0);
        }

        return null;
    }

    @Override
    public List<PartesInteresadas> allProveedores() {
        return getHibernateTemplate().loadAll(PartesInteresadas.class);
    }

    @Override
    public List<DetalleCuentas> allDetalleCuentas(int idPresupuesto) {
        DetachedCriteria dc = DetachedCriteria.forClass(DetalleCuentas.class);
        dc.add(Restrictions.eq("id.codigoPresupuesto", idPresupuesto));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return l;
        }
        return null;
    }

    @Override
    public LiquidarCajaChica elementoCajaChhica(CajaChicaBean obj) {
        DetachedCriteria dc = DetachedCriteria.forClass(LiquidarCajaChica.class);
        dc.add(Restrictions.eq("codigoRequerimientoCajaChica", obj.getCodigoRequerimiento()));
        dc.add(Restrictions.eq("unidadesEjecutoras.codigo", obj.getUnidadesEjecutoras()));
        dc.add(Restrictions.eq("anioPresupuesto", (short)obj.getAnyoPresupuesto()));
        List l = getHibernateTemplate().findByCriteria(dc);
        if (l.size() > 0) {
            return (LiquidarCajaChica) l.get(0);
        }
        return null;
    }

    @Override
    public void inserReqCajaChic(LiquidarCajaChica obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public List<DetalleLiquidarCajaChica> correlativo_Total_DetalleRequerimientoCaja(int pkRequerimientoCaja) {
        DetachedCriteria dc = DetachedCriteria.forClass(DetalleLiquidarCajaChica.class);
        dc.add(Restrictions.eq("id.fkRequeCaja", pkRequerimientoCaja));
        dc.addOrder(Order.asc("id.correlativo"));
        List l = getHibernateTemplate().findByCriteria(dc);
        if (l.size() > 0) {           
            return l;
        } else {
            return null;
        }
    }

    @Override
    public void insertDetalleRequerimientoCaja(DetalleLiquidarCajaChica obj) {
        getHibernateTemplate().save(obj);
    }
   
    
}
