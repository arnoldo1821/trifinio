/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

//import com.sigefi.entity.Bitacora;
import com.sigefi.entity.Bitacora;
import com.sigefi.entity.Rol;
import com.sigefi.entity.Usuarios;

/**
 *
 * @author alberto
 */
public interface LoginDAO {
    Usuarios validaLogin(Usuarios obj);
    Rol view(Short usuarioRol);
    void bitacora(Bitacora obj); 
}
