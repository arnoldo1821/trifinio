/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.Empresa;
import com.sigefi.entity.FirmasConta;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "firmasContaDAO")
public class FirmasContaImplDAO extends HibernateDaoSupport implements FirmasContaDAO{
    @Autowired
    public FirmasContaImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public void insertFirma(FirmasConta obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public void updateFirma(FirmasConta obj) {
        getHibernateTemplate().update(obj);
    }

    @Override
    public List<FirmasConta> listFirmas(Empresa empresaFirma) {
        DetachedCriteria dc = DetachedCriteria.forClass(FirmasConta.class);
        dc.add(Restrictions.eq("empresa", empresaFirma));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return l;
        }
        return null;
    }
    
    @Override
    public FirmasConta elementoFirmas(short empresaFirma) {
        DetachedCriteria dc = DetachedCriteria.forClass(FirmasConta.class);
        dc.add(Restrictions.eq("empresaFirmas", empresaFirma));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (FirmasConta) l.get(0);
        }
        return null;
    }

    @Override
    public Empresa elementoEmpresa(short idUnidadEjecutora,int anio) {
        DetachedCriteria dc = DetachedCriteria.forClass(Empresa.class);
        dc.add(Restrictions.eq("unidadesEjecutoras.codigo", idUnidadEjecutora));
        dc.add(Restrictions.eq("anyoContable", anio));
        
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (Empresa) l.get(0);
        }
        return null;
    }
    
}
