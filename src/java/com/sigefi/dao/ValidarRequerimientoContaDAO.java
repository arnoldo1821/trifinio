/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.CatalogoCuentasConta;
import com.sigefi.entity.CatalogosConta;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.DocumentosRequerimientos;
import com.sigefi.entity.Empresa;
import com.sigefi.entity.EstadoRequisicion;
import com.sigefi.entity.NoConformidades;
import com.sigefi.entity.RequerimientoPagos;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface ValidarRequerimientoContaDAO {
    List<RequerimientoPagos> listAllRequePago();
    List<DetalleRequerimientoPago> listDetalleRequePagoSelec(int incre_req_pk);
    List<DocumentosRequerimientos> listDocRequePagoSelec(int incre_req_pk);
    List<NoConformidades> listAllErrores();
    void insertEstadoRequisicion(EstadoRequisicion obj);
    
    void deleteEstadoRequerimiento(EstadoRequisicion obj);
    void deleteDocumentoRequerimiento(DocumentosRequerimientos obj);
    void deleteDetalleRequerimiento(DetalleRequerimientoPago obj);
    void deleteRequerimiento(RequerimientoPagos obj);
    
    Empresa elementoEmpresa(UnidadesEjecutoras unidadEjecutora,int anio);
    List<CatalogosConta> listCataloCuentaConta(Empresa emp,short anio);
   
}
