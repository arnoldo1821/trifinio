/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.FuenteFinanciamiento;
import java.util.List;

/**
 *
 * @author Elmer
 */
public interface FuenteFinanciamientoDAO {
    FuenteFinanciamiento elementoFuenteFinanciamiento(Integer codigo); 
    void insertFuenteFinanciamiento(FuenteFinanciamiento obj);
    void updateFuenteFinanciamiento(FuenteFinanciamiento obj);
    void deleteFuenteFinanciamiento(FuenteFinanciamiento obj);
    List<FuenteFinanciamiento> listFuenteFinanciamientoAll();
    List<FuenteFinanciamiento> listAllFuenteFinanciamiento();
}
