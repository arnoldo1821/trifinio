
package com.sigefi.dao;

import com.sigefi.beans.ModeloContratoBean;
import com.sigefi.entity.ClausulasModeloContrato;
import com.sigefi.entity.ModeloContrato;
import java.util.List;

/**
 *
 * @author SKAR
 */
public interface ClausulasModeloContratoDAO {
    
    void insert(ClausulasModeloContrato obj);
    void update(ClausulasModeloContrato obj);
    void delete(ClausulasModeloContrato obj);
    List<ClausulasModeloContrato> selectAll();
    List<ClausulasModeloContrato> selectAll(ModeloContrato obj);
    ClausulasModeloContrato select(ModeloContratoBean obj);
    
}
