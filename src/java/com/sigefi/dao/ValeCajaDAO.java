/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.ManualClasificatorio;
import com.sigefi.entity.ValeCaja;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Aldemaro Gonzalez
 */
public interface ValeCajaDAO {
    List<ManualClasificatorio> listManualClasiAll();
    ValeCaja elementoValeCaja(Date fecha,int numero);
    void insertValeCaja(ValeCaja obj);
    void updateValeCaja(ValeCaja obj);
}
