/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.ModeloContrato;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Oscar
 */
@Transactional
@Service(value = "modeloContratoDAO")
public class ModeloContratoImplDAO extends HibernateDaoSupport implements ModeloContratoDAO{

    @Autowired
    public ModeloContratoImplDAO(SessionFactory sessionFactory){
        super.setSessionFactory(sessionFactory);
    }
    
    @Override
    public void insert(ModeloContrato obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public List<ModeloContrato> selectAll() {
        return getHibernateTemplate().loadAll(ModeloContrato.class);
    }
    
    @Override
    public List selectAll(ModeloContrato obj) {
        DetachedCriteria dc = DetachedCriteria.forClass(ModeloContrato.class);
        dc.add(Restrictions.eq("codigoContrato", obj.getCodigoContrato()));
        return getHibernateTemplate().findByCriteria(dc);
    }
    
}
