/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.DocumentosRequerimientos;
import com.sigefi.entity.FirmasAutorizadas;
import com.sigefi.entity.FirmasAutorizadasProyecto;
import com.sigefi.entity.RequerimientoPagos;
import com.sigefi.entity.Usuarios;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "archivosRequerimientoDAO")
public class ArchivosRequerimientoImplDAO extends HibernateDaoSupport implements ArchivosRequerimientoDAO{
    @Autowired
    public ArchivosRequerimientoImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public List<RequerimientoPagos> listAllReqUsuario(String usuario) {
        DetachedCriteria dc = DetachedCriteria.forClass(RequerimientoPagos.class);
        dc.add(Restrictions.eq("usuario", usuario));
        dc.createAlias("estadoRequisicions", "estado");
        dc.add(Restrictions.eq("estado.codigoEstado",(short) 1));
        dc.add(Restrictions.eq("estado.control", true));
        
        List l = getHibernateTemplate().findByCriteria(dc);
        
        if(l.size()>0){
            return l;
        }
        
        return null;
    }

    @Override
    public DocumentosRequerimientos elementoDocReq(String tipoDoc, int incre_reque) {
        DetachedCriteria dc = DetachedCriteria.forClass(DocumentosRequerimientos.class);
        
        dc.add(Restrictions.eq("id.fkRequerimiento1",incre_reque));
        dc.add(Restrictions.eq("id.tipoDocumento",tipoDoc));
        
        List l = getHibernateTemplate().findByCriteria(dc);
        
        if(l.size()>0){
            return (DocumentosRequerimientos) l.get(0);
        }
        
        return null;
    }

    @Override
    public void insertDocReque(DocumentosRequerimientos obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public List<DocumentosRequerimientos> listAllDocReqUsuario(int pkIncreReque) {
        DetachedCriteria dc = DetachedCriteria.forClass(DocumentosRequerimientos.class);
        dc.add(Restrictions.eq("id.fkRequerimiento1", pkIncreReque));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return l;
        }
        return null;
    }

    @Override
    public List<String> listCorreoUsuarios(short idUnidadEjecutora) {
        DetachedCriteria dc = DetachedCriteria.forClass(FirmasAutorizadas.class);   
        dc.add(Restrictions.eq("unidadesEjecutoras.codigo", idUnidadEjecutora));
        List l = getHibernateTemplate().findByCriteria(dc);
        List<String> listCorreo = new ArrayList<>();
        
        if(l.size()>0){
            List<FirmasAutorizadas> lfa = l;
            for(FirmasAutorizadas obj1: lfa){
                DetachedCriteria dcU = DetachedCriteria.forClass(Usuarios.class);
                dcU.add(Restrictions.eq("usuario", obj1.getId().getUsuario()));
                List lu = getHibernateTemplate().findByCriteria(dcU);
                if(lu.size()>0){
                    listCorreo.add(((Usuarios)lu.get(0)).getEMail());
                }
            }
        }
        
        DetachedCriteria dc2 = DetachedCriteria.forClass(FirmasAutorizadasProyecto.class);   
        dc2.add(Restrictions.eq("unidadesEjecutoras.codigo", idUnidadEjecutora));
        List l2 = getHibernateTemplate().findByCriteria(dc2);
                
        if(l2.size()>0){
            List<FirmasAutorizadasProyecto> lfa2 = l2;
            for(FirmasAutorizadasProyecto obj2: lfa2){
                DetachedCriteria dcU2 = DetachedCriteria.forClass(Usuarios.class);
                dcU2.add(Restrictions.eq("usuario", obj2.getId().getUsuario()));
                List lu2 = getHibernateTemplate().findByCriteria(dcU2);
                if(lu2.size()>0){
                    listCorreo.add(((Usuarios)lu2.get(0)).getEMail());
                }
            }
        }
        
        if(listCorreo != null){
            if(listCorreo.size()>0){
                return listCorreo;
            }
        }
        return null;
        
    }

    @Override
    public List<DetalleRequerimientoPago> listAllDetalleRequerimiento(String usuario) {
        DetachedCriteria dc = DetachedCriteria.forClass(DetalleRequerimientoPago.class);
        dc.createAlias("requerimientoPagos", "req");
        dc.add(Restrictions.eq("req.usuario", usuario));
        dc.createAlias("req.estadoRequisicions", "estado");
        dc.add(Restrictions.eq("estado.codigoEstado",(short) 1));
        dc.add(Restrictions.eq("estado.control", true));
        
        List l = getHibernateTemplate().findByCriteria(dc);
        
        if(l.size()>0){
            return l;
        }
        
        return null;
    }
    
    
}
