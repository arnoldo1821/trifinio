
package com.sigefi.dao;

import com.sigefi.entity.ModeloContrato;
import java.util.List;

/**
 *
 * @author Oscar
 */
public interface ModeloContratoDAO {
    
    void insert(ModeloContrato obj);
    List<ModeloContrato> selectAll();
    List<ModeloContrato> selectAll(ModeloContrato obj);
    
}
