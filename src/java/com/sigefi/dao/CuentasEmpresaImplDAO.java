/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.CatalogoCuentasConta;
import com.sigefi.entity.CatalogosConta;
import com.sigefi.entity.Empresa;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "cuentasEmpresaDAO")
public class CuentasEmpresaImplDAO extends HibernateDaoSupport implements CuentasEmpresaDAO{
    @Autowired
    public CuentasEmpresaImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public Empresa consultarEmpresa(short idUnidadEje, int anio) {
        DetachedCriteria dc = DetachedCriteria.forClass(Empresa.class);
        dc.add(Restrictions.eq("unidadesEjecutoras.codigo", idUnidadEje));
        dc.add(Restrictions.eq("anyoContable", anio));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (Empresa) l.get(0);
        }
        return null;
    }

    @Override
    public CatalogoCuentasConta elementoCatalogoCuentas(String idCuenta) {
        List list = getHibernateTemplate().find("from CatalogoCuentasConta where codCatalocuenta = ?", idCuenta);
        if(list.size()>0){
            return (CatalogoCuentasConta) list.get(0);
        }
        return null;
    }

    @Override
    public void insertCatalogoCuenta(CatalogosConta obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public List<CatalogosConta> listCuentasAgregadas(Empresa empresa) {
        DetachedCriteria dc = DetachedCriteria.forClass(CatalogosConta.class);
        dc.add(Restrictions.eq("empresa", empresa));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return l;
        }
        return null;
    }

    @Override
    public CatalogosConta elementoCatalogoConta(String idCuenta, Empresa empresa) {
        DetachedCriteria dc = DetachedCriteria.forClass(CatalogosConta.class);
        dc.add(Restrictions.eq("codCatalogo", idCuenta));
        dc.add(Restrictions.eq("empresa", empresa));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (CatalogosConta) l.get(0);
        }
        return null;
    }
    
}
