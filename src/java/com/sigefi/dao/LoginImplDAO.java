/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.Bitacora;
import com.sigefi.entity.Rol;
import com.sigefi.entity.Usuarios;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author alberto
 */
@Transactional
@Service(value = "loginDAO")
public class LoginImplDAO extends HibernateDaoSupport implements  LoginDAO{

    @Autowired
    public LoginImplDAO(SessionFactory sessionFactory){
        super.setSessionFactory(sessionFactory);
    }
    
    @Override
    public Usuarios validaLogin(Usuarios obj) {
        List list = getHibernateTemplate().find("from Usuarios where usuario = ? and clave = ?",
                obj.getUsuario(), obj.getClave());
        if (list.size() > 0) {
            return ((Usuarios) list.get(0));
        }
        return null;
    }
    
    @Override
    public Rol view(Short usuarioRol) {
        List list = getHibernateTemplate().find("from Rol where idRol = ? ",usuarioRol);
        if (list.size() > 0) {
            return ((Rol) list.get(0));
        }
        return null;
    }

    @Override
    public void bitacora(Bitacora obj) {
        getHibernateTemplate().save(obj);
    }
    
}
