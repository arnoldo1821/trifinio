/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.UnidadesEjecutoras;
import com.sigefi.entity.Usuarios;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "sessionDao")
public class SessionImplDAO extends HibernateDaoSupport implements  SessionDAO{

    @Autowired
    public SessionImplDAO(SessionFactory sessionFactory){
        super.setSessionFactory(sessionFactory);
    }
    
    @Override
    public void updateCambioContra(Usuarios obj) {
        getHibernateTemplate().update(obj);
    }

    @Override
    public Usuarios getElemtoUsuario(String usu) {
        List list = getHibernateTemplate().find("from Usuarios where usuario = ?", 
            usu);
        if(list.size() > 0) {
            return (Usuarios) list.get(0);
        }
        return null;
    }

    @Override
    public UnidadesEjecutoras getElemtoUnidadEjecutora(short idUnidadEjecutora) {
        List list = getHibernateTemplate().find("from UnidadesEjecutoras where codigo = ?", 
            idUnidadEjecutora);
        if(list.size() > 0) {
            return (UnidadesEjecutoras) list.get(0);
        }
        return null;
    }
    
}
