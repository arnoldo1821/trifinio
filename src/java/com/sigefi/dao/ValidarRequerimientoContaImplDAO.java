/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.CatalogoCuentasConta;
import com.sigefi.entity.CatalogosConta;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.DocumentosRequerimientos;
import com.sigefi.entity.Empresa;
import com.sigefi.entity.EstadoRequisicion;
import com.sigefi.entity.NoConformidades;
import com.sigefi.entity.RequerimientoPagos;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "validarRequerimientoContaDAO")
public class ValidarRequerimientoContaImplDAO extends HibernateDaoSupport implements ValidarRequerimientoContaDAO {

    @Autowired
    public ValidarRequerimientoContaImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public List<RequerimientoPagos> listAllRequePago() {
        DetachedCriteria dc = DetachedCriteria.forClass(RequerimientoPagos.class);
        dc.createAlias("estadoRequisicions", "estado");
        dc.add(Restrictions.eq("estado.codigoEstado", (short)5));
        dc.add(Restrictions.eq("estado.control", true));
        dc.addOrder(Order.desc("fechaPresentacion"));
        List l = getHibernateTemplate().findByCriteria(dc);
        if (l.size() > 0) {
            return l;
        }
        return null;
    }

    @Override
    public List<DetalleRequerimientoPago> listDetalleRequePagoSelec(int incre_req_pk) {
        DetachedCriteria dc = DetachedCriteria.forClass(DetalleRequerimientoPago.class);
        dc.add(Restrictions.eq("id.fkPkRequerimiento2", incre_req_pk));
        List l = getHibernateTemplate().findByCriteria(dc);
        if (l.size() > 0) {
            return l;
        }
        return null;
    }

    @Override
    public List<DocumentosRequerimientos> listDocRequePagoSelec(int incre_req_pk) {
        DetachedCriteria dc = DetachedCriteria.forClass(DocumentosRequerimientos.class);
        dc.add(Restrictions.eq("id.fkRequerimiento1", incre_req_pk));
        List l = getHibernateTemplate().findByCriteria(dc);
        if (l.size() > 0) {
            return l;
        }
        return null;
    }

    @Override
    public List<NoConformidades> listAllErrores() {
        DetachedCriteria dc = DetachedCriteria.forClass(NoConformidades.class);
        List l = getHibernateTemplate().findByCriteria(dc);
        if (l.size() > 0) {
            return l;
        }

        return null;
    }

    @Override
    public void insertEstadoRequisicion(EstadoRequisicion obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public void deleteEstadoRequerimiento(EstadoRequisicion obj) {
        getHibernateTemplate().delete(obj);
    }

    @Override
    public void deleteDocumentoRequerimiento(DocumentosRequerimientos obj) {
        getHibernateTemplate().delete(obj);
    }

    @Override
    public void deleteDetalleRequerimiento(DetalleRequerimientoPago obj) {
        getHibernateTemplate().delete(obj);
    }

    @Override
    public void deleteRequerimiento(RequerimientoPagos obj) {
        getHibernateTemplate().delete(obj);
    }

    @Override
    public List<CatalogosConta> listCataloCuentaConta(Empresa emp, short anio) {        
        DetachedCriteria dc2 = DetachedCriteria.forClass(CatalogosConta.class);
        dc2.add(Restrictions.eq("empresa", emp));
        dc2.add(Restrictions.eq("anioEjecucion", anio));
        List l2 = getHibernateTemplate().findByCriteria(dc2);
        if(l2.size()>0){
            return l2;
        }
        return null;
    }

    @Override
    public Empresa elementoEmpresa(UnidadesEjecutoras unidadEjecutora, int anio) {
        DetachedCriteria dc = DetachedCriteria.forClass(Empresa.class);
        dc.add(Restrictions.eq("unidadesEjecutoras", unidadEjecutora));
        dc.add(Restrictions.eq("anyoContable", anio));
        List l = getHibernateTemplate().findByCriteria(dc);
        
        if(l.size()>0){
            return (Empresa) l.get(0);
        }
        return null;
    }

}
