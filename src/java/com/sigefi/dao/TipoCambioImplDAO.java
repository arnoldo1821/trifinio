/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.TipoCambio;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "tipoCambioImplDAO")
public class TipoCambioImplDAO extends HibernateDaoSupport implements TipoCambioDAO{
    
    @Autowired
    public TipoCambioImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

   @Override
    public TipoCambio elementoTipoCambio(Date fechaCambio) {
       List list = getHibernateTemplate().find("from TipoCambio WHERE fechaCambio = ?",fechaCambio);
        if(list.size() >0){
            return (TipoCambio) list.get(0);
        }
        
        return null;
    }

    @Override
    public void insertTipoCambio(TipoCambio obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public void updateTipoCambio(TipoCambio obj) {
        getHibernateTemplate().update(obj);
    }

    @Override
    public void deleteTipoCambio(TipoCambio obj) {
        getHibernateTemplate().delete(obj);
    }

    @Override
    public List<TipoCambio> listTipoCambioAll() {
        Criteria c = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(TipoCambio.class);
       // c.addOrder(Order.asc("fechaCambio"));
         c.addOrder(Order.desc("fechaCambio"));
        return c.list();
        //return getHibernateTemplate().loadAll(TipoCambio.class);
    }
    
    
}
