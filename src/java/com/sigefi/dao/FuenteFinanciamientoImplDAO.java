/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.FuenteFinanciamiento;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Elmer
 */
@Transactional
@Service(value = "fuentefinanciamientoImplDAO")
public class FuenteFinanciamientoImplDAO extends HibernateDaoSupport implements FuenteFinanciamientoDAO {

    @Autowired
    public FuenteFinanciamientoImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
    
    @Override
    public void insertFuenteFinanciamiento(FuenteFinanciamiento obj) {
      getHibernateTemplate().save(obj);
    }

    @Override
    public void updateFuenteFinanciamiento(FuenteFinanciamiento obj) {
        getHibernateTemplate().update(obj);
    }

    @Override
    public void deleteFuenteFinanciamiento(FuenteFinanciamiento obj) {
        getHibernateTemplate().delete(obj);
    }

    @Override
    public List<FuenteFinanciamiento> listFuenteFinanciamientoAll() {
        Criteria c = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(FuenteFinanciamiento.class);
        c.addOrder(Order.asc("codigo"));
        return c.list();
    }

    @Override
    public FuenteFinanciamiento elementoFuenteFinanciamiento(Integer codigo) {
       DetachedCriteria dc = DetachedCriteria.forClass(FuenteFinanciamiento.class);
        dc.add(Restrictions.eq("codigo",codigo));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (FuenteFinanciamiento) l.get(0);
        }    
        return null;
    }

    @Override
    public List<FuenteFinanciamiento> listAllFuenteFinanciamiento() {
        DetachedCriteria dc  = DetachedCriteria.forClass(FuenteFinanciamiento.class);
        dc.addOrder(Order.asc("codigo"));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return l;
        }
        return null;
    }
    
}
