/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.Ciudades;
import com.sigefi.entity.Usuarios;
import com.sigefi.entity.Rol;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrador
 */
@Transactional
@Service(value = "ejemploDAO")
public class EjemploImplDAO extends HibernateDaoSupport implements EjemploDAO{
    
    @Autowired
    public EjemploImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
    
    
}
