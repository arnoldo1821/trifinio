/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.Empresa;
import com.sigefi.entity.FirmasConta;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface FirmasContaDAO {
    Empresa elementoEmpresa(short idUnidadEjecutora,int anio);
    void insertFirma(FirmasConta obj);
    void updateFirma(FirmasConta obj);
    List<FirmasConta> listFirmas(Empresa empresaFirma);
    FirmasConta elementoFirmas(short empresaFirma);
    
}
