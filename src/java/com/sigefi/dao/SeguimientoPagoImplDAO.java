/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.RequerimientoPagos;
import com.sigefi.entity.SeguimientoPagos;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "seguimientoPagoDAO")
public class SeguimientoPagoImplDAO extends HibernateDaoSupport implements SeguimientoPagoDAO{
    @Autowired
    public SeguimientoPagoImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public int estadoRequerimiento(String usuario) {
        
        return 0;
    }

    @Override
    public List<RequerimientoPagos> listReqUsuario(String usuario) {
        DetachedCriteria dc = DetachedCriteria.forClass(RequerimientoPagos.class);
        dc.add(Restrictions.eq("usuario", usuario));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return l;
        }
        return null;
    }
    
}
