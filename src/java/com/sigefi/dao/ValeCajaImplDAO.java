/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.ManualClasificatorio;
import com.sigefi.entity.ValeCaja;
import java.util.Date;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.feed.RssChannelHttpMessageConverter;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Aldemaro Gonzalez
 */
@Transactional
@Service(value = "valeCajaDAO")
public class ValeCajaImplDAO extends HibernateDaoSupport implements ValeCajaDAO{
    @Autowired
    public ValeCajaImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public List<ManualClasificatorio> listManualClasiAll() {
        DetachedCriteria dc = DetachedCriteria.forClass(ManualClasificatorio.class);
        dc.add(Restrictions.eq("esDetalle", true));
        List l = getHibernateTemplate().findByCriteria(dc);
        return l;
    }

    @Override
    public ValeCaja elementoValeCaja(Date fecha, int numero) {
        DetachedCriteria dc = DetachedCriteria.forClass(ValeCaja.class);
        dc.add(Restrictions.eq("fecha", fecha));
        dc.add(Restrictions.eq("numero", numero));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (ValeCaja) l.get(0);
        }
        return null;
    }

    @Override
    public void insertValeCaja(ValeCaja obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public void updateValeCaja(ValeCaja obj) {
        getHibernateTemplate().update(obj);
    }
    
}
