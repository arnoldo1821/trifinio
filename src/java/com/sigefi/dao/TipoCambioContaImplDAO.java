/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.TipoCambioConta;
import java.util.Date;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "tipoCambioContaImplDAO")
public class TipoCambioContaImplDAO extends HibernateDaoSupport implements TipoCambioContaDAO{
    
    @Autowired
    public TipoCambioContaImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public TipoCambioConta elementoTipoCambioConta(Date fechaCambio) {
        List list = getHibernateTemplate().find("from TipoCambioConta WHERE fechaCambio = ?",fechaCambio);
        if(list.size() >0){
            return (TipoCambioConta) list.get(0);
        }
        
        return null;
    }

    @Override
    public void insertTipoCambioConta(TipoCambioConta obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public void updateTipoCambioConta(TipoCambioConta obj) {
        getHibernateTemplate().update(obj);
    }

    @Override
    public void deleteTipoCambioConta(TipoCambioConta obj) {
        getHibernateTemplate().delete(obj);
    }

    @Override
    public List<TipoCambioConta> listTipoCambioContaAll() {
        return getHibernateTemplate().loadAll(TipoCambioConta.class);
    }
    
    
}
