/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.CatalogoCuentas;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface CuentasDAO {
    CatalogoCuentas elementoCatalogoCuenta(int idCuenta);
    void insertCuenta(CatalogoCuentas obj);
    void updateCuenta(CatalogoCuentas obj);
    List<CatalogoCuentas> listNodoCatalogoCuentas(int numNivel);
    int ultimoRegistroCatalogoCuenta();
    int ultimoRegistroNivel(int numNivelIdCuenta);
    
    List<CatalogoCuentas> listAllCatalogoCuenta(short unidad);
}
