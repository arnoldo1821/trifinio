/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.beans.SeguimientoCuentasBancariasBean;
import com.sigefi.entity.CuentasBancarias;
import com.sigefi.entity.DetalleRequerimientoPago;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface CuentasBancariasDAO {
    List<CuentasBancarias> listAllCuentas();
    List<DetalleRequerimientoPago> listRequerimientoPago();
    CuentasBancarias elemntoCuentaBancaria(String codigoCuenta);
    CuentasBancarias elemntoCuentaBancaria(SeguimientoCuentasBancariasBean obj);
    DetalleRequerimientoPago elementoRequerimiento(SeguimientoCuentasBancariasBean obj);
    void insertCuentaBancaria(CuentasBancarias obj);
    void updateCuentaBancaria(CuentasBancarias obj);
    void deleteCuentaBancaria(CuentasBancarias obj);
}
