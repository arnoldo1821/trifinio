/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.beans.RequisicionBean;
import com.sigefi.entity.CentroCosto;
import com.sigefi.entity.Ciudades;
import com.sigefi.entity.CuentasPresupuesto;
import com.sigefi.entity.DetalleCuentas;
import com.sigefi.entity.DetalleCuentasId;
import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.EstadoRequisicion;
import com.sigefi.entity.PartesInteresadas;
import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.Rol;
import com.sigefi.entity.RequerimientoPagos;
import com.sigefi.entity.UnidadesEjecutoras;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Administrador
 */
public interface RequisicionDAO {
    
    void insertDetalleRequerimientoPagos(DetalleRequerimientoPago obj);
    void updateRequerimientoPagos(RequerimientoPagos obj);
    void deleteRequerimientoPagos(RequerimientoPagos obj);
    
    UnidadesEjecutoras elementoUnidadEjecutora(RequisicionBean obj);
    PartesInteresadas elementoProveedor(RequisicionBean obj);
    RequerimientoPagos elementoRequerimiento();
    
    List<RequerimientoPagos> allRequerimientoPagos();
    List<PartesInteresadas> allProveedores();
    List<CentroCosto> allCentro();
    List<DetalleCuentas> allDetalleCuentas(int idPresupuesto);
    
    Presupuestos elementoPresupuesto(RequisicionBean obj);
    RequerimientoPagos elementoRequerimientoPagos(RequisicionBean obj);
    void insertRequerimientoPagos(RequerimientoPagos obj);
//    short correlativoDetalleRequerimiento(String codigoRequerimiento);   
//    List<Object> correlativo_Total_DetalleRequerimiento(int pkRequerimiento);   
    List<DetalleRequerimientoPago> correlativo_Total_DetalleRequerimiento(int pkRequerimiento);   
    
    EstadoRequisicion elementoEstadoRequisicion(RequerimientoPagos req);
    void insertEstadoRequi(EstadoRequisicion obj);
    void updateEstadoRequi(EstadoRequisicion obj);
}
