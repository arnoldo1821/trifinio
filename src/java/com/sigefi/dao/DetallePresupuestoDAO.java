/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.beans.DetallePresupuestoBean;
import com.sigefi.entity.CuentasPresupuesto;
import com.sigefi.entity.DetalleCuentas;
import com.sigefi.entity.Presupuestos;
import java.util.List;
import java.util.Set;

/**
 *
 * @author AAldemaro
 */
public interface DetallePresupuestoDAO {
    List<CuentasPresupuesto> listCuentasSelec(DetallePresupuestoBean obj);
    DetalleCuentas consultarDetalleCuenta(int codigoPresupuesto,int codigoCuenta);
    void insertDetalleCuenta(DetalleCuentas obj);
    void insertarDetalleCuenta(DetalleCuentas obj);
    void updateDetalleCuenta(DetalleCuentas obj);
    List<CuentasPresupuesto> listAllCuentasPresupuesto(DetallePresupuestoBean obj);
    DetalleCuentas listAllDetalleCuentas(DetallePresupuestoBean obj,int presupuesto,int cuenta);
    Presupuestos elementoPresupuesto(DetallePresupuestoBean obj);
    Set<DetalleCuentas> consultarDetallePresupuesto(DetallePresupuestoBean obj);
}
