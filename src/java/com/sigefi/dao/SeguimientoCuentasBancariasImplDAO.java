/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.SegumientoCuentaBancaria;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Elmer
 */
@Transactional
@Service(value = "seguimientoCuentasBancariasDAO")
public class SeguimientoCuentasBancariasImplDAO extends HibernateDaoSupport implements SeguimientoCuentasBancariasDAO{
    @Autowired
    public SeguimientoCuentasBancariasImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public List<SegumientoCuentaBancaria> listAllSeguimientoCuentas(String codigoCuenta, Date inicio, Date fin) {
        /*select * from tesoreria.segumiento_cuenta_bancaria where codigo_cuenta='345523672-2342-123-1' 
        and fecha_transaccion between '2016-08-27' and '2016-12-22'
        order by fecha_transaccion*/

        String str="from SegumientoCuentaBancaria where codigoCuenta=? and fechaTransaccion between ? and ?";
        List<Object> queryParam=new ArrayList<Object>();
        queryParam.add(codigoCuenta); //queryParam.add(yourList.get(0));
        queryParam.add(inicio); //queryParam.add(yourList.get(1));
        queryParam.add(fin); //queryParam.add(yourList.get(2));
        DetachedCriteria dc  = DetachedCriteria.forClass(SegumientoCuentaBancaria.class);
        dc.add(Restrictions.eq("codigoCuenta", codigoCuenta));
        List l = getHibernateTemplate().find(str,queryParam.toArray());
        if(l.size()>0){
            return l;
        }
        return null;
    }

    @Override
    public SegumientoCuentaBancaria elemntoSeguimientoCuentaBancaria(String codigoCuenta) {
        DetachedCriteria dc  = DetachedCriteria.forClass(SegumientoCuentaBancaria.class);
        dc.add(Restrictions.eq("codigoCuenta", codigoCuenta));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (SegumientoCuentaBancaria) l.get(0);
        }
        return null;
    }

    @Override
    public void insertMovimiento(SegumientoCuentaBancaria obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public void updateMovimiento(SegumientoCuentaBancaria obj) {
        getHibernateTemplate().update(obj);
    }

    @Override
    public void deleteMovimiento(SegumientoCuentaBancaria obj) {
        getHibernateTemplate().delete(obj);
    }
    
}
