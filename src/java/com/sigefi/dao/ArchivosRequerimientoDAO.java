/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.DocumentosRequerimientos;
import com.sigefi.entity.RequerimientoPagos;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface ArchivosRequerimientoDAO {
    List<RequerimientoPagos> listAllReqUsuario(String usuario);
    List<DetalleRequerimientoPago> listAllDetalleRequerimiento(String usuario);
    DocumentosRequerimientos elementoDocReq(String tipoDoc,int incre_reque);
    void insertDocReque(DocumentosRequerimientos obj);
    List<DocumentosRequerimientos> listAllDocReqUsuario(int pkIncreReque);
    
    List<String> listCorreoUsuarios(short idUnidadEjecutora);
}
