/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.FirmasAutorizadas;
import com.sigefi.entity.FirmasAutorizadasProyecto;
import com.sigefi.entity.UnidadesEjecutoras;
import com.sigefi.entity.Usuarios;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface FirmasAutorizadasDAO {
    List<Usuarios> listAllUsuarios();
    List<FirmasAutorizadas> listAllFirmasAutorizadas();
    List<FirmasAutorizadasProyecto> listAllFirmasAutorizadasProyecto();
    
    UnidadesEjecutoras elementoUnidadEjecutora(short idUnidadEjecutora);
    
    FirmasAutorizadas elementoFirmaAutorizadas(String usuario,String cargo);
    FirmasAutorizadasProyecto elementoFirmaAutorizadasProyecto(String usuario,String cargo);
    void insertFirmaAutorizada(FirmasAutorizadas obj);
    void insertFirmaAutorizadaProyecto(FirmasAutorizadasProyecto obj);
    void deleteFirmaAutorizadaProyecto(FirmasAutorizadasProyecto obj);
}
