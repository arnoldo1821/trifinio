/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.beans.CajaChicaBean;
import com.sigefi.entity.DetalleCuentas;
import com.sigefi.entity.DetalleLiquidarCajaChica;
import com.sigefi.entity.LiquidarCajaChica;
import com.sigefi.entity.PartesInteresadas;
import com.sigefi.entity.Presupuestos;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface CajaChicaDAO {
    List<PartesInteresadas> allProveedores();
    Presupuestos elementoPresupuesto(CajaChicaBean obj);
    List<DetalleCuentas> allDetalleCuentas(int idPresupuesto);
    LiquidarCajaChica elementoCajaChhica(CajaChicaBean obj);
    void inserReqCajaChic(LiquidarCajaChica obj);
    List<DetalleLiquidarCajaChica> correlativo_Total_DetalleRequerimientoCaja(int pkRequerimientoCaja); 
    void insertDetalleRequerimientoCaja(DetalleLiquidarCajaChica obj);
}
