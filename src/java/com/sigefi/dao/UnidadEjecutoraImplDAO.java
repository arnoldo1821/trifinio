/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.Ciudades;
import com.sigefi.entity.FirmasAutorizadas;
import com.sigefi.entity.FirmasAutorizadasProyecto;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "unidadEjecutoraDAO")
public class UnidadEjecutoraImplDAO extends HibernateDaoSupport implements UnidadEjecutoraDAO{
    
    @Autowired
    public UnidadEjecutoraImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public void insertUnidad(UnidadesEjecutoras obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public List<Ciudades> listAllCiudades() {
        return getHibernateTemplate().loadAll(Ciudades.class);
    }

    @Override
    public List<UnidadesEjecutoras> listAllUnidadEjecutoras() {
        Criteria c = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(UnidadesEjecutoras.class);
        c.addOrder(Order.asc("nombreUnidad"));
        return c.list();
    }
    
    
    
    @Override
    public void insertFirmaAutorizada(FirmasAutorizadas obj) {
        getHibernateTemplate().save(obj);
    }    
    
    
    @Override
    public UnidadesEjecutoras elementoUnidadEjecutora(String nombreUnidad) {
        
        DetachedCriteria dc = DetachedCriteria.forClass(UnidadesEjecutoras.class);
        dc.add(Restrictions.eq("nombreUnidad", nombreUnidad));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (UnidadesEjecutoras) l.get(0);
        }    
        return null;
    }

    @Override
    public FirmasAutorizadas elementoFirmaAutorizadas(String usuario, String cargo) {
        
        Criteria c = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(FirmasAutorizadas.class);
        
        c.add(Restrictions.eq("id.usuario", usuario));
        c.add(Restrictions.eq("id.cargo", cargo));
        
        if(c.list().size()>0){
            return (FirmasAutorizadas) c.list().get(0);
        }
        
        return null;
    }

    @Override
    public FirmasAutorizadasProyecto elementoFirmaAutorizadasProyecto(String usuario, String cargo) {
        
        Criteria c = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(FirmasAutorizadasProyecto.class);
        
        c.add(Restrictions.eq("id.usuario", usuario));
        c.add(Restrictions.eq("id.cargo", cargo));
        
        if(c.list().size()>0){
            return (FirmasAutorizadasProyecto) c.list().get(0);
        }
        
        return null;
    }

    @Override
    public void insertFirmaAutorizadaProyecto(FirmasAutorizadasProyecto obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public void actualizaUnidad(UnidadesEjecutoras obj) {
    getHibernateTemplate().update(obj);   
    }

    @Override
    public UnidadesEjecutoras elementoUnidadEjecutora2(short codigoUnidad) {
   
    DetachedCriteria dc = DetachedCriteria.forClass(UnidadesEjecutoras.class);
        dc.add(Restrictions.eq("codigo", codigoUnidad));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (UnidadesEjecutoras) l.get(0);
        }    
        return null;
    }

       
    
}
