/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.FirmasAutorizadas;
import com.sigefi.entity.FirmasAutorizadasProyecto;
import com.sigefi.entity.UnidadesEjecutoras;
import com.sigefi.entity.Usuarios;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "firmasAutorizadasDAO")
public class FirmasAutorizadasImplDAO extends HibernateDaoSupport implements FirmasAutorizadasDAO {

    @Autowired
    public FirmasAutorizadasImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public List<Usuarios> listAllUsuarios() {
        return getHibernateTemplate().loadAll(Usuarios.class);
    }

    @Override
    public List<FirmasAutorizadas> listAllFirmasAutorizadas() {
        return getHibernateTemplate().loadAll(FirmasAutorizadas.class);
    }

    @Override
    public List<FirmasAutorizadasProyecto> listAllFirmasAutorizadasProyecto() {
        return getHibernateTemplate().loadAll(FirmasAutorizadasProyecto.class);
    }

    @Override
    public FirmasAutorizadas elementoFirmaAutorizadas(String usuario, String cargo) {
        Criteria c = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(FirmasAutorizadas.class);

        c.add(Restrictions.eq("id.usuario", usuario));
       // c.add(Restrictions.eq("id.cargo", cargo));

        if (c.list().size() > 0) {
            return (FirmasAutorizadas) c.list().get(0);
        }

        return null;
    }

    @Override
    public FirmasAutorizadasProyecto elementoFirmaAutorizadasProyecto(String usuario, String cargo) {
        Criteria c = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(FirmasAutorizadasProyecto.class);

        c.add(Restrictions.eq("id.usuario", usuario));
        //c.add(Restrictions.eq("id.cargo", cargo));

        if (c.list().size() > 0) {
            return (FirmasAutorizadasProyecto) c.list().get(0);
        }

        return null;
    }

    @Override
    public void insertFirmaAutorizada(FirmasAutorizadas obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public void insertFirmaAutorizadaProyecto(FirmasAutorizadasProyecto obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public UnidadesEjecutoras elementoUnidadEjecutora(short idUnidadEjecutora) {
        DetachedCriteria dc = DetachedCriteria.forClass(UnidadesEjecutoras.class);

        dc.add(Restrictions.eq("codigo", idUnidadEjecutora));

        List l = getHibernateTemplate().findByCriteria(dc);

        if (l.size() > 0) {
            return (UnidadesEjecutoras) l.get(0);
        }

        return null;
    }

    @Override
    public void deleteFirmaAutorizadaProyecto(FirmasAutorizadasProyecto obj) {
        getHibernateTemplate().delete(obj);
    }

}
