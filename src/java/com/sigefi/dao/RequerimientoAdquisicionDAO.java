/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.RequerimientoAdquisicion;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface RequerimientoAdquisicionDAO {
    List<UnidadesEjecutoras> listUnidades();
    Presupuestos elementoPresupuesto(short idUnidadEjec,int anioPre);
    RequerimientoAdquisicion consultarCodigoRequerimiento(String codigoReq);
}
