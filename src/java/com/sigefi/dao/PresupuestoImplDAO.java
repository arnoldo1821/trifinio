/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.CatalogoCuentas;
import com.sigefi.entity.CuentasPresupuesto;
import com.sigefi.entity.Empresa;
import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "presupuestoDAO")
public class PresupuestoImplDAO extends HibernateDaoSupport implements PresupuestoDAO{
    
    @Autowired
    public PresupuestoImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
    
    @Override
    public Presupuestos elementoPresupuesto(short idUnidadEjec,Date fechaCreacion,int anioPre) {
//        List list = getHibernateTemplate().find("from Presupuestos where unidadesEjecutoras.codigo = ? and fechaCreacionPresupuesto = ? and anyoPresupuesto = ?", 
//                idUnidadEjec,fechaCreacion,anioPre);        
        DetachedCriteria dc = DetachedCriteria.forClass(Presupuestos.class);
        
        dc.add(Restrictions.eq("unidadesEjecutoras.codigo", idUnidadEjec));
        dc.add(Restrictions.eq("fechaCreacionPresupuesto", fechaCreacion));
        dc.add(Restrictions.eq("anyoPresupuesto", anioPre));
        
        List list = getHibernateTemplate().findByCriteria(dc);
        
        if(list.size()>0){
            return (Presupuestos) list.get(0);
        }
        return null;
    }

    @Override
    public void insertPresupuesto(Presupuestos obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public void insertCatalogoCuenta(CuentasPresupuesto obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public List<CuentasPresupuesto> listCuentasPreAgregadas(int codigoPresupuesto) {
        DetachedCriteria dc = DetachedCriteria.forClass(CuentasPresupuesto.class);
        dc.add(Restrictions.eq("presupuestos.codigoPresupuesto", codigoPresupuesto));
        dc.createAlias("catalogoCuentas", "cat");
        dc.addOrder(Order.asc("cat.codigoCuenta"));
        List list = getHibernateTemplate().findByCriteria(dc);
       /* List list = getHibernateTemplate().find("from CuentasPresupuesto where presupuestos.codigoPresupuesto = ? ", 
                codigoPresupuesto);*/
        if(list.size()>0){
            return  list;
        }
        return null;
    }

    @Override
    public CatalogoCuentas elementoCatalogoCuentas(int id_cuenta) {
        List list = getHibernateTemplate().find("from CatalogoCuentas where idCuenta = ?", id_cuenta);
        if(list.size()>0){
            return (CatalogoCuentas) list.get(0);
        }
        return null;
    }

    @Override
    public CuentasPresupuesto CuentasPreElemento(int codigoPresupuesto,int codigoCuenta) {
        Criteria c = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(CuentasPresupuesto.class);
        
        c.add(Restrictions.eq("id.codigoPresupuesto", codigoPresupuesto));
        c.add(Restrictions.eq("id.codigoCuenta", codigoCuenta));
        
        if(c.list().size()>0){
            return (CuentasPresupuesto) c.list().get(0);
        }
        
        return null;
    }

    @Override
    public void insertEmpresa(Empresa obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public List<CatalogoCuentas> listCatalogoCuentas(UnidadesEjecutoras unidadesEjecutoras, Integer anioCuenta) {
        DetachedCriteria dc = DetachedCriteria.forClass(CatalogoCuentas.class);
        dc.add(Restrictions.eq("unidadesEjecutoras", unidadesEjecutoras));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return l;
        }
        return null;
    }
   
    
    
}
