/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.DetalleCuentas;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "detallesCuentasDAO")
public class DetallesCuentasImplDAO extends HibernateDaoSupport implements DetallesCuentasDAO{
    
    @Autowired
    public DetallesCuentasImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public void insertDetalleCuenta(DetalleCuentas obj) {
        getHibernateTemplate().save(obj);
    }
    
    
}
