/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.CatalogoCuentas;
import com.sigefi.entity.CuentasPresupuesto;
import com.sigefi.entity.Empresa;
import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface PresupuestoDAO {
    Presupuestos elementoPresupuesto(short idUnidadEjec,Date fechaCreacion,int anioPre);
    CatalogoCuentas elementoCatalogoCuentas(int id_cuenta);
    List<CatalogoCuentas> listCatalogoCuentas(UnidadesEjecutoras unidadesEjecutoras,Integer anioCuenta);
    void insertPresupuesto(Presupuestos obj);
    void insertEmpresa(Empresa obj);
    void insertCatalogoCuenta(CuentasPresupuesto obj);
    List<CuentasPresupuesto> listCuentasPreAgregadas(int codigoPresupuesto);
    CuentasPresupuesto CuentasPreElemento(int codigoPresupuesto,int codigoCuenta);
    
}
