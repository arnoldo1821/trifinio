/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.DocumentosRequerimientos;
import com.sigefi.entity.NoConformidades;
import com.sigefi.entity.RequerimientoPagos;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "validarRequerimientosTesoreriaDAO")
public class ValidarRequerimientosTesoreriaImplDAO extends HibernateDaoSupport implements ValidarRequerimientosTesoreriaDAO{
    @Autowired
    public ValidarRequerimientosTesoreriaImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public List<RequerimientoPagos> listAllRequePago() {
        DetachedCriteria dc = DetachedCriteria.forClass(RequerimientoPagos.class);
        dc.createAlias("estadoRequisicions", "estado");
        dc.add(Restrictions.eq("estado.codigoEstado", (short)6));
        dc.add(Restrictions.eq("estado.control", true));
        dc.addOrder(Order.desc("fechaPresentacion"));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return l;
        }
        return null;
    }

    @Override
    public List<DetalleRequerimientoPago> listDetalleRequePagoSelec(int incre_req_pk) {
        DetachedCriteria dc = DetachedCriteria.forClass(DetalleRequerimientoPago.class);
        dc.add(Restrictions.eq("id.fkPkRequerimiento2", incre_req_pk));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return l;
        }
        return null;
    }

    @Override
    public List<DocumentosRequerimientos> listDocRequePagoSelec(int incre_req_pk) {
        DetachedCriteria dc = DetachedCriteria.forClass(DocumentosRequerimientos.class);
        dc.add(Restrictions.eq("id.fkRequerimiento1", incre_req_pk));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return l;
        }
        return null;
    }

    @Override
    public List<NoConformidades> listAllErrores() {
        DetachedCriteria dc = DetachedCriteria.forClass(NoConformidades.class);
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return l;
        }
        
        return null;
    }

    @Override
    public List<DetalleRequerimientoPago> listAllDetalleRequerimiento() {
        DetachedCriteria dc = DetachedCriteria.forClass(DetalleRequerimientoPago.class);
        dc.createAlias("requerimientoPagos", "req");
        dc.createAlias("req.estadoRequisicions", "estado");
        dc.add(Restrictions.eq("estado.codigoEstado",(short) 6));
        dc.add(Restrictions.eq("estado.control", true));
        dc.addOrder(Order.desc("req.fechaPresentacion"));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return l;
        }
        return null;
    }
    
}
