/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.CatalogoCuentasConta;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface CatalogoContabilidadDAO {
    CatalogoCuentasConta elementoCatalogoCuenta(String idCuenta);
    void insertCuenta(CatalogoCuentasConta obj);
    void updateCuenta(CatalogoCuentasConta obj);
    List<CatalogoCuentasConta> listNodoCatalogoCuentas(String numNivel);
    String ultimoRegistroNivel(String numNivelIdCuenta);
    
    List<CatalogoCuentasConta> listAllCatalogoCuenta();
}
