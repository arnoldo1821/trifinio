/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.DetalleCuentas;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface ReprogramarPreDAO {
    List<DetalleCuentas> listDetalleCuentasPre(short unidadE,int anio);
    void modificarCuenta(DetalleCuentas obj);
    void insertarCuenta(DetalleCuentas obj);
}
