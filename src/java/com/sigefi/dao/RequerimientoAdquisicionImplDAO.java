/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.Presupuestos;
import com.sigefi.entity.RequerimientoAdquisicion;
import com.sigefi.entity.UnidadesEjecutoras;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "requerimientoAdquisicionDAO")
public class RequerimientoAdquisicionImplDAO extends HibernateDaoSupport implements RequerimientoAdquisicionDAO{
    @Autowired
    public RequerimientoAdquisicionImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public List<UnidadesEjecutoras> listUnidades() {
        DetachedCriteria dc = DetachedCriteria.forClass(UnidadesEjecutoras.class);
        dc.addOrder(Order.asc("nombreUnidad"));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return l;
        }
        return null;
    }

    @Override
    public Presupuestos elementoPresupuesto(short idUnidadEjec, int anioPre) {
        DetachedCriteria dc = DetachedCriteria.forClass(Presupuestos.class);
        
        dc.add(Restrictions.eq("unidadesEjecutoras.codigo", idUnidadEjec));
        dc.add(Restrictions.eq("anyoPresupuesto", anioPre));
        
        List list = getHibernateTemplate().findByCriteria(dc);
        
        if(list.size()>0){
            return (Presupuestos) list.get(0);
        }
        return null;
    }

    @Override
    public RequerimientoAdquisicion consultarCodigoRequerimiento(String codigoReq) {
        DetachedCriteria dc = DetachedCriteria.forClass(RequerimientoAdquisicion.class);
        dc.add(Restrictions.eq("codigoRequerimeinto", codigoReq));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (RequerimientoAdquisicion) l.get(0);
        }
        return null;
    }
    
    
    
}
