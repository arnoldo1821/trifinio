/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.CuentasPresupuesto;
import com.sigefi.entity.DetalleCuentas;
import com.sigefi.entity.Presupuestos;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "liquidarDAO")
public class LiquidarImplDAO extends HibernateDaoSupport implements LiquidarDAO{
    @Autowired
    public LiquidarImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public Presupuestos elementoPre(short unidad, int anio) {
        DetachedCriteria dc = DetachedCriteria.forClass(Presupuestos.class);
        dc.createAlias("unidadesEjecutoras","uni");
        dc.add(Restrictions.eq("uni.codigo", unidad));
        dc.add(Restrictions.eq("anyoPresupuesto", anio));
        dc.add(Restrictions.eq("liquidado", false));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (Presupuestos) l.get(0);
        }
        return null;
    }

    @Override
    public void insertPresupuesto(Presupuestos obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public void insertCuentasPresupuesto(CuentasPresupuesto obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public void insertDetalleCuentas(DetalleCuentas obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public void updatePresupuesto(Presupuestos obj) {
        getHibernateTemplate().update(obj);
    }
     
}
