/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.CentroCosto;
import java.util.List;

/**
 *
 * @author Elmer
 */
public interface CentroCostoDAO {
    CentroCosto elementoCentroCosto(Integer idcentroCosto); 
    void insertCentroCosto(CentroCosto obj);
    void updateCentroCosto(CentroCosto obj);
    void deleteCentroCosto(CentroCosto obj);
    List<CentroCosto> listCentroCostoAll();
}
