/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.DetalleRequerimientoPago;
import com.sigefi.entity.DocumentosRequerimientos;
import com.sigefi.entity.EstadoRequisicion;
import com.sigefi.entity.NoConformidades;
import com.sigefi.entity.RequerimientoPagos;
import java.util.List;

/**
 *
 * @author AAldemaro
 */
public interface ValidarRequerimientosDAO {
    DetalleRequerimientoPago elementoRequerimiento (int fkpkreq, short correlativo);
    List<RequerimientoPagos> listAllRequePago();
    List<DetalleRequerimientoPago> listAllDetalleRequerimiento ();
    List<DetalleRequerimientoPago> listDetalleRequePagoSelec(int incre_req_pk);
    List<DocumentosRequerimientos> listDocRequePagoSelec(int incre_req_pk);
    List<NoConformidades> listAllErrores();
    
    
    void deleteEstadoRequerimiento(EstadoRequisicion obj);
    void deleteDocumentoRequerimiento(DocumentosRequerimientos obj);
    void deleteDetalleRequerimiento(DetalleRequerimientoPago obj);
    void updateDetalleRequerimiento(DetalleRequerimientoPago obj);
    void updateRequerimientoPago(RequerimientoPagos obj);
    void deleteRequerimiento(RequerimientoPagos obj);
}
