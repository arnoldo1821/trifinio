/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.beans.DetallePresupuestoBean;
import com.sigefi.entity.CuentasPresupuesto;
import com.sigefi.entity.DetalleCuentas;
import com.sigefi.entity.Presupuestos;
import java.util.List;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "detallePresupuestoDAO")
public class DetallePresupuestoImplDAO extends HibernateDaoSupport implements DetallePresupuestoDAO{
    @Autowired
    public DetallePresupuestoImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public List<CuentasPresupuesto> listCuentasSelec(DetallePresupuestoBean obj) {
        Criteria c = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(CuentasPresupuesto.class);
        
       
        c.createAlias("presupuestos", "pre");
        c.createAlias("pre.unidadesEjecutoras", "unidad");
        c.add(Restrictions.eq("unidad.codigo", obj.getUnidadesEjecutoras()));
       
        c.add(Restrictions.eq("conDetalle",false));
        c.add(Restrictions.eq("pre.anyoPresupuesto", obj.getAnyoPresupuesto()));
        c.add(Restrictions.eq("pre.liquidado", false));
        
        
        c.createAlias("catalogoCuentas", "cuentaC");        
        c.add(Restrictions.eq("cuentaC.esDetalle", true));
        

        c.addOrder(Order.asc("id.codigoCuenta"));
        
        return c.list();
    }

    @Override
    public DetalleCuentas consultarDetalleCuenta(int codigoPresupuesto,int codigoCuenta) {
        Criteria c = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(DetalleCuentas.class);       
        c.add(Restrictions.eq("id.codigoPresupuesto", codigoPresupuesto));
        c.add(Restrictions.eq("id.codgioCuenta", codigoCuenta));

        if(c.list().size()>0){
            return (DetalleCuentas) c.list().get(0);
        }

        return null;
    }

    @Override
    public void insertDetalleCuenta(DetalleCuentas obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public void updateDetalleCuenta(DetalleCuentas obj) {
        getHibernateTemplate().update(obj);
    }

    @Override
    public Presupuestos elementoPresupuesto(DetallePresupuestoBean obj) {
        DetachedCriteria dc = DetachedCriteria.forClass(Presupuestos.class);
        dc.add(Restrictions.eq("anyoPresupuesto", obj.getAnyoPresupuesto()));
        dc.add(Restrictions.eq("unidadesEjecutoras.codigo", obj.getUnidadesEjecutoras()));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (Presupuestos) l.get(0);
        }
        
        return null;
    }

    @Override
    public Set<DetalleCuentas> consultarDetallePresupuesto(DetallePresupuestoBean obj) {
        DetachedCriteria dc = DetachedCriteria.forClass(Presupuestos.class);
        
        dc.add(Restrictions.eq("unidadesEjecutoras.codigo", obj.getUnidadesEjecutoras()));
        dc.add(Restrictions.eq("anyoPresupuesto", obj.getAnyoPresupuesto()));
        
        List lst = getHibernateTemplate().findByCriteria(dc);
        
        if (lst.size() > 0) {
            return ((Presupuestos) lst.get(0)).getDetalleCuentases();
        }else {
            return null;
        }
    }

    @Override
    public List<CuentasPresupuesto> listAllCuentasPresupuesto(DetallePresupuestoBean obj) {
        Criteria c = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(CuentasPresupuesto.class);
        
       
        c.createAlias("presupuestos", "pre");
        c.createAlias("pre.unidadesEjecutoras", "unidad");
        c.add(Restrictions.eq("unidad.codigo", obj.getUnidadesEjecutoras()));
       
        c.add(Restrictions.eq("conDetalle",false));
        c.add(Restrictions.eq("pre.anyoPresupuesto", obj.getAnyoPresupuesto()));
        c.add(Restrictions.eq("pre.liquidado", false));
        
        
        c.createAlias("catalogoCuentas", "cuentaC");        
        c.add(Restrictions.eq("cuentaC.esDetalle", true));
        

        c.addOrder(Order.asc("id.codigoCuenta"));
        
        return c.list();
    }

    @Override
    public void insertarDetalleCuenta(DetalleCuentas obj) {
        getHibernateTemplate().saveOrUpdate(obj);
    }

    @Override
    public DetalleCuentas listAllDetalleCuentas(DetallePresupuestoBean obj, int presupuesto, int cuenta) {
        DetachedCriteria dc = DetachedCriteria.forClass(DetalleCuentas.class);
        dc.createAlias("presupuestos", "pre");
        dc.createAlias("pre.unidadesEjecutoras", "uni");
        dc.add(Restrictions.eq("uni.codigo", obj.getUnidadesEjecutoras()));
        dc.add(Restrictions.eq("pre.anyoPresupuesto", obj.getAnyoPresupuesto()));
        dc.add(Restrictions.eq("pre.codigoPresupuesto", presupuesto));
        dc.add(Restrictions.eq("id.codgioCuenta", cuenta));
        dc.addOrder(Order.asc("id.codgioCuenta"));
        List l =  getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
        DetalleCuentas detalle = (DetalleCuentas) l.get(0);
        if(detalle != null){
        return detalle;
        }
        }
        return null;
    }
    
    
}
