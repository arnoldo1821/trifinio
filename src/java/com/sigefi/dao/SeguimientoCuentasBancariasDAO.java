/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.SegumientoCuentaBancaria;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Elmer
 */
public interface SeguimientoCuentasBancariasDAO {
    List<SegumientoCuentaBancaria> listAllSeguimientoCuentas(String codigoCuenta, Date inicio, Date fin);
    SegumientoCuentaBancaria elemntoSeguimientoCuentaBancaria(String codigoCuenta);
    void insertMovimiento(SegumientoCuentaBancaria obj);
    void updateMovimiento(SegumientoCuentaBancaria obj);
    void deleteMovimiento(SegumientoCuentaBancaria obj);
}
