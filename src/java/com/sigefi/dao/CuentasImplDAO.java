/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.CatalogoCuentas;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "cuentasDAO")
public class CuentasImplDAO extends HibernateDaoSupport implements CuentasDAO{
    
    @Autowired
    public CuentasImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public void insertCuenta(CatalogoCuentas obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public List<CatalogoCuentas> listNodoCatalogoCuentas(int numNivel) {
        Criteria c = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(CatalogoCuentas.class);
        c.add(Restrictions.eq("subcuentaDe", numNivel));
        c.addOrder(Order.asc("idCuenta"));
        
        return c.list();
    }

    @Override
    public int ultimoRegistroCatalogoCuenta() {
        Criteria c = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(CatalogoCuentas.class);
        c.add(Restrictions.ne("subcuentaDe", 0));
        c.setProjection(Projections.max("idCuenta"));        
        return ((CatalogoCuentas) c.list()).getIdCuenta();
    }

    @Override
    public int ultimoRegistroNivel(int numNivelIdCuenta) {
        
        Criteria c = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(CatalogoCuentas.class);
        
       // c.add(Restrictions.eq("subcuentaDe", numNivelIdCuenta));
        c.addOrder(Order.desc("idCuenta"));
        
        int idCuenta;
                
        if(c.list().size()>0){
            idCuenta = ((CatalogoCuentas) c.list().get(0)).getIdCuenta() + 1 ;
        }else{
            idCuenta = (numNivelIdCuenta*10)+1;
        }       
        Criteria c2 = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(CatalogoCuentas.class);
        c2.add(Restrictions.eq("idCuenta", idCuenta));
        
        if(c2.list().size()<=0){
            return idCuenta;
        }
        return 0;
    }

    @Override
    public List<CatalogoCuentas> listAllCatalogoCuenta(short unidad) {
        DetachedCriteria dc = DetachedCriteria.forClass(CatalogoCuentas.class);
        dc.add(Restrictions.eq("unidadesEjecutoras.codigo", unidad));
        dc.addOrder(Order.asc("idCuenta"));
        dc.add(Restrictions.eq("activa", true));
        
        List l =  getHibernateTemplate().findByCriteria(dc);
        
        if(l.size()>0){
            return l;
        }
        
        return null;
//        return getHibernateTemplate().loadAll(CatalogoCuentas.class);
    }

    @Override
    public CatalogoCuentas elementoCatalogoCuenta(int idCuenta) {
        DetachedCriteria dc = DetachedCriteria.forClass(CatalogoCuentas.class);
        dc.add(Restrictions.eq("idCuenta", idCuenta));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (CatalogoCuentas) l.get(0);
        }
        return null;
    }

    @Override
    public void updateCuenta(CatalogoCuentas obj) {
        getHibernateTemplate().update(obj);
    }
    
    
    
}
