/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.Empresa;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "revisionPagosDAO")
public class RevisionPagosImplDAO extends HibernateDaoSupport implements RevisionPagosDAO{
    @Autowired
    public RevisionPagosImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public Empresa consultarEmpresa(short idUnidadEje, int anio) {
        DetachedCriteria dc = DetachedCriteria.forClass(Empresa.class);
        dc.add(Restrictions.eq("unidadesEjecutoras.codigo", idUnidadEje));
        dc.add(Restrictions.eq("anyoContable", anio));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (Empresa) l.get(0);
        }
        return null;
    }
    
}
