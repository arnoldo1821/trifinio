/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.CatalogoCuentas;
import com.sigefi.entity.CuentasPresupuesto;

/**
 *
 * @author AAldemaro
 */
public interface MantenimientoCuentaDAO {
    void updateCuenta(CatalogoCuentas obj);
    void deleteCuenta(CatalogoCuentas obj);
}
