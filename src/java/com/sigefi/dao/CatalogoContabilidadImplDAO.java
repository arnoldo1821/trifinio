/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.entity.CatalogoCuentasConta;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author AAldemaro
 */
@Transactional
@Service(value = "catalogoContabilidadDAO")
public class CatalogoContabilidadImplDAO extends HibernateDaoSupport implements CatalogoContabilidadDAO{
    @Autowired
    public CatalogoContabilidadImplDAO(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public CatalogoCuentasConta elementoCatalogoCuenta(String idCuenta) {
        DetachedCriteria dc = DetachedCriteria.forClass(CatalogoCuentasConta.class);
        dc.add(Restrictions.eq("codCatalocuenta", idCuenta));
        List l = getHibernateTemplate().findByCriteria(dc);
        if(l.size()>0){
            return (CatalogoCuentasConta) l.get(0);
        }
        return null;
    }

    @Override
    public void insertCuenta(CatalogoCuentasConta obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public void updateCuenta(CatalogoCuentasConta obj) {
        getHibernateTemplate().update(obj);
    }

    @Override
    public List<CatalogoCuentasConta> listNodoCatalogoCuentas(String numNivel) {
        DetachedCriteria c = DetachedCriteria.forClass(CatalogoCuentasConta.class);
        c.add(Restrictions.eq("cuentapadre", numNivel));
        c.addOrder(Order.asc("codCatalocuenta"));
        
        List l = getHibernateTemplate().findByCriteria(c);
        
        if(l.size()>0){
            return l;
        }
        
        return null;
    }

    @Override
    public String ultimoRegistroNivel(String numNivelIdCuenta) {
        DetachedCriteria c = DetachedCriteria.forClass(CatalogoCuentasConta.class);
        
        c.add(Restrictions.eq("cuentapadre", numNivelIdCuenta));
        c.addOrder(Order.desc("codCatalocuenta"));
        
        List l = getHibernateTemplate().findByCriteria(c);
        
        String idCuenta;
                
        if(l.size()>0){
            idCuenta = String.valueOf(Integer.valueOf(((CatalogoCuentasConta) l.get(0)).getCodCatalocuenta()) + 1 );
        }else{
            idCuenta = String.valueOf((Integer.valueOf(numNivelIdCuenta)*10)+1);
        }       
        DetachedCriteria c2 = DetachedCriteria.forClass(CatalogoCuentasConta.class);
        c2.add(Restrictions.eq("codCatalocuenta", idCuenta));
        
        List l2 = getHibernateTemplate().findByCriteria(c2);
        
        if(l2.size()<=0){
            return idCuenta;
        }
        return null;
    }

    @Override
    public List<CatalogoCuentasConta> listAllCatalogoCuenta() {
        DetachedCriteria dc = DetachedCriteria.forClass(CatalogoCuentasConta.class);
        dc.addOrder(Order.asc("codCatalocuenta"));
        
        List l =  getHibernateTemplate().findByCriteria(dc);
        
        if(l.size()>0){
            return l;
        }
        
        return null;
    }
    
}
