/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.dao;

import com.sigefi.beans.ModeloContratoBean;
import com.sigefi.entity.ClausulasContrato;
import com.sigefi.entity.ClausulasModeloContrato;
import com.sigefi.entity.ModeloContrato;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author SKAR
 */
@Transactional
@Service(value = "clausulasModeloContratoDAO")
public class ClausulasModeloContratoImplDAO extends HibernateDaoSupport implements ClausulasModeloContratoDAO {
    
    @Autowired
    public ClausulasModeloContratoImplDAO(SessionFactory sessionFactory){
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public void insert(ClausulasModeloContrato obj) {
        getHibernateTemplate().save(obj);
    }

    @Override
    public void update(ClausulasModeloContrato obj) {
        getHibernateTemplate().update(obj);
    }

    @Override
    public void delete(ClausulasModeloContrato obj) {
        getHibernateTemplate().delete(obj);
    }

    @Override
    public List<ClausulasModeloContrato> selectAll() {
        return getHibernateTemplate().loadAll(ClausulasModeloContrato.class);
    }

    @Override
    public List selectAll(ModeloContrato obj) {
        DetachedCriteria dc = DetachedCriteria.forClass(ClausulasModeloContrato.class);
        dc.add(Restrictions.eq("id.codigoContrato", obj.getCodigoContrato()));
        return getHibernateTemplate().findByCriteria(dc);
    }

    @Override
    public ClausulasModeloContrato select(ModeloContratoBean obj) {
        DetachedCriteria dc = DetachedCriteria.forClass(ClausulasModeloContrato.class);
        dc.add(Restrictions.eq("id.codigoContrato", obj.getClausulasModeloContrato().getId().getCodigoContrato()));
        dc.add(Restrictions.eq("id.clausula", obj.getClausulasModeloContrato().getId().getClausula()));
        
        List<Object> lst = getHibernateTemplate().findByCriteria(dc);
        
        if(lst.size() > 0){
            return (ClausulasModeloContrato) lst.get(0);
        }
        return null;
    }
    
}
