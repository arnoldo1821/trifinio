/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.clases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author AAldemaro
 */
public class SubirArchivoServer {
    
    
    /*
        carpetaDireccionDocs : RUTA Y NOMBRE LA CARPETA 
        renombreArchivo      : NOMBRE DEL ARCHIVO RENOMBRADO
    */
    public String SubirArchivo(FileUploadEvent event,String carpetaDireccionDocs,String renombreArchivo) {

        InputStream inputStream = null;
        OutputStream outputStream = null;
        
        try {
            
        File directorio = new File(carpetaDireccionDocs);
        
        if(!directorio.exists()){
            directorio.mkdirs();
        }
        
        
        String nombreArch = event.getFile().getFileName();
        String extencion = nombreArch.substring(nombreArch.lastIndexOf("."));

        outputStream = new FileOutputStream(new File(carpetaDireccionDocs + renombreArchivo + extencion.toLowerCase()));
        inputStream = event.getFile().getInputstream();

        int read = 0;
        byte[] bytes = new byte[(int) event.getFile().getSize()];

        while ((read = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
        }

//        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Archivo Subido correctamente ", ""));
        
        String rutaNombreCompleta = carpetaDireccionDocs + renombreArchivo + extencion.toLowerCase();
        
        return rutaNombreCompleta;
        
        } catch (Exception e) {
            
        }finally{
            try {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (Exception e) {
            }
            
        }
        
        return "";
    }
    
    // urlDoc URL Y NOMBRE DEL DOCUMENTO
    public StreamedContent descargarArchivo(String urlDoc) {
        try {
            StreamedContent file;
            File uFile = new File(urlDoc);
            InputStream stream = new FileInputStream(uFile);
            String tipo = FacesContext.getCurrentInstance().getExternalContext().getMimeType(urlDoc);
            return ( new DefaultStreamedContent(stream, tipo, uFile.getName()));
        } catch (Exception ex) {
            
        }
        return null;
    }
    
    public void borrarArchivo(String urlNombreDoc){
        try {
            ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            String realPath = (String) servletContext.getRealPath("/"); // Sustituye "/" por el directorio ej: "/upload"
            String urld = realPath + "\\recursos\\imagenes\\otras\\usuario.jpg";
            if(!urlNombreDoc.equalsIgnoreCase(urld)){
//            if (!urlNombreDoc.equalsIgnoreCase("/ImagenesSIGEFI/fotoUsuarios/")) {
                File file = new File(urlNombreDoc);
                if (file.exists()) {
                    file.delete();
                }
//            }
            }
        } catch (Exception e) {
        }
    }
    
    public void renombrarArchivo(String urlNombreDoc,String urlNombreDoc2){
        try {
//            if (!urlNombreDoc.equalsIgnoreCase("/ImagenesSIGEFI/fotoUsuarios/")) {
                File file = new File(urlNombreDoc);
                
                if (file.exists()) {
                    File renombrarFile = new File(urlNombreDoc2);
                    file.renameTo(renombrarFile);
                }
//            }
        } catch (Exception e) {
        }
    }
    
}
