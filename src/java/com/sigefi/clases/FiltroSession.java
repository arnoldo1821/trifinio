/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.clases;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author aldemaro
 */
@WebFilter("/faces/view/*")
public class FiltroSession implements Filter{
    
    FilterConfig filterConfig;
    
    @Override
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        
        try {
            
            HttpServletRequest req = (HttpServletRequest) request;
            HttpServletResponse resp = (HttpServletResponse) response;
//            HttpSession session = req.getSession(true);
            HttpSession session = req.getSession(true);

            String requestUrl = req.getRequestURL().toString();
            Object userInSession = session.getAttribute("usuarioLogeado");

            if (userInSession == null && !requestUrl.contains("index.xhtml")) {
                resp.sendRedirect((req.getContextPath() + "/faces/index.xhtml"));
            } 
            else {
//                if (userInSession != null) {
//                    if (userInSession.equals("ADMIN")) {
//                        if (!requestUrl.contains("/views_administrador/")) {
//                            resp.sendRedirect(req.getContextPath() + "/faces/otrasViews/noAdmitida.xhtml");
//                            resp.resetBuffer();
//                        }
//                    } else {
//                        if (!requestUrl.contains("/views_usuario/")) {
//                            resp.sendRedirect(req.getContextPath() + "/faces/otrasViews/noAdmitida.xhtml");
//                            resp.resetBuffer();
//                        }
//                    }
//                }
                
                chain.doFilter(request, response);
            }
        } catch (Exception e) {
//            System.out.println("**************** "+e);
        }
    }

    @Override
    public void destroy() {
        this.filterConfig = null;
    }
    
    
}
