/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sigefi.clases;

import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrador
 */
public class FechaSistema {
    
    private Date fecha;
    private String fechaString;
    private String horaMinuSegundos;
    private Integer anio;
    private Date fechaCompleta;
    
    public FechaSistema() {
        Calendar cal = new GregorianCalendar();
        String horaMinSecSis = "";
        String aux = "";
        setAnio(cal.get(cal.YEAR));
        if ((cal.get(cal.MONTH) + 1) > 9) {
            if(cal.get(cal.DATE) > 9){
                aux = cal.get(cal.YEAR) + "-" + (cal.get(cal.MONTH) + 1) + "-" +cal.get(cal.DATE)  ;
            }else{
                aux = cal.get(cal.YEAR) + "-" + (cal.get(cal.MONTH) + 1) + "-" + "0"+cal.get(cal.DATE)  ;
            }
            
        } else {
            if(cal.get(cal.DATE) > 9){
                aux = cal.get(cal.YEAR) + "-" + "0"+(cal.get(cal.MONTH) + 1) + "-" +cal.get(cal.DATE)  ;
            }else{
                aux = cal.get(cal.YEAR) + "-" + "0"+(cal.get(cal.MONTH) + 1) + "-" + "0"+cal.get(cal.DATE)  ;
            }
            
        }
        
        if(cal.get(cal.HOUR) > 9){
            horaMinSecSis = " " + cal.get(cal.HOUR);
        }else{
            horaMinSecSis = " " + "0" + cal.get(cal.HOUR);
        }
        
        if(cal.get(cal.MINUTE) > 9){
            horaMinSecSis = horaMinSecSis.concat((":" + cal.get(cal.MINUTE)));
        }else{
            horaMinSecSis = horaMinSecSis.concat((":" + "0"+cal.get(cal.MINUTE)));
        }
        
        if(cal.get(cal.SECOND) > 9){
            horaMinSecSis = horaMinSecSis.concat(( ":" + cal.get(cal.SECOND)));
        }else{
            horaMinSecSis = horaMinSecSis.concat(( ":" + "0"+cal.get(cal.SECOND)));
        }
        setFechaString(aux);
        setHoraMinuSegundos(horaMinSecSis);
//        setFecha(Date.valueOf(aux));
        Calendar cal2 = new GregorianCalendar();
        String f = cal2.get(cal2.YEAR) + "-" + (cal2.get(cal2.MONTH) + 1) + "-" +cal2.get(cal2.DATE);
        String fComple = cal2.get(cal2.YEAR) + "-" + (cal2.get(cal2.MONTH) + 1) + "-" +cal2.get(cal2.DATE)+" "+cal.get(cal.HOUR)+":"+ cal.get(cal.MINUTE)+":"+ cal.get(cal.SECOND);
        
//        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
//        SimpleDateFormat formatoS = new SimpleDateFormat("dd-MM-yyyy HH:mm:SS");

        try {
            Date d = new Date();
            setFecha(d);
            setFechaCompleta(d);

        } catch (Exception ex) {
            System.out.println("**** "+ex);
        }

        
    }


    public String getFechaString() {
        return fechaString;
    }

    public void setFechaString(String fechaString) {
        this.fechaString = fechaString;
    }

    public String getHoraMinuSegundos() {
        return horaMinuSegundos;
    }

    public void setHoraMinuSegundos(String horaMinuSegundos) {
        this.horaMinuSegundos = horaMinuSegundos;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public Date getFechaCompleta() {
        return fechaCompleta;
    }

    public void setFechaCompleta(Date fechaCompleta) {
        this.fechaCompleta = fechaCompleta;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    
}
